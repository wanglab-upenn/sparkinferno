# -*- coding: utf-8 -*-
'''
This code takes in multiple intervals from a bed file through spark then
do the tabix query in parallel
'''

from __future__ import print_function
import subprocess as sp
import sys
from os import path, _exit
from tempfile import gettempdir
from uuid import uuid4 ## Random UUID generator
from shutil import copyfile


def tabix(x, indexpath, window_size=0, tabix_app='tabix'):
    LIST = x.split("\t")
    CHR = LIST[0]
    START_tmp = int(LIST[1])-window_size
    START = START_tmp if START_tmp>=0 else 0 ## Prevent input start becoming negative
    END = int(LIST[2])+window_size
    input_additional_list = [] if len(LIST) < 4 else LIST[3:] ## Collect additional fields in a bed file if needed
    #index query
    result = _tabix_query(indexpath, "%s:%d-%d" % (CHR, START, END), tabix_app=tabix_app)
    # hitlist = list()
    if len(result) > 0:
        for hit in result:
            try:
                hit_list = hit.split("\t")
            except TypeError:
                hit_list = hit.decode('utf-8').split("\t")
            if window_size > 0:
                tabix_queries = "\tTabix_query_start=%d\tTabix_query_end=%d"%(START, END)
                # hitlist.append([CHR, int(LIST[1]), int(LIST[2]), "\t".join(input_additional_list) + tabix_queries, "\t".join(hit_list), indexpath])
                yield([CHR, int(LIST[1]), int(LIST[2]), "\t".join(input_additional_list) + tabix_queries, "\t".join(hit_list), indexpath])
            else:
                # hitlist.append([CHR, START, END, "\t".join(input_additional_list), "\t".join(hit_list), indexpath])
                yield([CHR, START, END, "\t".join(input_additional_list), "\t".join(hit_list), indexpath])
    
    # return hitlist


def tabix_partitions(rdd_list, indexpath, window_size=0, tabix_app='tabix'):
    hitlist = list()
    for x in rdd_list:
        LIST = x.split("\t")
        CHR = LIST[0]
        START_tmp = int(LIST[1])-window_size
        START = START_tmp if START_tmp>=0 else 0 ## Prevent input start becoming negative
        END = int(LIST[2])+window_size
        input_additional_list = [] if len(LIST) < 4 else LIST[3:] ## Collect additional fields in a bed file if needed
        #index query
        result = _tabix_query(indexpath, "%s:%d-%d" % (CHR, START, END), tabix_app=tabix_app)
        if len(result) > 0:
            for hit in result:
                try:
                    hit_list = hit.split("\t")
                except TypeError:
                    hit_list = hit.decode('utf-8').split("\t")
                if window_size > 0:
                    tabix_queries = "\tTabix_query_start=%d\tTabix_query_end=%d"%(START, END)
                    hitlist.append([CHR, int(LIST[1]), int(LIST[2]), "\t".join(input_additional_list) + tabix_queries, "\t".join(hit_list), indexpath])
                else:
                    hitlist.append([CHR, START, END, "\t".join(input_additional_list), "\t".join(hit_list), indexpath])
    
    return hitlist


def _tabix_query(index_path, coordinate, tabix_app='tabix'):
    p = sp.Popen("%s -p bed %s %s" % (tabix_app, index_path, coordinate), stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
    stdout, stderr = p.communicate()
    if p.returncode != 0:
        sys.stderr.write('Fail.\nError %d: %s\n' % (p.returncode, stderr))
        _exit(1)
    else:            
        return stdout.splitlines()


def make_tabix_index(input_bed, index_dir='', temp_dir=gettempdir(), tabix_app='tabix', bgzip_app='bgzip'):
    if not path.exists(input_bed):
        sys.stderr.write(input_bed+": No such file or directory\n")
        sys.exit(1)
    else:
        if index_dir == '':
            ## Make a random UUID folder for the Giggle index if the index_dir argument is not given
            index_dir = path.join(temp_dir, str(uuid4()))
        else:
            if not path.exists(index_dir):
                try:
                    makedirs(index_dir)
                except IOError as err:
                    sys.stderr.write(err)
                    sys.exit(1)
            index_dir = path.abspath(path.expanduser(index_dir))
        
        input_bed_name, input_bed_ext = path.splitext(path.split(input_bed)[1])
        cmds = []
        if input_bed_ext == '.bed':
            ## Conduct sort and bgzip
            cmds.append("LC_ALL=C sort -T %s -k1,1 -k2,2n -k3,3n %s | %s > %s" % (temp_dir, input_bed, bgzip_app, path.join(index_dir, input_bed_name+'.bed.gz')))
            cmds.append("%s -0 -p bed %s" % (tabix_app, path.join(index_dir, input_bed_name+'.bed.gz')))
        if input_bed_ext == '.gz': ## Assume the input is a bgzip file
            copyfile(input_bed, path.join(index_dir, input_bed_name+'.gz'))
            cmds.append("%s -0 -p bed %s" % (tabix_app, path.join(index_dir, input_bed_name+'.bed.gz')))
        if not cmds: ## No command in cmds[]
            sys.stderr.write("Input should be a bed (.bed) or bgzip (.gz) file.\n")
            sys.exit(1)
            
        ## Start to run shell commands
        for cmd in cmds:
            print("Running the command: %s" % cmd)
            p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
            stdout, stderr = p.communicate()
            if p.returncode != 0:
                sys.stderr.write("Fail.\nError %d: %s\n" % (p.returncode, stderr))
                sys.exit(1)
                
        return index_dir
