# -*- coding: utf-8 -*-
'''
This code takes in multiple intervals from a bed file through spark then
do the giggle query in parallel
'''

from __future__ import print_function
import subprocess as sp
import sys
from os import path, makedirs, remove, _exit
from tempfile import gettempdir
from uuid import uuid4 ## Random UUID generator
from shutil import copyfile


def Gig(x, indexpath, window_size=0, giggle_app='giggle'):
    LIST = x.split("\t")
    CHR = LIST[0]
    START_tmp = int(LIST[1])-window_size
    START = START_tmp if START_tmp>=0 else 0 ## Prevent input start becoming negative
    END = int(LIST[2])+window_size
    input_additional_list = [] if len(LIST) < 4 else LIST[3:] ## Collect additional fields in a bed file if needed
    #index query
    result = _giggle_query(indexpath, "%s:%d-%d" % (CHR, START, END), giggle_app=giggle_app)
    hitlist = list()
    if len(result) > 0:
        for hit in result:
            try:
                hit_list = hit.split("\t")
            except TypeError:
                hit_list = hit.decode('utf-8').split("\t")
            filename = hit_list[-1].strip()
            hit_list = hit_list[:-1] ## the file path from giggle return is no needed
            track_chr = hit_list[0]
            track_start = int(hit_list[1])
            track_end = int(hit_list[2])
            if _confirm_overlap_boundary(CHR, START, END, track_chr, track_start, track_end):
                if window_size > 0:
                    giggle_queries = "\tGiggle_query_start=%d\tGiggle_query_end=%d"%(START, END)
                    hitlist.append([CHR, int(LIST[1]), int(LIST[2]), "\t".join(input_additional_list) + giggle_queries, "\t".join(hit_list), filename])
                else:
                    hitlist.append([CHR, START, END, "\t".join(input_additional_list), "\t".join(hit_list), filename])
    
    return hitlist


def Gig_partitions(rdd_list, indexpath, window_size=0, output_in='memory', temp_dir=gettempdir(), giggle_app='giggle', bgzip_app='bgzip'):
    tmp_bed_path = path.join(temp_dir, str(uuid4())+'.bed')
    fp = open(tmp_bed_path, 'w')
    
    for x in rdd_list:
        LIST = x.split("\t")
        CHR = LIST[0]
        START_tmp = int(LIST[1])-window_size
        START = START_tmp if START_tmp>=0 else 0 ## Prevent input start becoming negative
        END = int(LIST[2])+window_size
        if window_size > 0:
            if len(LIST) < 4:
                ## Add original input coordinates behind queried intervals (+/- window_size)
                fp.write("%s\t%d\t%d\t%s\t%s\n" % (CHR, START, END, LIST[1], LIST[2]))
            else:
                fp.write("%s\t%d\t%d\t%s\t%s\t%s\n" % (CHR, START, END, LIST[1], LIST[2], "\t".join(LIST[3:])))
        else:
            if len(LIST) < 4:
                fp.write("%s\t%d\t%d\n" % (CHR, START, END))
            else:
                fp.write("%s\t%d\t%d\t%s\n" % (CHR, START, END, "\t".join(LIST[3:])))
    fp.close()
    
    result = _giggle_file_query(indexpath, tmp_bed_path, output_in, giggle_app=giggle_app, bgzip_app=bgzip_app)
    hitlist = list()
    for hit in result:
        try:
            hit_list = hit.split("\t")
        except TypeError:
            hit_list = hit.decode('utf-8').split("\t")
        if hit_list[0][:2] == '##':
            chr = hit_list[0][2:]
            start = int(hit_list[1])
            end = int(hit_list[2])
            if window_size > 0:
                input_start = int(hit_list[3])
                input_end = int(hit_list[4])
                input_additional_list = hit_list[5:] if len(hit_list)>5 else []
            elif len(hit_list)>3:
                input_additional_list = hit_list[3:]
            else: input_additional_list = []
        else:
            filename = hit_list[-1].strip()
            hit_list = hit_list[:-1] ## the file path from giggle return is no needed
            track_chr = hit_list[0]
            track_start = int(hit_list[1])
            track_end = int(hit_list[2])
            if _confirm_overlap_boundary(chr, start, end, track_chr, track_start, track_end):
                if window_size > 0:
                    giggle_queries = "\tGiggle_query_start=%d\tGiggle_query_end=%d"%(start, end)
                    hitlist.append([chr, input_start, input_end, "\t".join(input_additional_list) + giggle_queries, "\t".join(hit_list), filename])
                else:
                    hitlist.append([chr, start, end, "\t".join(input_additional_list), "\t".join(hit_list), filename])
    
    remove(tmp_bed_path+'.gz')
    return hitlist


def Gig_index_count_partitions(rdd_list, indexpath, window_size=0, temp_dir=gettempdir(), giggle_app='giggle', bgzip_app='bgzip'):
    hitlist = list()
    for x_list in rdd_list:
        tmp_bed_path = path.join(temp_dir, str(uuid4())+'.bed')
        freq = x_list['frequency']
        # print(x_list)
        with open(tmp_bed_path, 'w') as fp:
            for x in x_list['coordinate_list']:
                # print(x)
                LIST = x.split("\t")
                CHR = LIST[0]
                START_tmp = int(LIST[1])-window_size
                START = START_tmp if START_tmp>=0 else 0 ## Prevent input start becoming negative
                END = int(LIST[2])+window_size
                fp.write("%s\t%d\t%d\n" % (CHR, START, END))
    
        result = _giggle_index_count_file_query(indexpath, tmp_bed_path, giggle_app=giggle_app, bgzip_app=bgzip_app)
    
        if len(result) > 0:
            for hit in result:
                try:
                    hit_list = hit.split("\t")
                except TypeError:
                    hit_list = hit.decode('utf-8').split("\t")
                ## E068_15_coreMarks_mnemonics_enhancer.bed.gz\tsize:110919\toverlaps:100
                filename = hit_list[0][1:] ## The output file name is in the first coloumn, slice [1:] indicates removal of the prefix "#" sign
                data_source_len = hit_list[1].replace('size:', '')
                overlap_count = hit_list[2].replace('overlaps:', '')
                
                hitlist.append(['', 0, 0, freq, data_source_len + "\t" + overlap_count, filename])
            
        remove(tmp_bed_path+'.gz')
        
    return hitlist
    

def _giggle_file_query(index_path, bedpath, output_in='memory', giggle_app='giggle', bgzip_app='bgzip'):
    cmds = ["%s %s" % (bgzip_app, bedpath)]
    if output_in == 'memory':
        cmds.append("%s search -v -o -i %s -q %s.gz" % (giggle_app, index_path, bedpath))
    elif output_in == 'disk':
        output_file = bedpath.replace('.bed', '.output')
        cmds.append("%s search -v -o -i %s -q %s.gz > %s" % (giggle_app, index_path, bedpath, output_file))
    else:
        sys.stderr.write('%s: Unknown argument of output_in. Support "memory" or "disk" argument only.\n' % output_in)
        _exit(1)
        
    for cmd in cmds:
        p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
        stdout, stderr = p.communicate()
    if p.returncode != 0:
        sys.stderr.write("Fail.\nError %d: %s\n" % (p.returncode, stderr))
        _exit(1)
    
    if output_in == 'memory':
        for line in stdout.splitlines():
            yield line
        
    elif output_in == 'disk':
        with open(output_file, 'r') as output_fp:
            for line in output_fp:
                yield line
                
        remove(output_file)
        
        
def _giggle_index_count_file_query(index_path, bedpath, giggle_app='giggle', bgzip_app='bgzip'):
    cmds = ["%s %s" % (bgzip_app, bedpath),
            "%s search -c -i %s -q %s.gz" % (giggle_app, index_path, bedpath)]
    for cmd in cmds:
        p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
        stdout, stderr = p.communicate()
    if p.returncode != 0:
        sys.stderr.write("Fail.\nError %d: %s\n" % (p.returncode, stderr))
        _exit(1)
    else:            
        return stdout.splitlines()


def _giggle_query(index_path, coordinate, giggle_app='giggle'):
    p = sp.Popen("%s search -v -i %s -r %s" % (giggle_app, index_path, coordinate), stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
    stdout, stderr = p.communicate()
    if p.returncode != 0:
        sys.stderr.write("Fail.\nError %d: %s\n" % (p.returncode, stderr))
        _exit(1)
    else:            
        return stdout.splitlines()


def _confirm_overlap_boundary(interval_chr, interval_start, interval_end, track_chr, track_start, track_end):
    if interval_chr != track_chr:
        return False

    ## Using 1-base for confirmation
    interval_start = interval_start+1
    track_start = track_start+1
    
    ## Case 1 of no overlap (track_start > interval_end):
    ## interval_start o----o interval_end
    ##          track_start o----o track_end
    ## Case 2 of no overlap (track_end < interval_start):
    ##    interval_start o----o interval_end
    ## track_start o----o track_end
    if not (track_start > interval_end or track_end < interval_start):
        return True
    else:
        return False


def make_giggle_index(input_bed, index_dir='', temp_dir=gettempdir(), giggle_app='giggle', bgzip_app='bgzip'):
    if not path.exists(input_bed):
        sys.stderr.write(input_bed+": No such file or directory\n")
        sys.exit(1)
    else:
        if index_dir == '':
            ## Make a random UUID folder for the Giggle index if the index_dir argument is not given
            index_dir = path.join(temp_dir, str(uuid4()))
        else:
            if not path.exists(index_dir):
                try:
                    makedirs(index_dir)
                except IOError as err:
                    sys.stderr.write(err)
                    sys.exit(1)
            index_dir = path.abspath(path.expanduser(index_dir))
        
        input_bed_name, input_bed_ext = path.splitext(path.split(input_bed)[1])
        cmds = []
        if input_bed_ext == '.bed':
            ## Conduct sort and bgzip
            cmds.append("LC_ALL=C sort -T %s -k1,1 -k2,2n -k3,3n %s | %s > %s" % (temp_dir, input_bed, bgzip_app, path.join(index_dir, input_bed_name+'.bed.gz')))
            cmds.append("%s index -i %s -o %s -s -f" % (giggle_app, path.join(index_dir, input_bed_name+'.bed.gz'), path.join(index_dir, 'giggle_index')))
        if input_bed_ext == '.gz': ## Assume the input is a bgzip file
            copyfile(input_bed, path.join(index_dir, input_bed_name+'.gz'))
            cmds.append("%s index -i %s -o %s -s -f" % (giggle_app, path.join(index_dir, input_bed_name+'.gz'), path.join(index_dir, 'giggle_index')))
        if not cmds: ## No command in cmds[]
            sys.stderr.write("Input should be a bed (.bed) or bgzip (.gz) file.\n")
            sys.exit(1)
            
        ## Start to run shell commands
        for cmd in cmds:
            print("Running the command: %s" % cmd)
            p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
            stdout, stderr = p.communicate()
            if p.returncode != 0:
                sys.stderr.write("Fail.\nError %d: %s\n" % (p.returncode, stderr))
                sys.exit(1)
                
        return path.join(index_dir, 'giggle_index')
