# -*- coding: utf-8 -*-
import pyspark
from pyspark.sql import Row
from pyspark.sql.types import *
from pyspark.sql import functions as F
import sparkinferno.inferno as inferno
from sparkinferno.tabix import make_tabix_index
import sparkinferno.config as conf
import sys
from os import path, makedirs, remove, errno, getcwd, _exit
import __main__
import subprocess as sp
import pandas as pd
from glob import glob
import logging
from tempfile import gettempdir
from uuid import uuid4
import re


this = sys.modules[__main__.__name__]
tmp_log_path = ''
worker_logger = None

def load_file_to_df(input_file, delimiter="\t", chr_col_name=None, pos_col_name=None, af_col_name=None, rsid_col_name=None, allele1_col_name=None, allele2_col_name=None, pval_col_name=None, beta_col_name=None, se_col_name=None, effect_dir_col_name=None, exclude_unspecified_cols=False):
    summary_stats_df = this.spark.read.format('csv').option('header', 'true').option('delimiter', delimiter).load(input_file).repartition(100)
    
    summary_stats_df = summary_stats_df\
                       .withColumn(chr_col_name, F.when(F.length(chr_col_name) > 3, F.col(chr_col_name)).otherwise(F.concat(F.lit('chr'), F.col(chr_col_name))))\
                       .withColumn(allele1_col_name, F.upper(F.col(allele1_col_name)))\
                       .withColumn(allele2_col_name, F.upper(F.col(allele2_col_name)))\
                       .withColumn(pos_col_name, F.col(pos_col_name).cast('long'))\
                       .withColumn('pos_start', F.col(pos_col_name)-1)\
                       .withColumn('snp_id', F.concat(F.col(chr_col_name), F.lit(':'), 'pos_start', F.lit('-'), F.col(pos_col_name)))\
                       .withColumn('snp_id', F.when((F.col(allele1_col_name).isNotNull()) & (F.col(allele2_col_name).isNotNull()), F.concat(F.col('snp_id'), F.lit(':'), F.col(allele1_col_name), F.lit('>'), F.col(allele2_col_name))).otherwise(F.col('snp_id')))
    
    ## Create the region_pos columns for recording SNPs of the initial input. Also, according to absent or present from the region_pos column, the input file that can be distinguished is an original GWAS summary stats or the output table from the previous step of INFERNO preprocessing (i.e., pval_expansion.py or ld_pruning.py), respectively.
    if 'region_pos' not in summary_stats_df.columns:
        ## The input from an original GWAS summary stats
        summary_stats_df = summary_stats_df.withColumn('region_pos', F.col('snp_id'))
    
    ## Order the output columns
    main_header = [chr_col_name, 'pos_start', pos_col_name, 'snp_id', allele1_col_name, allele2_col_name, 'region_pos', pval_col_name, af_col_name, beta_col_name, se_col_name, rsid_col_name, effect_dir_col_name]
    ## Remove None value from main_header
    main_header = [i for i in main_header if i]
    other_header = list(set(summary_stats_df.columns) - set(main_header))
    other_header.sort()
    if exclude_unspecified_cols:
        summary_stats_df = summary_stats_df.select(main_header)
    else:
        summary_stats_df = summary_stats_df.select(main_header + other_header)
        
    return summary_stats_df.persist(pyspark.StorageLevel.DISK_ONLY)


def gwas_qc(gwas_df, gadb_meta_file, chr_col_name=None, pos_col_name=None, af_col_name=None, rsid_col_name=None, allele1_col_name=None, allele2_col_name=None, skip_check_negative_strand=False, skip_check_ambiguous=False, skip_check_MAF=False, skip_normalizing_allele=False, diff_MAF_threshold=0.15, exclude_bed=None, genome_build='hg19', data_source='1kg', super_population='GLOBAL', temp_dir=gettempdir(), giggle_app='giggle', bgzip_app='bgzip', samtools_app='samtools', conf_path=''):
    
    get_rev_complementary_udf = F.udf(lambda c: _get_rev_complementary(c), StringType())
    is_indel_udf = F.udf(lambda x, y: _is_indel(x, y), BooleanType())
    
    ## Search the index number of the snp_id position and pick it up as a foreign key for table join
    additionalQueriedString_schema = [None for i in range(0, gwas_df.columns.index('snp_id')-3)]+['snp_id']
    
    ## Schema of 1kg BED
    ## 0:'chr', 1:'chr_start', 2:'chr_end', 3:'rsID', 4:'qual', 5:'strand', 6:'ref', 7:'alt', 8:'GLOBAL_AF', 9:'GLOBAL_AC', 10:'GLOBAL_AN', 11:'SUPER_POP_AF', 12:'SUPER_POP_AC', 13:'SUPER_POP_AN', 14:'SUB_POP_AF', 15:'SUB_POP_AC', 16:'SUB_POP_AN'
    hitString_schema = ['_chr', '_start', '_end', '_rsID', None, None, '_ref', '_alt', '_global_af', None, None, '_super_pop_af']
    hitString_schema = [data_source+x if x != None else None for x in hitString_schema]
    
    ## Load GADB dataframe
    gadb_df = inferno.loadgadb(gadb_meta_file).cache()
    if data_source == '1kg':
        ## Get the 1000 Genomes bed tracks from GADB
        data_source_filter = "Data Source:1k_genome_phase3,Output type:SNP_biallelic,Genome build:{},File format:bed bed17".format(genome_build)
    elif data_source == 'dbSNP':
        ## Not support dbSNP yet, changing to 1kg
        data_source = '1kg'
        data_source_filter = "Data Source:1k_genome_phase3,Output type:SNP_biallelic,Genome build:{},File format:bed bed17".format(genome_build)
        _print_log_message('The dbSNP data source is not available yet. Using 1000 Genomes instead.', 'warn')
    else:
        ## Not supported data source, changing to 1kg
        data_source = '1kg'
        data_source_filter = "Data Source:1k_genome_phase3,Output type:SNP_biallelic,Genome build:{},File format:bed bed17".format(genome_build)
        _print_log_message("{}: Unknown data source. Using 1000 Genomes instead.".format(data_source), 'warn')
    
    ## Overlap against 1000 Genome SNPs
    tracks_df = inferno.select(gadb_df, data_source_filter)
    
    overlap_df = inferno.loadgiggle(gwas_df.rdd, tracks_df, method='cmd_partitions', temp_dir=temp_dir, giggle_app=giggle_app, bgzip_app=bgzip_app)
    
    overlap_df = inferno.expand_columns_from_string(overlap_df, ['additionalQueriedString', 'hitString'], [additionalQueriedString_schema, hitString_schema], ["\t", "\t"])\
                 .drop('additionalQueriedString', 'hitString', 'absPath', 'chr', 'chrStart', 'chrEnd')
    
    ## Check INDEL variants
    gwas_df = gwas_df.withColumn('INDEL variant', \
    F.when(is_indel_udf(F.col(allele1_col_name), F.col(allele2_col_name)), F.lit('INDEL variant')))
    stats_col_list = ['INDEL variant']
    
    ## Process an exclude list
    if bool(exclude_bed):
        if not path.exists(exclude_bed):
            _print_log_message(exclude_bed + ': No such file or directory', 'error')
            sys.exit(1)
        
        excluded_region_list = inferno.load_bedfile_as_rdd(exclude_bed).map(lambda x: [x[0] if len(x[0])>3 else 'chr'+x[0], int(x[1]), int(x[2])]).collect()
        
        ## Walking through all regions via a loop to simplify the process may be time-consuming once lots of regions in the exclude list. Ideally, the list wouldn't contain too many regions, otherwise, should consider using Tabix to get overlaps instead.
        @F.udf(BooleanType())
        def _is_overlap(chr, pos):
            return_result = False
            for excl_chr, excl_start, excl_end in excluded_region_list:
                if (chr == excl_chr) and (pos > excl_start) and (pos <= excl_end):
                    return_result = True
            return return_result
        
        gwas_df = gwas_df.withColumn('Excluded by user', \
        F.when((_is_overlap(F.col(chr_col_name), F.col(pos_col_name))) & \
               (F.col('INDEL variant').isNull()), F.lit('Excluded by user')))
    else:
        gwas_df = gwas_df.withColumn('Excluded by user', F.lit(None).cast('string'))
    stats_col_list.append('Excluded by user')
    
    ## Get SNPs which are not found in 1kg/dbSNP
    overlap_df = gwas_df.join(overlap_df, 'snp_id', 'outer')\
                        .persist(pyspark.StorageLevel.DISK_ONLY)\
                        .select(gwas_df.columns + [i for i in hitString_schema if i])\
                        .withColumn('SNP not found', \
                        F.when((F.col(data_source+'_chr').isNull()) & \
                               (F.col('INDEL variant').isNull()) & \
                               (F.col('Excluded by user').isNull()), F.lit('SNP not found')))
    stats_col_list.append('SNP not found')
    
    ## Get specified MAF of 1kg
    if super_population == 'GLOBAL':
        overlap_df = overlap_df\
        .withColumn(data_source+'_maf', F.regexp_replace(overlap_df[data_source+'_global_af'], 'GLOBAL_AF=', ''))\
        .drop(data_source+'_global_af', data_source+'_super_pop_af')
    else: ## super population
        overlap_df = overlap_df\
        .withColumn(data_source+'_maf', F.regexp_replace(overlap_df[data_source+'_super_pop_af'], ".*%s_AF=(\d\.*\d*);*.*" % super_population, '$1'))\
        .drop(data_source+'_global_af', data_source+'_super_pop_af')        

    ## Cast type for further analyses
    if af_col_name is not None:
        overlap_df = overlap_df\
        .withColumn(af_col_name, F.col(af_col_name).cast('double'))\
        .withColumn(data_source+'_maf', F.col(data_source+'_maf').cast('double'))
    else:
        overlap_df = overlap_df\
                     .withColumn(data_source+'_maf', F.col(data_source+'_maf').cast('double'))
    
    ## Check ambiguous SNPs (i.e., allele1 == ALT and allele2 == REF and af_col_name > 0.65 and 1kg/dbSNP MAF < 0.35, thresholds refer to QCGWAS)
    if not skip_check_ambiguous:
        if af_col_name is not None:
            overlap_df = overlap_df.withColumn('Ambiguous SNP', \
            F.when((F.col(allele1_col_name)==F.col(data_source+'_alt')) & \
                   (F.col(allele2_col_name)==F.col(data_source+'_ref')) & \
                   (get_rev_complementary_udf(F.col(allele1_col_name))==F.col(allele2_col_name)) & \
                   (F.col(af_col_name) > 0.65) & \
                   (F.col(data_source+'_maf') < 0.35) & \
                   (F.col('INDEL variant').isNull()) & \
                   (F.col('Excluded by user').isNull()) & \
                   (F.col('SNP not found').isNull()), F.lit('Ambiguous SNP')))
        else:
            _print_log_message('Skip checking ambiguous SNPs due to the --af_col_name option is not specified', 'warn')
            overlap_df = overlap_df.withColumn('Ambiguous SNP', F.lit(None).cast('string'))
    else:
        overlap_df = overlap_df.withColumn('Ambiguous SNP', F.lit(None).cast('string'))
    stats_col_list.append('Ambiguous SNP')
            
    ## Check flipped alleles (i.e., allele1 == REF and allele2 == ALT)
    overlap_df = overlap_df.withColumn('Allele flip', \
    F.when((F.col(allele1_col_name)==F.col(data_source+'_ref')) & \
           (F.col(allele2_col_name)==F.col(data_source+'_alt')) & \
           (F.col('INDEL variant').isNull()) & \
           (F.col('Excluded by user').isNull()) & \
           (F.col('SNP not found').isNull()), F.lit('Allele flip')))
    stats_col_list.append('Allele flip')
    
    ## Check (flipped) negative-strand alleles
    if not skip_check_negative_strand:
        overlap_df = overlap_df.withColumn('Negative-strand SNP', \
        F.when((F.col(allele1_col_name)!=F.col(data_source+'_alt')) & \
               (F.col(allele2_col_name)!=F.col(data_source+'_ref')) & \
               (get_rev_complementary_udf(F.col(allele1_col_name))==F.col(data_source+'_alt')) & \
               (get_rev_complementary_udf(F.col(allele2_col_name))==F.col(data_source+'_ref')) & \
               (F.col('INDEL variant').isNull()) & \
               (F.col('Excluded by user').isNull()) & \
               (F.col('SNP not found').isNull()), F.lit('Negative-strand SNP')))
         
        overlap_df = overlap_df.withColumn('Flipped negative-strand SNP', \
        F.when((F.col(allele1_col_name)!=F.col(data_source+'_alt')) & \
               (F.col(allele2_col_name)!=F.col(data_source+'_ref')) & \
               (get_rev_complementary_udf(F.col(allele1_col_name))==F.col(data_source+'_ref')) & \
               (get_rev_complementary_udf(F.col(allele2_col_name))==F.col(data_source+'_alt')) & \
               (F.col('INDEL variant').isNull()) & \
               (F.col('Excluded by user').isNull()) & \
               (F.col('SNP not found').isNull()), F.lit('Flipped negative-strand SNP')))
    else:
        overlap_df = overlap_df.withColumn('Negative-strand SNP', F.lit(None).cast('string')).withColumn('Flipped negative-strand SNP', F.lit(None).cast('string'))
    stats_col_list.append('Negative-strand SNP')
    stats_col_list.append('Flipped negative-strand SNP')
        
    ## Check unmatchable alleles
    overlap_df = overlap_df.withColumn('Unmatchable allele', \
    F.when(((F.col(allele1_col_name)!=F.col(data_source+'_alt')) | \
            (F.col(allele2_col_name)!=F.col(data_source+'_ref'))) & \
           (F.col('INDEL variant').isNull()) & \
           (F.col('Excluded by user').isNull()) & \
           (F.col('SNP not found').isNull()) & \
           (F.col('Ambiguous SNP').isNull()) & \
           (F.col('Allele flip').isNull()) & \
           (F.col('Negative-strand SNP').isNull()) & \
           (F.col('Flipped negative-strand SNP').isNull()), F.lit('Unmatchable allele')))
    stats_col_list.append('Unmatchable allele')
    
    ## Check rsID
    if rsid_col_name is not None:
        overlap_df = overlap_df.withColumn('Inconsistent rsID', \
        F.when((F.col(rsid_col_name)!=F.col(data_source+'_rsID')) & \
               (F.col('INDEL variant').isNull()) & \
               (F.col('Excluded by user').isNull()) & \
               (F.col('SNP not found').isNull()), F.lit('Inconsistent rsID')))
        stats_col_list.append('Inconsistent rsID')
     
    ## Check MAF
    if not skip_check_MAF:
        if af_col_name is not None:
            overlap_df = overlap_df.withColumn('Allele frequency difference', \
            F.when((F.abs(F.col(af_col_name) - F.col(data_source+'_maf')) > diff_MAF_threshold) & \
                   (F.col('INDEL variant').isNull()) & \
                   (F.col('Excluded by user').isNull()) & \
                   (F.col('SNP not found').isNull()), F.lit('Allele frequency difference')))
            stats_col_list.append('Allele frequency difference')
        else:
            _print_log_message('Skip checking MAF due to the --af_col_name option is not specified', 'warn')
    
    ## Generate normalized (resolved) allele columns
    ## normalizing "SNP not found" and "Unmatchable allele"
    if not skip_normalizing_allele:
        ## Get genome reference paths from FILER
        gadb_df = inferno.loadgadb(gadb_meta_file)
        ref_data_source_filter = "Data Source:Reference Genome,Output type:Sequence,Genome build:{},File format:fasta".format(genome_build)
        fa_df = inferno.select(gadb_df, ref_data_source_filter).select(F.regexp_replace(F.col('File name'), '\.fa\.gz$', '').alias('chr'), 'absPath')
        
        ## Separate SNPs with tagged "SNP not found" or "Unmatchable allele" into a new dataframe (need_normalizing_df).
        need_normalizing_df = overlap_df.filter((F.col('SNP not found').isNotNull() | F.col('Unmatchable allele').isNotNull()))
        ## SNPs in this dataframe don't normalize via genome reference
        overlap_df = overlap_df.filter((F.col('SNP not found').isNull() & F.col('Unmatchable allele').isNull()))
        
        ## Apply loadsamtools_fa() to normalize need_normalizing_df SNPs via genome reference
        schema = StructType([StructField('snp_id', StringType(), False),
                     StructField('normalized_ref', StringType(), False),
                     StructField('normalized_alt', StringType(), False),
                     StructField('Unnormalized allele', StringType(),True)])
        
        normalized_result_df = need_normalizing_df\
        .select(F.col(chr_col_name).alias('chr'), 'snp_id')\
        .join(fa_df, 'chr', 'left_outer')\
        .groupBy('chr')\
        .agg(F.collect_list('snp_id').alias('snp_id_list'),\
             F.first('absPath').alias('absPath'))\
        .rdd\
        .mapPartitionsWithIndex(lambda i, x: loadsamtools_fa(i, x, temp_dir=temp_dir, samtools_app=samtools_app, conf_path=conf_path))\
        .toDF(schema)
        
        ## Directly take 1kg_ref and 1kg_alt alleles as normalization (exclude INDEL variants). Merge all SNPs together.
        overlap_df = overlap_df\
        .withColumn('normalized_ref', \
                    F.when((F.col('INDEL variant').isNull()) & \
                           (F.col('Excluded by user').isNull()), F.col(data_source+'_ref')))\
        .withColumn('normalized_alt', \
                    F.when((F.col('INDEL variant').isNull()) & \
                           (F.col('Excluded by user').isNull()), F.col(data_source+'_alt')))\
        .withColumn('Unnormalized allele', F.lit(None).cast('string'))\
        .unionByName(need_normalizing_df.join(normalized_result_df, 'snp_id', 'left_outer'))
        
        ## Add Unnormalized allele tag
        stats_col_list.append('Unnormalized allele')
    
    ## Sum up stats_* columns
    overlap_df = overlap_df.withColumn('QC_category', F.concat_ws(', ', *stats_col_list))
    
    ## Get Pass SNPs
    overlap_df = overlap_df.withColumn('QC_category', \
    F.when(F.col('QC_category')=='', F.lit('Pass'))\
     .otherwise(F.col('QC_category')))\
     .persist(pyspark.StorageLevel.DISK_ONLY)
    
    ## return a dataframe with QC_category column and a dictionary of the numbers of QC stats
    return overlap_df.drop(*stats_col_list), show_gwas_qc_summary(overlap_df, stats_col_list)
    

def loadsamtools_fa(worker_index, rdd_list, temp_dir=gettempdir(), samtools_app='samtools', conf_path=''):
    return_list = list() ## Expect return format: [[snp_id, normalized_ref, normalized_alt, Unnormalized allele], ...]
    for chr_grp in rdd_list:
        snp_dict = dict()
        tmp_queried_snp_path = path.join(temp_dir, str(uuid4()))
        fp =  open(tmp_queried_snp_path, 'w')
        
        for snp_id in chr_grp['snp_id_list']:
            snp_id_contain = snp_id.split(':')
            chr = snp_id_contain[0]
            pos = snp_id_contain[1].split('-')[1]
            A1A2 = snp_id_contain[2].split('>')
            A1 = A1A2[0]
            A2 = A1A2[1]
            queried_coordinate = "{}:{}-{}".format(chr, pos, pos)
            snp_dict[queried_coordinate] = {'snp_id': snp_id, 'A1': A1, 'A2': A2}
            fp.write(queried_coordinate + "\n")
        fp.close()
        
        p = sp.Popen("%s faidx %s -r %s" % (samtools_app, chr_grp['absPath'], tmp_queried_snp_path), stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            _print_log_message("[Worker %d] Fail. %d: %s\n" % (worker_index, p.returncode, stderr), 'error', conf_path=conf_path)
            _exit(1)
        else:
            get_returned_coordinate = False
            last_returned_coordinate = ''
            for line in stdout.splitlines():
                if isinstance(line, bytes):
                    line = line.decode('utf-8')
                if line.startswith('>'):
                    returned_coordinate = line[1:]
                    ## Return SNPs which CANNOT match up the genome reference
                    if get_returned_coordinate:
                        ## Process such case of unavailable coordinate:
                        ## >chr1:2000000000-2000000000 <- dealing with this unavailable coordinate
                        ## >chr1:100000012-100000012 <- the loop is going here
                        ## G
                        return_list.append([snp_dict[last_returned_coordinate]['snp_id'], snp_dict[last_returned_coordinate]['A1'], snp_dict[last_returned_coordinate]['A2'], 'Unnormalized allele'])
                        last_returned_coordinate = returned_coordinate
                    else:
                        get_returned_coordinate = True
                        last_returned_coordinate = returned_coordinate
                else:
                    ## Get nucleotide from the genome reference
                    REF = line.upper()
                    
                    ## Look for reference alleles and return normalized/unnormalized SNPs
                    if snp_dict[returned_coordinate]['A1'] == REF:
                        return_list.append([snp_dict[returned_coordinate]['snp_id'], snp_dict[returned_coordinate]['A1'], snp_dict[returned_coordinate]['A2'], None])
                        get_returned_coordinate = False
                    elif snp_dict[returned_coordinate]['A2'] == REF:
                        return_list.append([snp_dict[returned_coordinate]['snp_id'], snp_dict[returned_coordinate]['A2'], snp_dict[returned_coordinate]['A1'], None])
                        get_returned_coordinate = False
                    else:
                        return_list.append([snp_dict[returned_coordinate]['snp_id'], snp_dict[returned_coordinate]['A1'], snp_dict[returned_coordinate]['A2'], 'Unnormalized allele'])
                        get_returned_coordinate = False
                
            ## Process such case of unavailable coordinate:
            ## >chr1:100000012-100000012 
            ## G
            ## >chr1:2000000000-2000000000 <- dealing with this unavailable coordinate in the last line of stdout.splitlines()
            if get_returned_coordinate:
                return_list.append([snp_dict[last_returned_coordinate]['snp_id'], snp_dict[last_returned_coordinate]['A1'], snp_dict[last_returned_coordinate]['A2'], 'Unnormalized allele'])
        
        remove(tmp_queried_snp_path)
    
    return return_list


def show_gwas_qc_summary(df, stats_col_list):
    qc_summary_num_dict = {}
    qc_summary_num_dict['Pass'] = df.filter(F.col('QC_category') == 'Pass').count()
    for stats_col in stats_col_list:
        qc_summary_num_dict[stats_col] = df.filter(F.col(stats_col) == stats_col).count()
        
    return qc_summary_num_dict


def _get_rev_complementary(nucl):
    return_nucl = ''
    comp_dict = {'A':'T', 'T':'A', 'C':'G', 'G':'C'}
    for x in nucl[::-1]:
        try:
            return_nucl += comp_dict[x]
        except KeyError:
            return_nucl += x
    
    return return_nucl


def _is_indel(A1, A2):
    nucl = ['A', 'C', 'G', 'T']
    if (A1 in nucl) and (A2 in nucl) and len(A1)==1 and len(A2)==1: return False
    else: return True            


def summary_stats_df_to_bed(summary_stats_df, output_filename, temp_dir=gettempdir(), tabix_app='tabix', bgzip_app='bgzip'):
    output_dir, input_bed = path.split(output_filename)
    
    ## Save a header of the converted BED as another file using for spilt columns after running Tabix
    with open(path.join(output_dir, "{}.gz.header".format(output_filename)), 'w') as header_fp:
        header_fp.write("\t".join(summary_stats_df.columns))
    
    output_tmp_dir = path.join(output_dir, 'output_tmp')
    summary_stats_df.select(summary_stats_df.columns).write.format('csv').option('sep', "\t").mode('overwrite').save(output_tmp_dir, header=True)
    
    ## Output BED file
    inferno.coalesce_and_sort_saving_files(output_tmp_dir, output_filename, 'csv', header=False, remove_part_file_dir=True, temp_dir=temp_dir)
    ## Compress the BED file as bed.gz followed by making Tabix index
    make_tabix_index(output_filename, output_dir, temp_dir=temp_dir, tabix_app=tabix_app, bgzip_app=bgzip_app)
    
    remove(output_filename)
    
    return output_filename
    

def _inverse_effect_dir(effect_dir):
    return_str = ''
    for sign in effect_dir:
        if sign == '+': return_str += '-'
        elif sign == '-': return_str += '+'
        else: return_str += sign
    
    return return_str


def normalize_effect_direction(summary_stats_df, allele1_col_name=None, allele2_col_name=None, af_col_name=None, beta_col_name=None, effect_dir_col_name=None, target_col_prefix='normalized'):
    ## This function performs normalization of effect direction by comparison with the allele frequency column to determine the direction of beta values which represent expanded variants w/ or w/o consideration of effect direction (method=af), as well as by using alternative alleles as effect alleles from 1000 Genomes to normalize effect directions (method=alt, default).
    get_rev_complementary_udf = F.udf(lambda c: _get_rev_complementary(c), StringType())
    inverse_effect_dir_udf = F.udf(lambda x: _inverse_effect_dir(x), StringType())
    
    summary_stats_df = summary_stats_df\
                       .withColumn('need_inversed', \
                       F.when(( (F.col(allele1_col_name) != F.col(target_col_prefix+'_alt')) & \
                                (F.col(allele2_col_name) != F.col(target_col_prefix+'_ref'))\
                              ) & \
                              ( ( (F.col(allele1_col_name) == F.col(target_col_prefix+'_ref')) & \
                                  (F.col(allele2_col_name) == F.col(target_col_prefix+'_alt'))\
                                ) | \
                                ( (get_rev_complementary_udf(F.col(allele1_col_name)) == F.col(target_col_prefix+'_ref')) & \
                                  (get_rev_complementary_udf(F.col(allele2_col_name)) == F.col(target_col_prefix+'_alt'))\
                                )\
                              ), True)\
                        .otherwise(False))
    
    if bool(af_col_name):
        summary_stats_df = summary_stats_df\
                           .withColumn(af_col_name, F.col(af_col_name).cast('double'))\
                           .withColumn('normalized_maf', \
                           F.when(F.col('need_inversed'), 1-F.col(af_col_name))\
                           .otherwise(F.col(af_col_name)))
    
    if bool(beta_col_name):
        summary_stats_df = summary_stats_df\
                           .withColumn(beta_col_name, F.col(beta_col_name).cast('double'))\
                           .withColumn('normalized_beta', \
                           F.when(F.col('need_inversed'), F.col(beta_col_name)*(-1))\
                           .otherwise(F.col(beta_col_name)))
    
    if bool(effect_dir_col_name):
        summary_stats_df = summary_stats_df\
                           .withColumn('normalized_effect_direction', \
                           F.when(F.col('need_inversed'), inverse_effect_dir_udf(F.col(effect_dir_col_name)))\
                           .otherwise(F.col(effect_dir_col_name)))
    
    return summary_stats_df.drop('need_inversed')
    
    
def create_top_snp_list(target_path=path.join(getcwd(), 'user_top_snp_list.tsv')):
    try:
        fp = open(target_path, 'w')
    except IOError:
        _print_log_message(target_path + ": Cannot save the file\n", 'error')
        sys.exit(1)
        
    fp.write("chr\tpos\tregion_name\trsID (optional)\tallele1 (optional)\tallele2 (optional)\n")
    _print_log_message(target_path + ' has beed created successfully', 'info')
    

def load_top_snp_list(top_snp_list, gadb_meta_file, is_header=True, rsID_only=False, delimiter="\t", genome_build='hg19'):
    if rsID_only:
        schema = StructType([StructField('chr', StringType(),False),
                             StructField('pos_start', IntegerType(), False),
                             StructField('pos', IntegerType(), False),
                             StructField('snp_id', StringType(), False),
                             StructField('allele1', StringType(), False),
                             StructField('allele2', StringType(), False),
                             StructField('region_pos', StringType(), False),
                             StructField('region_name', StringType(), False),
                             StructField('rsID', StringType(), False)])
        sc = this.sc
        ## Retrieve needed columns from 1000 Genomes
        _print_log_message('Found top snp list is providing rsID column only. Retrieving needed columns from 1000 Genomes.')
               
        ## Get 1000 Genomes BED file paths from GADB
        gadb_df = inferno.loadgadb(gadb_meta_file)
        data_source_filter = "Data Source:1k_genome_phase3,Output type:SNP_biallelic,Genome build:{},File format:bed bed17".format(genome_build)
        tracks_list = inferno.select(gadb_df, data_source_filter).collect()

        if len(tracks_list) == 0:
            _print_log_message('1000 Genomes BED files are not found in GADB', 'error')
            bed_ref_path = []
        else:
            bed_ref_path = [t['absPath'] for t in tracks_list]        
        
        top_snp_list_df = sc.parallelize(bed_ref_path)\
                            .map(Row('bed_path'))\
                            .flatMap(lambda x: _lookup_1kg_by_rsid(x, top_snp_list))\
                            .toDF(schema)\
                            .cache()
        
    else:
        top_snp_list_df = this.spark.read.format('csv').option('header', is_header).option('delimiter', delimiter).load(top_snp_list)
        
        ## Report an error if the input list does not contain 6 reqiured columns
        if len(top_snp_list_df.columns) != 6:
            _print_log_message("The input contains need to include the 'chr', 'pos', 'region_name', 'rsID (optional)', 'allele1 (optional)', 'allele2 (optional)' columns in order and be delimited by a tab character\n", 'error')
            sys.exit(1)
    
        ## Replace header
        header_convert = {'_c0': 'chr', '_c1': 'pos', '_c2': 'region_name', '_c3': 'rsID', '_c4': 'allele1', '_c5': 'allele2'}
        if not is_header:
            for old_col, new_col in header_convert.items():
                top_snp_list_df = top_snp_list_df.withColumnRenamed(old_col, new_col)
        else: ## Remove (optional) from the header
            ## The original header of top_snp_list is ['chr', 'pos', 'region_name', 'rsID (optional)', 'allele1 (optional)', 'allele2 (optional)']
            for old_col in top_snp_list_df.columns:
                if ' (optional)' in old_col:
                    top_snp_list_df = top_snp_list_df.withColumnRenamed(old_col, old_col.replace(' (optional)', ''))
        
        top_snp_list_df = top_snp_list_df\
                          .withColumn('chr', F.when(F.length('chr') > 3, F.col('chr')).otherwise(F.concat(F.lit('chr'), F.col('chr'))))\
                          .withColumn('pos_start', (F.col('pos')-1).cast('long'))
        
        ## Change alleles to upper cases if existing and/or add snp_id and region_pos columns for the purpose of results from LD pruning and expansion which can be joined with when the input is top SNP list only
        top_snp_list_count = top_snp_list_df.count()
        allele1_count = top_snp_list_df.filter(top_snp_list_df.allele1.isNotNull()).count()
        allele2_count = top_snp_list_df.filter(top_snp_list_df.allele2.isNotNull()).count()
        if ('allele1' in top_snp_list_df.columns) and ('allele2' in top_snp_list_df.columns) and (top_snp_list_count == allele1_count == allele2_count):
            top_snp_list_df = top_snp_list_df\
                              .withColumn('allele1', F.upper(F.col('allele1')))\
                              .withColumn('allele2', F.upper(F.col('allele2')))\
                              .withColumn('snp_id', F.concat(F.col('chr'), F.lit(':'), F.col('pos_start'), F.lit('-'), F.col('pos'), F.lit(':'), F.col('allele1'), F.lit('>'), F.col('allele2')))\
                              .withColumn('region_pos', F.col('snp_id'))
        else:
            top_snp_list_df = top_snp_list_df\
                              .withColumn('snp_id', F.concat(F.col('chr'), F.lit(':'), F.col('pos_start'), F.lit('-'), F.col('pos')))\
                              .withColumn('region_pos', F.col('snp_id'))
        
        top_snp_list_header = ['chr', 'pos_start', 'pos', 'snp_id', 'allele1', 'allele2', 'region_pos', 'region_name', 'rsID']
        top_snp_list_header = [i for i in top_snp_list_header if i in top_snp_list_df.columns]
        top_snp_list_df = top_snp_list_df.select(top_snp_list_header)
        
    return top_snp_list_df


def _lookup_1kg_by_rsid(bed_path_rdd, rsid_list_path):
    cmd = 'zgrep -w -F -f {} {}'.format(rsid_list_path, bed_path_rdd.bed_path)
    p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
    stdout, stderr = p.communicate()
    if p.returncode == 2: ## For zgrep exit status, normally the exit status is 0 if a line is selected, 1 if no lines were selected, and 2 if an error occurred.
        _print_log_message("Fail. %d: %s" % (p.returncode, stderr), 'error')
        _exit(1)
        
    hitlist = list()
    for hit in stdout.splitlines():
        ## The format of BED file from 1000 Genomes:
        ## 0:'chr', 1:'chr_start', 2:'chr_end', 3:'rsID', 4:'qual', 5:'strand', 6:'ref', 7:'alt', 8:'GLOBAL_AF', 9:'GLOBAL_AC', 10:'GLOBAL_AN', 11:'SUPER_POP_AF', 12:'SUPER_POP_AC', 13:'SUPER_POP_AN', 14:'SUB_POP_AF', 15:'SUB_POP_AC', 16:'SUB_POP_AN'
        try:
            hit_list = hit.split("\t")
        except TypeError:
            hit_list = hit.decode('utf-8').split("\t")
        hit_list[1] = int(hit_list[1])
        hit_list[2] = int(hit_list[2])
        
        snp_id = "{}:{}-{}:{}>{}".format(hit_list[0], hit_list[1], hit_list[2], hit_list[6], hit_list[7])
        
        ## Output format:
        ## 0:'chr', 1:'chr_start', 2:'chr_end', 3:'snp_id', 4:'ref', 5:'alt', 6:'region_pos', 7:'region_name', 8:'rsID'
        hitlist.append(list(hit_list[0:3]) + [snp_id] + list(hit_list[6:8]) + [snp_id, snp_id, hit_list[3]])
    
    return hitlist
    
    
def get_genome_wide_significant_top_snp(summary_stats_df, pval_col_name=None, top_snp_thresh=5e-8):
    top_snp_df = summary_stats_df\
    .withColumn(pval_col_name, F.col(pval_col_name).cast('double'))\
    .filter(F.col(pval_col_name)<float(top_snp_thresh))\
    .withColumn('region_name', F.col('region_pos'))
        
    return top_snp_df


def get_top_snp_from_gwas_summary(top_snp_list_df, summary_stats_df, gwas_bed_path=None, tabix_app='tabix', chr_col_name=None, pos_col_name=None, allele1_col_name=None, allele2_col_name=None, rsid_col_name=None):
    ## GWAS + SNP list, retrieve needed columns from GWAS file and replace the original header of top_snp_list with as the same as a GWAS header
    top_snp_list_header_convert = {'chr': chr_col_name, 'pos': pos_col_name, 'rsID': rsid_col_name, 'allele1': allele1_col_name, 'allele2': allele2_col_name}
    for old_col, new_col in top_snp_list_header_convert.items():
        if new_col is not None:
            top_snp_list_df = top_snp_list_df.withColumnRenamed(old_col, new_col)
    
    top_snp_list_count = top_snp_list_df.count()
    allele1_count = top_snp_list_df.filter(top_snp_list_df[allele1_col_name].isNotNull()).count()
    allele2_count = top_snp_list_df.filter(top_snp_list_df[allele2_col_name].isNotNull()).count()
    if rsid_col_name is not None:
        rsID_count = top_snp_list_df.filter(top_snp_list_df[rsid_col_name].isNotNull()).count()
    else:
        rsID_count = 0
    
    top_snp_list_header = [chr_col_name, 'pos_start', pos_col_name, 'region_name', rsid_col_name, allele1_col_name, allele2_col_name]
    ## Remove None value from top_snp_list_header
    top_snp_list_header = [i for i in top_snp_list_header if i in top_snp_list_df.columns]
    
    top_snp_list_df = top_snp_list_df.select(top_snp_list_header)
    ## Run Tabix overlap
    overlap_df = inferno.loadtabix(top_snp_list_df.rdd, gwas_bed_path, method='cmd_partitions', tabix_app=tabix_app)
    
    additionalQueriedString_schema = ["__top_snp__%s" % col_name for col_name in top_snp_list_df.columns[3:]]
    hitString_schema = open(gwas_bed_path+'.header', 'r').readline().rstrip().split("\t")
    
    return_df = inferno.expand_columns_from_string(overlap_df, ['additionalQueriedString', 'hitString'], [additionalQueriedString_schema, hitString_schema], ["\t", "\t"]).drop('additionalQueriedString', 'hitString', 'absPath')
    
    if (allele1_col_name in top_snp_list_df.columns) and (allele2_col_name in top_snp_list_df.columns) and (top_snp_list_count == allele1_count == allele2_count):
        return_df = return_df.withColumn('allele_matchup', F.when(\
                    ((F.col("__top_snp__%s" % allele1_col_name)==F.col(allele1_col_name)) & (F.col("__top_snp__%s" % allele2_col_name)==F.col(allele2_col_name))) | \
                    ((F.col("__top_snp__%s" % allele1_col_name)==F.col(allele2_col_name)) & (F.col("__top_snp__%s" % allele2_col_name)==F.col(allele1_col_name))), 1)\
                    .otherwise(0))\
                    .filter("`allele_matchup` = 1")
    elif (rsid_col_name in top_snp_list_df.columns) and (top_snp_list_count == rsID_count):
        return_df = return_df.withColumn('raID_matchup', F.when(F.col("__top_snp__%s" % rsid_col_name)==F.col(rsid_col_name), 1).otherwise(0)).filter("`raID_matchup` = 1")
    
    return_df = return_df.withColumnRenamed('__top_snp__region_name', 'region_name')\
                         .select(summary_stats_df.columns + ['region_name'])
    
    return return_df
    

def pval_expansion(top_snp_df, gwas_bed_path, tabix_app='tabix', pval_col_name=None, beta_col_name=None, dist_threshold=500000, sig_multiplier=None, locus_thresh=None, effect_direction=False):
    ## Run Tabix overlap
    overlap_df = inferno.loadtabix(top_snp_df.rdd, gwas_bed_path, method='cmd_partitions', window_size=dist_threshold, tabix_app=tabix_app).cache()
    
    additionalQueriedString_schema = ["top_snp_%s" % col_name for i, col_name in enumerate(top_snp_df.columns) if i > 2]
    hitString_schema = ["expanded_%s" % col_name for col_name in open(gwas_bed_path+'.header', 'r').readline().rstrip().split("\t")]
    
    overlap_df = inferno.expand_columns_from_string(overlap_df, ['additionalQueriedString', 'hitString'], [additionalQueriedString_schema, hitString_schema], ["\t", "\t"]).drop('additionalQueriedString', 'hitString', 'absPath')
    
    ## Expanding variants with consistent effect directions
    if effect_direction:
        ## Cast right types of columns
        overlap_df = overlap_df\
        .withColumn("top_snp_%s" % pval_col_name, F.col("top_snp_%s" % pval_col_name).cast('double'))\
        .withColumn("expanded_%s" % pval_col_name, F.col("expanded_%s" % pval_col_name).cast('double'))\
        .withColumn("top_snp_%s" % beta_col_name, F.col("top_snp_%s" % beta_col_name).cast('double'))\
        .withColumn("expanded_%s" % beta_col_name, F.col("expanded_%s" % beta_col_name).cast('double'))
        
        ## Using sig_multiplier as a cutoff
        if sig_multiplier is not None:
            ## "`top_snp_pval`/sig_multiplier <= `expanded_pval` AND `expanded_pval` <= `top_snp_pval`*sig_multiplier AND `top_snp_beta`*`expanded_beta` > 0"
            overlap_df = overlap_df.filter("`expanded_%s` <= `top_snp_%s`*%s AND `top_snp_%s`*`expanded_%s` > 0" % (pval_col_name, pval_col_name, sig_multiplier, beta_col_name, beta_col_name))
            
        elif locus_thresh is not None:
            ## "`expanded_pval` <= locus_thresh AND `top_snp_beta`*`expanded_beta` > 0"
            overlap_df = overlap_df.filter("`expanded_%s` <= %s AND `top_snp_%s`*`expanded_%s` > 0" % (pval_col_name, locus_thresh, beta_col_name, beta_col_name))
            
        else:
            _print_log_message('Either relative (sig_multiplier) or absolute (locus_thresh) thresholds should be provided', 'error')
            sys.exit(1)
            
    ## Expanding variants with no consideration of effect direction
    else:
        ## Using sig_multiplier as a cutoff
        if sig_multiplier is not None:
            ## "`top_snp_pval`/sig_multiplier <= `expanded_pval` AND `expanded_pval` <= `top_snp_pval`*sig_multiplier"
            overlap_df = overlap_df.filter("`expanded_%s` <= `top_snp_%s`*%s" % (pval_col_name, pval_col_name, sig_multiplier))
            
        elif locus_thresh is not None:
            ## "`expanded_pval` <= locus_thresh"
            overlap_df = overlap_df.filter("`expanded_%s` <= %s" % (pval_col_name, locus_thresh))
            
        else:
            _print_log_message('Either relative (sig_multiplier) or absolute (locus_thresh) thresholds should be provided', 'error')
            sys.exit(1)
    
    return overlap_df


## This function derives a vcf subset from 1000 Genomes for LD pruning
def matchup_1kGenomes(pval_expansion_df, gadb_meta_file, output_dir, temp_dir=gettempdir(), tabix_app='tabix', chr_col_name=None, pos_col_name=None, allele1_col_name=None, allele2_col_name=None, super_population='EUR', genome_build='hg19', conf_path='', quiet=False):
    ## pval_expansion_df contains SNPs with the identical coordinates but different region_pos. In order to remove those duplication SNPs, drop region_pos and run distinct command. The coordinate_list column is using for the purpose of matching up 1000 Genomes in vcf.
    total_df_count = pval_expansion_df.count()
    allele1_count = pval_expansion_df.filter(pval_expansion_df[allele1_col_name].isNotNull()).count()
    allele2_count = pval_expansion_df.filter(pval_expansion_df[allele2_col_name].isNotNull()).count()
    if total_df_count == allele1_count == allele2_count: check_allele = True
    else: check_allele = False
    
    ## Get vcf files from GADB. These codes cannot be a part in loadtabix_vcf function bacause dataframe cannot work in workers.
    gadb_df = inferno.loadgadb(gadb_meta_file)
    data_source_filter = "Data Source:1k_genome_phase3,Output type:{} SUPER SNP_biallelic,Genome build:{},File format:vcf".format(super_population, genome_build)
    tracks_list = inferno.select(gadb_df, data_source_filter).collect()
    
    if len(tracks_list) == 0:
        _print_log_message('1000 Genomes VCF files are not found in GADB', 'error')
        sys.exit(1)
    else:
        vcf_ref_path_dict = dict([("chr{}".format(t['File name'].split('_')[0]), t['absPath']) for t in tracks_list])
    
    schema = StructType([StructField('snp_id', StringType(),True),
                         StructField('vcf_path', StringType(), True)])
    
    vcf_path_df = pval_expansion_df\
                  .drop('region_pos', 'region_name')\
                  .distinct()\
                  .withColumn('coordinate_list', F.concat(F.regexp_replace(chr_col_name, 'chr', ''), F.lit(':'), F.col(pos_col_name), F.lit('-'), F.col(pos_col_name)))\
                  .groupby(F.col(chr_col_name).alias('grouped_col'))\
                  .agg(F.collect_list('coordinate_list').alias('coordinate_list'),\
                       F.collect_list('snp_id').alias('snp_id'),\
                       F.first(F.col(chr_col_name)).alias('chr'))\
                  .rdd\
                  .mapPartitionsWithIndex(lambda i, x: loadtabix_vcf(i, x, vcf_ref_path_dict, output_dir, check_allele, temp_dir=temp_dir, tabix_app=tabix_app, conf_path=conf_path, quiet=quiet))\
                  .toDF(schema)\
                  .cache()
    
    ## Cache the RDD instead of dataframe to prevent executing loadtabix_vcf twice when saving the df (i.e., it doesn't work if cache the df)
    return_df = vcf_path_df.join(pval_expansion_df, 'snp_id', 'left')\
                           .select(pval_expansion_df.columns + ['vcf_path'])\
                           .cache()
    
    return return_df
    
    
## This function derives a vcf subset from 1000 Genomes with a specific window size for LD expansion
def matchup_1kGenomes_with_window_size(ld_pruning_df, gadb_meta_file, output_dir, temp_dir=gettempdir(), tabix_app='tabix', chr_col_name=None, pos_col_name=None, allele1_col_name=None, allele2_col_name=None, window_size=500000, super_population='EUR', genome_build='hg19', conf_path='', quiet=False):
    ## ld_pruning_df contains SNPs with the identical coordinates but different region_pos. To remove those duplication SNPs, drop region_pos and run distinct command. The coordinate_list column is using for the purpose of matching up 1000 Genomes in vcf. In order to make this function perform the same loadtabix_vcf() like matchup_1kGenomes(), run groupby() to aggregate columns as lists and ensure the same required format for loadtabix_vcf, despite the snp_id column has contained distinct element.
    total_df_count = ld_pruning_df.count()
    allele1_count = ld_pruning_df.filter(ld_pruning_df[allele1_col_name].isNotNull()).count()
    allele2_count = ld_pruning_df.filter(ld_pruning_df[allele2_col_name].isNotNull()).count()
    if total_df_count == allele1_count == allele2_count: check_allele = True
    else: check_allele = False
    
    ## Get vcf files from GADB. These codes cannot be a part in loadtabix_vcf function bacause dataframe cannot work in workers.
    gadb_df = inferno.loadgadb(gadb_meta_file)
    data_source_filter = "Data Source:1k_genome_phase3,Output type:{} SUPER SNP_biallelic,Genome build:{},File format:vcf".format(super_population, genome_build)
    tracks_list = inferno.select(gadb_df, data_source_filter).collect()
    
    if len(tracks_list) == 0:
        _print_log_message('1000 Genomes VCF files are not found in GADB', 'error')
        sys.exit(1)
    else:
        vcf_ref_path_dict = dict([("chr{}".format(t['File name'].split('_')[0]), t['absPath']) for t in tracks_list])
    
    schema = StructType([StructField('snp_id', StringType(),True),
                         StructField('vcf_path', StringType(), True)])
    
    vcf_path_df = ld_pruning_df\
                  .drop('region_pos', 'region_name')\
                  .distinct()\
                  .withColumn('coordinate_list', F.concat(F.regexp_replace(chr_col_name, 'chr', ''), F.lit(':'), (F.col(pos_col_name)-window_size).cast(DecimalType(20,0)), F.lit('-'), (F.col(pos_col_name)+window_size).cast(DecimalType(20,0))))\
                  .groupby(F.col('snp_id').alias('grouped_col'))\
                  .agg(F.collect_list('coordinate_list').alias('coordinate_list'),\
                       F.collect_list('snp_id').alias('snp_id'),\
                       F.first(F.col(chr_col_name)).alias('chr'))\
                  .rdd\
                  .mapPartitionsWithIndex(lambda i, x: loadtabix_vcf(i, x, vcf_ref_path_dict, output_dir, check_allele, temp_dir=temp_dir, tabix_app=tabix_app, conf_path=conf_path, quiet=quiet))\
                  .toDF(schema)\
                  .cache()
    
    return_df = vcf_path_df.join(ld_pruning_df, 'snp_id', 'left')\
                           .select(ld_pruning_df.columns + ['vcf_path'])\
                           .cache()
    
    return return_df
    
    
def loadtabix_vcf(worker_index, rdd_list, vcf_ref_path_dict, output_dir_parent, check_allele, temp_dir=gettempdir(), tabix_app='tabix', conf_path='', quiet=False):
    return_list = list()
    for pval_expansion_rdd in rdd_list: ## loop for each chr
        output_dir = path.join(output_dir_parent, pval_expansion_rdd.chr)
        _mkdirs(output_dir)
        
        ## Save a subset vcf which contains overlapped snps
        output_vcf = path.join(output_dir, "%s.plink.vcf.tmp" % pval_expansion_rdd.grouped_col.replace(':', '_').replace('>', '_'))
        vcf_fp = open(output_vcf, 'w')
        
        ## Overlap coordinate
        for idx, queried_coordinate in enumerate(pval_expansion_rdd.coordinate_list):
            p = sp.Popen("%s %s %s" % (tabix_app, vcf_ref_path_dict[pval_expansion_rdd.chr], queried_coordinate), stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
            stdout, stderr = p.communicate()
            if p.returncode != 0:
                _print_log_message("[Worker %d] Fail. %d: %s\n" % (worker_index, p.returncode, stderr), 'error', conf_path=conf_path)
                _exit(1)
            else:
                if stdout == '':
                    _print_log_message("[Worker %d] The SNP/interval %s cannot be found in 1000 Genomes\n" % (worker_index, pval_expansion_rdd.snp_id[idx]), 'warn', conf_path=conf_path)
                else:
                    for line in stdout.splitlines():
                        try:
                            content = line.split("\t")
                        except TypeError:
                            content = line.decode('utf-8').split("\t")
                        ## vcf schema: CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO, FORMAT, ...
                        content[0] = 'chr'+content[0] if len(content[0])<3 else content[0]
                        content[2] = "%s:%s-%s:%s>%s" % (content[0], str(int(content[1])-1), content[1], content[3], content[4]) if check_allele else "%s:%s-%s" % (content[0], str(int(content[1])-1), content[1])
                        CHR = content[0]
                        POS = content[1]
                        REF = content[3]
                        ALT = content[4]
                        
                        ## Consider SNP only, because Tabix reports the input coordinate within the INDEL of vcf
                        if len(REF)>1 or len(ALT)>1 or REF=='-' or ALT=='-': continue
                        
                        snp_id_parts = pval_expansion_rdd.snp_id[idx].split(':')
                        chr = snp_id_parts[0]
                        chr_pos = snp_id_parts[1].split('-')[1]
                        ## Inspect if the alleles are identical
                        if check_allele and (str(CHR) == str(chr) and int(POS) == int(chr_pos)):
                            # print(pval_expansion_rdd.snp_id[idx], line)
                            ## snp_id looks like: chr8:27467685-27467686:C>T
                            ## Get the last part by colon delimited, i.e., C>T
                            alleles = pval_expansion_rdd.snp_id[idx].split(':')[-1]
                            alleles = alleles.split('>')
                            
                            if REF == alleles[0] and ALT == alleles[1]:
                                ## Write SNP with right coordinate and alleles
                                vcf_fp.write("%s\n" % "\t".join(content))
                            elif REF == alleles[1] and ALT == alleles[0]:
                                content[2] = "%s:%s-%s:%s>%s" % (content[0], str(int(content[1])-1), content[1], content[4], content[3])
                                vcf_fp.write("%s\n" % "\t".join(content))
                            else:
                                _print_log_message("[Worker %d] The SNP %s alleles cannot match up with 1000 Genomes alleles (REF: %s, ALT: %s)\n" % (worker_index, pval_expansion_rdd.snp_id[idx], REF, ALT), 'warn', conf_path=conf_path)
                        ## Skip checking identity of alleles 
                        else:
                            vcf_fp.write("%s\n" % "\t".join(content))
                    
                    return_list.append([pval_expansion_rdd.snp_id[idx], output_vcf[:-4]]) ## output_vcf[:-4] means removing .tmp
                
        vcf_fp.close()
        
        ## Generate vcf header and sort vcf
        cmds = ["%s -H %s > %s" % (tabix_app, vcf_ref_path_dict[pval_expansion_rdd.chr], output_vcf[:-4]),
                "LC_ALL=C sort -T %s -k1,1 -k2,2n %s >> %s" % (temp_dir, output_vcf, output_vcf[:-4])]
        for cmd in cmds:
            if not quiet:
                _print_log_message("[Worker %d] Running bash command: %s" % (worker_index, cmd), 'info', conf_path=conf_path)
            sp.call(cmd, shell=True)
        
        remove(output_vcf)
        
    return return_list
    
    
def ld_pruning(pval_expansion_df, vcf_path_df, chr_col_name=None, pos_col_name=None, allele1_col_name=None, allele2_col_name=None, ld_threshold=0.7, window_size='500kb', quiet=False, plink_app='plink', conf_path=''):
    schema = StructType([StructField('__CHROM__', StringType(),False),
                         StructField('__POS__', IntegerType(), False),
                         StructField('__ID__', StringType(), False),
                         StructField('__REF__', StringType(), False),
                         StructField('__ALT__', StringType(), False),
                         StructField('__QUAL__', StringType(), False),
                         StructField('__FILTER__', StringType(), False),
                         StructField('__INFO__', StringType(), False),
                         StructField('__SOURCE__', StringType(), False)])
    
    ld_pruning_df = vcf_path_df.select('vcf_path')\
                    .distinct()\
                    .rdd\
                    .mapPartitionsWithIndex (lambda i, x: run_ld_pruning(i, x, ld_threshold, window_size, quiet, plink_app=plink_app, conf_path=conf_path))\
                    .mapPartitionsWithIndex (lambda i, x: collect_pruning_result(i, x, conf_path=conf_path))\
                    .toDF(schema)\
                    .cache()
        
    total_pval_expansion_count = pval_expansion_df.count()
    allele1_count = pval_expansion_df.filter(pval_expansion_df[allele1_col_name].isNotNull()).count()
    allele2_count = pval_expansion_df.filter(pval_expansion_df[allele2_col_name].isNotNull()).count()
    
    if (allele1_col_name is not None) and (allele2_col_name is not None) and (total_pval_expansion_count == allele1_count == allele2_count):
        return_df = pval_expansion_df\
                    .join(ld_pruning_df, \
                    (ld_pruning_df['__CHROM__']==pval_expansion_df[chr_col_name]) & \
                    (ld_pruning_df['__POS__']==pval_expansion_df[pos_col_name]) & \
                    (((ld_pruning_df['__REF__']==pval_expansion_df[allele1_col_name]) & (ld_pruning_df['__ALT__']==pval_expansion_df[allele2_col_name])) |\
                    ((ld_pruning_df['__REF__']==pval_expansion_df[allele2_col_name]) & (ld_pruning_df['__ALT__']==pval_expansion_df[allele1_col_name]))), \
                    'left')
    else: ## Considering given top SNP list only, the case of allele columns may be absent
        return_df = pval_expansion_df\
                    .join(ld_pruning_df, \
                    (ld_pruning_df['__CHROM__']==pval_expansion_df[chr_col_name]) & \
                    (ld_pruning_df['__POS__']==pval_expansion_df[pos_col_name]), \
                    'left')
    
    ## Considering return_prune_in_df has SNPs with not only __SOURCE__ == 'prune.in' but __SOURCE__ == None generated by left join. To add those SNPs (__SOURCE__ == None) which are not running LD pruning correctly (i.e., only one SNP in a chromosome) or are absent in the 1000 Genomomes back to the pruned set for further analyses.
    return_prune_in_df = return_df.filter( (return_df['__SOURCE__'] == 'prune.in') | (return_df['__SOURCE__'].isNull()) ).select(pval_expansion_df.columns)
    
    return_prune_out_df = return_df.filter(return_df['__SOURCE__'] == 'prune.out').select(pval_expansion_df.columns)
    
    return_prune_in_df.count() ## Trigger workers and collect log then
    _coalesce_worker_log() ## Owing to time delay from workers, only coalesce worker logs at the last step
    
    return return_prune_in_df, return_prune_out_df
    
    
def run_ld_pruning(worker_index, rdd_list, ld_threshold, window_size, quiet, plink_app='plink', conf_path=''):
    return_list = list()
    for vcf_path_rdd in rdd_list: ## loop for each vcf
        vcf_dir = path.dirname(vcf_path_rdd.vcf_path)
        p = sp.Popen([plink_app, '--vcf', vcf_path_rdd.vcf_path, '--indep-pairwise', window_size, '1', str(ld_threshold), '--r2', '--threads', '1'], stdout=sp.PIPE, stderr=sp.PIPE, cwd=vcf_dir)
        stdout, stderr = p.communicate()
        
        ## Silence message from PLINK 
        if not quiet:
            # sys.stdout.flush()
            # sys.stderr.write("%s\n" % stderr)
            # sys.stderr.flush()
            stdout = stdout.decode('ascii')
            if stdout != '':
                _print_log_message("[Worker %d] PLINK stdout:\n%s" % (worker_index, stdout), 'info', conf_path=conf_path)
            stderr = stderr.decode('ascii')
            if stderr != '':
                _print_log_message("[Worker %d] PLINK stderr:\n%s" % (worker_index, stderr), 'error', conf_path=conf_path)
            
        return_list.append(vcf_path_rdd)
        
    return return_list
    
    
def collect_pruning_result(worker_index, rdd_list, conf_path=''):
    prune_result_list = list()
    
    def _get_annotated_pruning_result(prune_file_path, vcf_path, source):
        ## Load plink.prune.in/out file as pandas df
        prune_file_df = pd.read_csv(prune_file_path, names=['ID'])
        
        ## Load PLINK input vcf as pandas df
        vcf_df = _read_vcf(vcf_path)
        ## Merge vcf into pruned SNPs
        merge_df = pd.merge(prune_file_df, vcf_df, how='left', on='ID')
        merge_df['SOURCE'] = source
        merge_df = merge_df[['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', 'SOURCE']]
        
        return merge_df.values.tolist()
    
    for vcf_rdd in rdd_list: ## loop for each vcf
        ## Load plink.prune.in and .out of PLINK output
        vcf_dir = path.dirname(vcf_rdd.vcf_path)
        prune_in_path = path.join(vcf_dir, 'plink.prune.in')
        prune_out_path = path.join(vcf_dir, 'plink.prune.out')
        
        if path.exists(prune_in_path):
            prune_result_list.extend(_get_annotated_pruning_result(prune_in_path, vcf_rdd.vcf_path, 'prune.in'))
        else:
            _print_log_message("[Worker %d] Cannot find plink.prune.in in %s, please check plink.log for more details" % (worker_index, vcf_dir), 'warn', conf_path=conf_path)
            
        if path.exists(prune_out_path):
            prune_result_list.extend(_get_annotated_pruning_result(prune_out_path, vcf_rdd.vcf_path, 'prune.out'))
        
        
    return prune_result_list


def ld_expansion(ld_pruning_df, vcf_path_df, gwas_df=None, chr_col_name=None, pos_col_name=None, af_col_name=None, allele1_col_name=None, allele2_col_name=None, ld_threshold=0.7, window_size=500000, quiet=False, plink_app='plink', conf_path=''):
    schema = StructType([StructField(chr_col_name, StringType(),False),
                         StructField('pos_start', IntegerType(), False),
                         StructField(pos_col_name, IntegerType(), False),
                         StructField('snp_id', StringType(), False),
                         StructField(allele1_col_name, StringType(), False),
                         StructField(allele2_col_name, StringType(), False),
                         StructField('maf_from_LD_expansion', DoubleType(), True),
                         StructField('top_snp_id', StringType(), False),
                         StructField('top_snp_maf', DoubleType(), True),
                         StructField('r2', DoubleType(), True),
                         StructField('dprime', DoubleType(), True)])

    ld_expansion_df = vcf_path_df\
    .select('snp_id', 'vcf_path')\
    .distinct()\
    .rdd\
    .mapPartitionsWithIndex(lambda i, x: run_ld_expansion(i, x, ld_threshold, window_size, quiet, plink_app=plink_app, conf_path=conf_path))\
    .mapPartitionsWithIndex(lambda i, x: collect_ld_expansion_result(i, x, conf_path=conf_path))\
    .toDF(schema)\
    .cache()
    
    ## The GWAS summary stats file is not given
    if gwas_df is None: gwas_df = ld_pruning_df.select('*').drop('region_name')
       
    ld_expansion_df_original_header = ld_expansion_df.columns
    ld_expansion_df = ld_expansion_df\
                      .join(ld_pruning_df.select(ld_pruning_df['snp_id'].alias('top_snp_id'), 'region_pos', 'region_name'), 'top_snp_id', 'left')
    
    ## Add '__GWAS__' prefix into all columns of gwas_df
    for col_name in gwas_df.columns:
        gwas_df = gwas_df.withColumnRenamed(col_name, '__GWAS__'+col_name)
    
    total_pval_expansion_count = ld_pruning_df.count()
    allele1_count = ld_pruning_df.filter(ld_pruning_df[allele1_col_name].isNotNull()).count()
    allele2_count = ld_pruning_df.filter(ld_pruning_df[allele2_col_name].isNotNull()).count()    
    
    ## Get column information back from GWAS
    if (allele1_col_name is not None) and (allele2_col_name is not None) and (total_pval_expansion_count == allele1_count == allele2_count):
        joined_df = ld_expansion_df.join(gwas_df, \
                                         (ld_expansion_df[chr_col_name] == gwas_df['__GWAS__'+chr_col_name]) & \
                                         (ld_expansion_df[pos_col_name] == gwas_df['__GWAS__'+pos_col_name]) & \
                                         (((ld_expansion_df[allele1_col_name] == gwas_df['__GWAS__'+allele1_col_name]) & \
                                           (ld_expansion_df[allele2_col_name] == gwas_df['__GWAS__'+allele2_col_name])) | \
                                          ((ld_expansion_df[allele1_col_name] == gwas_df['__GWAS__'+allele2_col_name]) & \
                                           (ld_expansion_df[allele2_col_name] == gwas_df['__GWAS__'+allele1_col_name]))), \
                                        'left')\
                                   .select(ld_expansion_df.columns + gwas_df.columns[gwas_df.columns.index('__GWAS__region_pos')+1:])
    else: ## Considering given top SNP list only, the case of allele columns may be absent
        joined_df = ld_expansion_df.join(gwas_df, \
                                         (ld_expansion_df[chr_col_name] == gwas_df['__GWAS__'+chr_col_name]) & \
                                         (ld_expansion_df[pos_col_name] == gwas_df['__GWAS__'+pos_col_name]), \
                                        'left')\
                                   .select(ld_expansion_df.columns + gwas_df.columns[gwas_df.columns.index('__GWAS__region_pos')+1:])
                               
    for col_name in gwas_df.columns:
        joined_df = joined_df.withColumnRenamed(col_name, col_name.replace('__GWAS__', ''))
       
    return_df = joined_df.select(ld_pruning_df.columns + list(ld_expansion_df_original_header[-5:])) ## ld_expansion_df_original_header[-5:] means that starts from the maf_from_LD_expansion column
    
    return_df.count() ## Trigger workers and collect log then
    _coalesce_worker_log() ## Owing to time delay from workers, only coalesce worker logs at the last step    
    
    return return_df
    
                    
def run_ld_expansion(worker_index, rdd_list, ld_threshold, window_size, quiet, plink_app='plink', conf_path=''):
    return_list = list()
    for vcf_path_rdd in rdd_list: ## loop for each vcf
        vcf_dir = path.dirname(vcf_path_rdd.vcf_path)
        vcf_file_name = path.splitext(path.basename(vcf_path_rdd.vcf_path))[0]
        p = sp.Popen([plink_app, '--allow-no-sex', '--vcf', vcf_path_rdd.vcf_path, '--r2', 'with-freqs', 'dprime', '--ld-snp', vcf_path_rdd.snp_id, '--ld-window', '99999', '--ld-window-kb', str(window_size), '--ld-window-r2', str(ld_threshold), '--out', vcf_file_name, '--threads', '1'], stdout=sp.PIPE, stderr=sp.PIPE, cwd=vcf_dir)
        stdout, stderr = p.communicate()
        
        ## Silence message from PLINK 
        if not quiet:
            # sys.stdout.flush()
            # sys.stderr.write("%s\n" % stderr)
            # sys.stderr.flush()
            stdout = stdout.decode('ascii')
            if stdout != '':
                _print_log_message("[Worker %d] PLINK stdout:\n%s" % (worker_index, stdout), 'info', conf_path=conf_path)
            stderr = stderr.decode('ascii')
            if stderr != '':
                _print_log_message("[Worker %d] PLINK stderr:\n%s" % (worker_index, stderr), 'error', conf_path=conf_path)
        
        return_list.append(vcf_path_rdd)
        
    return return_list
    
    
def collect_ld_expansion_result(worker_index, rdd_list, conf_path=''):
    return_list = list()
    for vcf_path_rdd in rdd_list: ## loop for each vcf
        ## Load plink.ld of PLINK output
        vcf_dir = path.dirname(vcf_path_rdd.vcf_path)
        vcf_file_name = path.splitext(path.basename(vcf_path_rdd.vcf_path))[0]
        plink_ld = path.join(vcf_dir, "%s.ld" % vcf_file_name)
        
        if not path.exists(plink_ld):
            independent_signal = vcf_file_name.split('.')[0]
            _print_log_message("[Worker %d] Cannot find %s in %s due to no valid variants, please check %s.log for more details. Directly add the independent signal (%s) into the LD expansion set.\n" % (worker_index, plink_ld, vcf_dir, vcf_file_name, re.sub(r'(.+)_(.+)_(\w)_(\w)', r'\1:\2:\3>\4', independent_signal)), 'warn', conf_path=conf_path)
            
            tmp_str = independent_signal.split('_')
            chr = tmp_str[0]
            chrStartEnd = tmp_str[1].split('-')
            start = int(chrStartEnd[0])
            end = int(chrStartEnd[1])
            ref = tmp_str[2]
            alt = tmp_str[3]
            snp_id = "{}:{}:{}>{}".format(chr, tmp_str[1], ref, alt)
            return_list.append([chr, start, end, snp_id, ref, alt, None, snp_id, None, None, None])
        
        else:
            ## The schema of PLINK LD file:
            ## ['CHR_A', 'BP_A', 'SNP_A', 'MAF_A', 'CHR_B', 'BP_B', 'SNP_B', 'MAF_B', 'R2', 'DP']
            plink_ld_df = pd.read_csv(plink_ld, delim_whitespace=True, header=0)
            if plink_ld_df.shape[0] == 0: continue ## Skip empty file
            
            try:
                plink_ld_df[['chr', 'pos', 'alleles']] = plink_ld_df['SNP_B'].str.split(':', expand=True)
                plink_ld_df[['ref', 'alt']] = plink_ld_df['alleles'].str.split('>', expand=True)
            except ValueError:
                plink_ld_df[['chr', 'pos']] = plink_ld_df['SNP_B'].str.split(':', expand=True)
                plink_ld_df['ref'] = plink_ld_df['alt'] = ''
                
            plink_ld_df[['start', 'end']] = plink_ld_df['pos'].str.split('-', expand=True).astype('int64')
            plink_ld_df[['MAF_A', 'MAF_B', 'R2', 'DP']] = plink_ld_df[['MAF_A', 'MAF_B', 'R2', 'DP']].astype('float64')

            return_list.extend(plink_ld_df[['chr', 'start', 'end', 'SNP_B', 'ref', 'alt', 'MAF_B', 'SNP_A', 'MAF_A', 'R2', 'DP']].values.tolist())
            
    return return_list
    

def preprocessing_summary_stat(based_file_path, comparison_file_list, output_filename, temp_dir=gettempdir()):
    if len(comparison_file_list) > 0:
        spark = this.spark
        if not path.exists(based_file_path):
            _print_log_message(based_file_path + ': No such file or directory', 'error')
            sys.exit(1)
        
        based_file_df = spark.read.format('csv').option('header', 'true').option('delimiter', "\t").load(based_file_path).select('region_name')
        
        ## Join the template table with output TSV files in each pre-processing step
        for step_name, file_path in comparison_file_list:
            if not path.exists(file_path): continue
            col_name = "Number of {}".format(step_name)
            
            comparison_file_df = spark.read.format('csv').option('header', 'true').option('delimiter', "\t").load(file_path).groupBy('region_name').agg(F.count('snp_id').alias(col_name))
            
            based_file_df = based_file_df.join(comparison_file_df, 'region_name', 'left_outer')
        
        based_file_df = based_file_df.na.fill(dict([(c, 0) for c in based_file_df.columns[1:]]))
        # based_file_df.show(based_file_df.count(), truncate=False)
        save_df(based_file_df, output_filename, temp_dir=temp_dir)
    
        return output_filename
    else:
        return None


def save_df(df, output_filename, temp_dir=gettempdir()):
    output_dir = path.dirname(output_filename)
    output_tmp_dir = path.join(output_dir, 'output_tmp')
    df.write.format('csv').option('sep', "\t").mode('overwrite').save(output_tmp_dir, header=True)
    inferno.coalesce_and_sort_saving_files(output_tmp_dir, output_filename, 'csv', remove_part_file_dir=True, temp_dir=temp_dir)
    
    
def save_df_as_bed(df, output_filename, af_col_name=None, rsid_col_name=None, allele1_col_name=None, allele2_col_name=None, pval_col_name=None, temp_dir=gettempdir()):
    ## The format of BED file for INFERNO2:
    ## chr  chr_start  chr_end  rsID  MAF  snp_id  ref  alt  pval  region_pos  region_name  attribute

    output_dir = path.dirname(output_filename)
    output_tmp_dir = path.join(output_dir, 'output_bed_tmp')
    
    ## 'chr', 'chrStart', 'chrEnd'
    selected_header = list(df.columns[:3])
    ## lookup needed columns
    if rsid_col_name in df.columns:
        selected_header.append(rsid_col_name)
    else:
        df = df.withColumn('__rsID_not_found__', F.lit(None).cast('string'))
        selected_header.append('__rsID_not_found__')
        
    if af_col_name in df.columns:
        selected_header.append(af_col_name)
    else:
        df = df.withColumn('__maf_not_found__', F.lit(None).cast('string'))
        selected_header.append('__maf_not_found__')
    
    selected_header.extend(['snp_id', allele1_col_name, allele2_col_name])
    
    if pval_col_name in df.columns:
        selected_header.append(pval_col_name)
    else:
        df = df.withColumn('__pval_not_found__', F.lit(None).cast('string'))
        selected_header.append('__pval_not_found__')
    
    selected_header.extend(['region_pos', 'region_name'])
    
    if 'dprime' in df.columns: ## Represent results include LD expansion
        df = df.withColumn('__attribute__', \
        F.when(F.col('maf_from_LD_expansion').isNotNull(), \
               F.concat(F.lit('MAF_from_LD_expansion='), F.col('maf_from_LD_expansion'), F.lit(';TOP_SNP_ID='), F.col('top_snp_id'), F.lit(';TOP_SNP_MAF='), F.col('top_snp_maf'), F.lit(';R2='), F.col('r2'), F.lit(';Dprime='), F.col('dprime')))\
        .otherwise(F.concat(F.lit('MAF_from_LD_expansion=NA;TOP_SNP_ID='), F.col('top_snp_id'), F.lit(';TOP_SNP_MAF=NA;R2=NA;Dprime=NA'))))
        selected_header.append('__attribute__')
        
        ## To prevent redundant SNPs which show due to LD expansion by different LD pruned SNPs (i.e., multiple Top_snp_id values show in one expanded SNP), collect as a list in the attribute column using groupby('snp_id')
        exprs = [F.first(x).alias(x) for x in selected_header[:-1] if x != 'snp_id']
        exprs.append(F.collect_list(selected_header[-1]).cast('string').alias(selected_header[-1]))
        ## Additionally apply order by region_name in order to make the result which has the identical expanded SNPs as well as r2 values from different independent signals can be reproduced
        df = df.orderBy(['r2', 'region_name'], ascending=[False, True]).groupBy('snp_id').agg(*exprs)
        
    else: ## Save in an intermediate step
        df = df.withColumn('__attribute__', F.lit(None).cast('string'))
        selected_header.append('__attribute__')
        
        exprs = [F.first(x).alias(x) for x in selected_header if x != 'snp_id']
        df = df.orderBy(['region_pos', 'region_name']).groupBy('snp_id').agg(*exprs)
    
    df.select(selected_header).write.format('csv').option('sep', "\t").mode('overwrite').save(output_tmp_dir, header=True, nullValue=None)
    inferno.coalesce_and_sort_saving_files(output_tmp_dir, output_filename, 'csv', header=False, remove_part_file_dir=True, temp_dir=temp_dir)


def reset_region_pos(df, replace_with_col):
    if replace_with_col in df.columns:
        return df.withColumn('region_pos', F.col(replace_with_col))
    else: return df
    
    
def _read_vcf(path):
    lines = []
    with open(path, 'r') as fp:
        for line in fp:
            if line.startswith('#'): continue
            line = line.strip().split("\t")[:8]
            if len(line[0]) < 3: line[0] = 'chr' + line[0]
            line[1] = int(line[1])
            lines.append(line)
    
    return pd.DataFrame(lines, columns=['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO'])


def _mkdirs(p):
    if not path.exists(p):
        try:
            makedirs(p)
        except OSError as e:
            if e.errno != errno.EEXIST: raise
            pass


def _generate_tmp_log_path(conf_path=''):
    global tmp_log_path
    
    ## Load configure file
    if conf_path != '' and path.exists(conf_path):
        conf.read_config_file(conf_path)
    
    if tmp_log_path == '':
        from uuid import uuid4 ## Random UUID generator

        tmp_log_path = path.join(conf.TMP_PATH, str(uuid4())+'.tmp.log')
        open(path.join(conf.TMP_PATH, 'log_list.tmp.log'), 'a').write(tmp_log_path + "\n")
            
            
def _print_log_message(msg, level='info', conf_path=''): ## conf_path propagates configure file path from main script for processing in a worker
    msg = str(msg).rstrip()
    global tmp_log_path
    global worker_logger
    
    try:
        logger_method = eval("this.logger.%s" % level)
    except AttributeError:
        if worker_logger is None:
            ## Load configure file
            if conf_path != '' and path.exists(conf_path):
                conf.read_config_file(conf_path)
            
            ## Assign logger to temporarily save messages from workers
            _generate_tmp_log_path(conf_path=conf_path)
            this_script_name = path.splitext(path.basename(__file__))[0]
            worker_logger = logging.getLogger(this_script_name)
            file_handler = logging.FileHandler(tmp_log_path)
            file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
            worker_logger.addHandler(file_handler)
        
        logger_method = eval("worker_logger.%s" % level)
    
    logger_method(msg)
    

def _coalesce_worker_log():
    log_list_path = path.join(conf.TMP_PATH, 'log_list.tmp.log')
    
    ## Read the tmp log paths line by line
    def _get_tmp_log_path():
        with open(log_list_path, 'r') as fp:
            for line in fp: yield line.rstrip()
    
    try:    
        ## False if the --skip_logging option is given from frontend script
        is_logger_for_workers_existing_handlers = True if len(this.logger_for_workers.handlers) > 0 else False
    except AttributeError:
        ## No logger_for_workers is assign in the frontend script
        is_logger_for_workers_existing_handlers = False
    finally:
        if path.exists(log_list_path):
            if is_logger_for_workers_existing_handlers:
                from shutil import copyfileobj
                
                ## Get the log file name from this.logger_for_workers
                handler = this.logger_for_workers.handlers[0]
                target_filename = handler.baseFilename
                
                ## Coalesce tmp logs into the frontend script log
                t_fp = open(target_filename, 'a')
                for line in _get_tmp_log_path():
                    try:
                        f_fp = open(line, 'r')
                        copyfileobj(f_fp, t_fp)
                        f_fp.close
                        remove(line)
                    except:
                        continue
                t_fp.close()
                
            else:  ## Directly remove tmp log files because --skip_logging option is given
                for line in _get_tmp_log_path():
                    remove(line)
                    
            remove(log_list_path)
            