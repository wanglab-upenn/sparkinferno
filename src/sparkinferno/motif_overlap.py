# -*- coding: utf-8 -*-
import pyspark
from pyspark.sql.types import *
import sys
import math
from decimal import Decimal, ROUND_HALF_UP ## To get a precise float and change to round half up (The round() function uses round half even as default)
import __main__
from os import path, remove
import sparkinferno.inferno
import sparkinferno.config as conf
import logging
import datetime
from tempfile import gettempdir


this = sys.modules[__main__.__name__]
tmp_log_path = ''
worker_logger = None

def motif_overlap(bed_df, pwm_dict, conf_path=''):
    # print("Running get_pwm_score.")
    pwm_schema = [StructField('snp_pos', IntegerType(), True),
                  StructField('relative_snp_pos', IntegerType(), True),
                  StructField('ref_pwm', StringType(), True),
                  StructField('alt_pwm', StringType(), True),
                  StructField('delta_pwm', StringType(), True),
                  StructField('rounded_delta_pwm', StringType(), True),
                  StructField('ref_prob', StringType(), True),
                  StructField('alt_prob', StringType(), True)]
    result_df = bed_df\
    .rdd.map(lambda x: _get_pwm_score(x, pwm_dict.value, conf_path=conf_path))\
    .toDF(StructType(bed_df.schema.fields + pwm_schema))\
    .persist(pyspark.StorageLevel.DISK_ONLY)
    
    result_df.count()
    _coalesce_worker_log()
    # result_df.show()
    # result_df.write.format('csv').option("delimiter", "\t").mode("overwrite").save(reportpath, header='true')
    
    return(result_df)
    
    
def convert_pwm2dict(motif_pwm_file):
    pwm_dict = {}
    with open(motif_pwm_file, 'r') as motif_pwms:
        ## store the TF and PWM that we build up
        this_tf = ""
        this_pwm = []
        for line in motif_pwms:
            line_data = line.strip().split("\t")
            if line[0]==">":
                ## if we have been building up a PWM, store it
                if this_tf:
                    if this_tf not in pwm_dict:
                        ## store it in a list so that we can add others
                        pwm_dict[this_tf] = [this_pwm]
                    else:
                        # print("TF %s found multiple times in PWM data!" % (this_tf))
                        # print("Previous length: %d, current length: %d" % (len(pwm_dict[this_tf][0]), len(this_pwm)))
                        pwm_dict[this_tf].append(this_pwm)
                ## reset the tracking variables
                this_tf = line_data[1].split("/")[0].upper()
                this_pwm = []
            else:
                ## add this PWM info to the list
                this_pwm.append([float(x) for x in line_data])
                
        ## store the last PWM
        if this_tf not in pwm_dict:
            pwm_dict[this_tf] = [this_pwm]
        else:
            pwm_dict[this_tf].append(this_pwm)
        
    ## The final structure of the returned pwm_dict would look like as:
    ## {'FOXA1(FORKHEAD)': [[0.483, 0.026, 0.01, 0.481],
    ##                       ...
    ##                      [0.997, 0.001, 0.001, 0.001]],
    ##                     [[0.498, 0.005, 0.001, 0.496],
    ##                       ...
    ##                      [0.997, 0.001, 0.001, 0.001]],
    ##       'ZNF711(ZF)': [[0.588, 0.22, 0.13, 0.063],
    ##                       ...
    ##                      [0.094, 0.171, 0.64, 0.095]]}
    return(pwm_dict)


## Require fields of an input row: 'chr','chrStart','chrEnd','absPath','snp_rsID','ref','alt','motif_chr','motif_start','motif_end','motif_name','log_odds_score','motif_strand','motif_seq'
def _get_pwm_score(row, pwm_dict, conf_path=''):   
    ## Keep the input fields for returning the final result
    # row_list = inferno.spark.sparkContext.parallelize(row).collect()
    row_list = [v for v in row]
    
    ## Create a dict for taking the complement of bases
    comp_dict = {"A":"T", "T":"A", "C":"G", "G":"C"}
    this_motif_name = str(row.motif_name).upper()
    snp_pos = int(row.chrEnd) ## The input bed file should be 0-base; snp_pos is 1-base based on dbSNP
    chrStart = int(row.chrStart)
    motif_start = int(row.motif_start)
    motif_end = int(row.motif_end)
    motif_strand = str(row.motif_strand)
    motif_seq = str(row.motif_seq)
    rsID = str(row.rsID)
    snp_ref = str(row.ref).upper() if motif_strand == '+' else comp_dict[str(row.ref).upper()]
    snp_alt = str(row.alt).upper() if motif_strand == '+' else comp_dict[str(row.alt).upper()]
    
    if this_motif_name in pwm_dict:            
        ## To figure out the SNP position in a motif (using 1-base)
        if motif_strand == '+':
            relative_pos = snp_pos-(motif_start+1)+1
        else:
            relative_pos = motif_end-snp_pos+1
        
        ## If a motif has more than 2 PWM tables, we needs to figure out which one is correct and picking up by its length. If both motif name and length are identical, picking up the first one temporarily.
        if len(pwm_dict[this_motif_name])>1:
            pwm_found = False
            for pwm in pwm_dict[this_motif_name]:
                if len(pwm) == len(motif_seq):
                    this_pwm = pwm
                    pwm_found = True
                    break
            if not pwm_found:
                _print_log_message("No PWM matching length %d found for TF %s" % (len(motif_seq), this_motif_name), 'error', conf_path=conf_path)
                
        else:
            this_pwm = pwm_dict[this_motif_name][0]
        
        ## now calculate the PWM log-odds scores for reference and alternate
        ref_score = 0
        alt_score = 0
        ## we also want to store the reference and allele probabilities
        ref_prob = '-1'
        alt_prob = '-1'
        # create a convenience dict for mapping bases to PWM indices
        pwm_map = {"A":0, "C":1, "G":2, "T":3}
        
        for seq_base_pos, seq_base in enumerate(motif_seq):
            ## for now, just use an equiprobable distribution
            try:
                ref_score += Decimal(math.log(Decimal(str(this_pwm[seq_base_pos][pwm_map[seq_base]]))/Decimal('0.25')))
            except:
                _print_log_message("List index out of range, seq_base_pos=%s, seq_base=%s for SNP %s and TF %s" % (str(seq_base_pos), seq_base, rsID, this_motif_name), 'warn', conf_path=conf_path)
                continue
                
            ## for debugging purposes, check whether our major allele matches
            if seq_base_pos+1==relative_pos:
                ## store the reference probability
                ref_prob = str(this_pwm[seq_base_pos][pwm_map[seq_base]])
                    
                ## store the alternate probability
                alt_prob = str(this_pwm[seq_base_pos][pwm_map[snp_alt]])
                
                ## now update the alternate sequence score
                alt_score += Decimal(math.log(Decimal(str(this_pwm[seq_base_pos][pwm_map[snp_alt]]))/Decimal('0.25')))
            else:
                alt_score += Decimal(math.log(Decimal(str(this_pwm[seq_base_pos][pwm_map[seq_base]]))/Decimal('0.25')))
                
        delta_pwm = Decimal(math.log(Decimal(str(alt_prob))/Decimal(str(ref_prob))))
        ## The row composition
        ## ['chr','chrStart','chrEnd','absPath','snp_rsID','ref','alt','motif_chr','motif_start','motif_end','motif_name','log_odds_score','motif_strand','motif_seq','snp_pos','relative_snp_pos','ref_pwm','alt_pwm','delta_pwm','rounded_delta_pwm','ref_prob','alt_prob']
        return(row_list+[snp_pos, relative_pos, '{:.11f}'.format(ref_score), '{:.11f}'.format(alt_score), '{:.11f}'.format(delta_pwm), str(delta_pwm.quantize(Decimal('0.00'), rounding=ROUND_HALF_UP)), ref_prob, alt_prob])
    else:
        return(row_list+[snp_pos, None, None, None, None, None, None, None])


def _generate_tmp_log_path(conf_path=''):
    global tmp_log_path
    
    ## Load configure file
    if conf_path != '' and path.exists(conf_path):
        conf.read_config_file(conf_path)
    
    if tmp_log_path == '':
        from uuid import uuid4 ## Random UUID generator

        tmp_log_path = path.join(conf.TMP_PATH, str(uuid4())+'.tmp.log')
        open(path.join(conf.TMP_PATH, 'log_list.tmp.log'), 'a').write(tmp_log_path + "\n")
            
            
def _print_log_message(msg, level='info', conf_path=''): ## conf_path propagates configure file path from main script for processing in a worker
    msg = str(msg).rstrip()
    global tmp_log_path
    global worker_logger
    
    try:
        logger_method = eval("this.logger.%s" % level)
    except AttributeError:
        if worker_logger is None:
            ## Load configure file
            if conf_path != '' and path.exists(conf_path):
                conf.read_config_file(conf_path)
                
            ## Assign logger to temporarily save messages from workers
            _generate_tmp_log_path(conf_path=conf_path)
            this_script_name = path.splitext(path.basename(__file__))[0]
            worker_logger = logging.getLogger(this_script_name)
            file_handler = logging.FileHandler(tmp_log_path)
            file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
            worker_logger.addHandler(file_handler)
        
        logger_method = eval("worker_logger.%s" % level)
    
    logger_method(msg)
    

def _coalesce_worker_log():
    log_list_path = path.join(conf.TMP_PATH, 'log_list.tmp.log')
    
    ## Read the tmp log paths line by line
    def _get_tmp_log_path():
        with open(log_list_path, 'r') as fp:
            for line in fp: yield line.rstrip()
    
    try:    
        ## False if the --skip_logging option is given from frontend script
        is_logger_for_workers_existing_handlers = True if len(this.logger_for_workers.handlers) > 0 else False
    except AttributeError:
        ## No logger_for_workers is assign in the frontend script
        is_logger_for_workers_existing_handlers = False
    finally:
        if path.exists(log_list_path):
            if is_logger_for_workers_existing_handlers:
                from shutil import copyfileobj
                
                ## Get the log file name from this.logger_for_workers
                handler = this.logger_for_workers.handlers[0]
                target_filename = handler.baseFilename

                ## Coalesce tmp logs into the frontend script log
                t_fp = open(target_filename, 'a')
                for line in _get_tmp_log_path():
                    try:
                        f_fp = open(line, 'r')
                        copyfileobj(f_fp, t_fp)
                        f_fp.close
                        remove(line)
                    except:
                        continue
                t_fp.close()
                
            else:  ## Directly remove tmp log files because --skip_logging option is given
                for line in _get_tmp_log_path():
                    remove(line)
                    
            remove(log_list_path)
            