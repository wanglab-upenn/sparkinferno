# -*- coding: utf-8 -*-
from pyspark.sql.types import *
from pyspark.sql import functions as F
from pyspark.sql import Row, Window
from pyspark.rdd import RDD
from sparkinferno.inferno import expand_columns_from_string, load_bedfile_as_rdd
import sys
from os import path
import scipy.stats as stats
from numpy.random import uniform
from decimal import Decimal, ROUND_HALF_UP
from random import shuffle
import __main__
import pandas as pd
import sparkinferno.gig as gig
from tempfile import gettempdir


this = sys.modules[__main__.__name__]

def interval_replication(bed_df, n=0):
    n_to_array = F.udf(lambda x: list(range(1, x+1)), ArrayType(IntegerType()))
    replicated_df = bed_df.withColumn('frequency', F.explode(n_to_array(F.lit(n))))
    # replicated_df = bed_df.select('*', F.explode(n_to_array(F.lit(n))).alias('frequency')) ## equal to above line
    
    return replicated_df
    

def shuffle_coordinate(bed_df, genome_build='chr19'):
    chr19_chr_len_dict = {'chr1': 249250621, 'chr2': 243199373, 'chr3': 198022430, 'chr4': 191154276, 'chr5': 180915260, 'chr6': 171115067, 'chr7': 159138663, 'chr8': 146364022, 'chr9': 141213431, 'chr10': 135534747, 'chr11': 135006516, 'chr12': 133851895, 'chr13': 115169878, 'chr14': 107349540, 'chr15': 102531392, 'chr16': 90354753, 'chr17': 81195210, 'chr18': 78077248, 'chr19': 59128983, 'chr20': 63025520, 'chr21': 48129895, 'chr22': 51304566, 'chrX': 155270560, 'chrY': 59373566}
    chr38_chr_len_dict = {'chr1': 248956422, 'chr2': 242193529, 'chr3': 198295559, 'chr4': 190214555, 'chr5': 181538259, 'chr6': 170805979, 'chr7': 159345973, 'chr8': 145138636, 'chr9': 138394717, 'chr10': 133797422, 'chr11': 135086622, 'chr12': 133275309, 'chr13': 114364328, 'chr14': 107043718, 'chr15': 101991189, 'chr16': 90338345, 'chr17': 83257441, 'chr18': 80373285, 'chr19': 58617616, 'chr20': 64444167, 'chr21': 46709983, 'chr22': 50818468, 'chrX': 156040895, 'chrY': 57227415}
    
    if genome_build == 'hg19': chr_len_dict = chr19_chr_len_dict
    elif genome_build == 'hg38': chr_len_dict = chr38_chr_len_dict
    elif genome_build == 'hg38-lifted': chr_len_dict = chr38_chr_len_dict
    else:
        sys.stderr.write(genome_build + ': Unknown genome build\n')
        sys.exit(1)

    
    shuffled_interval_df = bed_df.groupBy('frequency', 'chr').agg(F.count('chr').alias('size'), F.collect_list('interval_len').alias('interval_len_list')).rdd.mapPartitions(lambda x: _get_rand_uniform(x, chr_len_dict)).toDF(['frequency', 'chr', 'chrStart', 'chrEnd'])
    
    return shuffled_interval_df
    
    
def _get_rand_uniform(rdd_list, chr_len_dict):
    return_list = list()
    for x in rdd_list:
        frequency = x[0]
        chr = x[1]
        size = x[2]
        interval_len_list = x[3]
        
        ## Generate random start position using uniform distribution, using precise round
        randStart = [int(Decimal(str(l)).quantize(Decimal('0'), rounding=ROUND_HALF_UP)) for l in uniform(low=0, high=chr_len_dict[chr], size=size)]
        randStart_len = len(randStart)
        ## Shuffle interval orders with the same length of intervals
        shuffle(interval_len_list)
        ## According to the length of intervals to apply end position 
        randEnd = [v + interval_len_list[i] for i,v in enumerate(randStart)]
        
        return_list.extend([list(l) for l in zip([frequency]*randStart_len, [chr]*randStart_len, randStart, randEnd)])
    return return_list


def get_simulated_overlaps(bed_df, annotate_df, replicate_frequency=1, window_size=0, temp_dir=gettempdir(), giggle_app='giggle', bgzip_app='bgzip'):
        spark = this.spark
        sc = this.sc
        
        @F.udf(StringType())
        def _get_dir(p): return path.dirname(p)
        
        bedDir_list = annotate_df.withColumn('bedDir', _get_dir(F.col('bedPath'))).select('bedDir').distinct().collect()
        genome_build = annotate_df.select('Genome build').collect()[0]['Genome build']
        
        schema = StructType([StructField('chr', StringType(),False),
                             StructField('chrStart', IntegerType(), False),
                             StructField('chrEnd', IntegerType(), False),
                             StructField('additionalQueriedString', StringType(), False),
                             StructField('hitString', StringType(), False),
                             StructField('bedPath', StringType(), False)])
        union_giggledf = spark.createDataFrame(sc.emptyRDD(), schema)
        
        replicated_df = interval_replication(bed_df, replicate_frequency)
        # print("replicated_df= %d"%replicated_df.count())
        shuffle_coordinate_df = shuffle_coordinate(replicated_df, genome_build)
        # print("shuffle_coordinate_df= %d"%shuffle_coordinate_df.count())
        for giggle_index_path in bedDir_list:
            giggle_index_path = path.join(giggle_index_path['bedDir'].replace('giggle_index', ''), 'giggle_index')
            
            giggledf = shuffle_coordinate_df.withColumn('coordinate', F.concat_ws("\t", F.col('chr'), F.col('chrStart'), F.col('chrEnd'))).groupBy('frequency').agg(F.collect_list('coordinate').alias('coordinate_list')).rdd.mapPartitions(lambda x: gig.Gig_index_count_partitions(x, giggle_index_path, window_size=window_size, temp_dir=temp_dir, giggle_app=giggle_app, bgzip_app=bgzip_app)).toDF(schema)
            
            union_giggledf = union_giggledf.union(giggledf)
            
        overlaps_count_df = expand_columns_from_string(union_giggledf, ['additionalQueriedString', 'hitString'], [['frequency'], [None, 'n11']], ["\t", "\t"]).drop('chr', 'chrStart', 'chrEnd', 'additionalQueriedString', 'hitString')
        # print("overlaps_count_df= %d"%overlaps_count_df.count())
        
        return overlaps_count_df


def empirical_distribution_function(observed_df, simulated_pval_df, groupby_col_name='bedPath'):
    needed_col_from_observed_df = ['bedPath', 'Observed overlapping interval']
    
    if isinstance(groupby_col_name, list):
        needed_col_from_observed_df.extend(groupby_col_name)
        simulated_pval_df = simulated_pval_df.join(observed_df.select(list(set(needed_col_from_observed_df))), 'bedPath', 'left_outer')
        
        if 'bedPath' not in groupby_col_name:
            simulated_pval_df = simulated_pval_df.drop('bedPath')
        
        wp = Window.partitionBy(groupby_col_name)
        wp_freq = Window.partitionBy(groupby_col_name + ['frequency']) ## frequency records the ith time of simulation
        wp_sum_n11_per_freq = Window.partitionBy(groupby_col_name + ['Sum_n11_per_freq']) ## n11 means the # of overlaps in simulated results
        
    else: ## groupby_col_name is a string
        needed_col_from_observed_df.append(groupby_col_name)
        simulated_pval_df = simulated_pval_df.join(observed_df.select(list(set(needed_col_from_observed_df))), 'bedPath', 'left_outer')
        
        if groupby_col_name != 'bedPath':
            simulated_pval_df = simulated_pval_df.drop('bedPath')
        
        wp = Window.partitionBy(groupby_col_name)
        wp_freq = Window.partitionBy(groupby_col_name, 'frequency')
        wp_sum_n11_per_freq = Window.partitionBy(groupby_col_name, 'Sum_n11_per_freq')
    
    """
    Line 138: Processing the case of the same "groupby_col_name" values are present in multiple tracks per simulation
    Line 139-140: Keep one record per "groupby_col_name" value per simulation
    Line 141-142: Compute average and standard deviation per "groupby_col_name" value
    Line 143: Group by the "Sum_n11_per_freq" values per "groupby_col_name" value to get the overlap occurrence in all simulations
    Line 144-145: Remove duplicate records and keep unique "occurrence" values per "groupby_col_name" value
    Line 146: "total_occurrence" equal to total simulation times
    Line 147: Count how many times of simulations that their overlaps are less than or equal to the observed overlaps
    Line 148: Count how many times of simulations that their overlaps are greater than the observed overlaps
    Line 149-151: Compute empirical p-values
    """
    simulated_pval_df = simulated_pval_df\
    .withColumn('Sum_n11_per_freq', F.sum(F.col('n11')).over(wp_freq))\
    .drop('n11')\
    .distinct()\
    .withColumn('Average simulated overlaps', F.avg(F.col('Sum_n11_per_freq')).over(wp))\
    .withColumn('Standard deviation of simulated overlaps', F.stddev(F.col('Sum_n11_per_freq')).over(wp))\
    .withColumn('occurrence', F.count(F.lit(1)).over(wp_sum_n11_per_freq))\
    .drop('frequency')\
    .distinct()\
    .withColumn('total_occurrence', F.sum('occurrence').over(wp))\
    .withColumn('left_occurrence', F.when(F.col('Sum_n11_per_freq') <= F.col('Observed overlapping interval'), F.col('occurrence')).otherwise(0))\
    .withColumn('right_occurrence', F.when(F.col('Sum_n11_per_freq') > F.col('Observed overlapping interval'), F.col('occurrence')).otherwise(0))\
    .withColumn('Empirical p-value (left)', F.sum('left_occurrence').over(wp)/F.col('total_occurrence'))\
    .withColumn('Empirical p-value (right)', F.sum('right_occurrence').over(wp)/F.col('total_occurrence'))\
    .withColumn('Empirical p-value (two-tail)', F.least('Empirical p-value (left)', 'Empirical p-value (right)')*2)
    
    if isinstance(groupby_col_name, list):
        selected_cols = groupby_col_name + ['Average simulated overlaps', 'Standard deviation of simulated overlaps', 'Empirical p-value (left)', 'Empirical p-value (right)', 'Empirical p-value (two-tail)']
    else:
        selected_cols = [groupby_col_name] + ['Average simulated overlaps', 'Standard deviation of simulated overlaps', 'Empirical p-value (left)', 'Empirical p-value (right)', 'Empirical p-value (two-tail)']
    
    simulated_pval_df = simulated_pval_df\
                        .select(selected_cols)\
                        .distinct()
        
    return observed_df.join(simulated_pval_df, groupby_col_name, 'left_outer')
                            

def empirical_pval_enrichment(bedfile, annotate_df, groupby_col_name='bedPath', replicate_frequency=1, window_size=0, temp_dir=gettempdir(), giggle_app='giggle', bgzip_app='bgzip'):
    ## Consider groupby_col_name is a list
    if isinstance(groupby_col_name, list):
        for col_name in groupby_col_name:
            if col_name not in annotate_df.columns:
                sys.stderr.write(groupby_col_name+': Column name not found\n')
                sys.exit(1)
    else:
        if groupby_col_name not in annotate_df.columns:
            sys.stderr.write(groupby_col_name+': Column name not found\n')
            sys.exit(1)
    
    if not isinstance(bedfile, RDD):
        bedfile = load_bedfile_as_rdd(bedfile)
    
    bed_df = bedfile\
             .map(lambda y: Row(chr=y[0],
                                chrStart=int(y[1]),
                                chrEnd=int(y[2])))\
             .toDF()\
             .distinct()\
             .withColumn('interval_len', F.lit(F.col('chrEnd')-F.col('chrStart')))
    
    does_run_group_summary_annotateGADB = False
    for needed_col in ['Observed overlapping interval', 'Total number of intervals from GADB data source', 'Total covered bases of GADB data source']:
        if needed_col not in annotate_df.columns:
            summary_df = group_summary_annotateGADB(annotate_df, groupby_col_name)
            does_run_group_summary_annotateGADB = True
            break ## Running once is enough

    if not does_run_group_summary_annotateGADB:
        summary_df = annotate_df.select('*')
    
    ## Simulate overlaps
    simulated_overlaps_count_df = get_simulated_overlaps(bed_df, annotate_df, replicate_frequency=replicate_frequency, window_size=window_size, temp_dir=temp_dir, giggle_app=giggle_app, bgzip_app=bgzip_app)
    
    # simulated_overlaps_count_df = simulated_overlaps_count_df.join(summary_df, 'bedPath')
    # simulated_overlaps_count_df.show(truncate=False)
    ## Apply EDF
    return empirical_distribution_function(summary_df, simulated_overlaps_count_df, groupby_col_name=groupby_col_name)
    

def fisher_exact_test(n11, n12, n21, n22):
    ## alternative = ['two-sided', 'less', 'greater']
    oddsratio, pvalue_left = stats.fisher_exact([[n11, n12], [n21, n22]], alternative='less')
    oddsratio, pvalue_right = stats.fisher_exact([[n11, n12], [n21, n22]], alternative='greater')
    oddsratio, pvalue_two_tail = stats.fisher_exact([[n11, n12], [n21, n22]], alternative='two-sided')
    
    return [float(pvalue_left), float(pvalue_right), float(pvalue_two_tail), float(oddsratio)]
    
    
def run_fisher_exact_test(df):
    for i in ['n11', 'n12', 'n21', 'n22']:
        if i not in df.columns:
            sys.stderr.write(i+': Column name not found\n')
            sys.exit(1)
            
    fisher_exact_test_udf = F.udf(fisher_exact_test, ArrayType(DoubleType()))
    return_df = df.withColumn('pval_odds', fisher_exact_test_udf(F.col('n11'), F.col('n12'), F.col('n21'), F.col('n22')))\
                  .withColumn('p-value (left)', F.col('pval_odds').getItem(0))\
                  .withColumn('p-value (right)', F.col('pval_odds').getItem(1))\
                  .withColumn('p-value (two-tail)', F.col('pval_odds').getItem(2))\
                  .withColumn('odds ratio', F.col('pval_odds').getItem(3))\
                  .drop('pval_odds')
                  
    return return_df
    
    
def fisher_exact_enrichment(bedfile, annotate_df, groupby_col_name='bedPath', N=0):
    """
    Fisher's exact test
    +------------------+------------------------+------------------------+---------+
    |                  |Data source intervals(+)|Data source intervals(-)|Row total|
    +------------------+------------------------+------------------------+---------+
    |Input intervals(+)|           n11          |          n12           |    N1   |
    +------------------+------------------------+------------------------+---------+
    |Input intervals(-)|           n21          |          n22           |   N-N1  |
    +------------------+------------------------+------------------------+---------+
    |      Column total|            N2          |         N-N2           |    N    |
    +------------------+------------------------+------------------------+---------+
    (+) = overlapped, (-) = non-overlapped
    Human genome size = 3,088,269,832 bp ## hg38 from chr1-22, X and Y
      x = Average length of intervals from input = sum(chrEnd - chrStart) / Number of intervals
      y = Average length of intervals from GADB data source = Total covered bases of GADB data source / N2
      N = Number of possible intervals in human genome = Human genome size / (N1 + N2)
     N1 = Total number of input intervals
     N2 = Total number of intervals from GADB data source
    n11 = Observed overlapping interval
    n12 = Non-overlapping interval = N1 - n11
    n21 = Non-overlapped interval from GADB data source = N2 - n11
    n22 = Number of intervals neither in input nor in GADB data source = N - (n11 + n12 + n21)
    """
    
    ## Consider groupby_col_name is a list
    if isinstance(groupby_col_name, list):
        for col_name in groupby_col_name:
            if col_name not in annotate_df.columns:
                sys.stderr.write(groupby_col_name+': Column name not found\n')
                sys.exit(1)
    else:
        if groupby_col_name not in annotate_df.columns:
            sys.stderr.write(groupby_col_name+': Column name not found\n')
            sys.exit(1)
            
    if not isinstance(N, int):
        sys.stderr.write('The N argument MUST be the integer type\n')
        sys.exit(1)
    
    HUMAN_GENOME_SIZE = 3088269832
    
    if not isinstance(bedfile, RDD):
        bedfile = load_bedfile_as_rdd(bedfile)
    
    bed_df = bedfile\
             .map(lambda y: Row(chr=y[0],
                                chrStart=int(y[1]),
                                chrEnd=int(y[2])))\
             .toDF()\
             .distinct()\
             .withColumn('interval_len', F.lit(F.col('chrEnd')-F.col('chrStart')))

    bed_input_intervals = bed_df.count()
    bed_mean_interval_size = bed_df.select(F.sum('interval_len').alias('interval_len')).collect()[0]['interval_len']/bed_input_intervals
    
    does_run_group_summary_annotateGADB = False
    for needed_col in ['Observed overlapping interval', 'Total number of intervals from GADB data source', 'Total covered bases of GADB data source']:
        if needed_col not in annotate_df.columns:
            summary_df = group_summary_annotateGADB(annotate_df, groupby_col_name)
            does_run_group_summary_annotateGADB = True
            break ## Running once is enough
    
    if not does_run_group_summary_annotateGADB:
        summary_df = annotate_df.select('*')
    
    summary_df = summary_df.withColumnRenamed('Distinctly observed overlapping interval', 'n11')\
                           .withColumnRenamed('Total number of intervals from GADB data source', 'N2')\
                           .withColumn('N1', F.lit(bed_input_intervals).cast('int'))\
                           .withColumn('n12', (F.col('N1')-F.col('n11')).cast('int'))\
                           .withColumn('n21', (F.col('N2')-F.col('n11')).cast('int'))\
                           .withColumn('x', F.lit(bed_mean_interval_size).cast('double'))\
                           .withColumn('y', (F.col('Total covered bases of GADB data source')/F.col('N2')).cast('double'))
                           
    if N == 0:
        ## Consider possible N by estimation
        summary_df = summary_df.withColumn('N', F.greatest(F.col('n11'), F.col('n12'), F.col('n21'), F.lit(HUMAN_GENOME_SIZE)/(F.col('x')+F.col('y'))).cast('int'))
    else:
        ## Use user provided N 
        summary_df = summary_df.withColumn('N', F.greatest(F.col('n11'), F.col('n12'), F.col('n21'), F.lit(N)).cast('int'))
    
    summary_df = summary_df.withColumn('n22', F.greatest(F.lit(0), F.col('N')-F.col('n11')-F.col('n12')-F.col('n21')).cast('int'))
    
    ## Apply Fisher's exact test
    summary_df = run_fisher_exact_test(summary_df)
    
    ## Rearrangement of the column names
    summary_df_headers = list(summary_df.columns[:summary_df.columns.index('n11')]) + ['n11', 'n12', 'N1', 'n21', 'n22', 'N2', 'N', 'x', 'y', 'Total covered bases of GADB data source', 'p-value (left)', 'p-value (right)', 'p-value (two-tail)', 'odds ratio']
    summary_df = summary_df\
    .select(summary_df_headers)\
    .withColumnRenamed('n11', 'Distinctly observed overlapping interval')\
    .withColumnRenamed('n12', 'Non-overlapping interval')\
    .withColumnRenamed('N1', 'Total number of input intervals')\
    .withColumnRenamed('n21', 'Non-overlapped interval from GADB data source')\
    .withColumnRenamed('n22', 'Number of intervals neither in input nor in GADB data source')\
    .withColumnRenamed('N2', 'Total number of intervals from GADB data source')\
    .withColumnRenamed('N', 'Number of possible intervals in human genome')\
    .withColumnRenamed('x', 'Average length of intervals from input')\
    .withColumnRenamed('y', 'Average length of intervals from GADB data source')
    
    return summary_df


def group_summary_annotateGADB(annotate_df, groupby_col_name):
    wp = Window.partitionBy(groupby_col_name)

    summary_df = annotate_df\
    .withColumn('Interval', F.concat_ws(' ', 'chr', 'chrStart', 'chrEnd'))\
    .withColumn('Distinctly observed overlapping interval', F.size(F.collect_set('Interval').over(wp)))\
    .withColumn('Observed overlapping interval', F.count('Interval').over(wp))\
    .withColumn('Number of intervals', F.sum('Number of intervals').over(wp))\
    .withColumn('bp covered', F.sum('bp covered').over(wp))\
    .select(list(set([groupby_col_name, 'bedPath'])) + ['Distinctly observed overlapping interval', 'Observed overlapping interval', F.col('Number of intervals').alias('Total number of intervals from GADB data source'), F.col('bp covered').alias('Total covered bases of GADB data source'), 'Genome build'])\
    .distinct()

    return summary_df