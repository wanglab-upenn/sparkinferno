# -*- coding: utf-8 -*-
import pyspark
from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark.sql import functions as F
import sparkinferno.inferno as inferno
from sparkinferno.tabix import tabix
import sparkinferno.config as conf
from os import path, makedirs, remove, _exit
import sys
import pandas as pd
import __main__
import logging
import datetime
from tempfile import gettempdir
import subprocess as sp
import feather
from collections import defaultdict
from uuid import uuid4
import gzip
import pickle


this = sys.modules[__main__.__name__]
tmp_log_path = ''
worker_logger = None


def init_parameters(output_base_dir='', gwas_sample_size=0, expansion_type='LD_block', ld_block_window_size=500000, expansion_window_size=500000, gwas_snp_id_col_pos=4, gwas_ref_col_pos=5, gwas_alt_col_pos=6, gwas_pval_col_pos=8, gwas_maf_col_pos=9, gwas_beta_col_pos=10, gwas_se_col_pos=11, gwas_rsID_col_pos=12, analysis_type='quant', gwas_sample_proportion=0.0, coloc_h4_cutoff=0.5, coloc_abf_cutoff=0.5, coloc_nsnps_cutoff=5, genome_build='hg19', gtex_version='v7', gzip=False, temp_dir=gettempdir(), gadb_meta_file='', ensg2symbol_file=path.join(path.dirname(__file__), '..', 'data', 'ensembl_id_to_gene_symbol.txt'), gtex_sample_size_file=path.join(path.dirname(__file__), '..', 'data', 'GTEx_*_sample_sizes.tsv'), prefix='', giggle_app='giggle', tabix_app='tabix', bgzip_app='bgzip', plink_app='plink', rscript_app='Rscript', config_file=''):
    ## Process necessary arguments as a dictionary for functions

    ## Column numbers of paras use 1-based
    paras = {'output_base_dir': output_base_dir, 
             'expansion_type': expansion_type, ## It can be set as "LD_block" or "constant." Use LD block size as the length for expansion or a constant length (i.e., expansion_window_size = 500kb by default).
             'ld_block_window_size': ld_block_window_size, ## Window size for expansion in the LD_block mode
             'expansion_window_size': expansion_window_size, ## Window size for expansion in the constant mode
             'analysis_type': analysis_type, ## the type of data in a dataset either "quant" or "cc" to denote quantitative or case-control, respectively
             'gwas_sample_size': gwas_sample_size,
             'gwas_sample_proportion': gwas_sample_proportion,
             'coloc_h4_cutoff': coloc_h4_cutoff,
             'coloc_abf_cutoff': coloc_abf_cutoff,
             'coloc_nsnps_cutoff': coloc_nsnps_cutoff,
             'genome_build': genome_build,
             'gtex_version': gtex_version,
             'gzip': gzip,
             'GWAS_bed_pos': {
                 'snp_id_col': gwas_snp_id_col_pos, ## The column position of snp_id in the input GWAS file
                 'allele1_col': gwas_ref_col_pos, ## The column position of allele1 in the input GWAS file
                 'allele2_col': gwas_alt_col_pos, ## The column position of allele2 in the input GWAS file
                 'pval_col': gwas_pval_col_pos, ## The column position of P-value in the input GWAS file
                 'maf_col': gwas_maf_col_pos, ## The column position of MAF in the input GWAS file
                 'beta_col': gwas_beta_col_pos, ## The column position of beta value in the input GWAS file
                 'se_col': gwas_se_col_pos, ## The column position of standard error in the input GWAS file
                 'rsID_col': gwas_rsID_col_pos, ## The column position of rsID in the input GWAS file
             },
             'temp_dir': temp_dir, 
             'gadb_meta_file': gadb_meta_file, 
             'ensg2symbol_file': ensg2symbol_file, 
             'gtex_sample_size_file': gtex_sample_size_file,
             'prefix': prefix, 
             'giggle_app': giggle_app, 
             'tabix_app': tabix_app, 
             'bgzip_app': bgzip_app, 
             'plink_app': plink_app, 
             'rscript_app': rscript_app, 
             'config_file': config_file, 
            }
                
    return paras


def load_inferno_bed(input_bed):
    ## The header of BED file for INFERNO2
    bed_header = ['chr', 'chr_start', 'chr_end', 'rsID', 'MAF', 'snp_id', 'ref', 'alt', 'pval', 'region_pos', 'region_name', 'attribute']
    top_snp_df = inferno.load_bedfile_as_rdd(input_bed, header_list=bed_header).toDF()
    
    return top_snp_df


def find_region_pos_coordinate(top_snp_df, paras):
    top_snp_df = top_snp_df.groupby('region_pos', 'region_name')\
                             .agg(F.min('chr_start').alias('ld_block_start'), \
                                  F.max('chr_end').alias('ld_block_end'))

    region_pos_cols = F.split(top_snp_df['region_pos'], ':') ## 0:'chr1', 1:'207679306-207679307', 2:'A>G'
    region_pos_pos = F.split(region_pos_cols.getItem(1), '-') ## 0: '207679306', 1:'207679307'
    
    ## Return position from region_pos
    region_pos_coordinate_df = top_snp_df\
                               .withColumn('chr', region_pos_cols.getItem(0))\
                               .withColumn('start', region_pos_pos.getItem(0).cast('int'))\
                               .withColumn('end', region_pos_pos.getItem(1).cast('int'))\
                               .select('chr', 'start', 'end', 'region_pos', 'region_name', 'ld_block_start', 'ld_block_end')
    
    if paras['expansion_type'].lower() == 'constant':
        region_pos_coordinate_df = region_pos_coordinate_df\
                                   .withColumn('ld_block_start', F.col('start')-paras['expansion_window_size'])\
                                   .withColumn('ld_block_end', F.col('end')+paras['expansion_window_size'])
    
    return region_pos_coordinate_df


def get_gwas_overlap(region_pos_coordinate_df, giggle_index, paras):    
    ## Expand SNPs in GWAS using internal size of tag regions
    gwas_overlaps_df = inferno.loadgiggle(region_pos_coordinate_df.select('chr', 'ld_block_start', 'ld_block_end').rdd, giggle_index, method='cmd_partitions', temp_dir=paras['temp_dir'], giggle_app=paras['giggle_app'], bgzip_app=paras['bgzip_app'])
    
    ## Get and sort the column order of GWAS BED
    hitString_schema_paras = sorted(paras['GWAS_bed_pos'].items(), key=lambda v: v[1])
    hitString_schema_idx = 4
    hitString_schema_list = ['snp_chr', 'snp_start', 'snp_end']
    empty_cols = []
    for idx, val in hitString_schema_paras:
        if val == -1:
            empty_cols.append(idx)
            continue
            
        if val != hitString_schema_idx:
            hitString_schema_list.extend([None]*(val-hitString_schema_idx))
            
        hitString_schema_list.append(idx)
        hitString_schema_idx = val+1
    # print(hitString_schema_list)
    
    gwas_overlaps_df = inferno.expand_columns_from_string(gwas_overlaps_df, ['hitString'], [hitString_schema_list], ["\t"])
    # gwas_overlaps_df.show()
    for empty_col in empty_cols:
        gwas_overlaps_df = gwas_overlaps_df.withColumn(empty_col, F.lit(''))
    
    selected_header = ['snp_chr', 'snp_start', 'snp_end', 'allele1_col', 'allele2_col', 'rsID_col', 'maf_col', 'pval_col', 'beta_col', 'se_col', 'snp_id_col']
    gwas_overlaps_df = gwas_overlaps_df.select(selected_header).distinct().persist(pyspark.StorageLevel.DISK_ONLY)
    gwas_overlaps_count = gwas_overlaps_df.select('snp_chr', 'snp_start', 'snp_end', 'allele1_col', 'allele2_col').distinct().count()
    gwas_overlaps_list = gwas_overlaps_df.collect()
    
    # return_dict = {}
    # for snp in gwas_overlaps_list:
        # return_dict[snp[-1]] = list(map(lambda x: x, snp))
        
    # return return_dict
    
    ## Function codes from Fancy John, https://stackoverflow.com/questions/29348345/declaring-a-multi-dimensional-dictionary-in-python
    def nested_dict(n, type):
        if n == 1:
            return defaultdict(type)
        else:
            return defaultdict(lambda: nested_dict(n-1, type))
    
    ## The output structure of return_nested_dict becomes:
    ## {'chr8': {'chr8:27898579-27898580:T>C': ['chr8', '27898579', '27898580', 'T', 'C', 'rs17058502', '0.1213', '0.4074', '0.0202', '0.1234', 'chr8:27898579-27898580:T>C'], ...}, 
    ##  ...}
    return_nested_dict = nested_dict(2, list)
    for snp in gwas_overlaps_list:
        return_nested_dict[snp[0]][snp[-1]] = list(map(lambda x: x, snp))
    # print(return_nested_dict)
    
    gwas_overlaps_df.unpersist(blocking=True)
    
    return_dict_pickle_paths = {}
    for chr, snp_dict in return_nested_dict.items():
        pickle_path = path.join(paras['temp_dir'], "%s_dict.pickle" % chr)
        pickle.dump(return_nested_dict[chr], open(pickle_path, 'wb'))
        return_dict_pickle_paths[chr] = pickle_path
    
    return return_dict_pickle_paths, gwas_overlaps_count
    
    
def get_eqtl_gene_id(region_pos_coordinate_df, paras):
    ## In this function, using each tag region to derive corresponding tissues and genes from eQTL
    region_pos_additionalQueriedString_schema = ('region_pos', 'region_name', 'ld_block_start', 'ld_block_end')
    eqtl_hitString_schema = (None, None, None, None, None, 'gene_id', None, None, None, None)
    
    ## Load GADB dataframe
    gadb_df = inferno.loadgadb(paras['gadb_meta_file'])
    ## Specify the GTEx tracks in GADB
    gtex_datafilter = "Data Source:GTEx_%s,Output type:eQTL_all_associations" % paras['gtex_version']
    gtex_tracks_df = inferno.select(gadb_df, gtex_datafilter).cache()
    df1 = inferno.expand_columns_from_string(inferno.loadgiggle(region_pos_coordinate_df.rdd, gtex_tracks_df, method='cmd_partitions', temp_dir=paras['temp_dir'], giggle_app=paras['giggle_app'], bgzip_app=paras['bgzip_app']), ['additionalQueriedString', 'hitString'], [region_pos_additionalQueriedString_schema, eqtl_hitString_schema], ["\t", "\t"]).select('chr', 'chrStart', 'chrEnd', 'absPath', 'region_pos', 'region_name', 'ld_block_start', 'ld_block_end', 'gene_id').distinct().withColumnRenamed('chrStart', 'start').withColumnRenamed('chrEnd', 'end')
    
    region_pos_coordinate_in_eqtl_df = df1\
    .join(gtex_tracks_df.select('absPath', 'cell type', 'Tissue category'), 'absPath')\
    .withColumnRenamed('cell type', 'tissue')\
    .withColumnRenamed('Tissue category', 'tissue_category')\
    .select('chr', 'start', 'end', 'region_pos', 'region_name', 'ld_block_start', 'ld_block_end', 'tissue', 'tissue_category', 'gene_id', 'absPath')\
    
    gtex_tracks_df.unpersist(blocking=True)
    
    return region_pos_coordinate_in_eqtl_df


def get_coloc_input_paths(top_tissue_gene_df, dict_pickle_paths, paras):
    ## This function generates an RDD format list of input file paths for R COLOC package
    matchup_schema = StructType([\
                  StructField('snp_chr', StringType(),True),\
                  StructField('snp_start', StringType(),True),\
                  StructField('snp_end', StringType(),True),\
                  StructField('GTEx_ref', StringType(),True),\
                  StructField('GTEx_alt', StringType(),True),\
                  StructField('region_pos', StringType(),True),\
                  StructField('region_name', StringType(),True),\
                  StructField('ld_block_start', StringType(),True),\
                  StructField('ld_block_end', StringType(),True),\
                  StructField('tissue', StringType(),True),\
                  StructField('tissue_category', StringType(),True),\
                  StructField('gene_id', StringType(),True),\
                  StructField('tss_distance', StringType(),True),\
                  StructField('pval_nominal', StringType(),True),\
                  StructField('slope', StringType(),True),\
                  StructField('slope_se', StringType(),True),\
                  StructField('GTEx_minor_allele_count', StringType(),True),\
                  StructField('GTEx_maf', StringType(),True),\
                  StructField('gene_symbol', StringType(),True),\
                  StructField('GWAS_ref', StringType(),True),\
                  StructField('GWAS_alt', StringType(),True),\
                  StructField('rsID', StringType(),True),\
                  StructField('maf', StringType(),True),\
                  StructField('pval', StringType(),True),\
                  StructField('beta', StringType(),True),\
                  StructField('se', StringType(),True),\
                  StructField('snp_id', StringType(),True)])
    
    eqtl_gwas_matchup_df = top_tissue_gene_df\
                           .groupby('region_name', 'tissue')\
                           .agg(F.first('chr').alias('chr'), \
                                F.first('start').alias('start'), \
                                F.first('end').alias('end'), \
                                F.first('region_pos').alias('region_pos'), \
                                F.first('ld_block_start').alias('ld_block_start'), \
                                F.first('ld_block_end').alias('ld_block_end'), \
                                F.first('tissue_category').alias('tissue_category'), \
                                F.collect_list('gene_id').alias('gene_id_list'), \
                                F.first('absPath').alias('absPath'))\
                           .rdd\
                           .flatMap(lambda x: _eqtl_gwas_matchup(x, dict_pickle_paths[x.chr], paras))\
                           .toDF(matchup_schema)\
                           .persist(pyspark.StorageLevel.DISK_ONLY)
    
    exprs = [F.collect_list(x).alias(x) for x in eqtl_gwas_matchup_df.columns if x not in ['region_name', 'region_pos', 'ld_block_start', 'ld_block_end', 'tissue', 'tissue_category', 'gene_symbol', 'gene_id']]
    exprs.extend([F.first('ld_block_start').alias('ld_block_start'), F.first('ld_block_end').alias('ld_block_end')])
    coloc_input_paths = eqtl_gwas_matchup_df.groupby('region_name', 'region_pos', 'tissue', 'tissue_category', 'gene_symbol', 'gene_id')\
                        .agg(*exprs)\
                        .filter(F.size(F.col('snp_chr')) >= paras['coloc_nsnps_cutoff'])\
                        .rdd\
                        .flatMap(lambda x: _get_coloc_input_paths(x, paras))
    
    eqtl_gwas_matchup_df.unpersist(blocking=True)
    
    return coloc_input_paths


def _eqtl_gwas_matchup(row_groupby_tag_tissue_rdd, gwas_dict_path, paras):
    tabix_input = "{}\t{}\t{}".format(row_groupby_tag_tissue_rdd.chr, row_groupby_tag_tissue_rdd.ld_block_start, row_groupby_tag_tissue_rdd.ld_block_end)
    
    ## Load GWAS dictionary from Pickle. Using dict() to convert type from collections.defaultdict to dict, in order to capture KeyError exception below.
    gwas_dict = dict(pickle.load(open(gwas_dict_path, 'rb')))
    
    ## Map Ensembl ID to gene symbol
    ensg2gene_file = paras['ensg2symbol_file'].replace('*', paras['genome_build'])
    ensg2gene_dict = pd.read_csv(ensg2gene_file, sep="\t", header=0, usecols=['Ensembl_gene_id', 'Gene_symbol']).set_index('Ensembl_gene_id')['Gene_symbol'].to_dict()
    
    eqtl_overlaps = tabix(tabix_input, row_groupby_tag_tissue_rdd.absPath, tabix_app=paras['tabix_app'])
    gwas_eqtl_merged_list = []
    for eqtl_overlap_str in eqtl_overlaps:
        eqtl_overlap_list = eqtl_overlap_str[4].split("\t")
        tmp_list = eqtl_overlap_list[:10]
        
        if len(eqtl_overlap_list) == 13:
            ## For GTEx v7 & v8 BED13 format, add GTEx_maf into tmp_list
            ## The GTEx hitString locates in eqtl_overlap_list = ['snp_chr', 'snp_start', 'snp_end', 'GTEx_ref', 'GTEx_alt', 'gene_id', 'tss_distance', 'pval_nominal', 'slope', 'slope_se', 'ma_samples', 'GTEx_minor_allele_count', 'GTEx_maf']
            tmp_list.extend(eqtl_overlap_list[-2:])
        else:
            ## For GTEx v6p BED10 format, no maf in this version, keep blank
            ## The GTEx hitString locates in eqtl_overlap_list = ['snp_chr', 'snp_start', 'snp_end', 'GTEx_ref', 'GTEx_alt', 'gene_id', 'tss_distance', 'pval_nominal', 'slope', 'slope_se']
            tmp_list.extend(['', ''])
        
        # tmp_list[1] = int(tmp_list[1])
        # tmp_list[2] = int(tmp_list[2])
        tmp_list[3] = tmp_list[3].upper()
        tmp_list[4] = tmp_list[4].upper()
        # tmp_list[6] = int(tmp_list[6])
        # tmp_list[7] = float(tmp_list[7])
        # tmp_list[8] = float(tmp_list[8])
        # tmp_list[9] = float(tmp_list[9])
        try:
            ## The gene symbol is converted by ensg2gene_dict and additionally appended into the gene_symbol column
            tmp_list.append(ensg2gene_dict[tmp_list[5].split('.')[0]])
        except KeyError:
            tmp_list.append('')
        
        ## Filter out GTEx overlaps whose genes don't belong to a tag SNP
        if tmp_list[5] not in row_groupby_tag_tissue_rdd.gene_id_list: continue
        
        ## Merge GWAS and eQTL by the keys of coordinates between GWAS and eQTL tables, and add 8 GWAS columns ('GWAS_ref', 'GWAS_alt', 'rsID', 'maf', 'pval', 'beta', 'se', 'snp_id') into tmp_list behind 'gene_symbol'
        dict_key = "%s:%s-%s:%s>%s" % (tmp_list[0], tmp_list[1], tmp_list[2], tmp_list[3], tmp_list[4])
        try:
            gwas_subtable = gwas_dict[dict_key][-8:]
            tmp_list.extend(gwas_subtable)
        except KeyError:
            dict_key = "%s:%s-%s:%s>%s" % (tmp_list[0], tmp_list[1], tmp_list[2], tmp_list[4], tmp_list[3])
            try:
                gwas_subtable = gwas_dict[dict_key][-8:]
                tmp_list.extend(gwas_subtable)
            except KeyError:
                continue
        
        ## Add the tag snp and tissue information back
        ## gwas_eqtl_merged_list = ['snp_chr', 'snp_start', 'snp_end', 'GTEx_ref', 'GTEx_alt', 'region_pos', 'region_name', 'ld_block_start', 'ld_block_end', 'tissue', 'tissue_category', 'gene_id', 'tss_distance', 'pval_nominal', 'slope', 'slope_se', 'GTEx_minor_allele_count', 'GTEx_maf', 'gene_symbol', 'GWAS_ref', 'GWAS_alt', 'rsID', 'maf', 'pval', 'beta', 'se', 'snp_id']
        gwas_eqtl_merged_list.append(tmp_list[:5]+[row_groupby_tag_tissue_rdd.region_pos, row_groupby_tag_tissue_rdd.region_name, row_groupby_tag_tissue_rdd.ld_block_start, row_groupby_tag_tissue_rdd.ld_block_end, row_groupby_tag_tissue_rdd.tissue, row_groupby_tag_tissue_rdd.tissue_category]+tmp_list[5:])
    
    return gwas_eqtl_merged_list


def _get_coloc_input_paths(make_input_rdd, paras):    
    ## Generate the input files for colocalization analysis from input RDD grouped by region_pos, tissue, and gene_id
    schema = ['snp_chr', 'snp_start', 'snp_end', 'GTEx_ref', 'GTEx_alt', 'region_pos', 'region_name', 'ld_block_start', 'ld_block_end', 'tissue', 'tissue_category', 'gene_id', 'tss_distance', 'pval_nominal', 'slope', 'slope_se', 'GTEx_minor_allele_count', 'GTEx_maf', 'gene_symbol', 'GWAS_ref', 'GWAS_alt', 'rsID', 'maf', 'pval', 'beta', 'se', 'snp_id']
    
    save_dir = path.join(paras['output_base_dir'], 'coloc_input', make_input_rdd['region_name'].replace('/', '_'), make_input_rdd['tissue'].replace(' ','_'))
    if not _is_path_exist(save_dir, False):
        try:
            makedirs(save_dir)
        except:
            pass
            
    if paras['gzip']:
        save_path = path.join(save_dir, make_input_rdd['gene_id']+'.tsv.gz')
        fp = gzip.open(save_path, 'wb')
        
        w_header = "\t".join(schema) + "\n"
        fp.write(w_header.encode())
        
        for i in range(0, len(make_input_rdd.snp_chr)):
            w_content = "\t".join([make_input_rdd[k][i] if k not in ['region_name', 'region_pos', 'ld_block_start', 'ld_block_end', 'tissue', 'tissue_category', 'gene_symbol', 'gene_id'] else make_input_rdd[k] for k in schema]) + "\n"
            fp.write(w_content.encode())
        
    else:
        save_path = path.join(save_dir, make_input_rdd['gene_id']+'.tsv')
        fp = open(save_path, 'w')
        
        w_header = "\t".join(schema) + "\n"
        fp.write(w_header)
        
        for i in range(0, len(make_input_rdd.snp_chr)):
            w_content = "\t".join([make_input_rdd[k][i] if k not in ['region_name', 'region_pos', 'ld_block_start', 'ld_block_end', 'tissue', 'tissue_category', 'gene_symbol', 'gene_id'] else make_input_rdd[k] for k in schema]) + "\n"
            fp.write(w_content)
    
    fp.close()
    return [save_path]


def coloc(path_list_rdd, paras):
    coloc_result_schema = StructType([\
                  StructField('region_name', StringType(),True),\
                  StructField('region_pos', StringType(),True),\
                  StructField('ld_block_start', StringType(),True),\
                  StructField('ld_block_end', StringType(),True),\
                  StructField('tissue', StringType(),True),\
                  StructField('gtex_tissue_class', StringType(),True),\
                  StructField('eqtl_gene_name', StringType(),True),\
                  StructField('eqtl_gene_id', StringType(),True),\
                  StructField('nsnps', IntegerType(),True),\
                  StructField('PP_H0_abf', DoubleType(),True),\
                  StructField('PP_H1_abf', DoubleType(),True),\
                  StructField('PP_H2_abf', DoubleType(),True),\
                  StructField('PP_H3_abf', DoubleType(),True),\
                  StructField('PP_H4_abf', DoubleType(),True),\
                  StructField('result_table_path', StringType(),True)])
                  
    gtex_sample_size_file = paras['gtex_sample_size_file'].replace('*', paras['gtex_version'])
    gtex_file_size_dict = this.sc.broadcast(get_gtex_file_size(gtex_sample_size_file)).value
    
    coloc_summary_df = path_list_rdd.mapPartitionsWithIndex(lambda i, x: _run_coloc(i, x, gtex_file_size_dict, paras)).filter(lambda line: len(line)>0).toDF(coloc_result_schema).cache()
    
    coloc_summary_df.count()
    _coalesce_worker_log()
    
    return coloc_summary_df

    
def _run_coloc(worker_index, input_rdds, gtex_file_size_dict, paras):
    file_path_list_for_Rscript_input = path.join(paras['temp_dir'], str(uuid4()))
    file_path_list_fp = open(file_path_list_for_Rscript_input, 'w')
    input_path_list = list()
    is_using_beta_se = True if paras['GWAS_bed_pos']['beta_col']>0 and paras['GWAS_bed_pos']['se_col']>0 else False
    
    is_input_rdds_empty = True
    for input_rdd in input_rdds:
        if not _is_path_exist(input_rdd, True): _exit(1)
        
        tissue = pd.read_csv(input_rdd, sep="\t", nrows=1, engine='python').iloc[0]['tissue']
        input_path_list.append(input_rdd)
        file_path_list_fp.write("{}\t{}\n".format(input_rdd, gtex_file_size_dict[tissue]))
        is_input_rdds_empty = False
        
    file_path_list_fp.close()
    
    ## If input RDD is empty
    if is_input_rdds_empty:
        remove(file_path_list_for_Rscript_input)
        yield []
    else:
        ## Apply Rscript to run coloc package
        cmd = [str(x) for x in [paras['rscript_app'], 
                                path.join(path.dirname(path.abspath(__file__)), 'r_scripts', 'run_coloc_batch.R'), 
                                file_path_list_for_Rscript_input,
                                paras['gwas_sample_size'], 
                                paras['analysis_type'], 
                                paras['gwas_sample_proportion'], 
                                "TRUE" if is_using_beta_se else "FALSE"]]
        
        p = sp.Popen(cmd)
        p.wait()
        remove(file_path_list_for_Rscript_input)
        
        for input_rdd in input_path_list:
            ## The schema of input_df:
            ## 0:'snp_chr', 1:'snp_start', 2:'snp_end', 3:'GTEx_ref', 4:'GTEx_alt', 5:'region_pos', 6:'region_name', 7:'ld_block_start', 8:'ld_block_end', 9:'tissue', 10:'tissue_category', 11:'gene_id', 12:'tss_distance', 13:'pval_nominal', 14:'slope', 15:'slope_se', 16:'GTEx_minor_allele_count', 17: 'GTEx_maf', 18:'gene_symbol', 19:'GWAS_ref', 20:'GWAS_alt', 21:'rsID', 22:'maf', 23:'pval', 24:'beta', 25:'se', 26:'snp_id'
            input_df = pd.read_csv(input_rdd, sep="\t", engine='python')
            
            ## Needed columns for merging into summary_pandas_df
            region_pos = input_df.iloc[0]['region_pos']
            region_name = input_df.iloc[0]['region_name']
            tissue = input_df.iloc[0]['tissue']
            tissue_category = input_df.iloc[0]['tissue_category']
            gene_id = input_df.iloc[0]['gene_id']
            gene_symbol = input_df.iloc[0]['gene_symbol']
            ld_block_start = input_df.iloc[0]['ld_block_start']
            ld_block_end = input_df.iloc[0]['ld_block_end']

            input_dir = path.dirname(input_rdd)
            output_dir = input_dir.replace('coloc_input', 'coloc_output')
            
            if paras['gzip']:
                output_filename = path.join(output_dir, "%s.tsv.gz" % gene_id)
            else:
                output_filename = path.join(output_dir, "%s.tsv" % gene_id)
            
            if not _is_path_exist(output_dir, False):
                try:
                    makedirs(output_dir)
                except IOError as err:
                    _print_log_message("[Worker {}] {}".format(worker_index, err), 'error', conf_path=paras['config_file'])
                    _exit(1)
            
            ## Collect coloc output from feather files
            try:
                ## Read summary df
                feather_file_path = path.join(output_dir, gene_id+'.summary.feather')
                summary_pandas_df = feather.read_dataframe(feather_file_path, use_threads=False)
                remove(feather_file_path)
                ## Read results df
                feather_file_path = path.join(output_dir, gene_id+'.results.feather')
                results_pandas_df = feather.read_dataframe(feather_file_path, use_threads=False)
                remove(feather_file_path)
            except:
                _print_log_message("[Worker {}] Cannot read the file: {}".format(worker_index, feather_file_path), 'error', conf_path=paras['config_file'])
                _exit(1)
            
            results_row_num = results_pandas_df.shape[0] ## Get the number of rows in the dataframe
            summary_pandas_df[['nsnps']] = summary_pandas_df[['nsnps']].astype('int64')
            
            ## Add additional columns into summary_pandas_df
            input_groupby_headers = {'region_name': region_name, 'region_pos': region_pos, 'ld_block_start': ld_block_start, 'ld_block_end': ld_block_end, 'tissue': tissue, 'tissue_category': tissue_category, 'gene_symbol': gene_symbol, 'gene_id': gene_id}
            for k, v in input_groupby_headers.items():
                summary_pandas_df[k] = v
            summary_pandas_df['result_table_path'] = output_filename
                
            ## Merge summary_pandas_df into results_pandas_df
            results_pandas_df = pd.concat([results_pandas_df, pd.concat([summary_pandas_df]*results_row_num, ignore_index=True)], axis=1)
            
            if is_using_beta_se:
                ## The results_pandas_df from coloc output doesn't show 'MAF.df1', 'pvalues.df2', 'pvalues.df1', 'MAF.df2' when input beta and se values. Thus, adding these columns from the coloc input.
                additional_columns_df = input_df[['snp_chr', 'snp_start', 'snp_end', 'GTEx_ref', 'GTEx_alt', 'GWAS_ref', 'GWAS_alt', 'rsID', 'snp_id', 'maf', 'pval', 'GTEx_minor_allele_count', 'GTEx_maf']].fillna('NA').rename(columns={'snp_id':'snp', 'maf':'MAF.df1', 'pval':'pvalues.df1', 'GTEx_maf':'MAF.df2'})
            else:
                ## The R script inside always uses beta and se as the GTEx inputs, consequently, coloc output doesn't show 'MAF.df2'. Need to add it back here.
                additional_columns_df = input_df[['snp_chr', 'snp_start', 'snp_end', 'GTEx_ref', 'GTEx_alt', 'GWAS_ref', 'GWAS_alt', 'rsID', 'snp_id', 'GTEx_minor_allele_count', 'GTEx_maf']].fillna('NA').rename(columns={'snp_id': 'snp', 'GTEx_maf':'MAF.df2'})
            
            results_pandas_df = pd.merge(results_pandas_df, additional_columns_df, how='left', on='snp')
            
            ## Save full results of each SNPs into a file
            if paras['gzip']:
                results_pandas_df[['region_name','region_pos','ld_block_start','ld_block_end','tissue','tissue_category','gene_symbol','gene_id','snp','snp_chr','snp_start','snp_end', 'GTEx_ref', 'GTEx_alt', 'GWAS_ref', 'GWAS_alt','rsID','pvalues.df1','MAF.df1','V.df1','z.df1','r.df1','lABF.df1','GTEx_minor_allele_count','MAF.df2','V.df2','z.df2','r.df2','lABF.df2','internal.sum.lABF','SNP.PP.H4','nsnps','PP.H0.abf','PP.H1.abf','PP.H2.abf','PP.H3.abf','PP.H4.abf']].to_csv(output_filename, sep="\t", index=False, compression='gzip')
            else:
                results_pandas_df[['region_name','region_pos','ld_block_start','ld_block_end','tissue','tissue_category','gene_symbol','gene_id','snp','snp_chr','snp_start','snp_end', 'GTEx_ref', 'GTEx_alt', 'GWAS_ref', 'GWAS_alt','rsID','pvalues.df1','MAF.df1','V.df1','z.df1','r.df1','lABF.df1','GTEx_minor_allele_count','MAF.df2','V.df2','z.df2','r.df2','lABF.df2','internal.sum.lABF','SNP.PP.H4','nsnps','PP.H0.abf','PP.H1.abf','PP.H2.abf','PP.H3.abf','PP.H4.abf']].to_csv(output_filename, sep="\t", index=False)
                    
            ## Yield a coloc summary table
            return_list = summary_pandas_df[['region_name','region_pos','ld_block_start','ld_block_end','tissue','tissue_category','gene_symbol','gene_id','nsnps','PP.H0.abf','PP.H1.abf','PP.H2.abf','PP.H3.abf','PP.H4.abf','result_table_path']].values.tolist()
            
            yield return_list[0]


def summary_stats_from_files(file_name, reportpath, inferno_bed_path, paras):
    coloc_summary_df = inferno.loadColocSummary(file_name, reportpath)
    inferno_bed_df = load_inferno_bed(inferno_bed_path)
    
    summary_stats(coloc_summary_df, inferno_bed_df, paras)


def find_passed_h4_cutoff_combinations(coloc_summary_df, paras):
    return coloc_summary_df.filter(coloc_summary_df['`PP_H4_abf`'] > paras['coloc_h4_cutoff'])


def summary_stats(coloc_summary_df, inferno_bed_df, paras):
    ## Summarizing colocalized SNPs from each output
    top_snp_schema = ['chr', 'chr_start', 'chr_end', 'snp_id', 'region_pos', 'region_name']
    top_snp_list = pd.DataFrame(inferno_bed_df.select(top_snp_schema).collect(), columns=top_snp_schema)['snp_id'].unique().tolist()
    top_snp_list = this.sc.broadcast(top_snp_list).value
    
    ## Column names with ".df1" and ".df2" suffixes which return from _run_summary_stats() will be replaced with "_GWAS" and "_GTEx" here, respectively
    schema = StructType([StructField('region_name', StringType(),False),\
                         StructField('region_pos', StringType(),False),\
                         StructField('ld_block_start', StringType(),False),\
                         StructField('ld_block_end', StringType(),False),\
                         StructField('tissue', StringType(), False),\
                         StructField('tissue_category', StringType(), False),\
                         StructField('gene_symbol', StringType(), True),\
                         StructField('gene_id', StringType(), False),\
                         StructField('snp', StringType(), False),\
                         StructField('snp_chr', StringType(), False),\
                         StructField('snp_start', StringType(), False),\
                         StructField('snp_end', StringType(), False),\
                         StructField('ref_GWAS', StringType(), False),\
                         StructField('alt_GWAS', StringType(), False),\
                         StructField('ref_GTEx', StringType(), False),\
                         StructField('alt_GTEx', StringType(), False),\
                         StructField('rsID', StringType(), False),\
                         StructField('pvalues_GWAS', DoubleType(), False),\
                         StructField('NONREF_AF_GWAS', DoubleType(), False),\
                         StructField('V_GWAS', DoubleType(), False),\
                         StructField('z_GWAS', DoubleType(), False),\
                         StructField('r_GWAS', DoubleType(), False),\
                         StructField('lABF_GWAS', DoubleType(), False),\
                         StructField('minor_allele_count_GTEx', IntegerType(), True),\
                         StructField('NONREF_AF_GTEx', DoubleType(), True),\
                         StructField('V_GTEx', DoubleType(), False),\
                         StructField('z_GTEx', DoubleType(), False),\
                         StructField('r_GTEx', DoubleType(), False),\
                         StructField('lABF_GTEx', DoubleType(), False),\
                         StructField('internal_sum_lABF', DoubleType(), False),\
                         StructField('SNP_PP_H4', DoubleType(), False),\
                         StructField('nsnps', IntegerType(), False),\
                         StructField('PP_H0_abf', DoubleType(), False),\
                         StructField('PP_H1_abf', DoubleType(), False),\
                         StructField('PP_H2_abf', DoubleType(), False),\
                         StructField('PP_H3_abf', DoubleType(), False),\
                         StructField('PP_H4_abf', DoubleType(), False),\
                         StructField('num_prob_expanded_snp', IntegerType(), False),\
                         StructField('high_coloc_snp', StringType(), False),\
                         StructField('is_in_top_snp', IntegerType(), False),\
                         StructField('is_max_H4', IntegerType(), False)])

    stats_result_df = coloc_summary_df.rdd.flatMap(lambda x: _run_summary_stats(x, top_snp_list, paras)).toDF(schema)
    
    return stats_result_df


def _run_summary_stats(coloc_summary_sub_rdd, top_snp_list, paras):
    coloc_snp_df = pd.read_csv(coloc_summary_sub_rdd.result_table_path, sep="\t", engine='python', dtype={'pvalues.df1': 'float64', 'MAF.df1': 'float64', 'V.df1': 'float64', 'z.df1': 'float64', 'r.df1': 'float64', 'lABF.df1': 'float64', 'GTEx_minor_allele_count': 'int64', 'MAF.df2': 'float64', 'V.df2': 'float64', 'z.df2': 'float64', 'r.df2': 'float64', 'lABF.df2': 'float64', 'internal.sum.lABF': 'float64', 'SNP.PP.H4': 'float64', 'nsnps': 'int64', 'PP.H0.abf': 'float64', 'PP.H1.abf': 'float64', 'PP.H2.abf': 'float64', 'PP.H3.abf': 'float64', 'PP.H4.abf': 'float64'})
    
    ## Set default values as "0" for the two additional columns using for the purpose of distinguishing SNPs within the provided top SNPs as well as the maximum H4 ABF of SNPs for each gene
    coloc_snp_df['is_in_top_snp'] = coloc_snp_df['is_max_H4'] = 0
    
    ## Add snp_flip column to consider allele flip, and then assign "1" in the is_in_top_snp column if a SNP is in the top_snp_list.
    snp_id_cols = coloc_snp_df['snp'].str.split(':', expand=True) ## 0:'chr1', 1:'207679306-207679307', 2:'A>G'
    snp_id_alleles = snp_id_cols[2].str.split('>', expand=True) ## 0: A, 1:G
    coloc_snp_df['snp_flip'] = snp_id_cols[0]+':'+snp_id_cols[1]+':'+snp_id_alleles[1]+'>'+snp_id_alleles[0]
    
    coloc_snp_df.loc[coloc_snp_df['snp'].isin(top_snp_list), ['is_in_top_snp']] = 1
    coloc_snp_df.loc[coloc_snp_df['snp_flip'].isin(top_snp_list), ['is_in_top_snp']] = 1
    
    ## Assign "1" in the is_max_H4 column if a SNP has the largest value of H4 ABF
    coloc_snp_df.loc[coloc_snp_df['SNP.PP.H4'].values.argmax(), ['is_max_H4']]=1
    high_coloc_snp = coloc_snp_df.loc[coloc_snp_df['is_max_H4'] == 1, 'snp'].to_string(index=False).strip()
    
    ## Descending sort by the SNP.PP.H4 column 
    coloc_snp_df = coloc_snp_df.sort_values(by=['SNP.PP.H4'], ascending=False)
    
    ## Add cumsum_H4 column for cumulative sum of the SNP.PP.H4 column
    coloc_snp_df['cumsum_H4'] = coloc_snp_df['SNP.PP.H4'].cumsum()

    ## In order to find high colocalized SNPs in an expanded set with the paras['coloc_abf_cutoff'] cutoff, first get SNPs with the cumulative sum of the SNP.PP.H4 < 0.5, next get the first SNP which just over the cutoff, finally concat the two dataframe together
    tmp_df1 = coloc_snp_df.loc[coloc_snp_df['cumsum_H4'] <= paras['coloc_abf_cutoff']]
    tmp_df2 = coloc_snp_df.loc[coloc_snp_df['cumsum_H4'] > paras['coloc_abf_cutoff']].iloc[[0]]
    return_df = pd.concat([tmp_df1, tmp_df2])
    return_df['num_prob_expanded_snp'] = return_df.shape[0]
    return_df['high_coloc_snp'] = high_coloc_snp
    
    return return_df[['region_name', 'region_pos', 'ld_block_start', 'ld_block_end', 'tissue', 'tissue_category', 'gene_symbol', 'gene_id', 'snp', 'snp_chr', 'snp_start', 'snp_end', 'GTEx_ref', 'GTEx_alt', 'GWAS_ref', 'GWAS_alt', 'rsID', 'pvalues.df1', 'MAF.df1', 'V.df1', 'z.df1', 'r.df1', 'lABF.df1', 'GTEx_minor_allele_count', 'MAF.df2', 'V.df2', 'z.df2', 'r.df2', 'lABF.df2', 'internal.sum.lABF', 'SNP.PP.H4', 'nsnps', 'PP.H0.abf', 'PP.H1.abf', 'PP.H2.abf', 'PP.H3.abf', 'PP.H4.abf', 'num_prob_expanded_snp', 'high_coloc_snp', 'is_in_top_snp', 'is_max_H4']].values.tolist()


def save_summary_stats(coloc_summary_stats_df, paras):
    ## Save stats tables as files
    output_dir = path.join(paras['output_base_dir'])
    if not _is_path_exist(output_dir, False):
        try:
            makedirs(output_dir)
        except IOError as err:
            _print_log_message(err, 'error')
            sys.exit(1)
    
    ## Save eqtl_info_0.5_thresh_expanded.tsv. The file contains all co-localized signals whose SNP_PP_H4 values pass the threshold
    ordered_schema_for_file_1 = ['region_name', 'region_pos', 'ld_block_start', 'ld_block_end', 'tissue', 'tissue_category', 'gene_symbol', 'gene_id', 'prob_expanded_snp', 'num_prob_expanded_snp', 'high_coloc_snp', 'snp_chr', 'snp_start', 'snp_end', 'ref_GWAS', 'alt_GWAS', 'ref_GTEx', 'alt_GTEx', 'rsID', 'pvalues_GWAS', 'NONREF_AF_GWAS', 'V_GWAS', 'z_GWAS', 'r_GWAS', 'NONREF_beta_GWAS', 'lABF_GWAS', 'minor_allele_count_GTEx', 'NONREF_AF_GTEx', 'V_GTEx', 'z_GTEx', 'r_GTEx', 'NONREF_beta_GTEx', 'lABF_GTEx', 'internal_sum_lABF', 'target_gene_direction', 'SNP_PP_H4', 'nsnps', 'PP_H0_abf', 'PP_H1_abf', 'PP_H2_abf', 'PP_H3_abf', 'PP_H4_abf']
    
    coloc_summary_stats_df\
    .filter(coloc_summary_stats_df['is_in_top_snp'] == 1)\
    .drop('is_in_top_snp')\
    .drop('is_max_H4')\
    .withColumnRenamed('snp', 'prob_expanded_snp')\
    .withColumn('NONREF_beta_GWAS', F.col('z_GWAS')*F.sqrt('V_GWAS'))\
    .withColumn('NONREF_beta_GTEx', F.col('z_GTEx')*F.sqrt('V_GTEx'))\
    .withColumn('target_gene_direction', \
        F.when(F.col('NONREF_beta_GTEx') > 0, F.lit('Increase'))\
        .otherwise(F.when(F.col('NONREF_beta_GTEx') < 0, F.lit('Decrease'))))\
    .toPandas()[ordered_schema_for_file_1]\
    .to_csv(path.join(output_dir, "%seqtl_info_%s_thresh_expanded.tsv" % (paras['prefix'], str(paras['coloc_abf_cutoff']))), sep="\t", index=False)
    
    ## Save eqtl_info_highest_SNP_PP_H4.tsv. The file contains co-localized signals with the highest SNP_PP_H4 value
    ordered_schema_for_file_2 = ['region_name', 'region_pos', 'ld_block_start', 'ld_block_end', 'tissue', 'tissue_category', 'gene_symbol', 'gene_id', 'top_coloc_snp', 'snp_chr', 'snp_start', 'snp_end', 'ref_GWAS', 'alt_GWAS', 'ref_GTEx', 'alt_GTEx', 'rsID', 'pvalues_GWAS', 'NONREF_AF_GWAS', 'V_GWAS', 'z_GWAS', 'r_GWAS', 'NONREF_beta_GWAS', 'lABF_GWAS', 'minor_allele_count_GTEx', 'NONREF_AF_GTEx', 'V_GTEx', 'z_GTEx', 'r_GTEx', 'NONREF_beta_GTEx', 'lABF_GTEx', 'internal_sum_lABF', 'target_gene_direction', 'SNP_PP_H4', 'nsnps', 'PP_H0_abf', 'PP_H1_abf', 'PP_H2_abf', 'PP_H3_abf', 'PP_H4_abf']
    coloc_summary_stats_df\
    .filter((coloc_summary_stats_df['is_in_top_snp'] == 1) & (coloc_summary_stats_df['is_max_H4'] == 1))\
    .drop('is_in_top_snp')\
    .drop('is_max_H4')\
    .drop('num_prob_expanded_snp')\
    .withColumnRenamed('high_coloc_snp', 'top_coloc_snp')\
    .withColumn('NONREF_beta_GWAS', F.col('z_GWAS')*F.sqrt('V_GWAS'))\
    .withColumn('NONREF_beta_GTEx', F.col('z_GTEx')*F.sqrt('V_GTEx'))\
    .withColumn('target_gene_direction', \
        F.when(F.col('NONREF_beta_GTEx') > 0, F.lit('Increase'))\
        .otherwise(F.when(F.col('NONREF_beta_GTEx') < 0, F.lit('Decrease'))))\
    .toPandas()[ordered_schema_for_file_2]\
    .to_csv(path.join(output_dir, paras['prefix']+'eqtl_info_highest_SNP_PP_H4.tsv'), sep="\t", index=False)


def add_ld_r2(coloc_summary_stats_df, paras):
    from sparkinferno.preprocessing import loadtabix_vcf, run_ld_expansion, collect_ld_expansion_result
    
    output_dir = path.join(paras['output_base_dir'], 'coloc_output')
    if not _is_path_exist(output_dir, False):
        try:
            makedirs(output_dir)
        except IOError as err:
            _print_log_message(err, 'error')
            sys.exit(1)
    
    gadb_df = inferno.loadgadb(paras['gadb_meta_file'])
    data_source_filter = "Data Source:1k_genome_phase3,Output type:{} SUPER SNP_biallelic,Genome build:{},File format:vcf".format('EUR', paras['genome_build'])
    tracks_list = inferno.select(gadb_df, data_source_filter).collect()
    
    if len(tracks_list) == 0:
        _print_log_message('1000 Genomes VCF files are not found in GADB', 'error')
        sys.exit(1)
    else:
        vcf_ref_path_dict = dict([("chr{}".format(t['File name'].split('_')[0]), t['absPath']) for t in tracks_list])
    
    schema = StructType([StructField('snp_chr', StringType(),False),
                         StructField('snp_pos_start', IntegerType(), False),
                         StructField('snp_pos_end', IntegerType(), False),
                         StructField('snp_id', StringType(), False),
                         StructField('snp_ref', StringType(), False),
                         StructField('snp_alt', StringType(), False),
                         StructField('snp_maf', DoubleType(), False),
                         StructField('region_pos', StringType(), False),
                         StructField('gwas_maf', DoubleType(), False),
                         StructField('r2', DoubleType(), False),
                         StructField('dprime', DoubleType(), False)])
    
    coloc_summary_stats_part_df = coloc_summary_stats_df.select('region_name', 'region_pos', 'ld_block_start', 'ld_block_end').distinct()
    
    vcf_path_df = coloc_summary_stats_part_df\
                  .withColumn('chr', F.split(F.col('region_pos'), ':').getItem(0))\
                  .withColumn('coordinate_list', F.concat(F.regexp_replace(F.col('chr'), 'chr', ''), F.lit(':'), F.col('ld_block_start'), F.lit('-'), F.col('ld_block_end')))\
                  .groupby(F.col('region_pos').alias('grouped_col'))\
                  .agg(F.collect_list('coordinate_list').alias('coordinate_list'),\
                       F.collect_list('region_pos').alias('snp_id'),\
                       F.first(F.col('chr')).alias('chr'))\
                  .rdd\
                  .mapPartitionsWithIndex(lambda i, x: loadtabix_vcf(i, x, vcf_ref_path_dict, path.join(paras['output_base_dir'], 'PLINK_LD_block'), check_allele=True, temp_dir=paras['temp_dir'], tabix_app=paras['tabix_app'], conf_path=paras['config_file'], quiet=True))\
                  .map(lambda y: Row(snp_id=y[0], vcf_path=y[1]))\
                  .mapPartitionsWithIndex(lambda i, x: run_ld_expansion(i, x, 0, paras['ld_block_window_size'], quiet=True, plink_app=paras['plink_app'], conf_path=paras['config_file']))\
                  .mapPartitionsWithIndex(lambda i, x: collect_ld_expansion_result(i, x, conf_path=paras['config_file']))\
                  .toDF(schema)\
                  .cache()
    
    return_df = coloc_summary_stats_part_df\
                .join(vcf_path_df, 'region_pos', 'left_outer')\
                .select(coloc_summary_stats_part_df.columns + [c for c in vcf_path_df.columns if c != 'region_pos'])
    
    return_df.toPandas().to_csv(path.join(output_dir, 'LD_blocks.tsv'), sep="\t", index=False)
    
    return return_df

def get_gtex_file_size(gtex_sample_size_file):
    gtex_sample_size = {}
    with open(gtex_sample_size_file, 'r') as fp:
        for line in fp:
            line = line.strip()
            ## ignore blank lines or beginning with #
            if not line or line.startswith('#'): continue 
            fields = line.split("\t")
            gtex_sample_size[fields[0]] = int(fields[1])
            
    return gtex_sample_size
    
    
def _is_path_exist(dir, error_msg=False):
    if path.exists(dir):
        return True
    else:
        if error_msg: _print_log_message(dir+': No such file or directory', 'error')
        return False


def _generate_tmp_log_path(conf_path=''):
    global tmp_log_path
    
    ## Load configure file
    if conf_path != '' and path.exists(conf_path):
        conf.read_config_file(conf_path)
    
    if tmp_log_path == '':
        from uuid import uuid4 ## Random UUID generator
        
        tmp_log_path = path.join(conf.TMP_PATH, str(uuid4())+'.tmp.log')
        open(path.join(conf.TMP_PATH, 'log_list.tmp.log'), 'a').write(tmp_log_path + "\n")
            
            
def _print_log_message(msg, level='info', conf_path=''): ## conf_path propagates configure file path from main script for processing in a worker
    msg = str(msg).rstrip()
    global tmp_log_path
    global worker_logger
    
    try:
        logger_method = eval("this.logger.%s" % level)
    except AttributeError:
        if worker_logger is None:
            ## Load configure file
            if conf_path != '' and path.exists(conf_path):
                conf.read_config_file(conf_path)
            
            ## Assign logger to temporarily save messages from workers
            _generate_tmp_log_path(conf_path=conf_path)
            this_script_name = path.splitext(path.basename(__file__))[0]
            worker_logger = logging.getLogger(this_script_name)
            file_handler = logging.FileHandler(tmp_log_path)
            file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
            worker_logger.addHandler(file_handler)
        
        logger_method = eval("worker_logger.%s" % level)
    
    logger_method(msg)
    

def _coalesce_worker_log():
    log_list_path = path.join(conf.TMP_PATH, 'log_list.tmp.log')
    
    ## Read the tmp log paths line by line
    def _get_tmp_log_path():
        with open(log_list_path, 'r') as fp:
            for line in fp: yield line.rstrip()
    
    try:    
        ## False if the --skip_logging option is given from frontend script
        is_logger_for_workers_existing_handlers = True if len(this.logger_for_workers.handlers) > 0 else False
    except AttributeError:
        ## No logger_for_workers is assign in the frontend script
        is_logger_for_workers_existing_handlers = False
    finally:
        if path.exists(log_list_path):
            if is_logger_for_workers_existing_handlers:
                from shutil import copyfileobj
                
                ## Get the log file name from this.logger_for_workers
                handler = this.logger_for_workers.handlers[0]
                target_filename = handler.baseFilename
                
                ## Coalesce tmp logs into the frontend script log
                t_fp = open(target_filename, 'a')
                for line in _get_tmp_log_path():
                    try:
                        f_fp = open(line, 'r')
                        copyfileobj(f_fp, t_fp)
                        f_fp.close
                        remove(line)
                    except:
                        continue
                t_fp.close()
                
            else:  ## Directly remove tmp log files because --skip_logging option is given
                for line in _get_tmp_log_path():
                    remove(line)
                    
            remove(log_list_path)
