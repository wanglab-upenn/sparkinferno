# -*- coding: utf-8 -*-
from os import path, makedirs
import sys
from distutils.spawn import find_executable
from glob import glob
from tempfile import gettempdir
from shutil import copyfile


my_dir = path.dirname(__file__)

## Advanced parameter block start
## Motif PWM file for motif overlapping analysis
MOTIF_PWM_FILE = path.join(my_dir, '..', 'data', 'known.motifs.homer.v4.10.3.updated.txt')

## Parameters for co-localization
GTEX_SAMPLE_SIZE_FILE = path.join(my_dir, '..', 'data', 'GTEx_*_sample_sizes.tsv')
ENSEMBL_ID_TO_GENE_SYMBOL_FILE = path.join(my_dir, '..', 'data', 'ensembl_id_to_gene_symbol.txt')

## Parameters for logging format
LOG_DATEFMT = '%Y-%m-%d %H:%M:%S'
LOG_FORMAT = {'DEBUG': '%(asctime)-15s %(levelname)s %(filename)s:%(lineno)s - %(message)s',
              'INFO': '%(asctime)-15s %(levelname)s - %(message)s',
              'WARNING': '%(asctime)-15s %(levelname)s %(filename)s:%(lineno)s - %(message)s',
              'ERROR': '%(asctime)-15s %(levelname)s %(filename)s:%(lineno)s - %(message)s'}
## Advanced parameter block end
## =======================================
## Assign default parameters
PYTHON_APP = ''
GIGGLE_APP = ''
TABIX_APP = ''
PLINK_APP = ''
BGZIP_APP = ''
RSCRIPT_APP = ''
SAMTOOLS_APP = ''

## Parameters for log
## The log level supports DEBUG, INFO, WARNING, and ERROR
LOG_LEVEL = 'INFO'

## SPARK configuration
SPARK_CONF = [('spark.master', 'local[*]'), ('spark.driver.memory', '56g'), ('spark.executor.memory', '4g'), ('spark.driver.maxResultSize', '3g'), ('spark.sql.shuffle.partitions', '200'), ('spark.sql.codegen.wholeStage', 'false'), ('spark.debug.maxToStringFields', 200)]

GADB_META_FILE = ''
TMP_PATH = ''


## NOTE: Please DON'T CHANGE anything below
def set_parameter(var, val, use_eval=False):
    var = var.strip().upper()
    if isinstance(val, str):
        val = val.strip()
    
    if (var is not None) or (var != ''):
        if use_eval:
            globals()[var] = eval(val)
        else:
            globals()[var] = val


def check_config():
    return_flag = True
    for var, val in globals().items():
        if var[-4:] == '_APP':
            if not path.exists(val):
                if var[:-4] == 'RSCRIPT':
                    alt_path = find_executable('Rscript')
                else:
                    alt_path = find_executable(var[:-4].lower())
                    
                if alt_path is not None:
                    if val == '':
                        sys.stderr.write("Warning: {} is empty, using {} instead\n".format(var, alt_path))
                    else:
                        sys.stderr.write("Warning: Cannot find {} in the path {}, using {} instead\n".format(var, val, alt_path))
                    set_parameter(var, alt_path)
                else:
                    sys.stderr.write("Error: Cannot find the path: {} in the {} variable from a configuration file\n".format(val, var))
                    return_flag = False
        
        elif var[-5:] == '_FILE':
            file_paths = glob(val)
            if len(file_paths) == 0:
                sys.stderr.write("Error: Cannot find the path: {} in the {} variable from a configuration file\n".format(val, var))
                return_flag = False
    return return_flag
    

def read_config_file(cfg_path=''):
    try:
        line_num = 0
        with open(cfg_path, 'r') as fp:
            for line in fp:
                line_num += 1
                if line.startswith('#'): continue
                if line.strip() == '': continue
                line = line.split('=')
                line[0] = line[0].strip()
                if len(line) == 1: line[1] = ''
                else: line[1] = line[1].strip()
                if line[0] == 'TMP_PATH' and line[1] == '': line[1] = gettempdir()
                if line[0] == 'SPARK_CONF' or line[0] == 'LOG_FORMAT':
                    set_parameter(line[0], line[1], True)
                else:
                    set_parameter(line[0], line[1].replace("'", '').replace('"', ''))
        
        globals()['SPARK_CONF'].append(('spark.local.dir', globals()['TMP_PATH']))
        
    except IndexError:
        sys.stderr.write("Error: Unexpected error in line {}: {}\n".format(line_num, '='.join(line)))
                

def write_config_file(cfg_dir=''):
    cfg_var_list = ['PYTHON_APP', 'GIGGLE_APP', 'TABIX_APP', 'PLINK_APP', 'BGZIP_APP', 'RSCRIPT_APP', 'SAMTOOLS_APP', 'LOG_LEVEL', 'LOG_DATEFMT', 'LOG_FORMAT', 'SPARK_CONF', 'MOTIF_PWM_FILE', 'GTEX_SAMPLE_SIZE_FILE', 'ENSEMBL_ID_TO_GENE_SYMBOL_FILE', 'GADB_META_FILE', 'TMP_PATH']
    current_setting_dict = {}
    
    for var, val in globals().items():
        if var in cfg_var_list: current_setting_dict[var] = str(val)
    
    config_from = path.join(my_dir, 'sparkinferno.cfg')
    config_to = path.join(cfg_dir, 'sparkinferno.cfg')
    with open(config_from, 'r') as fp_from, open(config_to, 'w') as fp_to:
        for line in fp_from:
            line_list = line.split('=')
            line_list[0] = line_list[0].strip() ## var from the template file
            if line_list[0] in current_setting_dict.keys():
                fp_to.write("{} = {}\n".format(line_list[0], current_setting_dict[line_list[0]]))
                del(current_setting_dict[line_list[0]])
            else:
                fp_to.write(line)
                
        ## Add variables that are not existing in the template
        if len(current_setting_dict)>0:
            fp_to.write("\n## Other parameters\n")
            for var, val in current_setting_dict.items():
                fp_to.write("{} = {}\n".format(var, val))
    
    return config_to


def get_config_template(output_dir):
    if not path.exists(output_dir): makedirs(output_dir)
    
    cfg_template_path = path.join(my_dir, 'sparkinferno.cfg')
    output_path = path.join(output_dir, 'sparkinferno.cfg')
    copyfile(cfg_template_path, output_path)
    
    
