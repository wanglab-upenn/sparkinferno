# -*- coding: utf-8 -*-
from os import path, remove, makedirs
import re
import csv
import pyspark
import sparkinferno.config as conf
import gadb.gadb as gadb
import sparkinferno.gig as gig
from sparkinferno.tabix import tabix, tabix_partitions
from pyspark.sql import SparkSession, DataFrame, Row
from pyspark.sql import functions as F
from pyspark.sql.types import StringType, IntegerType, StructField, StructType, ArrayType, DoubleType
from pyspark.rdd import RDD
from pyspark.conf import SparkConf
import sys
import subprocess as sp
import __main__
import logging
from tempfile import gettempdir
from shutil import rmtree
import itertools


## Store SparkSession in the main script
this = sys.modules[__main__.__name__]
this.spark = None
this.sc = None


## Initialize the logging module
log_level = eval("logging.%s" % conf.LOG_LEVEL)
logging.basicConfig(level = log_level,
                    format = conf.LOG_FORMAT[conf.LOG_LEVEL],
                    datefmt = conf.LOG_DATEFMT)


def make_spark_session():
    if this.spark is None:
        _print_log_message('Initializing SPARK session')
        
        spark_conf = SparkConf().setAll(conf.SPARK_CONF)

        spark = SparkSession \
                .builder \
                .appName('SparkINFERNO') \
                .config(conf=spark_conf) \
                .getOrCreate()
                
        sc = spark.sparkContext
        sc.setLogLevel('ERROR')
        this.spark = spark
        this.sc = sc
    else:
        try:
            this.logger.warning('A SPARK session has been created')
        except AttributeError:
            logging.warning('A SPARK session has been created')
    
    ## Remove unremoved log list before this round
    previous_log_list = path.join(conf.TMP_PATH, 'log_list.tmp.log')
    if path.exists(previous_log_list): remove(previous_log_list)
    
    return spark, sc


def loadgadb(input_gadb_meta_file):
    gadbdf = gadb.load(this.spark, input_gadb_meta_file)
    # gadb.schema(gadbdf)
    return gadb.getAbsPath(gadbdf)


def select(df, filter):
    if isinstance(filter, str):
        return_gadb_df = gadb.select(df, filter)
    
    if isinstance(filter, list):
        for filter_element in filter:
            gadb_df = gadb.select(df, filter_element)
            if 'return_gadb_df' in dir():
                return_gadb_df = return_gadb_df.union(gadb_df)
            else:
                return_gadb_df = gadb_df.select('*')
    
    # return_gadb_df.show(3)
    return return_gadb_df


def getPath(df, path_type):
    filedpath = df.select(path_type).distinct().rdd.flatMap(lambda x: x).collect()
    return filedpath
    
    
def load_bedfile_as_rdd(bedfile, partition_num=100, header_list=[], delimiter="\t"):
    sc = this.sc
    bed_rdd = sc.textFile(bedfile, partition_num)\
                .filter(lambda line: len(line)>0 and not(line.startswith('#')))\
                .distinct()\
                .map(lambda x: x.split(delimiter))
                
    if len(header_list) > 0:
        row = Row(*['_{}'.format(idx) if val is None else val for idx, val in enumerate(header_list)]) ## Using _# as a column name if header_list has None in certain position
        
        ## Elements in header_list can allow partial columns of a BED file. No need always covers entire columns of a BED file in header_list. Make the 2nd (start) and 3rd (end) columns as int type.
        bed_rdd = bed_rdd.map(lambda y: row(*[int(y[idx]) if idx == 1 or idx == 2 else y[idx] for idx, _ in enumerate(header_list)]))
        
    return bed_rdd


def loadgiggle(bedfile, gadb_df, **kwargs):
    ## Set default parameters
    method = 'cmd'
    partition_num = 100
    window_size = 0
    output_in = 'memory'
    temp_dir = gettempdir()
    giggle_app='giggle'
    bgzip_app='bgzip'
    
    for key, value in kwargs.items():
        if key == 'method':
            method = value
        elif key == 'partition_num':
            partition_num = int(value)
        elif key == 'window_size':
            window_size = int(value)
        elif key == 'output_in': ## Support 'memory' (default) or 'disk' type
            output_in = value
        elif key == 'temp_dir':
            if not _is_path_exist(value): makedirs(value)
            temp_dir = value
        elif key == 'giggle_app':
            giggle_app = value
        elif key == 'bgzip_app':
            bgzip_app = value
            
    spark = this.spark
    sc = this.sc
    schema = StructType([StructField('chr', StringType(),False),
                         StructField('chrStart', IntegerType(), False),
                         StructField('chrEnd', IntegerType(), False),
                         StructField('additionalQueriedString', StringType(), False),
                         StructField('hitString', StringType(), False),
                         StructField('absPath', StringType(), False)])
    return_giggledf = spark.createDataFrame(sc.emptyRDD(), schema)
    
    if isinstance(gadb_df, DataFrame):
        filedpath = getPath(gadb_df, 'filepath')
    else:
        filedpath = [gadb_df]
        
    for path in filedpath:
        path = path.replace('giggle_index', '')
        if not isinstance(bedfile, RDD):
            bedfile = load_bedfile_as_rdd(bedfile, partition_num=partition_num)
        
        if method == 'cmd_partitions': ## Using Giggle with bash command with mapPartitions
            giggledf = bedfile.map(lambda x: "\t".join([u"{}".format(y) for y in x]))\
                              .mapPartitions(lambda x: gig.Gig_partitions(x, path + '/giggle_index', window_size, output_in=output_in, temp_dir=temp_dir, giggle_app=giggle_app, bgzip_app=bgzip_app)).toDF(schema)
        else : ## Using Giggle with bash command with flatMap
            giggledf = bedfile.map(lambda x: "\t".join([u"{}".format(y) for y in x]))\
                              .flatMap(lambda x: gig.Gig(x, path + '/giggle_index', window_size, giggle_app=giggle_app)).toDF(schema)
                         
        return_giggledf = return_giggledf.union(giggledf)
    # return_giggledf.show(3)
    return return_giggledf
    

def loadtabix(bedfile, gadb_df, **kwargs):
    ## Set default parameters
    method = 'cmd'
    partition_num = 100
    window_size = 0
    tabix_app='tabix'
    
    for key, value in kwargs.items():
        if key == 'method':
            method = value
        elif key == 'partition_num':
            partition_num = int(value)
        elif key == 'window_size':
            window_size = int(value)
        elif key == 'tabix_app':
            tabix_app = value
            
    spark = this.spark
    sc = this.sc
    schema = StructType([StructField('chr', StringType(),False),
                         StructField('chrStart', IntegerType(), False),
                         StructField('chrEnd', IntegerType(), False),
                         StructField('additionalQueriedString', StringType(), False),
                         StructField('hitString', StringType(), False),
                         StructField('absPath', StringType(), False)])
    return_tabixdf = spark.createDataFrame(sc.emptyRDD(), schema)
    
    if isinstance(gadb_df, DataFrame):
        filedpath = getPath(gadb_df, 'absPath')
    else:
        filedpath = [gadb_df]
    
    for path in filedpath:
        if not isinstance(bedfile, RDD):
            if not isinstance(bedfile, RDD):
                bedfile = load_bedfile_as_rdd(bedfile, partition_num=partition_num)
            
        if method == 'cmd_partitions':
            tabixdf = bedfile.map(lambda x: "\t".join([u"{}".format(y) for y in x]))\
                             .mapPartitions(lambda x: tabix_partitions(x, path, window_size, tabix_app=tabix_app)).toDF(schema)
        else:
            tabixdf = bedfile.map(lambda x: "\t".join([u"{}".format(y) for y in x]))\
                             .flatMap(lambda x: tabix(x, path, window_size, tabix_app=tabix_app)).toDF(schema)
                         
        return_tabixdf = return_tabixdf.union(tabixdf)
    # return_tabixdf.show(3)
    return return_tabixdf
    

def convert_hitString2df(giggle_df, hitString_header):
    split_col = F.split(giggle_df['hitString'], '\t')
    for i,h in enumerate(hitString_header):
        giggle_df = giggle_df.withColumn(h, split_col.getItem(i))
    # giggle_df.show(3)
    return giggle_df


def expand_columns_from_string(df, col_name_list, expanded_header_list, separator_list):
    if len(col_name_list) == len(expanded_header_list) == len(separator_list):
        for idx, col_name in enumerate(col_name_list):
            split_col = F.split(df[col_name], separator_list[idx])
            for i,h in enumerate(expanded_header_list[idx]):
                if h == None: continue
                df = df.withColumn(h, split_col.getItem(i))
    else:
        sys.stderr.write('Different length of input lists.\n')

    return df


def annotateGADB(giggledf, filtered_gadbdf, data_source=None, hitString_offset=0, ensg2symbol_file=path.join(path.dirname(__file__), '..', 'data', 'ensembl_id_to_gene_symbol.txt')):
    sc = this.sc
    ## To speed up, using a dictionary to merge giggledf and filtered_gadbdf instead of table join
    def replace_col_by_dict(col, ref_dict):
        try:
            return ref_dict[col]
        except KeyError:
            return None

    filtered_gadbdf_end_index = filtered_gadbdf.columns.index('File format') ## Get columns from beginning to "Assay"
    # gadb_dict = sc.broadcast(filtered_gadbdf.rdd.map(lambda r: (r['absPath'],"\t".join(list(r[:filtered_gadbdf_end_index])+[r[-1]]))).collectAsMap()).value
    gadb_dict = filtered_gadbdf.rdd.map(lambda r: (r['absPath'],"\t".join(list(r[:filtered_gadbdf_end_index])+[r[-1]]))).collectAsMap()
    
    replace_absPath_udf = F.udf(lambda c: replace_col_by_dict(c, gadb_dict), StringType())
    giggledf = giggledf.withColumn('absPath', replace_absPath_udf(giggledf['absPath'])).filter(F.col('absPath').isNotNull())
    # giggledf = giggledf.replace(gadb_dict, subset='absPath') ## Above udf approach is faster than the replace method
    
    additionalQueriedString_schema = ['rsID', None, None, None, None, None, 'region_name']
    return_df = expand_columns_from_string(giggledf, ['additionalQueriedString', 'absPath'], [additionalQueriedString_schema, list(filtered_gadbdf.columns[:filtered_gadbdf_end_index]) + ['bedPath']], ["\t", "\t"]).drop('absPath')
    
    if data_source == 'GTEx_v6':
        return_df = expand_columns_from_string(return_df, ['hitString'], [[None]*(8+hitString_offset) + ['GTEx Ensembl gene']], ["\t"]).withColumn('GTEx gene symbol', F.regexp_extract('hitString', r'gene_name=(.+?);', 1))
        
    elif data_source == 'GTEx_v6p' or data_source == 'GTEx_v7' or data_source == 'GTEx_v8':
        return_df = expand_columns_from_string(return_df, ['hitString'], [[None]*(5+hitString_offset) + ['GTEx Ensembl gene']], ["\t"]).withColumn('GTEx gene symbol', F.regexp_replace(F.col('GTEx Ensembl gene'), '\.*\d*$', ''))
        
        ## Create a dictionary for looking up gene symbols
        genome_build = return_df.select('Genome build').collect()[0]['Genome build']
        
        ## Load ENSEMBL ID to GENE SYMBOL file as a dictionary
        ensg2symbol_dict = {None: ''} ## Consider the situation of Ensembl gene id is empty
        with open(ensg2symbol_file.replace('*', genome_build), 'r') as fp:
            for line in fp:
                line = line.strip()
                ## ignore blank lines or beginning with #
                if not line or line.startswith('#'): continue 
                line_data = line.split("\t")
                ensg2symbol_dict[line_data[1]] = line_data[2]
        # ensg2symbol_dict = sc.broadcast(ensg2symbol_dict).value
        
        replace_gene_symbol_udf = F.udf(lambda c: replace_col_by_dict(c, ensg2symbol_dict), StringType())
        return_df = return_df.withColumn('GTEx gene symbol', replace_gene_symbol_udf(return_df['GTEx gene symbol']))
        
    elif data_source == 'INFERNO_genomic_partition':
        get_genomic_element_category_udf = F.udf(lambda x: x.split('/')[-1].split('_')[1], StringType())
        
        get_genomic_element_udf = F.udf(lambda x: x.split("\t")[3+hitString_offset].split(';')[0] if x.split("\t")[4+hitString_offset] == '.' else x.split("\t")[4+hitString_offset], StringType())
        
        ## Get gene symbol information from hitString of INFERNO genomic partition
        @F.udf(StringType())
        def get_gene_symbol(used_bed, hit_string):
            if used_bed == 'mRNA':
                match_gene = re.search(r'geneSymbol=(.+?);', hit_string.split("\t")[3+hitString_offset])
                if match_gene: return None if match_gene.group(1) == 'n/a' else match_gene.group(1)
                else: return None
            elif used_bed == 'lncRNA':
                match_gene = re.search(r'gene_id=(.+?);', hit_string.split("\t")[3+hitString_offset])
                if match_gene: return match_gene.group(1)
                else: return None
            else: ## used_bed == 'repeat'
                return None
        
        return_df = return_df\
        .withColumn('Element source category', get_genomic_element_category_udf(return_df['bedPath']))\
        .withColumn('Genomic element', get_genomic_element_udf(return_df['hitString']))\
        .withColumn('Gene symbol', get_gene_symbol(F.col('Element source category'), return_df['hitString']))
                             
    elif data_source == 'TargetScan_v7p2':
        return_df = expand_columns_from_string(return_df, ['hitString'], [[None]*(hitString_offset) + ['chrom', 'chromStart', 'chromEnd', 'miRNA-Gene pair', None, 'strand']], ["\t"])
        
        ## Replace the miRNA-Gene pair column from the structure of gene:miRNA with miRNA@gene
        split_col = F.split(return_df['miRNA-Gene pair'], ':')
        return_df = return_df\
                    .withColumn('miRNA', split_col.getItem(1))\
                    .withColumn('target_gene', split_col.getItem(0))\
                    .withColumn('miRNA-Gene pair', F.concat_ws('@', F.col('miRNA'), F.col('target_gene')))\
                    .withColumn('miRNA-Target position pair', F.concat('miRNA', F.lit('@'), 'chrom', F.lit(':'), 'chromStart', F.lit('-'), 'chromEnd', F.lit(':'), 'strand'))\
                    .drop('miRNA', 'target_gene', 'chrom', 'chromStart', 'chromEnd', 'strand')
        
    elif data_source == 'DASHR2':
        return_df = expand_columns_from_string(return_df, ['hitString'], [[None]*(23+hitString_offset) + ['rnaID', 'rnaClass']], ["\t"]).withColumn('rnaID', F.when(F.col('rnaID') == '.', F.lit(None)).otherwise(F.col('rnaID')))

    
    return return_df


def interval_summary(input_df, data_source='', prefix=''):
    if 'GTEx_v' in data_source:
        summaryinterval = input_df.withColumn('Interval', F.concat_ws(' ','chr', 'chrStart', 'chrEnd')).withColumn('Tissue category-cell type', F.array('Tissue category', 'cell type')).withColumn('Gene name-cell type', F.array('GTEx gene symbol', 'cell type')).withColumn('Gene name-tissue category', F.array('GTEx gene symbol', 'Tissue category'))

        interval = summaryinterval.orderBy('Interval')\
                  .groupBy('Interval')\
                  .agg(F.first('chr').cast('string').alias('chr'),\
                       F.first('chrStart').cast('string').alias('chrStart'),\
                       F.first('chrEnd').cast('string').alias('chrEnd'),\
                       F.count('Interval').alias('__prefix__Number of overlaps'),\
                       F.collect_set('cell type').cast('string').alias('__prefix__Tissue list'),\
                       F.countDistinct('cell type').alias('__prefix__Number of tissues'),\
                       F.collect_set('Tissue category').cast('string').alias('__prefix__Tissue category list'),\
                       F.countDistinct('Tissue category').alias('__prefix__Number of tissue categories'),\
                       F.collect_set('Tissue category-cell type').cast('string').alias('__prefix__Tissue category-cell type'),\
                       F.collect_set('GTEx Ensembl gene').cast('string').alias('__prefix__Ensembl gene list'),\
                       F.collect_set('GTEx gene symbol').cast('string').alias('__prefix__Gene symbol list'),\
                       F.countDistinct('GTEx gene symbol').alias('__prefix__Number of genes'),\
                       F.collect_set('Gene name-cell type').cast('string').alias('__prefix__Gene name-cell type'),\
                       F.collect_set('Gene name-tissue category').cast('string').alias('__prefix__Gene name-tissue category'))
                       
    elif data_source == 'INFERNO_genomic_partition':
        summaryinterval = input_df.withColumn('Interval', F.concat_ws(' ','chr', 'chrStart', 'chrEnd')).withColumn('Genomic element-gene symbol', F.when((F.col('Element source category')!='repeat') & (F.col('Gene symbol').isNotNull()), F.array('Genomic element', 'Gene symbol')))
        
        interval = summaryinterval.orderBy('Interval')\
                  .groupBy('Interval')\
                  .agg(F.first('chr').cast('string').alias('chr'),\
                       F.first('chrStart').cast('string').alias('chrStart'),\
                       F.first('chrEnd').cast('string').alias('chrEnd'),\
                       F.count('Interval').alias('__prefix__Number of overlaps'),\
                       F.collect_set('Genomic element').cast('string').alias('__prefix__Genomic element list'),\
                       F.countDistinct('Genomic element').alias('__prefix__Number of genomic elements'),\
                       F.collect_set('Genomic element-gene symbol').alias('__prefix__Genomic element-gene symbol list'))
        ## Convert empty arrays to null
        interval = interval.withColumn('__prefix__Genomic element-gene symbol list', F.when(F.size(interval['__prefix__Genomic element-gene symbol list']) == 0, F.lit(None)).otherwise(interval['__prefix__Genomic element-gene symbol list'].cast(StringType())))
        
    elif data_source == 'HOMER':
        summaryinterval = input_df.withColumn('Interval', F.concat_ws(' ','chr', 'chrStart', 'chrEnd')).withColumn('Motif interval list', F.concat('motif_chr', F.lit(':'), 'motif_start', F.lit('-'), 'motif_end', F.lit(':'), 'motif_strand'))
        
        interval = summaryinterval.orderBy('Interval')\
                  .groupBy('Interval')\
                  .agg(F.first('chr').cast('string').alias('chr'),\
                       F.first('chrStart').cast('string').alias('chrStart'),\
                       F.first('chrEnd').cast('string').alias('chrEnd'),\
                       F.count('Interval').alias('__prefix__Number of overlaps'),\
                       F.first('rsID').cast('string').alias('rsID'),\
                       F.first('ref').cast('string').alias('ref'),\
                       F.first('alt').cast('string').alias('alt'),\
                       F.collect_list('Motif interval list').cast('string').alias('__prefix__Motif interval list'),\
                       F.collect_list('motif_name').cast('string').alias('__prefix__Motif name list'),\
                       F.collect_list('log_odds_score').cast('string').alias('__prefix__Log odds score list'),\
                       F.collect_list('motif_seq').cast('string').alias('__prefix__Motif sequence list'),\
                       F.countDistinct('motif_name').alias('__prefix__Number of distinct motif'),\
                       F.collect_list('snp_pos').cast('string').alias('__prefix__SNP position list'),\
                       F.collect_list('relative_snp_pos').cast('string').alias('__prefix__Relative SNP position list'),\
                       F.collect_list('ref_pwm').cast('string').alias('__prefix__ref_pwm'),\
                       F.collect_list('alt_pwm').cast('string').alias('__prefix__alt_pwm'),\
                       F.collect_list('delta_pwm').cast('string').alias('__prefix__delta_pwm'),\
                       F.collect_list('rounded_delta_pwm').cast('string').alias('__prefix__rounded_delta_pwm'),\
                       F.collect_list('ref_prob').cast('string').alias('__prefix__ref_prob'),\
                       F.collect_list('alt_prob').cast('string').alias('__prefix__alt_prob'))
                       
    elif data_source == 'TargetScan_v7p2':
        summaryinterval = input_df.withColumn('Interval', F.concat_ws(' ','chr', 'chrStart', 'chrEnd'))
        
        interval = summaryinterval.orderBy('Interval')\
                  .groupBy('Interval')\
                  .agg(F.first('chr').cast('string').alias('chr'),\
                       F.first('chrStart').cast('string').alias('chrStart'),\
                       F.first('chrEnd').cast('string').alias('chrEnd'),\
                       F.count('Interval').alias('__prefix__Number of overlaps'),\
                       F.collect_set('miRNA-Gene pair').cast('string').alias('__prefix__Distinct_miRNA-Gene pair list'),\
                       F.countDistinct('miRNA-Gene pair').alias('__prefix__Number of distinct miRNA-Gene pair'),\
                       F.collect_set('miRNA-Target position pair').cast('string').alias('__prefix__Distinct_miRNA-Target position pair list'),\
                       F.countDistinct('miRNA-Target position pair').alias('__prefix__Number of distinct miRNA-Target position pair'))
    
    elif data_source == 'DASHR2':
        summaryinterval = input_df.withColumn('Interval', F.concat_ws(' ','chr', 'chrStart', 'chrEnd'))

        interval = summaryinterval.orderBy('Interval')\
                  .groupBy('Interval')\
                  .agg(F.first('chr').cast('string').alias('chr'),\
                       F.first('chrStart').cast('string').alias('chrStart'),\
                       F.first('chrEnd').cast('string').alias('chrEnd'),\
                       F.count('Interval').alias('__prefix__Number of overlaps'),\
                       F.collect_set('cell type').cast('string').alias('__prefix__Tissue list'),\
                       F.countDistinct('cell type').alias('__prefix__Number of tissues'),\
                       F.array_remove(F.collect_set('rnaID'), '').alias('__prefix__rnaID list'),\
                       F.collect_set('rnaClass').cast('string').alias('__prefix__rnaClass list'),\
                       F.countDistinct('rnaClass').alias('__prefix__Number of rnaClass'))
        ## Convert empty arrays to null
        interval = interval.withColumn('__prefix__rnaID list', F.when(F.size(interval['__prefix__rnaID list']) == 0, F.lit(None)).otherwise(interval['__prefix__rnaID list'].cast(StringType())))
    
    else:
        summaryinterval = input_df.withColumn('Interval', F.concat_ws(' ','chr', 'chrStart', 'chrEnd')).withColumn('Tissue category-cell type', F.array('Tissue category', 'cell type'))
        
        interval = summaryinterval.orderBy('Interval')\
                  .groupBy('Interval')\
                  .agg(F.first('chr').cast('string').alias('chr'),\
                       F.first('chrStart').cast('string').alias('chrStart'),\
                       F.first('chrEnd').cast('string').alias('chrEnd'),\
                       F.count('Interval').alias('__prefix__Number of overlaps'),\
                       F.collect_set('cell type').cast('string').alias('__prefix__Tissue list'),\
                       F.countDistinct('cell type').alias('__prefix__Number of tissues'),\
                       F.collect_set('Tissue category').cast('string').alias('__prefix__Tissue category list'),\
                       F.countDistinct('Tissue category').alias('__prefix__Number of tissue categories'),\
                       F.collect_set('Tissue category-cell type').cast('string').alias('__prefix__Tissue category-cell type'))
                       
    interval = interval.withColumn('__prefix__Data source', F.lit(data_source if data_source else prefix))
    ## interval_summary = interval.select('chr', 'chrStart', 'chrEnd', '__prefix__Number of overlaps', '__prefix__Tissue list', '__prefix__Number of tissues', '__prefix__Tissue category list', '__prefix__Number of tissue categories', '__prefix__Annotation type')
    metadata = interval.toDF(*(re.sub('__prefix__', prefix+'_', c) for c in interval.columns)).drop('Interval')
    # metadata.show(3)

    return metadata


def track_summary(bedfile, annotateGADB_df, data_source, method=None, N=0, replicate_frequency=0, window_size=0, temp_dir=gettempdir(), giggle_app='giggle', bgzip_app='bgzip'):
    import sparkinferno.enrichment as enrichment
    
    annotateGADB_df_count = annotateGADB_df.count()
    
    if method == 'empirical_pval' and annotateGADB_df_count > 0:
        if replicate_frequency == 0: replicate_frequency = 10000 ## Set default value

        track_summary_df = enrichment.empirical_pval_enrichment(bedfile, annotateGADB_df, groupby_col_name='bedPath', replicate_frequency=replicate_frequency, window_size=window_size, temp_dir=temp_dir, giggle_app=giggle_app, bgzip_app=bgzip_app)
        track_summary_df = track_summary_df.drop('Total covered bases of GADB data source')
        
    elif method == 'fisher_exact' and annotateGADB_df_count > 0:
        track_summary_df = enrichment.fisher_exact_enrichment(bedfile, annotateGADB_df, groupby_col_name='bedPath', N=N)
        
    else:
        track_summary_df = enrichment.group_summary_annotateGADB(annotateGADB_df, 'bedPath')
        track_summary_df = track_summary_df.drop('Total covered bases of GADB data source')
    
    track_summary_df = track_summary_df.withColumn('Data source', F.lit(data_source))
    
    return track_summary_df


def tissue_summary(bedfile, annotateGADB_df, data_source, method=None, N=0, replicate_frequency=0, window_size=0, temp_dir=gettempdir(), giggle_app='giggle', bgzip_app='bgzip'):
    import sparkinferno.enrichment as enrichment
    
    annotateGADB_df_count = annotateGADB_df.count()
    grouped_annotateGADB_df = enrichment.group_summary_annotateGADB(annotateGADB_df, groupby_col_name='cell type')
    
    if method == 'empirical_pval' and annotateGADB_df_count > 0:
        if replicate_frequency == 0: replicate_frequency = 10000 ## Set default value

        tissue_summary_df = enrichment.empirical_pval_enrichment(bedfile, grouped_annotateGADB_df, groupby_col_name='cell type', replicate_frequency=replicate_frequency, window_size=window_size, temp_dir=temp_dir, giggle_app=giggle_app, bgzip_app=bgzip_app)
        tissue_summary_df = tissue_summary_df.drop('Total covered bases of GADB data source')
        
    elif method == 'fisher_exact' and annotateGADB_df_count > 0:
        tissue_summary_df = enrichment.fisher_exact_enrichment(bedfile, grouped_annotateGADB_df, groupby_col_name='cell type', N=N)
        
    else:
        tissue_summary_df = grouped_annotateGADB_df
        tissue_summary_df = tissue_summary_df.drop('Total covered bases of GADB data source')
    
    tissue_summary_df = tissue_summary_df.withColumnRenamed('cell type', 'Tissue')\
                                         .drop('Genome build', 'bedPath')\
                                         .distinct()\
                                         .withColumn('Data source', F.lit(data_source))

    return tissue_summary_df


def metaTable(input_based_file, gwas_tsv_path=None, partition_num=100, *dfs):
    # Meta-table: summary per interval across data sources/annotation types
    if not _is_path_exist(input_based_file, True): sys.exit(1)
    
    spark = this.spark
    input_based_file_name, input_based_file_ext = path.splitext(path.split(input_based_file)[1])
    if input_based_file_ext == '.bed':
        metatable = load_bedfile_as_rdd(input_based_file, partition_num=partition_num)\
                    .map(lambda y: Row(chr=y[0],
                                       chrStart=y[1],
                                       chrEnd=y[2],
                                       rsID=y[3],
                                       maf=y[4],
                                       snp_id=y[5],
                                       ref=y[6],
                                       alt=y[7],
                                       pval=y[8],
                                       region_pos=y[9],
                                       region_name=y[10],
                                       attribute=y[11])\
                                   if len(y) == 12 else \
                                   Row(chr=y[0],
                                       chrStart=y[1],
                                       chrEnd=y[2]))\
                    .toDF()
        if len(metatable.columns) == 12:
            metatable = metatable.select('chr', 'chrStart', 'chrEnd', 'rsID', 'maf', 'snp_id', 'ref', 'alt', 'pval', 'region_pos', 'region_name', 'attribute')
        else:
            metatable = metatable.select('chr','chrStart','chrEnd')
    
    elif input_based_file_ext == '.tsv':
        metatable = spark.read.format('csv').option('header', 'true').option('delimiter', "\t").load(input_based_file)
        metatable = metatable.withColumnRenamed(metatable.columns[0], 'chr')\
                             .withColumnRenamed(metatable.columns[1], 'chrStart')\
                             .withColumnRenamed(metatable.columns[2], 'chrEnd')
    
    ## Using GWAS summary stats file as the meta-table template instead of the BED file
    if bool(gwas_tsv_path):
        specified_column_name_path = path.join(path.split(gwas_tsv_path)[0], 'specified_column_names.tsv')
        if _is_path_exist(specified_column_name_path):
            gwas_col_list = open(specified_column_name_path, 'r').readline().rstrip().split("\t")
            gwas_df = spark.read.format('csv').option('header', 'true').option('delimiter', "\t").load(gwas_tsv_path).repartition(partition_num)
            selected_gwas_cols = gwas_col_list + ['1kg_rsID', '1kg_ref', '1kg_alt', '1kg_maf', 'normalized_ref', 'normalized_alt', 'normalized_maf', 'normalized_beta', 'normalized_effect_direction', 'QC_category']
            ## Remove absent columns from the list
            selected_gwas_cols = [c for c in selected_gwas_cols if c in gwas_df.columns]
            
            ## Select necessary columns and rename chr chrStart chrEnd
            gwas_df = gwas_df.select(selected_gwas_cols)\
                             .withColumnRenamed(selected_gwas_cols[0], 'chr')\
                             .withColumnRenamed(selected_gwas_cols[1], 'chrStart')\
                             .withColumnRenamed(selected_gwas_cols[2], 'chrEnd')
            
            ## Join metatable and gwas_df together
            if 'attribute' in metatable.columns:
                selected_metatable_cols = ['chr', 'chrStart', 'chrEnd', 'region_pos', 'region_name', 'attribute']
            else:
                selected_metatable_cols = ['chr', 'chrStart', 'chrEnd', 'region_pos', 'region_name']
            
            metatable = metatable.select(selected_metatable_cols)\
                                 .join(gwas_df, ['chr', 'chrStart', 'chrEnd'], 'left_outer')
            
            ## Rearrange the column order of the meta-table template and add the allele effect column
            rearranged_cols = ['chr', 'chrStart', 'chrEnd']
            last_gwas_col_num = selected_gwas_cols.index('1kg_rsID')
            if len(gwas_col_list) > 3: ## It means having specified columns
                ## Add "GWAS_" prefix
                rearranged_cols.extend([F.col(x).alias("GWAS_{}".format(x)) for x in selected_gwas_cols[3:last_gwas_col_num]])
            rearranged_cols.extend(['region_name', 'region_pos'])
            if 'attribute' in metatable.columns:
                rearranged_cols.append('attribute')
            rearranged_cols.extend(selected_gwas_cols[last_gwas_col_num:])
            metatable = metatable.select(rearranged_cols)\
                                 .withColumn('normalized_beta', F.col('normalized_beta').cast('double'))\
                                 .withColumn('allele effect', \
                                    F.when(F.col('normalized_beta')>0, F.lit('Risk'))\
                                    ## Normalized_beta can contain NA, thus using .otherwise(F.when())
                                    .otherwise(F.when(F.col('normalized_beta')<0, F.lit('Protective'))))
    
    ## Join tables from functional genomic overlaps
    if(len(dfs) > 0):
        for df in dfs:
            ## Remove duplicate columns of joined tables
            df = df.drop('rsID', 'ref', 'alt')
            
            metatable = metatable.join(df, ['chr', 'chrStart', 'chrEnd'], 'full')
        
    return metatable

    
def metaTable_from_files(input_based_file, folders, gwas_tsv_path=None, partition_num=100):
    # Meta-table: summary per interval across data sources/annotation types
    if not _is_path_exist(input_based_file, True): sys.exit(1)
    
    spark = this.spark
    input_based_file_name, input_based_file_ext = path.splitext(path.split(input_based_file)[1])
    if input_based_file_ext == '.bed':
        metatable = load_bedfile_as_rdd(input_based_file, partition_num=partition_num)\
                    .map(lambda y: Row(chr=y[0],
                                       chrStart=y[1],
                                       chrEnd=y[2],
                                       rsID=y[3],
                                       maf=y[4],
                                       snp_id=y[5],
                                       ref=y[6],
                                       alt=y[7],
                                       pval=y[8],
                                       region_pos=y[9],
                                       region_name=y[10],
                                       attribute=y[11])\
                                   if len(y) == 12 else \
                                   Row(chr=y[0],
                                       chrStart=y[1],
                                       chrEnd=y[2]))\
                    .toDF()
        if len(metatable.columns) == 12:
            metatable = metatable.select('chr', 'chrStart', 'chrEnd', 'rsID', 'maf', 'snp_id', 'ref', 'alt', 'pval', 'region_pos', 'region_name', 'attribute')
        else:
            metatable = metatable.select('chr', 'chrStart', 'chrEnd')
    
    elif input_based_file_ext == '.tsv':
        metatable = spark.read.format('csv').option('header', 'true').option('delimiter', "\t").load(input_based_file)
        metatable = metatable.withColumnRenamed(metatable.columns[0], 'chr')\
                             .withColumnRenamed(metatable.columns[1], 'chrStart')\
                             .withColumnRenamed(metatable.columns[2], 'chrEnd')
    
    ## Using GWAS summary stats file as the meta-table template instead of the BED file
    if bool(gwas_tsv_path):
        specified_column_name_path = path.join(path.split(gwas_tsv_path)[0], 'specified_column_names.tsv')
        if _is_path_exist(specified_column_name_path):
            gwas_col_list = open(specified_column_name_path, 'r').readline().rstrip().split("\t")
            gwas_df = spark.read.format('csv').option('header', 'true').option('delimiter', "\t").load(gwas_tsv_path).repartition(partition_num)
            selected_gwas_cols = gwas_col_list + ['1kg_rsID', '1kg_ref', '1kg_alt', '1kg_maf', 'normalized_ref', 'normalized_alt', 'normalized_maf', 'normalized_beta', 'normalized_effect_direction', 'QC_category']
            ## Remove absent columns from the list
            selected_gwas_cols = [c for c in selected_gwas_cols if c in gwas_df.columns]
            
            ## Select necessary columns and rename chr chrStart chrEnd
            gwas_df = gwas_df.select(selected_gwas_cols)\
                             .withColumnRenamed(selected_gwas_cols[0], 'chr')\
                             .withColumnRenamed(selected_gwas_cols[1], 'chrStart')\
                             .withColumnRenamed(selected_gwas_cols[2], 'chrEnd')
            
            ## Join metatable and gwas_df together
            if 'attribute' in metatable.columns:
                selected_metatable_cols = ['chr', 'chrStart', 'chrEnd', 'region_pos', 'region_name', 'attribute']
            else:
                selected_metatable_cols = ['chr', 'chrStart', 'chrEnd', 'region_pos', 'region_name']
            
            metatable = metatable.select(selected_metatable_cols)\
                                 .join(gwas_df, ['chr', 'chrStart', 'chrEnd'], 'left_outer')
            
            ## Rearrange the column order of the meta-table template and add the allele effect column
            rearranged_cols = ['chr', 'chrStart', 'chrEnd']
            last_gwas_col_num = selected_gwas_cols.index('1kg_rsID')
            if len(gwas_col_list) > 3: ## It means having specified columns
                ## Add "GWAS_" prefix
                rearranged_cols.extend([F.col(x).alias("GWAS_{}".format(x)) for x in selected_gwas_cols[3:last_gwas_col_num]])
            rearranged_cols.extend(['region_name', 'region_pos'])
            if 'attribute' in metatable.columns:
                rearranged_cols.append('attribute')
            rearranged_cols.extend(selected_gwas_cols[last_gwas_col_num:])
            metatable = metatable.select(rearranged_cols)\
                                 .withColumn('normalized_beta', F.col('normalized_beta').cast('double'))\
                                 .withColumn('allele effect', \
                                    F.when(F.col('normalized_beta')>0, F.lit('Risk'))\
                                    ## Normalized_beta can contain NA, thus using .otherwise(F.when())
                                    .otherwise(F.when(F.col('normalized_beta')<0, F.lit('Protective'))))
    
    ## Join tables from functional genomic overlaps
    if(len(folders) > 0):
        for folder in folders:
            if not _is_path_exist(folder, True): sys.exit(1)
            
            ## Load files and remove duplicate columns of joined tables
            df = spark.read.format('csv').option('header', 'true').option('delimiter', "\t").load(path.join(folder, 'part-*.tsv')).drop('rsID', 'ref', 'alt')
                        
            metatable = metatable.join(df, ['chr', 'chrStart', 'chrEnd'], 'full')
    
    return metatable

    
def metaTable_concordant(metatable, col_name1, col_name2):
    ## Generate the column of concordant GTEx category, i.e., 'gtex_Tissue category list' and 'chromHMM_enhancers_Tissue category list'
    if col_name1 in metatable.schema.names and col_name2 in metatable.schema.names:
        metatable = metatable.withColumn('concordant GTEx category', F.array_intersect(F.split(F.regexp_replace(metatable[col_name1], '\[|\]', ''), ', ').cast(ArrayType(StringType())), F.split(F.regexp_replace(metatable[col_name2], '\[|\]', ''), ', ').cast(ArrayType(StringType()))))
        ## Convert empty arrays to null
        metatable = metatable.withColumn('concordant GTEx category', F.when(F.size(metatable['concordant GTEx category']) == 0, F.lit(None)).otherwise(metatable['concordant GTEx category'].cast(StringType())))
        
    return metatable


def metaTable_locus_overlap_summary_stat(metatable):
    agg_headers = []
    for col_name in metatable.columns:
        if '_Number of overlaps' in col_name:
            agg_headers.append(col_name)
    
    exprs = [F.sum(x).cast('int').alias(x) for x in agg_headers]
    locus_overlap_summary_stat_df = metatable.select(['region_name']+agg_headers).groupBy('region_name').agg(*exprs)
    
    return locus_overlap_summary_stat_df.na.fill(0)

def coalesce_and_sort_saving_files(part_file_dir, output_file_path, ext='tsv', header=True, sort_by_coordinate=True, remove_part_file_dir=False, sort_by_col_num=0, temp_dir=gettempdir()):
    if not _is_path_exist(part_file_dir, True): sys.exit(1)
    cmds = list()
    if header:
        ## Using find instead of head/tail part-* directly to avoid too many part files lead to the "argument list too long" error from bash
        cmds.append("find %s -maxdepth 1 -type f ! -size 0 -name 'part-*.%s' -print -quit | xargs head -n 1 > %s" % (part_file_dir, ext, output_file_path)) ## Add a header from the first part file
        if sort_by_coordinate:
            cmds.append("find %s -maxdepth 1 -type f ! -size 0 -name 'part-*.%s' -print0 | xargs -0 tail -q -n+2 | LC_ALL=C sort -T %s -k1,1 -k2,2n -k3,3n >> %s" % (part_file_dir, ext, temp_dir, output_file_path))
        elif sort_by_col_num > 0:
            cmds.append("find %s -maxdepth 1 -type f ! -size 0 -name 'part-*.%s' -print0 | xargs -0 tail -q -n+2 | LC_ALL=C sort -T %s -k%s,%s >> %s" % (part_file_dir, ext, temp_dir, str(sort_by_col_num), str(sort_by_col_num), output_file_path))
        else:
            cmds.append("find %s -maxdepth 1 -type f ! -size 0 -name 'part-*.%s' -print0 | xargs -0 tail -q -n+2 >> %s" % (part_file_dir, ext, output_file_path))
    else:
        if sort_by_coordinate:
            cmds.append("find %s -maxdepth 1 -type f ! -size 0 -name 'part-*.%s' -print0 | xargs -0 tail -q -n+2 | LC_ALL=C sort -T %s -k1,1 -k2,2n -k3,3n > %s" % (part_file_dir, ext, temp_dir, output_file_path))
        elif sort_by_col_num > 0:
            cmds.append("find %s -maxdepth 1 -type f ! -size 0 -name 'part-*.%s' -print0 | xargs -0 tail -q -n+2 | LC_ALL=C sort -T %s -k%s,%s > %s" % (part_file_dir, ext, temp_dir, str(sort_by_col_num), str(sort_by_col_num), output_file_path))
        else:
            cmds.append("find %s -maxdepth 1 -type f ! -size 0 -name 'part-*.%s' -print0 | xargs -0 tail -q -n+2 > %s" % (part_file_dir, ext, output_file_path))
        
    for cmd in cmds:
        p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            sys.stderr.write("Fail.\nError %d: %s\n" % (p.returncode, stderr))
            sys.exit(1)

    if remove_part_file_dir: rmtree(part_file_dir)
    

def saveAnnotateGADB(overlap, dataSource, reportpath, delimiter = '@@@', remove_part_file_dir=False, temp_dir=gettempdir()):
    overlap = overlap.withColumn('additionalQueriedString', F.regexp_replace(overlap['additionalQueriedString'], "\t", delimiter)).withColumn('hitString', F.regexp_replace(overlap['hitString'], "\t", delimiter))
    part_file_dir_path = path.join(reportpath, dataSource, 'GADB_summary')
    gadb.save(overlap, part_file_dir_path)
    coalesce_and_sort_saving_files(part_file_dir_path, path.join(part_file_dir_path, '..', 'coalesced_GADB_summary.tsv'), remove_part_file_dir=remove_part_file_dir, temp_dir=temp_dir)


def saveInterval(interval_summary, dataSource, reportpath, remove_part_file_dir=False, temp_dir=gettempdir()):
    part_file_dir_path = path.join(reportpath, dataSource, 'interval_summary')
    gadb.save(interval_summary, part_file_dir_path)
    coalesce_and_sort_saving_files(part_file_dir_path, path.join(part_file_dir_path, '..', 'coalesced_interval_summary.tsv'), remove_part_file_dir=remove_part_file_dir, temp_dir=temp_dir)


def saveTrack(track_summary, dataSource, reportpath, remove_part_file_dir=False, temp_dir=gettempdir()):
    part_file_dir_path = path.join(reportpath, dataSource, 'track_summary')
    gadb.save(track_summary, part_file_dir_path)
    coalesce_and_sort_saving_files(part_file_dir_path, path.join(part_file_dir_path, '..', 'coalesced_track_summary.tsv'), sort_by_coordinate=False, remove_part_file_dir=remove_part_file_dir, temp_dir=temp_dir)


def saveTissue(tissues_summary, dataSource, reportpath, remove_part_file_dir=False, temp_dir=gettempdir()):
    part_file_dir_path = path.join(reportpath, dataSource, 'tissue_summary')
    gadb.save(tissues_summary, part_file_dir_path)
    coalesce_and_sort_saving_files(part_file_dir_path, path.join(part_file_dir_path, '..', 'coalesced_tissue_summary.tsv'), sort_by_coordinate=False, remove_part_file_dir=remove_part_file_dir, temp_dir=temp_dir)


def saveMetatable(metatable, reportpath, prefix='', remove_part_file_dir=False, temp_dir=gettempdir()):
    part_file_dir_path = path.join(reportpath, prefix+'metatable')
    gadb.save(metatable, part_file_dir_path)
    coalesce_and_sort_saving_files(part_file_dir_path, path.join(part_file_dir_path, '..', 'coalesced_metatable.tsv'), remove_part_file_dir=remove_part_file_dir, temp_dir=temp_dir)


def saveMotif(motif, dataSource, reportpath, remove_part_file_dir=False, temp_dir=gettempdir()):
    part_file_dir_path = path.join(reportpath, dataSource, 'motif')
    gadb.save(motif, part_file_dir_path)
    coalesce_and_sort_saving_files(part_file_dir_path, path.join(part_file_dir_path, '..', 'coalesced_motif_summary.tsv'), remove_part_file_dir=remove_part_file_dir, temp_dir=temp_dir)

def saveColocSummary(coloc, reportpath, remove_part_file_dir=False, temp_dir=gettempdir()):
    part_file_dir_path = path.join(reportpath, 'coloc_summary')
    gadb.save(coloc, part_file_dir_path)
    coalesce_and_sort_saving_files(part_file_dir_path, path.join(part_file_dir_path, '..', 'coalesced_coloc_summary.tsv'), remove_part_file_dir=remove_part_file_dir, temp_dir=temp_dir)


def loadColocSummary(file_name, reportpath):
    spark = this.spark
    coloc_summary_df = spark.read.format('csv').option('header', 'true').option('delimiter', "\t").load(path.join(reportpath, 'coloc_summary', file_name))

    return coloc_summary_df


def save_locus_overlap_summary_stat(gwas_locus_summary, reportpath, prefix='', remove_part_file_dir=False, temp_dir=gettempdir()):
    part_file_dir_path = path.join(reportpath, prefix+'locus_overlap_summary_stat')
    gadb.save(gwas_locus_summary, part_file_dir_path)
    coalesce_and_sort_saving_files(part_file_dir_path, path.join(part_file_dir_path, '..', 'coalesced_locus_overlap_summary_stat.tsv'), remove_part_file_dir=remove_part_file_dir, temp_dir=temp_dir)


def get_motif_overlap(giggledf, motif_pwm_file, hitString_schema=None, conf_path=''):
    from sparkinferno.motif_overlap import convert_pwm2dict, motif_overlap

    if not _is_path_exist(motif_pwm_file, True): sys.exit(1)

    spark = this.spark
    if hitString_schema == None:
        additionalQueriedString_schema = ('rsID', None, None, 'ref', 'alt')
        hitString_schema = ('motif_chr', 'motif_start', 'motif_end', 'motif_name', 'log_odds_score', 'motif_strand', 'motif_seq')
    pwm_dict = spark.sparkContext.broadcast(convert_pwm2dict(motif_pwm_file))
    bed_df = expand_columns_from_string(giggledf, ['additionalQueriedString', 'hitString'], [additionalQueriedString_schema, hitString_schema], ["\t", "\t"]).withColumn('motif_seq', F.upper(F.col('motif_seq'))).drop('additionalQueriedString', 'hitString', 'absPath')
    
    return motif_overlap(bed_df, pwm_dict, conf_path=conf_path)


def join_metatable_with_coloc_summary_stats(metatable_file, coloc_summary_stats_file, output_base_dir=None, remove_part_file_dir=False, temp_dir=gettempdir()):
    if not _is_path_exist(metatable_file, True): sys.exit(1)
    if not _is_path_exist(coloc_summary_stats_file, True): sys.exit(1)
    
    spark = this.spark
    metatable_df = spark.read.format('csv').option('header', 'true').option('delimiter', "\t").load(metatable_file)
    coloc_summary_stats_df = spark.read.format('csv')\
                                  .option('header', 'true')\
                                  .option('delimiter', "\t")\
                                  .load(coloc_summary_stats_file)\
                                  .withColumnRenamed('region_pos', 'coloc_region_pos')\
                                  .withColumnRenamed('region_name', 'coloc_region_name')\
                                  .withColumn('GTEx_gene-tissue', F.array('gene_symbol', 'tissue'))\
                                  .withColumn('GTEx_gene-tissue_category', F.array('gene_symbol', 'tissue_category'))\
                                  .withColumn('minor_allele_count_GTEx', F.array('gene_symbol', 'tissue_category', 'minor_allele_count_GTEx'))\
                                  .withColumn('NONREF_AF_GTEx', F.array('gene_symbol', 'tissue_category', 'NONREF_AF_GTEx'))\
                                  .withColumn('NONREF_beta_GTEx', F.array('gene_symbol', 'tissue_category', 'NONREF_beta_GTEx'))\
                                  .withColumn('lABF_GTEx', F.array('gene_symbol', 'tissue_category', 'lABF_GTEx'))\
                                  .withColumn('internal_sum_lABF', F.array('gene_symbol', 'tissue_category', 'internal_sum_lABF'))\
                                  .withColumn('target_gene_direction', F.array('gene_symbol', 'tissue_category', 'target_gene_direction'))\
                                  .withColumn('SNP_PP_H4', F.array('gene_symbol', 'tissue_category', 'SNP_PP_H4'))\
                                  .withColumn('nsnps', F.array('gene_symbol', 'tissue_category', 'nsnps'))\
                                  .withColumn('PP_H0_abf', F.array('gene_symbol', 'tissue_category', 'PP_H0_abf'))\
                                  .withColumn('PP_H1_abf', F.array('gene_symbol', 'tissue_category', 'PP_H1_abf'))\
                                  .withColumn('PP_H2_abf', F.array('gene_symbol', 'tissue_category', 'PP_H2_abf'))\
                                  .withColumn('PP_H3_abf', F.array('gene_symbol', 'tissue_category', 'PP_H3_abf'))\
                                  .withColumn('PP_H4_abf', F.array('gene_symbol', 'tissue_category', 'PP_H4_abf'))
    
    coloc_summary_stats_grouped_df = coloc_summary_stats_df\
    .groupby('snp_chr', 'snp_start', 'snp_end', 'coloc_region_pos', 'coloc_region_name')\
    .agg(F.first('ld_block_start').cast('string').alias('ld_block_start'), \
         F.first('ld_block_end').cast('string').alias('ld_block_end'), \
         F.collect_set('tissue').cast('string').alias('GTEx_distinct_tissue_list'), \
         F.countDistinct('tissue').alias('Number_of_distinct_GTEx_tissue'), \
         F.collect_set('tissue_category').cast('string').alias('GTEx_distinct_tissue_category_list'), \
         F.countDistinct('tissue_category').alias('Number_of_GTEx_distinct_tissue_category'), \
         F.collect_set('gene_symbol').cast('string').alias('GTEx_distinct_gene_symbol_list'), \
         F.collect_set('gene_id').cast('string').alias('GTEx_distinct_gene_id_list'), \
         F.countDistinct('gene_id').alias('Number_of_GTEx_distinct_gene_id'), \
         F.first('top_coloc_snp').alias('top_coloc_snp'), \
         F.collect_list('GTEx_gene-tissue').cast('string').alias('GTEx_gene-tissue pair list'), \
         F.collect_list('GTEx_gene-tissue_category').cast('string').alias('GTEx_gene-tissue_category pair list'), \
         F.first('pvalues_GWAS').cast('string').alias('pvalues_GWAS'), \
         F.first('NONREF_AF_GWAS').cast('string').alias('NONREF_AF_GWAS'), \
         F.first('NONREF_beta_GWAS').cast('string').alias('NONREF_beta_GWAS'), \
         F.first('lABF_GWAS').cast('string').alias('lABF_GWAS'), \
         F.collect_list('minor_allele_count_GTEx').cast('string').alias('minor_allele_count_GTEx'), \
         F.collect_list('NONREF_AF_GTEx').cast('string').alias('NONREF_AF_GTEx'), \
         F.collect_list('NONREF_beta_GTEx').cast('string').alias('NONREF_beta_GTEx'), \
         F.collect_list('lABF_GTEx').cast('string').alias('lABF_GTEx'), \
         F.collect_list('internal_sum_lABF').cast('string').alias('internal_sum_lABF'), \
         F.collect_list('target_gene_direction').cast('string').alias('target_gene_direction'), \
         F.collect_list('SNP_PP_H4').cast('string').alias('SNP_PP_H4'), \
         F.collect_list('nsnps').cast('string').alias('nsnps'), \
         F.collect_list('PP_H0_abf').cast('string').alias('PP_H0_abf'), \
         F.collect_list('PP_H1_abf').cast('string').alias('PP_H1_abf'), \
         F.collect_list('PP_H2_abf').cast('string').alias('PP_H2_abf'), \
         F.collect_list('PP_H3_abf').cast('string').alias('PP_H3_abf'), \
         F.collect_list('PP_H4_abf').cast('string').alias('PP_H4_abf'))
                                          
    return_df = metatable_df.join(coloc_summary_stats_grouped_df, (metatable_df.chr == coloc_summary_stats_grouped_df.snp_chr) & (metatable_df.chrStart == coloc_summary_stats_grouped_df.snp_start) & (metatable_df.chrEnd == coloc_summary_stats_grouped_df.snp_end) & (metatable_df.region_pos == coloc_summary_stats_grouped_df.coloc_region_pos) & (metatable_df.region_name == coloc_summary_stats_grouped_df.coloc_region_name), 'left_outer').drop('coloc_region_pos', 'coloc_region_name')
    
    if output_base_dir is not None:
        save_dir = path.join(output_base_dir, 'coloc_stats_metatable')
        gadb.save(return_df, save_dir)
        coalesce_and_sort_saving_files(save_dir, path.join(output_base_dir, 'coalesced_coloc_stats_metatable.tsv'), remove_part_file_dir=remove_part_file_dir, temp_dir=temp_dir)
    
    return return_df


def get_snp_and_tissue_category_combinations(input_bed, tissue_category_df, GADB_summary_folders, concordant_data_sources=[]):
    spark = this.spark
    
    if not isinstance(input_bed, RDD):
        input_bed = load_bedfile_as_rdd(input_bed)
    
    snp_df = input_bed\
             .map(lambda y: Row(chr=y[0],
                                chrStart=int(y[1]),
                                chrEnd=int(y[2]),
                                region_name=y[9])\
                            if len(y) == 12 else \
                            Row(chr=y[0],
                                chrStart=int(y[1]),
                                chrEnd=int(y[2]),
                                region_name="{}:{}-{}".format(y[0], y[1], y[2])))\
             .toDF()\
             .distinct()
    
    ## Make a template of all possible combinations from two dataframes
    all_snp_tissue_combination_df = snp_df.crossJoin(tissue_category_df).withColumn('count', F.lit(0))
    
    if(len(GADB_summary_folders) > 0):
        for GADB_summary_folder in GADB_summary_folders:
            if not _is_path_exist(GADB_summary_folder, True): sys.exit(1)
            
            ## Load files and remove duplicate columns of joined tables
            GADB_summary_df = spark.read.format('csv')\
                                        .option('header', 'true')\
                                        .option('delimiter', "\t")\
                                        .load(path.join(GADB_summary_folder, 'part-*.tsv'))\
                                        .withColumn('count', F.lit(1))\
                                        .select(all_snp_tissue_combination_df.columns)\
                                        .distinct()
                        
            all_snp_tissue_combination_df = all_snp_tissue_combination_df.union(GADB_summary_df)
    
    @F.udf(ArrayType(ArrayType(StringType())))
    def combination_list(x):
        return_combinations = []
        for i in range(2, len(x)+1):
            return_combinations.extend(list(y) for y in itertools.combinations(x, i))
            
        return return_combinations
    
    ## Get concordant tissues from different data sources
    if len(concordant_data_sources) > 0:
        all_snp_tissue_combination_df = all_snp_tissue_combination_df\
        .unionByName(\
            all_snp_tissue_combination_df.filter(( (F.col('count') > 0) & (F.col('Data Source').isin(concordant_data_sources)) ))\
            .groupBy('chr', 'chrStart', 'chrEnd', 'region_name', 'Tissue category')\
            .agg(\
                F.sum('count').alias('count'),
                F.collect_list('Data Source').alias('Data Source')
            )\
            .filter(F.col('count') > 1)\
            .withColumn('Data Source', F.explode(combination_list(F.col('Data Source'))))\
            .withColumn('count', F.size(F.col('Data Source')))\
            .select('chr', 'chrStart', 'chrEnd', 'region_name', F.array_sort(F.col('Data Source')).alias('Data Source'), 'Tissue category', 'count')\
            .withColumn('Data Source', F.concat_ws('+', 'Data Source'))\
        )
    
    return all_snp_tissue_combination_df


def add_concordant_tissue_into_metatable(snp_tissue_combination_df, metatable_df, concordant_data_sources=[]):
    concordant_data_sources.sort()
        
    for i in range(2, len(concordant_data_sources)+1):
        for data_source_combinations in itertools.combinations(concordant_data_sources, i):
            data_source_combination_str = '+'.join(data_source_combinations)
            
            metatable_df = metatable_df\
            .join(\
                snp_tissue_combination_df\
                    .filter(( (F.col('count') > 1) & (F.col('Data Source') == data_source_combination_str) ))\
                    .groupBy('chr', 'chrStart', 'chrEnd')\
                    .agg(\
                        F.count('Tissue category').alias('Number of concordant tissue category'),
                        F.collect_list('Tissue category').alias('Tissue category')
                    )\
                    .select('chr', 'chrStart', 'chrEnd', 
                        F.array_sort(F.col('Tissue category')).cast('string')\
                        .alias("Concordant tissue category ({})".format(data_source_combination_str)), 
                        F.col('Number of concordant tissue category').alias("Number of concordant tissue category ({})".format(data_source_combination_str))), 
                ['chr', 'chrStart', 'chrEnd'], 'left_outer')
    
    return metatable_df
    

def generate_tissue_category_list_for_tile_plot(snp_tissue_combination_df, output_file_path, data_sources=[], temp_dir=gettempdir()):
    snp_tissue_combination_df = snp_tissue_combination_df\
    .filter(F.col('Data Source').isin(data_sources))\
    .groupBy('region_name', 'Data Source', 'Tissue category')\
    .agg(F.sum(F.col('count')).alias('count'))\
    .filter((F.col('count') > 0))\
    .select(F.col('region_name'), F.col('Data Source').alias('data_source'), F.col('Tissue category').alias('tissue_category'))
    
    save_tmp_path = path.join(temp_dir, 'tissue_category_list')
    gadb.save(snp_tissue_combination_df, save_tmp_path)
    coalesce_and_sort_saving_files(save_tmp_path, output_file_path, sort_by_coordinate=False, remove_part_file_dir=True, sort_by_col_num=2,temp_dir=temp_dir)
    
    return output_file_path


def tissue_category_tile_plot(input_list_path, output_plot_path, rscript_app='Rscript'):
    if not _is_path_exist(input_list_path, True): sys.exit(1)
    
    cmd = [rscript_app, 
           path.join(path.dirname(path.abspath(__file__)), 'r_scripts', 'tissue_category_tile_plot.R'),
           input_list_path,
           output_plot_path]
           
    p = sp.Popen(cmd)
    p.wait()
    

def generate_genomic_element_list_for_bar_plot(input_bed, GADB_summary_df, output_file_path, temp_dir=gettempdir()):
    if not isinstance(input_bed, RDD):
        input_bed = load_bedfile_as_rdd(input_bed)
    
    snp_df = input_bed\
             .map(lambda y: Row(chr=y[0],
                                chrStart=int(y[1]),
                                chrEnd=int(y[2])))\
             .toDF()\
             .distinct()
    
    replace_dict = dict([('3utr_exon', "3'UTR exon"),
                         ('3utr_intron', "3'UTR intron"),
                         ('5utr_exon', "5'UTR exon"),
                         ('5utr_intron', "5'UTR intron"),
                         ('exon', 'Exon'),
                         ('intron', 'Intron'),
                         ('promoter', 'Promoter'),
                         ('lncRNA_exon', 'lncRNA exon'),
                         ('lncRNA_intron', 'lncRNA intron')])
    
    # replace_col_udf = F.udf(lambda x: replace_dict[x], StringType())
    @F.udf(StringType())
    def replace_col_udf(x):
        try:
            return replace_dict[x]
        except KeyError:
            return x
    
    genomic_element_list_df = snp_df\
    .join(GADB_summary_df.select('chr', 'chrStart', 'chrEnd', 'Element source category', 'Genomic element'), ['chr', 'chrStart', 'chrEnd'], 'left_outer')\
    .withColumn('Genomic element', \
        F.when(F.col('Genomic element').isNull(), F.lit('Intergenic'))\
         .otherwise(F.col('Genomic element')))\
    .withColumn('Genomic element', \
        F.when(F.col('Element source category')=='repeat', F.lit('Repeat'))\
         .otherwise(replace_col_udf(F.col('Genomic element'))))\
    .withColumnRenamed('Element source category', 'Element_source_category')\
    .withColumnRenamed('Genomic element', 'Genomic_element')\
    .distinct()
    
    save_tmp_path = path.join(temp_dir, 'genomic_element_list')
    gadb.save(genomic_element_list_df, save_tmp_path)
    coalesce_and_sort_saving_files(save_tmp_path, output_file_path, sort_by_coordinate=False, remove_part_file_dir=True, temp_dir=temp_dir)
    
    return output_file_path
    
    
def genomic_element_bar_plot(input_list_path, output_plot_path, rscript_app='Rscript'):
    if not _is_path_exist(input_list_path, True): sys.exit(1)
    
    cmd = [rscript_app, 
           path.join(path.dirname(path.abspath(__file__)), 'r_scripts', 'genomic_element_bar_plot.R'),
           input_list_path,
           output_plot_path]
           
    p = sp.Popen(cmd)
    p.wait()
    

def _is_path_exist(dir, error_msg=False):
    if path.exists(dir):
        return True
    else:
        if error_msg: sys.stderr.write(dir+': No such file or directory\n')
        return False


def _print_log_message(msg, level='info'):
    msg = str(msg).rstrip()
    
    try:
        logger_method = eval("this.logger.%s" % level)
    except AttributeError:
        logger_method = eval("logging.%s" % level)
    
    logger_method(msg)
