# -*- coding: utf-8 -*-
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import StringType
from pyspark.sql.functions import *
import os
import fnmatch


def load(spark, input):
    df = spark.read.option("sep", "\t").option("header", "true").csv(input)
    return df

def schema(df):
    df.printSchema()
    return df.schema.names

def select(df, column_status):

    dict = {}
    if "," in column_status:
        num = column_status.count(",")
        data = []
        collects = ""
        col = []
        for x in column_status.split(','):
            key = x.split(':')
            data.append(key[0])
            dict = {key[0] + ":" + key[1]}
            # print(dict)

            for key in dict:
                filtercol = key.split(':')[0]
                # print(filtercol)
                status = key.split(':')[1]
                # print(status)
                data = []
                val = ""
                count = 0


                if "|" in status:
                    for x in status.split("|"):
                        data.append(x)
                        count += 1
                    default = "`" + filtercol + "`" + " IN ('"
                    for i in range(count):
                        num = (i+1)
                        val += data[count - num] + "','"

                    s = default + val +"')"
                    s = s.replace( ",'')", ")")
                    #print(str(s))

                elif "&" in status:
                    for x in status.split("&"):
                        data.append(x)
                        count += 1
                        v = ""
                    default = "`" + filtercol + "`" + " = '"

                    for i in range(count):
                        num = (i + 1)
                        val = data[count - num] + "' and "
                        v += default + val
                    s = v
                    #print(str(v))

                else:
                    s = "`" + filtercol + "`" + " = '" + status + "'"
                    #print(str(s))

            col.append(str(s))
        # print(str(col).replace("' , '", " and ").replace("[", " ").replace("]", " ").replace('"', "").replace(", `",
                                                                                                              # " and `"))
        result = df.filter(str(col).replace("' , '", " and ").replace("[", " ").replace("]", " ").replace('"', "").replace(", `",
                                                                                                                  " and `"))


    else:
        filtercol = column_status.split(':')[0]
        status = column_status.split(':')[1]
        # print(filtercol)
        # print(status)
        data = []
        count = 0

        if "|" in status:

            for x in status.split("|"):
                data.append(x)
                count += 1
            s = "`" + filtercol + "`" + " IN ('" + data[count - 2] + "','" + data[count - 1] + "')"
            # print(s)
            result = df.filter(s)

        elif "&" in status:
            for x in status.split("&"):
                data.append(x)
                count += 1
            s = "`" + filtercol + "`" + " = '" + data[count - 2] + "' and " + "`" + filtercol + "`" + " = '" + data[
                count - 1] + "'"
            # print(s)
            result = df.filter(s)

        else:
            s = "`" + filtercol + "`" + " = '" + status + "'"
            # print(s)
            result = df.filter(s)

    return result


def getFields(df):
    return df.select('File name')


def getPaths(df):
    path = df.select('filepath').distinct().collect()
    return path

def getAbsPath(df):
    concat_udf = F.udf(lambda cols: "/".join([x if x is not None else "*" for x in cols]), StringType())
    giggleinput = df.withColumn("absPath", concat_udf(F.array("filepath", "File name")))
    # giggleinput.show(3)
    return giggleinput


def save(result, reportpath):
    # print("Generate report on " + reportpath)
    result.write.format('csv').option("sep", "\t").mode("overwrite").save(reportpath, header='true', nullValue='NA')
    # result.coalesce(1).write.format('csv').option("sep", "\t").mode("overwrite").save(reportpath, header='true')
    # result.repartition(1)\
          # .write\
          # .csv(reportpath, header="true", sep='\t', mode="overwrite")
    ## test if write.csv() worked:
    if not os.path.isfile(os.path.join(reportpath, '_SUCCESS')):
        return_code = 1
        print("return code = ", return_code)
        print("ERROR: Spark write.csv() failed")
        return return_code
    for file in os.listdir(reportpath):
        if fnmatch.fnmatch(file, '*.csv'):
            os.rename(reportpath+"/"+file, reportpath+"/"+file.replace('.csv', '.tsv'))


def selectFilters(df, filters):
    dict = {}
    for i in filters:
      data = select(df, i)
      filedname = getFields(data)
      filedpath = getPaths(data)
      gadbdf = getAbsPath(data)
      dict = {"i" + ":" + gadbdf}
      # print(dict)
    return gadbdf
