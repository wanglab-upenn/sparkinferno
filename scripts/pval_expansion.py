#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import path, makedirs, getcwd
import sys
import argparse
import pyspark
import sparkinferno.inferno as inferno
import sparkinferno.config as conf
import sparkinferno.preprocessing as preprocessing
import datetime
import logging
import re


def _is_path_exist(dir, error_msg=False):
    if path.exists(dir):
        return True
    else:
        if error_msg: logger.error(dir+': No such file or directory')
        return False

if __name__ == "__main__":
    usage = "%(prog)s <-t top_snp_list -g GWAS_summary_stats -c col# -p col# -A col# -a col# -P col#> [options]"
    parser = argparse.ArgumentParser(usage=usage, description='Expand tag SNPs by distance and significance')
    parser.add_argument('-t', '--top_snp_list', required=True, action='store', default=None, help='A formatted top SNPs list defined by users. This file is created by top_snp.py This is required. [Default: None]')
    parser.add_argument('-c', '--chr_col', type=int, required=True, help='The column number of chromosomes in the top SNP list (1-based, -c 1 indicates the first column). This is required.')
    parser.add_argument('-p', '--pos_col', type=int, required=True, help='The column number of SNP positions in the top SNP list (1-based). This is required.')
    parser.add_argument('-A', '--allele1_col', type=int, required=True, help='The column number of genomic refernece alleles in the top SNP list (1-based). This is required.')
    parser.add_argument('-a', '--allele2_col', type=int, required=True, help='The column number of alternative alleles in the top SNP list (1-based). This is required.')
    parser.add_argument('-P', '--pval_col', type=int, required=True, help='The column number of SNP p-values in the top SNP list (1-based). This is required.')
    parser.add_argument('-b', '--beta_col', type=int, help='The column number of SNP beta values in the top SNP list. Required to define expanded sets that all have consistent minor allele effect directions (1-based)')
    parser.add_argument('-s', '--se_col', type=int, help='The column number of standard errors in the top SNP list (1-based)')
    parser.add_argument('-f', '--af_col', type=int, help='The column number of SNP allele frequency values in the top SNP list (1-based)')
    parser.add_argument('-r', '--rsid_col', type=int, help='The column number of SNP rsIDs in the top SNP list (1-based)')
    parser.add_argument('-e', '--effect_dir_col', type=int, help='The column number of effect directions (1-based) for the GWAS meta-analysis in GWAS summary statistics')
    parser.add_argument('-g', '--gwas_summary', required=True, action='store', default=None, help='The file path of GWAS summary statistics. This is required. [Default: None]')
    parser.add_argument('-d', '--delimiter', action='store', default="\t", help='Split columns of GWAS summary stats using the delimiter [Default: "\\t"]')
    parser.add_argument('-L', '--locus_thresh', default=5e-8, type=float, help='The absolute p-value cutoff to use for locus-wide significance analysis (only one of sig_multiplier and locus_thresh may be specified) [Default: 5e-8]')
    parser.add_argument('-S', '--sig_multiplier', type=float, help='The multiplier range for significance to use (i.e. a value of 10 means one order of magnitude)')
    parser.add_argument('-D', '--dist_threshold', default=500000, type=int, help='How far away from each tag SNP to look, in base pair [Default: 500000]')
    parser.add_argument('-F', '--full_table', action='store_true', default=False, help='Output the full table of p-value expansion [Default: False]')
    parser.add_argument('--genome_wide_sig', action='store_true', default=False, help='Specify the input top SNP list is from genome-wide significant SNPs. This function helps to reset the region_pos as like as expanded snp_id in an output BED file. [Default: False]')
    parser.add_argument('-o', '--output_dir', action='store', default=getcwd(), help="The path to the desired output directory [Default: {}]".format(getcwd()))
    parser.add_argument('-B', '--output_bed', action='store_true', default=False, help='Save results as a BED file for downstream analysis of INFERNO2 [Default: False]')
    parser.add_argument('--config', action='store', help='Provide a specific configuration file to use')
    parser.add_argument('--prefix', action='store', default='', help='Add a prefix to ouptput files [Default: None]')
    parser.add_argument('--skip_logging', action='store_true', default=False, help='Skip logging messages as a file [Default: False]')


    ## Show help without providing any options
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
        
    pargs = parser.parse_args()
    
    ## Generate output dir
    if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    ## Process configuration file
    conf_file_in_output_dir = path.join(pargs.output_dir, 'sparkinferno.cfg')
    if _is_path_exist(conf_file_in_output_dir):
        ## sparkinferno.cfg is found at the output dir, the analysis has beed conducted before
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
        else:
            conf.read_config_file(conf_file_in_output_dir)
    else:
        ## A new started analysis, no existing configuration file is found in the output directory
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
        else:
            parser.error('The --config option is required for a new started analysis')
    
    ## Initialize the logging module
    this_script_name = path.splitext(path.basename(__file__))[0]
    logger = logging.getLogger(this_script_name)
    if not pargs.skip_logging:
        ## Set log path and initialize file handler
        log_dir = path.join(pargs.output_dir, 'log')
        if not _is_path_exist(log_dir): makedirs(log_dir)
        
        log_file_path = path.join(log_dir, "%s_%s.log" % (this_script_name, datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")))
        file_handler = logging.FileHandler(log_file_path)
        ## Set formatter
        file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
        logger.addHandler(file_handler)
    
    if not _is_path_exist(pargs.top_snp_list, True): sys.exit(1)
    if not _is_path_exist(pargs.gwas_summary, True): sys.exit(1)
    # if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    # pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    logger.info('Running command: %s' % ' '.join([re.sub(r'(^$| +)', r'"\1"', arg, count=1) for arg in sys.argv])) ## Show quotation marks between a space or null character. (Example: -d " " or --prefix "")
    
    summary_report_msg_list = ["[Summary Report of P-value Expansion]"]
    
    if pargs.prefix != '': pargs.prefix = pargs.prefix + '_'
    
    ## Get top SNP list header
    schema_list = open(pargs.top_snp_list, 'r').readline().rstrip().split("\t")
    
    ## Check if a user uses a wrong file as an input of top SNP list
    if ('rsID (optional)' in schema_list):
        logger.error('Found the unformatted top SNP list. Please perform top_snp.py first to obtain the formatted top SNP list by GWAS summary stats.')
        sys.exit(1)
    
    ## Convert column numbers from options with the "_col" suffix to *_col_name variables using schema_list to match up
    for parg in vars(pargs).keys():
        if parg[-4:] == '_col':
            parg_val = eval("pargs.%s" % parg)
            if parg_val is not None:
                ## e.g., chr_col_name = schema_list[pargs.chr_col-1]
                globals()["%s_name" % parg] = schema_list[parg_val-1]
            else:
                globals()["%s_name" % parg] = None
    
    kwargs = dict(
        chr_col_name = chr_col_name,
        pos_col_name = pos_col_name,
        af_col_name = af_col_name,
        rsid_col_name = rsid_col_name,
        allele1_col_name = allele1_col_name,
        allele2_col_name = allele2_col_name,
        pval_col_name = pval_col_name,
        beta_col_name = beta_col_name,
        se_col_name = se_col_name,
        effect_dir_col_name = effect_dir_col_name)
        
    spark, sc = inferno.make_spark_session()
    
    ## Step for processing the top SNPs file
    if 'snp_id' in schema_list:
        ## A formatted table by GWAS summary stats or 1000 Genomes
        top_snp_df = preprocessing.load_file_to_df(pargs.top_snp_list, **kwargs)    
    else:
        logger.error('Unknown format file')
        sys.exit(1)
    
    ## Load and convert GWAS summary stats to BED and index it if the file is not existing
    ## Get gwas_summary file name
    GWAS_summary_stats_file_name = path.splitext(pargs.gwas_summary)[0]
    bed_index_path = GWAS_summary_stats_file_name+'.bed.gz'
    if not _is_path_exist(bed_index_path+'.tbi'):    
        ## Load GWAS summary stats
        summary_stats_df = preprocessing.load_file_to_df(pargs.gwas_summary, delimiter = pargs.delimiter, **kwargs)
        # summary_stats_df.show(truncate=False)
        
        if af_col_name and beta_col_name:
            logger.info('Found allele frequency and beta value columns. Identifying variants with consistent effect directions.')
            ## Compare direction of beta value
            summary_ststs_dir_df = preprocessing.effect_direction(summary_stats_df, af_col_name=af_col_name, beta_col_name=beta_col_name)
            # summary_ststs_dir_df.show(truncate=False)
        else:
            logger.info('Allele frequency and/or beta value columns are not found. Identifying variants without consideration of effect direction.')
            summary_ststs_dir_df = summary_stats_df
            
        bed_path = preprocessing.summary_stats_df_to_bed(summary_ststs_dir_df, GWAS_summary_stats_file_name+'.bed', temp_dir=conf.TMP_PATH, tabix_app=conf.TABIX_APP, bgzip_app=conf.BGZIP_APP)
        # print(bed_path)
        
        summary_stats_df.unpersist(blocking=True)

    ## Start performing p-value expansion
    output_full_table_filename = None
    
    ## Verify significance thresholds
    if pargs.sig_multiplier is not None:
        if pargs.sig_multiplier < 1.0:
            logger.error('Invalid --sig_multiplier significance threshold (should be larger than 1)')
            sys.exit(1)
            
        pval_expansion_output_filename = path.join(pargs.output_dir, "%spval_expanded_snps.sig_multiplier_%.1f.tsv" % (pargs.prefix, pargs.sig_multiplier))
        
        if pargs.full_table:
            output_full_table_filename = path.join(pargs.output_dir, "%spval_expanded_snps.full_table.sig_multiplier_%.1f.tsv" % (pargs.prefix, pargs.sig_multiplier))
    else:
        if pargs.locus_thresh < 0.0 or pargs.locus_thresh > 1.0:
            logger.error('Invalid --locus_thresh significance threshold (should be between 0 and 1)')
            sys.exit(1)
            
        pval_expansion_output_filename = path.join(pargs.output_dir, "%spval_expanded_snps.locus_thresh_%.0e.tsv" % (pargs.prefix, pargs.locus_thresh))
        
        if pargs.full_table:
            output_full_table_filename = path.join(pargs.output_dir, "%spval_expanded_snps.full_table.locus_thresh_%.0e.tsv" % (pargs.prefix, pargs.locus_thresh))
    
    ## Get p-value expanded SNPs
    pval_expansion_df = preprocessing.pval_expansion(top_snp_df, bed_index_path, tabix_app=conf.TABIX_APP, pval_col_name=pval_col_name, beta_col_name=beta_col_name, dist_threshold=pargs.dist_threshold, sig_multiplier=pargs.sig_multiplier, locus_thresh=pargs.locus_thresh, effect_direction=True if bool(af_col_name) and bool(beta_col_name) else False).persist(pyspark.StorageLevel.DISK_ONLY)
    # pval_expansion_df.show(truncate=False)
    
    summary_report_msg_list.append("Identified %d unique expanded SNP(s) from %d top SNP(s)" % (pval_expansion_df.select('expanded_snp_id').distinct().count(), top_snp_df.select('snp_id').distinct().count()))
    logger.info(summary_report_msg_list[-1])
    
    top_snp_df.unpersist(blocking=True)
    
    ## Generate full table user specified before replacing the header
    if pargs.full_table:
        preprocessing.save_df(pval_expansion_df, output_full_table_filename, temp_dir=conf.TMP_PATH)
    
    ## Return expended_* + top_snp_region_pos columns and remove columns with an expanded_ or top_snp_ prefix
    selected_header_list = [h for h in pval_expansion_df.columns if 'expanded_' in h]
    result_df = pval_expansion_df.select(selected_header_list + ['top_snp_region_pos', 'top_snp_region_name'])
    for col_name in result_df.columns:
        result_df = result_df.withColumnRenamed(col_name, col_name.replace('expanded_', ''))
    
    result_df = result_df.drop('region_pos')\
                         .withColumnRenamed('top_snp_region_pos', 'region_pos')\
                         .withColumnRenamed('top_snp_region_name', 'region_name')\
                         .select(schema_list)\
                         .persist(pyspark.StorageLevel.DISK_ONLY)
            
    ## Save df as a file
    preprocessing.save_df(result_df, pval_expansion_output_filename, temp_dir=conf.TMP_PATH)
    
    pval_expansion_df.unpersist(blocking=True)
    
    ## Output BED format for downstream analysis of INFERNO2
    if pargs.output_bed:
        ## For genome-wide significant anslysis, reset region position for co-localization before saving a BED file
        if pargs.genome_wide_sig:
            result_df = preprocessing.reset_region_pos(result_df, 'snp_id')
            
        preprocessing.save_df_as_bed(result_df, pval_expansion_output_filename.replace('.tsv', '.bed'), af_col_name=af_col_name, rsid_col_name=rsid_col_name, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, pval_col_name=pval_col_name, temp_dir=conf.TMP_PATH)

    result_df.unpersist(blocking=True)
    
    ## Generate summary report
    open(path.join(pargs.output_dir, 'summary_report.txt'), 'w').write("\n".join(summary_report_msg_list)+"\n\n")
