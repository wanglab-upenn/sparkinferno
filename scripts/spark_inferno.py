#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import path, makedirs, getcwd, remove
import sys
import argparse
import subprocess as sp
import select
import gzip
from shutil import copyfile, copyfileobj
import datetime
import logging
from timeit import default_timer as timer
import re
import sparkinferno.config as conf
from tempfile import gettempdir


def _is_path_exist(dir, error_msg=False):
    if path.exists(dir):
        return True
    else:
        if error_msg: sys.stderr.write(dir+": No such file or directory\n")
        return False

def _option_dict2list(option_dict):
    return_list = list()
    for k, v in option_dict.items():
        if v is True:
            return_list.extend([k, ''])
        elif v is False:
            continue
        elif v is not None:
            return_list.extend([k, v])
    return return_list
        
def run_cmd(cmd):
    cmd = ' '.join([str(i) for i in cmd if i is not None])
    p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
    
    ## Display stdout and stderr simultaneously
    while True:
        reads = [p.stdout.fileno(), p.stderr.fileno()]
        ret = select.select(reads, [], [])
        for fd in ret[0]:
                if fd == p.stdout.fileno():
                    sys.stdout.write(p.stdout.readline().decode('utf-8')) 
                    sys.stdout.flush()
                if fd == p.stderr.fileno():
                    sys.stderr.write(p.stderr.readline().decode('utf-8')) 
                    sys.stderr.flush()
                    
        if p.poll() != None: break

def _get_converted_num_of_col_options(old_path, new_path):
    ## Load headers
    old_header_list = open(old_path, 'r').readline().rstrip().split(pargs.delimiter)
    new_header_list = open(new_path, 'r').readline().rstrip().split(pargs.delimiter if old_path == new_path else "\t") ## If given --skip_gwas_qc, old_path == new_path becomes True, use the same delimiter as the input GWAS file, otherwise use "\t" in the .qc.tsv file
    
    ## Make a mapping dictionary
    col_num_raw_to_formatted = {None: None}
    for parg in dir(pargs):
        if parg[-4:] == '_col':
            parg_val = eval("pargs.%s" % parg)
            if parg_val is not None:
                try:
                    ## e.g., chr_col_name = schema_list[pargs.chr_col-1]
                    col_num_raw_to_formatted[parg_val] = new_header_list.index(old_header_list[parg_val-1])+1
                except ValueError:
                    continue
    
    ## Generate a converted list with options and coorespoding column numbers and remove unspecified columns
    col_dict = {'-c': col_num_raw_to_formatted[pargs.chr_col], '-p': col_num_raw_to_formatted[pargs.pos_col], '-P': col_num_raw_to_formatted[pargs.pval_col], '-s': col_num_raw_to_formatted[pargs.se_col]}
    
    ## Apply the original allele1 and allele2 to reference and alternative alleles
    if pargs.skip_gwas_qc or pargs.skip_normalizing_allele:
        col_dict['-A'] = col_num_raw_to_formatted[pargs.allele1_col]
        col_dict['-a'] = col_num_raw_to_formatted[pargs.allele2_col]
    else: ## Apply normalized_ref and normalized_alt to the allele1 and allele2 columns
        col_dict['-A'] = new_header_list.index('normalized_ref')+1
        col_dict['-a'] = new_header_list.index('normalized_alt')+1
    
    ## Apply 1kg_maf from 1000 Genomes to the allele frequency column of input GWAS. Condition: --use_1kg_maf is given as well as --af_col and --skip_gwas_qc options are all NOT given
    if bool(pargs.use_1kg_maf) and not(bool(pargs.af_col) or bool(pargs.skip_gwas_qc)):
        col_dict['-f'] = new_header_list.index("{}_maf".format(pargs.data_source))+1
    ## Replace raw allele frequency with the normalized one
    elif bool(pargs.af_col) and not bool(pargs.skip_gwas_qc):
        col_dict['-f'] = new_header_list.index('normalized_maf')+1
    else: ## Apply the original allele frequency
        col_dict['-f'] = col_num_raw_to_formatted[pargs.af_col]
    
    ## Replace raw beta with normalized beta
    if bool(pargs.beta_col) and not bool(pargs.skip_gwas_qc):
        col_dict['-b'] = new_header_list.index('normalized_beta')+1
    else:
        col_dict['-b'] = col_num_raw_to_formatted[pargs.beta_col]
    
    ## Replace raw effect directions with normalized the normalized one
    if bool(pargs.effect_dir_col) and not bool(pargs.skip_gwas_qc):
        col_dict['-e'] = new_header_list.index('normalized_effect_direction')+1
    else:
        col_dict['-e'] = col_num_raw_to_formatted[pargs.effect_dir_col]
    
    ## Apply 1kg_rsID from 1000 Genomes to the rsID column of input GWAS. Condition: --use_1kg_rsid is given as well as --rsid_col and --skip_gwas_qc options are all NOT given
    if bool(pargs.use_1kg_rsid) and not(bool(pargs.rsid_col) or bool(pargs.skip_gwas_qc)):
        col_dict['-r'] = new_header_list.index("{}_rsID".format(pargs.data_source))+1
    else: ## Apply the original rsID
        col_dict['-r'] = col_num_raw_to_formatted[pargs.rsid_col]
    
    return col_dict


if __name__ == "__main__":
    usage = "%(prog)s <-g GWAS_summary_stats/-t top_snp_list -c col# -p col# -A col# -a col#> [options]"
    parser = argparse.ArgumentParser(usage=usage, description='Provide GWAS summary statistics and/or top (lead) SNP list to run the complete SparkINFERNO pipeline (GWAS QC, finding top/genome-wide SNPs, p-value expansion, LD pruning, LD expansion, INFERNO genomic overlap, and co-localization analysis).')
    ## Options for global
    parser.add_argument('-g', '--gwas_summary', action='store', default=None, help='The file path of GWAS summary statistics [Default: None]')
    parser.add_argument('-c', '--chr_col', type=int, help='The column number of chromosomes (1-based, -c 1 indicates the first column) in GWAS summary statistics. This is required.')
    parser.add_argument('-p', '--pos_col', type=int, help='The column number of SNP positions (1-based) in GWAS summary statistics. This is required.')
    parser.add_argument('-A', '--allele1_col', type=int, help='The column number of effect alleles (1-based) in GWAS summary statistics. This is required.')
    parser.add_argument('-a', '--allele2_col', type=int, help='The column number of non-effect alleles (1-based) in GWAS summary statistics. This is required.')
    parser.add_argument('-P', '--pval_col', type=int, help='The column number of SNP p-values (1-based) in GWAS summary statistics. Required for identification of genome-wide significant SNPs, p-value expansion step, and co-localization analysis.')
    parser.add_argument('-b', '--beta_col', type=int, help='The column number of SNP beta values (1-based) in GWAS summary statistics. Required to define expanded sets that all have consistent minor allele effect directions.')
    parser.add_argument('-s', '--se_col', type=int, help='The column number of standard errors (1-based) in GWAS summary statistics')
    parser.add_argument('-f', '--af_col', type=int, help='The column number of SNP Allele frequency values (1-based) in GWAS summary statistics. Required for GWAS QC step if the --skip_check_MAF or --skip_check_ambiguous option is not given.')
    parser.add_argument('-r', '--rsid_col', type=int, help='The column number of SNP rsIDs (1-based) in GWAS summary statistics')
    parser.add_argument('-e', '--effect_dir_col', type=int, help='The column number of effect directions (1-based) for the GWAS meta-analysis in GWAS summary statistics')
    parser.add_argument('-d', '--delimiter', action='store', default="\t", help='Split columns of the GWAS summary statistics using the delimiter [Default: "\\t"]')
    parser.add_argument('-t', '--top_snp_list', action='store', default=None, help='Input a user-defined top SNPs list. The columns MUST contain "chr", "pos" and "region_name" columns. Providing optional columns such as "allele1" and "allele2" or "rsID" contributes to distinguish corresponding SNPs precisely. Using the -C/--create_template option to create a list template. [Default: None]')
    parser.add_argument('-R', '--rsid_only', action='store_true', default=False, help='The top SNP list contains rsID only [Default: False]')
    parser.add_argument('-H', '--no_header', action='store_true', default=False, help='The top SNP list DO NOT contain a header [Default: False]')
    parser.add_argument('-G', '--genome_build', action='store', default='hg19', help='The version of human genome reference [Default: hg19]')
    parser.add_argument('--gtex_version', action='store', choices=['v7', 'v8'], default='v7', help='The GETx version using in INFERNO genomic overlap and co-localization analysis steps [Default: v7]')
    parser.add_argument('-o', '--output_dir', action='store', default=getcwd(), help="The path to the desired output directory [Default: {}]".format(getcwd()))
    parser.add_argument('--prefix', action='store', default='', help='Add a prefix to ouptput files [Default: None]')
    parser.add_argument('--skip_logging', action='store_true', default=False, help='Skip logging messages as a file [Default: False]')
    parser.add_argument('--config', action='store', help='Provide a specific configuration file to use. This option requires --annotation_metafile to be set simultaneously in a new started analysis.')
    parser.add_argument('--annotation_metafile', action='store', help='Specify a path of annotation metafile and save into the configuration file. This option requires --config to be set simultaneously in a new started analysis.')
    parser.add_argument('--tmp_dir', action='store', help='Specify a path of the temp directory [Default: OUTPUT_DIR/tmp]')
    parser.add_argument('--annotation_dir', action='store', help=argparse.SUPPRESS)
    parser.add_argument('--keep_intermediate_files', action='store_true', default=False, help='Keep intermediate files/folders. Note: This function requires additional disk space. [Default: False]')
    parser.add_argument('--replace_dot', action='store_true', default=False, help='Replace all dots in the header with underlines in place [Default: False]')
    parser.add_argument('--version', action='store_true', default=False, help='Show SparkINFERNO version number')
    
    ## Options for GWAS QC
    group_qc = parser.add_argument_group('GWAS quality control')
    group_qc.add_argument('--skip_check_negative_strand', action='store_true', default=False, help='Skip checking negative-strand SNPs [Default: False]')
    group_qc.add_argument('--skip_check_ambiguous', action='store_true', default=False, help='Skip checking SNPs with a strand-independent allele-pair (i.e. A/T or C/G SNPs [Default: False]')
    group_qc.add_argument('--skip_check_MAF', action='store_true', default=False, help='Skip checking allele frequency between observed and reference SNPs [Default: False]')
    group_qc.add_argument('--skip_normalizing_allele', action='store_true', default=False, help='Skip normalizing alleles (i.e., resolving reference and alternative alleles). By default, the QC step performs the process of allele normalization via the information from SNPs of 1000 Genomes Project or genome reference (for those SNPs with tagged "SNP not found" or "Unmatchable allele") and then generate "normalized_ref" and "normalized_alt" columns in the output. [Default: False]')
    group_qc.add_argument('--diff_MAF_threshold', default=0.15, type=float, help='Allowed difference between observed and reference allele frequency [Default: 0.15]')
    group_qc.add_argument('--data_source', action='store', choices=['1kg', 'dbsnp'], default='1kg', help='Database for matching up with 1000 Genomes Project (1kg) or dbSNP (Not support yet) [Default: 1kg]')
    group_qc.add_argument('--super_population_qc', action='store', choices=['GLOBAL', 'AFR', 'AMR', 'EAS', 'EUR', 'SAS'], default='GLOBAL', help='Set "GLOBAL" for all populations or a specific super population of 1000 Genomes data. Five super populations are provided: AFR (African), AMR (Ad Mixed American), EAS (East Asian), EUR (European) and SAS (South Asian). [Default: GLOBAL]')
    group_qc.add_argument('--use_1kg_maf', action='store_true', default=False, help='Using minor allele frequency from 1000 Genomes as GWAS input. This function supports that the input GWAS summary statistics without allele frequency information can conduct colocalization analysis as well by using allele frequency from 1000 Genomes instead. Activate this option must meet the condition that --af_col/-f and --skip_gwas_qc options are all NOT given. [Default: False]')
    group_qc.add_argument('--use_1kg_rsid', action='store_true', default=False, help='Using rsID from 1000 Genomes as GWAS input. Activate this option must meet the condition that --rsid_col/-r and --skip_gwas_qc options are all NOT given. [Default: False]')
    group_qc.add_argument('-x', '--exclude_unspecified_cols', action='store_true', default=False, help='Skip reading unspecified columns [Default: False]')
    group_qc.add_argument('-X', '--exclude_bed', action='store', default=None, help='Provide a BED format file containing excluded regions [Default: None]')
    
    ## Options for top SNP analysis
    group_top_snp = parser.add_argument_group('Output a formatted TSV file of top SNPs as an input for downstream GWAS pre-processing')
    group_top_snp.add_argument('-T', '--top_snp_thresh', default=5e-8, type=float, help='The absolute p-value cutoff to use for genome-wide significance analysis [Default: 5e-8]')
    group_top_snp.add_argument('-C', '--create_template', action='store_true', default=False, help='Create an empty template of top SNP list at OUTPUT_DIR/user_top_snp_list.tsv [Default: False]')
    
    ## Options for p-value expansion
    group_pval_expansion = parser.add_argument_group('Expand tag SNPs by distance and significance')
    group_pval_expansion.add_argument('-L', '--locus_thresh', default=5e-8, type=float, help='The absolute p-value cutoff to use for locus-wide significance analysis (only one of sig_multiplier and locus_thresh may be specified) [Default: 5e-8]')
    group_pval_expansion.add_argument('-S', '--sig_multiplier', type=float, help='The multiplier range for significance to use (i.e. a value of 10 means one order of magnitude)')
    group_pval_expansion.add_argument('-D', '--dist_threshold', default=500000, type=int, help='How far away from each tag SNP to look, in base pair [Default: 500000]')
    group_pval_expansion.add_argument('-F', '--full_table', action='store_true', default=False, help='Output the full table of p-value expansion [Default: False]')
    group_pval_expansion.add_argument('--pval_expansion_gw', action='store_true', default=False, help='Run p-value expansion step in genome-wide significant analysis [Default: False]')

    ## Options for LD pruning
    group_ld_pruning = parser.add_argument_group('Identify independent signals/loci using linkage disequilibrium (LD) pruning')
    group_ld_pruning.add_argument('--ld_threshold', default=0.7, type=float, help='The r2 threshold for LD pruning and expansion [Default: 0.7]')
    group_ld_pruning.add_argument('--window_size_kb', default='500kb', help='How far away from each SNP to prune [Default: 500kb]')
    group_ld_pruning.add_argument('--super_population_vcf', action='store', choices=['AFR', 'AMR', 'EAS', 'EUR', 'SAS'], default='EUR', help='Specify a super population of 1000 Genomes genotype VCF file for LD pruning and expansion. Five super populations are provided: AFR (African), AMR (Ad Mixed American), EAS (East Asian), EUR (European) and SAS (South Asian). [Default: EUR]')
    group_ld_pruning.add_argument('-q', '--quiet', action='store_true', default=False, help='Silence the message from PLINK [Default: False]')
    
    ## Options for LD expansion
    group_ld_expansion = parser.add_argument_group('LD expansion by PLINK')
    group_ld_expansion.add_argument('--window_size', default=500000, type=int, help='How far away from each SNP to expand [Default: 500000]')
    
    ## Options for INFERNO genomic overlap
    group_genomic_overlap = parser.add_argument_group('Perform the INFERNO pipeline for parallelized array/collection of genomic analyses')
    group_genomic_overlap.add_argument('--fantom5_window_size', default=1000, type=int, help='How far away from each SNP to expand for FANTOM5 overlaps [Default: 1000]')
    group_genomic_overlap.add_argument('--replicate_frequency', default=1000, type=int, help='How many times of simulation in the empirical test [Default: 1000]')
    group_genomic_overlap.add_argument('--enrichment_method', action='store', default='empirical_pval', help='Support empirical p-value (empirical_pval) or Fisher\'s exact test (fisher_exact) method in enrichment analysis [Default: empirical_pval]')
    group_genomic_overlap.add_argument('--skip_fantom5', action='store_true', default=False, help='Skip FANTOM5 overlap [Default: False]')
    group_genomic_overlap.add_argument('--skip_gtex', action='store_true', default=False, help='Skip GTEx overlap [Default: False]')
    group_genomic_overlap.add_argument('--skip_chromhmm', action='store_true', default=False, help='Skip ChromHMM enhancers overlap [Default: False]')
    group_genomic_overlap.add_argument('--skip_genomic_partition', action='store_true', default=False, help='Skip INFERNO genomic partition overlap [Default: False]')
    group_genomic_overlap.add_argument('--skip_targetscan', action='store_true', default=False, help='Skip TargetScan overlap [Default: False]')
    group_genomic_overlap.add_argument('--skip_dashr2', action='store_true', default=False, help='Skip DASHR2 overlap [Default: False]')
    group_genomic_overlap.add_argument('--skip_homer', action='store_true', default=False, help='Skip Homer transcription factor binding sites overlap [Default: False]')
    group_genomic_overlap.add_argument('--skip_enrichment', action='store_true', default=False, help='Skip enrichment analysis in track and tissue summaries [Default: False]')
    group_genomic_overlap.add_argument('--skip_genomic_element_plot', action='store_true', default=False, help='Skip to generate a bar plot for genomic elements [Default: False]')
    group_genomic_overlap.add_argument('--skip_tissue_category_plot', action='store_true', default=False, help='Skip to generate a tile plot for tissue category [Default: False]')
    
    ## Options for co-localization analysis
    group_colocalization = parser.add_argument_group('Run co-localization analysis between GWAS and GTEx eQTL')
    group_colocalization.add_argument('--sample_size', type=int, help='Number of samples in GWAS. This is required unless --skip_coloc option is given.')
    group_colocalization.add_argument('--analysis_type', action='store', choices=['quant', 'cc'], default='quant', help='The data type of the GWAS dataset. Choosing "quant" or "cc" type denotes GWAS data is quantitative or case-control type, respectively. When specified as the cc type, the --sample_proportion option for a case/control proportion of samples MUST be provided, simultaneously. [Default: quant]')
    group_colocalization.add_argument('--sample_proportion', type=float, help='Number of sample proportion in GWAS')
    group_colocalization.add_argument('--expansion_type', action='store', choices=['LD_block', 'constant'], default='LD_block', help='Define how to obtain expanded SNPs from each tag SNP. This option can be set as "LD_block" or "constant", the former represents using the length of LD block to expand SNPs from GWAS; the latter represents using a constant value to expand. [Default: LD_block]')
    group_colocalization.add_argument('--expansion_window_size', default=500000, type=int, help='How far away from each tag SNP to expand. This option is enabled when the --expansion_type option is set "constant." [Default: 500000]')
    group_colocalization.add_argument('--h4_cutoff', default=0.5, type=float, help='The cutoff value of locus posterior probabilities of H4 (one common causal variant) in coloc summary stats [Default: 0.5]')
    group_colocalization.add_argument('--abf_cutoff', default=0.5, type=float, help='The cutoff for a cumulative value of the SNP posterior probabilities of H4 [Default: 0.5]')
    group_colocalization.add_argument('--nsnps_cutoff', default=5, type=int, help='The cutoff of SNP number in a SNP-gene association [Default: 5]')
    group_colocalization.add_argument('--gzip', action='store_true', default=False, help='Compress input and output files for colocalization analysis [Default: False]')
    
    ## Options for skipping specified steps
    group_skip_options = parser.add_argument_group('Options for skipping specified steps')
    group_skip_options.add_argument('--skip_gwas_qc', action='store_true', default=False, help='Skip GWAS QC step. Strongly recommend that DO NOT skip this step. [Default: False]')
    group_skip_options.add_argument('--skip_top_snp', action='store_true', default=False, help='Skip identifying top SNP step [Default: False]')
    group_skip_options.add_argument('--skip_pval_expansion', action='store_true', default=False, help='Skip p-value expansion step when giving top SNP list and GWAS summary statistics files [Default: False]')
    group_skip_options.add_argument('--skip_ld_pruning', action='store_true', default=False, help='Skip LD pruning step [Default: False]')
    group_skip_options.add_argument('--skip_ld_expansion', action='store_true', default=False, help='Skip LD expansion step [Default: False]')
    group_skip_options.add_argument('--skip_genomic_overlap', action='store_true', default=False, help='Skip INFERNO genomic overlap pipeline [Default: False]')
    group_skip_options.add_argument('--skip_coloc', action='store_true', default=False, help='Skip co-localization analysis [Default: False]')

    ## Show help without providing any options
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
    
    pargs = parser.parse_args()
    script_dir = path.dirname(__file__)
    
    ## Show version number
    if pargs.version:
        import sparkinferno
        print(sparkinferno.__version__)
        sys.exit(0)
        
    ## Generate output dir
    if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    ## Process configuration file
    conf_file_in_output_dir = path.join(pargs.output_dir, 'sparkinferno.cfg')
    if _is_path_exist(conf_file_in_output_dir):
        ## sparkinferno.cfg is found at the output dir, the analysis has beed conducted before
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
        else:
            conf.read_config_file(conf_file_in_output_dir)
            pargs.config = conf_file_in_output_dir
    else:
        ## A new started analysis, no existing configuration file is found in the output directory
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
        else:
            parser.error('The --config option is required for a new started analysis')
    
    ## Create an empty top SNP list only
    if bool(pargs.create_template):
        cmd = [conf.PYTHON_APP, 
               path.join(script_dir, 
               'top_snp.py'), 
               '-C', 
               '-o', 
               pargs.output_dir, 
               '--config',
               pargs.config
              ]
        run_cmd(cmd)
        sys.exit(0)
    
    ## Generate default_tmp_dir variable. Prioritization: pargs.tmp_dir > TMP_PATH (!= gettempdir()) in the existing configuration file > pargs.output_dir/tmp. The reason of TMP_PATH != gettempdir() is that TMP_PATH is set the default directory as gettempdir() by config module if TMP_PATH is empty in the configuration file.
    if bool(pargs.tmp_dir):
        default_tmp_dir = pargs.tmp_dir
    else:
        ## Set default_tmp_dir = OUTPUT_DIR/tmp 
        default_tmp_dir = path.join(pargs.output_dir, 'tmp') if conf.TMP_PATH == gettempdir() else conf.TMP_PATH
    if not _is_path_exist(default_tmp_dir): makedirs(default_tmp_dir)
    conf.set_parameter('TMP_PATH', default_tmp_dir)
    ## Replace default SPARK temporary directory with default_tmp_dir
    new_spark_conf = []
    for var, val in conf.SPARK_CONF[:]:
        if var == 'spark.local.dir':
            new_spark_conf.append((var, default_tmp_dir))
        else:
            new_spark_conf.append((var, val))
    conf.set_parameter('SPARK_CONF', new_spark_conf)

    ## Process annotation meta file
    if bool(pargs.annotation_metafile):
        if not _is_path_exist(pargs.annotation_metafile, True): sys.exit(1)
        ## Set the GADB_META_FILE parameter as pargs.annotation_metafile in configuration file
        conf.set_parameter('GADB_META_FILE', pargs.annotation_metafile)
    else:
        if conf.GADB_META_FILE == '':
            parser.error('The --annotation_metafile option is required for a new started analysis')
    
    ## Check all paths in the configuration file are available
    if not conf.check_config: sys.exit(1)
    
    ## Confirm input files
    if pargs.top_snp_list is not None:
        pargs.top_snp_list = path.abspath(path.expanduser(pargs.top_snp_list))
        if not _is_path_exist(pargs.top_snp_list, True): sys.exit(1)
    if pargs.gwas_summary is not None:
        pargs.gwas_summary = path.abspath(path.expanduser(pargs.gwas_summary))
        if not _is_path_exist(pargs.gwas_summary, True): sys.exit(1)
    if (pargs.skip_coloc is False) and (pargs.sample_size is None) and bool(pargs.gwas_summary):
        parser.error('The --sample_size option for co-localization analysis is required')
    if (pargs.skip_coloc is False) and (pargs.analysis_type == 'cc') and (pargs.sample_proportion is None) and bool(pargs.gwas_summary):
        parser.error('The --sample_proportion option is required when case-control analysis type specified')
    
    ## Initialize the logging module
    import sparkinferno.inferno as inferno  ## Import is necessary to trigger logging.basicConfig
    this_script_name = path.splitext(path.basename(__file__))[0]
    logger = logging.getLogger(this_script_name)
    if not pargs.skip_logging:
        ## Set log path and initialize file handler
        log_dir = path.join(pargs.output_dir, 'log')
        if not _is_path_exist(log_dir): makedirs(log_dir)
        
        log_file_path = path.join(log_dir, "%s_%s.log" % (this_script_name, datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")))
        file_handler = logging.FileHandler(log_file_path)
        ## Set formatter
        file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
        logger.addHandler(file_handler)
    
    if (not pargs.skip_gtex) and (pargs.genome_build == 'hg19') and (pargs.gtex_version == 'v8'):
        logger.warning("The GTEx v8 doesn't support hg19, use the GTEx version v7 by default.")
        pargs.gtex_version = 'v7'
    elif (not pargs.skip_gtex) and (pargs.genome_build == 'hg38') and (pargs.gtex_version != 'v8'):
        logger.warning("The GTEx %s doesn't support hg38, use the GTEx version v8 by default." % pargs.gtex_version)
        pargs.gtex_version = 'v8'
        
    ## Prepare additional necessary files and copy them to pargs.output_dir/.data/ for sharing with SPARK workers. Considering the cluster mode, SparkINFERNO module might not install at the same position as the driver. 
    shared_data_dir = path.join(pargs.output_dir, '.data')
    if not _is_path_exist(shared_data_dir): makedirs(shared_data_dir)
    for var_name in [v for v in dir(conf) if '_FILE' in v]:
        file_from = eval("conf.%s" % var_name)
        
        if var_name == 'GTEX_SAMPLE_SIZE_FILE':
            file_from = file_from.replace('*', pargs.gtex_version)
        
        file_to = path.join(shared_data_dir, path.basename(file_from))
        copyfile(file_from, file_to)
        conf.set_parameter(var_name, file_to)
        
    ## Write a configuration file to the output directory and use this file in all analysis scripts
    pargs.config = conf.write_config_file(pargs.output_dir)
    
    ## Start the pipeline
    logger.info("Running command: %s" % ' '.join([re.sub(r'(^$| +)', r'"\1"', arg, count=1) for arg in sys.argv])) ## Show quotation marks between a space or null character. (Example: -d " " or --prefix "")
    timeit_script_start = timer() ## Estimate totally elapsed time
    
    ## Step number for output directories and output path dictionary
    step_number = 1
    output_dir_dict = {}
    ## This list uses for saving the paths of output TSV files followed by calculating numbers of SNPs in each step
    step_output_tsv_list = []
    
    if bool(pargs.gwas_summary):
        ## Gunzip the input GWAS file
        gwas_summary_file_name = path.splitext(path.basename(pargs.gwas_summary))[0]
        if path.splitext(pargs.gwas_summary)[1] == '.gz':
            logger.info('Uncompressing gzip file')
            timeit_gunzip_start = timer()
            
            input_gwas_file = path.join(pargs.output_dir, gwas_summary_file_name)
            with gzip.open(pargs.gwas_summary, 'rb') as fp_in, open(input_gwas_file, 'wb') as fp_out:
                copyfileobj(fp_in, fp_out)
                
            pargs.gwas_summary = input_gwas_file
            gwas_summary_file_name = path.splitext(path.basename(gwas_summary_file_name))[0] ## Remove the file ext before .gz to get the basename
            
            logger.info("Uncompress done. Time elapsed: %.2f sec." % (timer()-timeit_gunzip_start))
    
        ## Check dots in the header
        header_str = open(pargs.gwas_summary, 'r').readline().rstrip()
        if re.search(r'\.', header_str):
            if pargs.replace_dot:
                logger.info('Replace all dot characters in the header with underline characters in place.')
                run_cmd(["sed -i '1s/%s/%s/g' %s" % ('\.', '_', pargs.gwas_summary)])
            else:
                logger.error('Found dot characters in the header, add the --replace_dot option to replace with underline characters in place.')
                sys.exit(1)
        
        ## Run GWAS QC
        if pargs.skip_gwas_qc is False:
            logger.info('Running GWAS QC')
            timeit_gwas_qc_start = timer()
        
        output_dir_dict['GWAS QC'] = path.join(pargs.output_dir, "{}_GWAS_QC".format(step_number))
        
        option_dict = {'-g': pargs.gwas_summary, 
                       '-c': pargs.chr_col, 
                       '-p': pargs.pos_col, 
                       '-A': pargs.allele1_col, 
                       '-a': pargs.allele2_col, 
                       '-f': pargs.af_col, 
                       '-P': pargs.pval_col, 
                       '-b': pargs.beta_col, 
                       '-s': pargs.se_col, 
                       '-r': pargs.rsid_col, 
                       '-e': pargs.effect_dir_col, 
                       '-o': output_dir_dict['GWAS QC'], 
                       '--skip_normalizing_allele': pargs.skip_normalizing_allele, 
                       '--diff_MAF_threshold': pargs.diff_MAF_threshold, 
                       '--skip_check_negative_strand': pargs.skip_check_negative_strand, 
                       '--genome_build': pargs.genome_build, 
                       '--data_source': pargs.data_source, 
                       '-S': pargs.super_population_qc, 
                       '--qc_available': True, 
                       '--exclude_unspecified_cols': pargs.exclude_unspecified_cols, 
                       '-X': pargs.exclude_bed, 
                       '--config': pargs.config, 
                       '--prefix': "'{}'".format(pargs.prefix), 
                       '--skip_logging': pargs.skip_logging}
        
        if pargs.af_col is None:
            option_dict['--skip_check_ambiguous'] = True
            option_dict['--skip_check_MAF'] = True
            logger.info('The -f/--af_col option is not specified, automatically adding --skip_check_ambiguous and --skip_check_MAF options when applying gwas_qc.py')
        
        else:
            option_dict['--skip_check_ambiguous'] = pargs.skip_check_ambiguous
            option_dict['--skip_check_MAF'] = pargs.skip_check_MAF
        
        cmd = [conf.PYTHON_APP, path.join(script_dir, 'gwas_qc.py')]
        cmd.extend(_option_dict2list(option_dict))
        if pargs.delimiter != "\t": cmd.extend(['-d', "'{}'".format(pargs.delimiter)])
        
        if pargs.skip_gwas_qc is False:
            run_cmd(cmd)
            step_number += 1
            logger.info("GWAS QC done. Time elapsed: %.2f sec." % (timer()-timeit_gwas_qc_start))
            
            ## Expect output file name
            gwas_summary_from_gwas_qc = path.join(output_dir_dict['GWAS QC'], "%s%s.qc_available.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, gwas_summary_file_name))
        
        else:
            ## Directly using raw GWAS summary statistics file as an input for the downstream analyses if skipping this step
            gwas_summary_from_gwas_qc = pargs.gwas_summary
    
    
        ## This block is using for the conversion of column numbers from the input GWAS summary statistics to the table of GWAS QC results
        if _is_path_exist(gwas_summary_from_gwas_qc):
            converted_column_number_options_dict = _get_converted_num_of_col_options(pargs.gwas_summary, gwas_summary_from_gwas_qc)
    
    ## Different phases in finding top SNPs, p-value expansion, and LD pruning steps according to the condition of given files
    if bool(pargs.gwas_summary) and bool(pargs.top_snp_list):
        ## [gwas_summary + top_snp_list]        
        ## Find top SNPs
        if pargs.skip_top_snp is False: 
            logger.info('Finding top SNPs')
            timeit_top_snp_start = timer()
            
        output_dir_dict['Top SNP'] = path.join(pargs.output_dir, "{}_Top_SNP".format(step_number))
        
        option_dict = {'-g': gwas_summary_from_gwas_qc, 
                       '-t': pargs.top_snp_list, 
                       '--genome_build': pargs.genome_build, 
                       '--rsid_only': pargs.rsid_only, 
                       '--no_header': pargs.no_header, 
                       '--top_snp_thresh': pargs.top_snp_thresh, 
                       '-o': output_dir_dict['Top SNP'], 
                       '--output_bed': True, 
                       '--config': pargs.config, 
                       '--prefix': "'{}'".format(pargs.prefix), 
                       '--skip_logging': pargs.skip_logging}
                       
        cmd = [conf.PYTHON_APP, path.join(script_dir, 'top_snp.py')]
        cmd.extend(_option_dict2list(converted_column_number_options_dict))
        cmd.extend(_option_dict2list(option_dict))
        if pargs.skip_gwas_qc and pargs.delimiter != "\t": cmd.extend(['-d', "'{}'".format(pargs.delimiter)]) ## Consider --skip_gwas_qc is given and the delimiter is not "\t"
        
        if pargs.skip_top_snp is False:
            run_cmd(cmd)
            step_number += 1
            logger.info("Finding top SNPs done. Time elapsed: %.2f sec." % (timer()-timeit_top_snp_start))
        
        ## Expect output file name from the step of finding top SNPs 
        top_snp_list_from_gwas = path.join(output_dir_dict['Top SNP'], "%suser_defined_gwas_top_snps.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix))
        template_table_for_locus_overlap_summary_stat = top_snp_list_from_gwas
        
        if _is_path_exist(top_snp_list_from_gwas):
            converted_column_number_options_dict = _get_converted_num_of_col_options(pargs.gwas_summary, top_snp_list_from_gwas)
            col_num_for_coloc_dict = converted_column_number_options_dict.copy()

        
        ## Run p-value expansion
        if pargs.pval_col is None:
            logger.warning('Skip p-value expansion step because of missing the required option -P/--pval_col')
        
        else:
            if pargs.skip_pval_expansion is False:
                logger.info('Running p-value expansion')
                timeit_pval_expansion_start = timer()
                
            output_dir_dict['P-value expansion'] = path.join(pargs.output_dir, "{}_P-value_expansion".format(step_number))
            
            if _is_path_exist(top_snp_list_from_gwas):
                step_output_tsv_list.append(('top SNP matching up with GWAS', top_snp_list_from_gwas))
                
                option_dict = {'-g': gwas_summary_from_gwas_qc, 
                               '-t': top_snp_list_from_gwas, 
                               '-D': pargs.dist_threshold, 
                               '-F': pargs.full_table, 
                               '-o': output_dir_dict['P-value expansion'], 
                               '--output_bed': True, 
                               '--config': pargs.config, 
                               '--prefix': "'{}'".format(pargs.prefix), 
                               '--skip_logging': pargs.skip_logging}
                               
                if pargs.sig_multiplier is not None: option_dict['-S'] = pargs.sig_multiplier
                else: option_dict['-L'] = pargs.locus_thresh
                
                cmd = [conf.PYTHON_APP, path.join(script_dir, 'pval_expansion.py')]
                cmd.extend(_option_dict2list(converted_column_number_options_dict))
                cmd.extend(_option_dict2list(option_dict))
                if pargs.skip_gwas_qc and pargs.delimiter != "\t": cmd.extend(['-d', "'{}'".format(pargs.delimiter)]) ## Consider --skip_gwas_qc is given and the delimiter is not "\t"
                
                if pargs.skip_pval_expansion is False:
                    run_cmd(cmd)
                    step_number += 1
                    logger.info("P-value expansion done. Time elapsed: %.2f sec." % (timer()-timeit_pval_expansion_start))
                
            else:
                if pargs.skip_top_snp:
                    logger.error("%s: File not found. Try to remove --skip_top_snp option and run this script again." % top_snp_list_from_gwas)
                else:
                    logger.error("%s: File not found" % top_snp_list_from_gwas)
                sys.exit(1)
        
        ## Run LD pruning
        if pargs.skip_ld_pruning is False:
            logger.info('Running LD pruning')
            timeit_ld_pruning_start = timer()
            
        output_dir_dict['LD pruning'] = path.join(pargs.output_dir, "{}_LD_pruning".format(step_number))
        
        ## If the p-value column is not specified, expect output file name from the step of finding top SNPs (i.e., The script skips p-value expansion step automatically).
        if pargs.pval_col is None:
            top_snp_list_from_pval_expansion = top_snp_list_from_gwas
        ## Expect output file name from the step of p-value expansion
        elif '-S' in option_dict.keys():
            top_snp_list_from_pval_expansion = path.join(output_dir_dict['P-value expansion'], "%spval_expanded_snps.sig_multiplier_%.1f.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, option_dict['-S']))
        else:
            top_snp_list_from_pval_expansion = path.join(output_dir_dict['P-value expansion'], "%spval_expanded_snps.locus_thresh_%.0e.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, option_dict['-L']))
        
        if _is_path_exist(top_snp_list_from_pval_expansion):
            step_output_tsv_list.append(('p-value expanded SNP', top_snp_list_from_pval_expansion))
            
            option_dict = {'-t': top_snp_list_from_pval_expansion, 
                           '--ld_threshold': pargs.ld_threshold, 
                           '--window_size': pargs.window_size_kb, 
                           '-q': pargs.quiet, 
                           '-S': pargs.super_population_vcf, 
                           '-o': output_dir_dict['LD pruning'], 
                           '--genome_build': pargs.genome_build, 
                           '--output_bed': True, 
                           '--config': pargs.config, 
                           '--prefix': "'{}'".format(pargs.prefix), 
                           '--skip_logging': pargs.skip_logging,
                           '--keep_intermediate_files': pargs.keep_intermediate_files}
            
            cmd = [conf.PYTHON_APP, path.join(script_dir, 'ld_pruning.py')]
            cmd.extend(_option_dict2list(converted_column_number_options_dict))
            cmd.extend(_option_dict2list(option_dict))
            
            if pargs.skip_ld_pruning is False:
                run_cmd(cmd)
                step_number += 1
                logger.info("LD pruning done. Time elapsed: %.2f sec." % (timer()-timeit_ld_pruning_start))
        
        else:
            if pargs.skip_pval_expansion:
                logger.error("%s: File not found. Try to remove --skip_pval_expansion option and run this script again." % top_snp_list_from_pval_expansion)
            elif pargs.pval_col is None:
                logger.error("%s: File not found. Try to remove --skip_top_snp option and run this script again." % top_snp_list_from_pval_expansion)
            else:
                logger.error("%s: File not found" % top_snp_list_from_pval_expansion)
            sys.exit(1)
            
    elif bool(pargs.gwas_summary):
        if pargs.pval_col is None:
            logger.error("The -P/--pval_col option is required when identifying genome-wide significant SNPs, i.e., no top SNPs list (-t/--top_snp_list) provided.")
            sys.exit(1)
            
        ## [Genome-wide significant SNPs (gwas_summary only)]
        ## Find genome-wide significant SNPs (top SNPs)
        ## Find top SNPs
        if pargs.skip_top_snp is False:
            logger.info('Finding genome-wide significant SNPs')
            timeit_genome_wide_sig_start = timer()
            
        output_dir_dict['Top SNP'] = path.join(pargs.output_dir, "{}_Top_SNP".format(step_number))
        
        option_dict = {'-g': gwas_summary_from_gwas_qc, 
                       '--top_snp_thresh': pargs.top_snp_thresh, 
                       '-o': output_dir_dict['Top SNP'], 
                       '--output_bed': True, 
                       '--config': pargs.config, 
                       '--prefix': "'{}'".format(pargs.prefix), 
                       '--skip_logging': pargs.skip_logging}
                       
        cmd = [conf.PYTHON_APP, path.join(script_dir, 'top_snp.py')]
        cmd.extend(_option_dict2list(converted_column_number_options_dict))
        cmd.extend(_option_dict2list(option_dict))
        if pargs.skip_gwas_qc and pargs.delimiter != "\t": cmd.extend(['-d', "'{}'".format(pargs.delimiter)]) ## Consider --skip_gwas_qc is given and the delimiter is not "\t"
        
        if pargs.skip_top_snp is False:
            run_cmd(cmd)
            step_number += 1
            logger.info("Finding genome-wide significant SNPs done. Time elapsed: %.2f sec." % (timer()-timeit_genome_wide_sig_start))
        
        ## Expect output file name from the step of finding genome-wide significant SNPs
        genome_wide_sig_snp_list = path.join(output_dir_dict['Top SNP'], "%sgenome-wide_significant_snps.pval_%.0e.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, pargs.top_snp_thresh))
        template_table_for_locus_overlap_summary_stat = genome_wide_sig_snp_list
        
        ## This block is using for the conversion of column numbers from the input GWAS summary statistics (or GWAS QC output) to the table of genome-wide significant SNPs
        if _is_path_exist(genome_wide_sig_snp_list):
            converted_column_number_options_dict = _get_converted_num_of_col_options(pargs.gwas_summary, genome_wide_sig_snp_list)
            col_num_for_coloc_dict = converted_column_number_options_dict.copy()

                
        ## Run LD pre-pruning. This step is disabled by default setting, but is enabled if p-value expansion is activated.
        if pargs.pval_expansion_gw:
            logger.info('Running LD pre-pruning')
            timeit_ld_pre_pruning_start = timer()
            
        output_dir_dict['LD pre-pruning'] = path.join(pargs.output_dir, "{}_LD_pre-pruning".format(step_number))
            
        if _is_path_exist(genome_wide_sig_snp_list):
            step_output_tsv_list.append(('genome-wide significant SNP matching up with GWAS', genome_wide_sig_snp_list))
            
            option_dict = {'-t': genome_wide_sig_snp_list, 
                           '--ld_threshold': pargs.ld_threshold, 
                           '--window_size': pargs.window_size_kb, 
                           '--pre_prune': True, 
                           '-q': pargs.quiet, 
                           '-S': pargs.super_population_vcf, 
                           '-o': output_dir_dict['LD pre-pruning'], 
                           '--genome_build': pargs.genome_build, 
                           '--output_bed': True, 
                           '--config': pargs.config, 
                           '--prefix': "'{}'".format(pargs.prefix), 
                           '--skip_logging': pargs.skip_logging,
                           '--keep_intermediate_files': pargs.keep_intermediate_files}
            
            cmd = [conf.PYTHON_APP, path.join(script_dir, 'ld_pruning.py')]
            cmd.extend(_option_dict2list(converted_column_number_options_dict))
            cmd.extend(_option_dict2list(option_dict))
            
            if pargs.pval_expansion_gw:
                run_cmd(cmd)
                step_number += 1
                logger.info("LD pre-pruning done. Time elapsed: %.2f sec." % (timer()-timeit_ld_pre_pruning_start))
            
        else:
            if pargs.skip_top_snp:
                logger.error("%s: File not found. Try to remove --skip_top_snp option and run this script again." % genome_wide_sig_snp_list)
            else:
                logger.error("%s: File not found" % genome_wide_sig_snp_list)
            sys.exit(1)
        
        ## Run p-value expansion. This step is disabled by default setting in the genome-wide significant analysis.
        if pargs.pval_expansion_gw:
            logger.info('Running p-value expansion')
            timeit_pval_expansion_start = timer()
            
        output_dir_dict['P-value expansion'] = path.join(pargs.output_dir, "{}_P-value_expansion".format(step_number))
        
        ## Expect output file name from the step of LD pre-pruning
        pre_pruned_snp_list = path.join(output_dir_dict['LD pre-pruning'], "%sLD_pre-pruning_snps.window_size_%s.ld_threshold_%.1f.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, pargs.window_size_kb, pargs.ld_threshold))
        
        if _is_path_exist(pre_pruned_snp_list):
            step_output_tsv_list.append(('LD pre-pruned SNP', pre_pruned_snp_list))
            
            option_dict = {'-g': gwas_summary_from_gwas_qc, 
                           '-t': pre_pruned_snp_list, 
                           '-D': pargs.dist_threshold, 
                           '-F': pargs.full_table, 
                           '-o': output_dir_dict['P-value expansion'], 
                           '--genome_wide_sig': True, 
                           '--output_bed': True, 
                           '--config': pargs.config, 
                           '--prefix': "'{}'".format(pargs.prefix), 
                           '--skip_logging': pargs.skip_logging}
                           
            if pargs.sig_multiplier is not None: option_dict['-S'] = pargs.sig_multiplier
            else: option_dict['-L'] = pargs.locus_thresh
            
            cmd = [conf.PYTHON_APP, path.join(script_dir, 'pval_expansion.py')]
            cmd.extend(_option_dict2list(converted_column_number_options_dict))
            cmd.extend(_option_dict2list(option_dict))
            if pargs.skip_gwas_qc and pargs.delimiter != "\t": cmd.extend(['-d', "'{}'".format(pargs.delimiter)]) ## Consider --skip_gwas_qc is given and the delimiter is not "\t"
            
            if pargs.pval_expansion_gw:
                run_cmd(cmd)
                step_number += 1
                logger.info("P-value expansion done. Time elapsed: %.2f sec." % (timer()-timeit_pval_expansion_start))
        
        else:
            if pargs.pval_expansion_gw:
                logger.error("%s: File not found" % pre_pruned_snp_list)
                sys.exit(1)
            
        ## Run LD pruning
        if pargs.skip_ld_pruning is False:
            logger.info('Running LD pruning')
            timeit_ld_pruning_start = timer()
            
        output_dir_dict['LD pruning'] = path.join(pargs.output_dir, "{}_LD_pruning".format(step_number))
        
        ## Expect output file name from the step of find top SNPs / p-value expansion
        if pargs.pval_expansion_gw:
            ## Have the p-value expansion result
            if '-S' in option_dict.keys():
                top_snp_list_from_pval_expansion = path.join(output_dir_dict['P-value expansion'], "%spval_expanded_snps.sig_multiplier_%.1f.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, option_dict['-S']))
            else:
                top_snp_list_from_pval_expansion = path.join(output_dir_dict['P-value expansion'], "%spval_expanded_snps.locus_thresh_%.0e.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, option_dict['-L']))
        else:
            ## Doesn't run p-value expansion (default setting)
            top_snp_list_from_pval_expansion = genome_wide_sig_snp_list
        
        if _is_path_exist(top_snp_list_from_pval_expansion):
            if pargs.pval_expansion_gw:
                step_output_tsv_list.append(('p-value expanded SNP', top_snp_list_from_pval_expansion))
            
            option_dict = {'-t': top_snp_list_from_pval_expansion, 
                           '--ld_threshold': pargs.ld_threshold, 
                           '--window_size': pargs.window_size_kb, 
                           '--genome_wide_sig': True, 
                           '-q': pargs.quiet, 
                           '-S': pargs.super_population_vcf, 
                           '-o': output_dir_dict['LD pruning'], 
                           '--genome_build': pargs.genome_build, 
                           '--output_bed': True, 
                           '--config': pargs.config, 
                           '--prefix': "'{}'".format(pargs.prefix), 
                           '--skip_logging': pargs.skip_logging,
                           '--keep_intermediate_files': pargs.keep_intermediate_files}
            
            cmd = [conf.PYTHON_APP, path.join(script_dir, 'ld_pruning.py')]
            cmd.extend(_option_dict2list(converted_column_number_options_dict))
            cmd.extend(_option_dict2list(option_dict))
            
            if pargs.skip_ld_pruning is False:
                run_cmd(cmd)
                step_number += 1
                logger.info("LD pruning done. Time elapsed: %.2f sec." % (timer()-timeit_ld_pruning_start))
        
        else:
            if pargs.pval_expansion_gw is False:
                logger.error("%s: File not found. Try to remove --skip_top_snp option and run this script again." % top_snp_list_from_pval_expansion)
            else:
                logger.error("%s: File not found" % top_snp_list_from_pval_expansion)
            sys.exit(1)
        
    elif bool(pargs.top_snp_list):
        ## [top_snp_list only]
        if pargs.skip_top_snp is False:
            logger.info('Generating a formatted top SNP file')
            timeit_formatted_top_snp_start = timer()
            
        output_dir_dict['Top SNP'] = path.join(pargs.output_dir, "{}_Top_SNP".format(step_number))
        
        ## Fixed column order because of the formatted top SNP list
        pargs.chr_col = 1
        pargs.pos_col = 3
        pargs.allele1_col = 5
        pargs.allele2_col = 6
        pargs.rsid_col = 9
        
        ## Generate a formatted top SNP file
        option_dict = {'-T': pargs.top_snp_list, 
                       '--genome_build': pargs.genome_build, 
                       '--rsid_only': pargs.rsid_only, 
                       '--no_header': pargs.no_header, 
                       '--top_snp_thresh': pargs.top_snp_thresh, 
                       '-o': output_dir_dict['Top SNP'], 
                       '--output_bed': True, 
                       '--config': pargs.config, 
                       '--prefix': "'{}'".format(pargs.prefix), 
                       '--skip_logging': pargs.skip_logging}
                       
        cmd = [conf.PYTHON_APP, path.join(script_dir, 'top_snp.py')]
        cmd.extend(_option_dict2list(option_dict))
        
        if pargs.skip_top_snp is False:
            run_cmd(cmd)
            step_number += 1
            logger.info("Generating a formatted top SNP file done. Time elapsed: %.2f sec." % (timer()-timeit_formatted_top_snp_start))
        
        ## Run LD pruning
        if pargs.skip_ld_pruning is False:
            logger.info('Running LD pruning')
            timeit_ld_pruning_start = timer()
            
        output_dir_dict['LD pruning'] = path.join(pargs.output_dir, "{}_LD_pruning".format(step_number))
        
        ## Expect output file name of the formatted top SNP file
        formatted_top_snp_list = path.join(output_dir_dict['Top SNP'], "%suser_defined_top_snps.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix))
        template_table_for_locus_overlap_summary_stat = formatted_top_snp_list
        
        if _is_path_exist(formatted_top_snp_list):
            step_output_tsv_list.append(('top SNP matching up with 1000 Genomes', formatted_top_snp_list))
            
            option_dict = {'-t': formatted_top_snp_list, 
                           '-c': pargs.chr_col, 
                           '-p': pargs.pos_col, 
                           '-A': pargs.allele1_col, 
                           '-a': pargs.allele2_col, 
                           '-r': pargs.rsid_col, 
                           '--ld_threshold': pargs.ld_threshold, 
                           '--window_size': pargs.window_size_kb, 
                           '-q': pargs.quiet, 
                           '-S': pargs.super_population_vcf, 
                           '-o': output_dir_dict['LD pruning'], 
                           '--genome_build': pargs.genome_build, 
                           '--output_bed': True, 
                           '--config': pargs.config, 
                           '--prefix': "'{}'".format(pargs.prefix), 
                           '--skip_logging': pargs.skip_logging,
                           '--keep_intermediate_files': pargs.keep_intermediate_files}
            
            cmd = [conf.PYTHON_APP, path.join(script_dir, 'ld_pruning.py')]
            cmd.extend(_option_dict2list(option_dict))
            
            if pargs.skip_ld_pruning is False:
                run_cmd(cmd)
                step_number += 1
                logger.info("LD pruning done. Time elapsed: %.2f sec." % (timer()-timeit_ld_pruning_start))
            
        else:
            if pargs.skip_top_snp:
                logger.error("%s: File not found. Try to remove --skip_top_snp option and run this script again." % formatted_top_snp_list)
            else:
                logger.error("%s: File not found" % formatted_top_snp_list)
            sys.exit(1)
        
    else:
        parser.error('arguments --gwas_summary, --top_snp_only, or --create_template must be given one of them at least.')


    ## Run LD expansion
    if pargs.skip_ld_expansion is False:
        logger.info('Running LD expansion')
        timeit_ld_expansion_start = timer()
        
    output_dir_dict['LD expansion'] = path.join(pargs.output_dir, "{}_LD_expansion".format(step_number))
    
    ## Expect output file name from the step of LD pruning
    top_snp_list_from_ld_pruning = path.join(output_dir_dict['LD pruning'], "%sLD_pruning_snps.window_size_%s.ld_threshold_%.1f.tsv" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, option_dict['--window_size'], option_dict['--ld_threshold']))
    
    if _is_path_exist(top_snp_list_from_ld_pruning):
        step_output_tsv_list.append(('LD pruned SNP', top_snp_list_from_ld_pruning))
        
        cmd = [conf.PYTHON_APP, path.join(script_dir, 'ld_expansion.py')]
        
        if bool(pargs.gwas_summary):
            option_dict = {'-g': gwas_summary_from_gwas_qc, 
                           '-t': top_snp_list_from_ld_pruning, 
                           '--ld_threshold': pargs.ld_threshold, 
                           '--window_size': pargs.window_size, 
                           '-q': pargs.quiet, 
                           '-S': pargs.super_population_vcf, 
                           '-o': output_dir_dict['LD expansion'], 
                           '--genome_build': pargs.genome_build, 
                           '--output_bed': True, 
                           '--config': pargs.config, 
                           '--prefix': "'{}'".format(pargs.prefix), 
                           '--skip_logging': pargs.skip_logging,
                           '--keep_intermediate_files': pargs.keep_intermediate_files}
                           
            cmd.extend(_option_dict2list(converted_column_number_options_dict))
            if pargs.skip_gwas_qc and pargs.delimiter != "\t": cmd.extend(['-d', "'{}'".format(pargs.delimiter)]) ## Consider --skip_gwas_qc is given and the delimiter is not "\t"
            
        else: ## For top_snp_list only
            option_dict = {'-t': formatted_top_snp_list, 
                           '-c': pargs.chr_col, 
                           '-p': pargs.pos_col, 
                           '-A': pargs.allele1_col, 
                           '-a': pargs.allele2_col, 
                           '-r': pargs.rsid_col, 
                           '-t': top_snp_list_from_ld_pruning, 
                           '--ld_threshold': pargs.ld_threshold, 
                           '--window_size': pargs.window_size, 
                           '-q': pargs.quiet, 
                           '-S': pargs.super_population_vcf, 
                           '-o': output_dir_dict['LD expansion'], 
                           '--genome_build': pargs.genome_build, 
                           '--output_bed': True, 
                           '--config': pargs.config, 
                           '--prefix': "'{}'".format(pargs.prefix), '--skip_logging': pargs.skip_logging,
                           '--keep_intermediate_files': pargs.keep_intermediate_files}
        
        cmd.extend(_option_dict2list(option_dict))
        
        if pargs.skip_ld_expansion is False:
            run_cmd(cmd)
            step_number += 1
            logger.info("LD expansion done. Time elapsed: %.2f sec." % (timer()-timeit_ld_expansion_start))
        
    else:
        if pargs.skip_ld_pruning:
            logger.error("%s: File not found. Try to remove --skip_ld_pruning option and run this script again." % top_snp_list_from_ld_pruning)
        else:
            logger.error("%s: File not found" % top_snp_list_from_ld_pruning)
        sys.exit(1)


    ## Run INFERNO genomic overlap
    if pargs.skip_genomic_overlap is False:
        logger.info('Running INFERNO genomic overlap')
        timeit_genomic_overlap_start = timer()
        
    output_dir_dict['INFERNO genomic overlap'] = path.join(pargs.output_dir, "{}_INFERNO_genomic_overlap".format(step_number))
    
    ## Expect output file name from the step of LD expansion
    inferno_bed_from_ld_expansion = path.join(output_dir_dict['LD expansion'], "%sLD_expansion_snps.window_size_%s.ld_threshold_%.1f.bed" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, pargs.window_size, pargs.ld_threshold))
    
    if _is_path_exist(inferno_bed_from_ld_expansion):
        step_output_tsv_list.append(('LD expanded SNP', inferno_bed_from_ld_expansion.replace('.bed', '.tsv')))
        
        option_dict = {'-b': inferno_bed_from_ld_expansion, 
                       '--genome_build': pargs.genome_build, 
                       '--gtex_version': pargs.gtex_version, 
                       '--fantom5_window_size': pargs.fantom5_window_size, 
                       '--replicate_frequency': pargs.replicate_frequency, 
                       '--enrichment_method': pargs.enrichment_method, 
                       '--skip_fantom5': pargs.skip_fantom5, 
                       '--skip_gtex': pargs.skip_gtex, 
                       '--skip_chromhmm': pargs.skip_chromhmm, 
                       '--skip_genomic_partition': pargs.skip_genomic_partition, 
                       '--skip_targetscan': pargs.skip_targetscan, 
                       '--skip_dashr2': pargs.skip_dashr2, 
                       '--skip_homer': pargs.skip_homer, 
                       '--skip_enrichment': pargs.skip_enrichment, 
                       '--skip_genomic_element_plot': pargs.skip_genomic_element_plot, 
                       '--skip_tissue_category_plot': pargs.skip_tissue_category_plot, 
                       '-o': output_dir_dict['INFERNO genomic overlap'], 
                       '--config': pargs.config, 
                       '--prefix': "'{}'".format(pargs.prefix), 
                       '--skip_logging': pargs.skip_logging,
                       '--keep_intermediate_files': pargs.keep_intermediate_files}
            
        ## If any one of the four conditions which can skip co-localization analysis is present, remove part files in a metatable folder. The first condition indicates top_snp_list only.
        # if (not(bool(pargs.gwas_summary)) and bool(pargs.top_snp_list)) or (pargs.pval_col is None) or (pargs.af_col is None and not(pargs.use_1kg_maf)) or bool(pargs.skip_coloc):
            # option_dict['--remove_metatable_part_file_dir'] = not(pargs.keep_intermediate_files)
        
        cmd = [conf.PYTHON_APP, path.join(script_dir, 'inferno_genomic_overlap.py')]
        cmd.extend(_option_dict2list(option_dict))
        ## Use GWAS summary statistics as the template for meta-table generation
        if bool(pargs.gwas_summary): cmd.extend(['-g', gwas_summary_from_gwas_qc])
        
        if pargs.skip_genomic_overlap is False:
            run_cmd(cmd)
            step_number += 1
            logger.info("INFERNO genomic overlap done. Time elapsed: %.2f sec." % (timer()-timeit_genomic_overlap_start))
        
    else:
        if pargs.skip_ld_expansion:
            logger.error("%s: File not found. Try to remove --skip_ld_expansion option and run this script again." % inferno_bed_from_ld_expansion)
        else:
            logger.error("%s: File not found" % inferno_bed_from_ld_expansion)
        sys.exit(1)

    if bool(pargs.gwas_summary):
        if pargs.pval_col is None or (pargs.af_col is None and not(pargs.use_1kg_maf)):
            logger.warning('Skip co-localization analysis because of missing the required option -P/--pval_col and/or -f/--af_col. Adding --use_1kg_maf option to use 1000 Genomes allele frequency data if --pval_col is available.')
            
        else:
            ## Run co-localization analysis
            if pargs.skip_coloc is False:
                logger.info('Running co-localization analysis')
                timeit_coloc_start = timer()
                
            output_dir_dict['Co-localization analysis'] = path.join(pargs.output_dir, "{}_Co-localization_analysis".format(step_number))
            
            ## Expect output file names from the steps of LD expansion and finding top SNPs
            inferno_bed_from_ld_expansion = path.join(output_dir_dict['LD expansion'], "%sLD_expansion_snps.window_size_%s.ld_threshold_%.1f.bed" % (pargs.prefix + '_' if pargs.prefix != '' else pargs.prefix, pargs.window_size, pargs.ld_threshold))
            ## Get the formatted GWAS summary BED file
            gwas_summary_bed_from_top_snp = path.splitext(gwas_summary_from_gwas_qc)[0]+'.bed.gz'
            ## Get metatable and add co-localization results into it
            metatable_from_inferno_genomic_overlap = path.join(output_dir_dict['INFERNO genomic overlap'], 'coalesced_metatable.tsv')
            
            if not _is_path_exist(metatable_from_inferno_genomic_overlap):
                if pargs.skip_genomic_overlap:
                    logger.warning("%s: File not found. Automatically skip metatable integration step. Try to remove --skip_genomic_overlap option and run this script again." % metatable_from_inferno_genomic_overlap)
                else:
                    logger.warning("%s: File not found. Automatically skip metatable integration step." % metatable_from_inferno_genomic_overlap)
            
            if not _is_path_exist(inferno_bed_from_ld_expansion):
                if pargs.skip_ld_expansion:
                    logger.error("%s: File not found. Try to remove --skip_ld_expansion option and run this script again." % inferno_bed_from_ld_expansion)
                else:
                    logger.error("%s: File not found" % inferno_bed_from_ld_expansion)
                sys.exit(1)
            elif not _is_path_exist(gwas_summary_bed_from_top_snp):
                    if pargs.skip_top_snp:
                        logger.error("%s: File not found. Try to remove --skip_top_snp option and run this script again." % gwas_summary_bed_from_top_snp)
                    else:
                        logger.error("%s: File not found" % gwas_summary_bed_from_top_snp)
                    sys.exit(1)
            else:
                option_dict = {'-b': inferno_bed_from_ld_expansion, 
                               '-g': gwas_summary_bed_from_top_snp, 
                               '-s': pargs.sample_size, 
                               '--genome_build': pargs.genome_build, 
                               '--gtex_version': pargs.gtex_version, 
                               '-t': pargs.analysis_type, 
                               '-e': pargs.expansion_type, 
                               '-W': pargs.expansion_window_size, 
                               '--ref_col': col_num_for_coloc_dict['-A'], 
                               '--alt_col': col_num_for_coloc_dict['-a'], 
                               '--pval_col': col_num_for_coloc_dict['-P'], 
                               '--maf_col': col_num_for_coloc_dict['-f'], 
                               '--beta_col': col_num_for_coloc_dict['-b'], 
                               '--se_col': col_num_for_coloc_dict['-s'], 
                               '--rsid_col': col_num_for_coloc_dict['-r'], 
                               '--h4_cutoff': pargs.h4_cutoff, 
                               '--abf_cutoff': pargs.abf_cutoff, 
                               '--nsnps_cutoff': pargs.nsnps_cutoff, 
                               '--gzip': pargs.gzip, 
                               '-o': output_dir_dict['Co-localization analysis'], 
                               '--config': pargs.config, 
                               '--prefix': "'{}'".format(pargs.prefix), 
                               '--skip_logging': pargs.skip_logging,
                               '--keep_intermediate_files': pargs.keep_intermediate_files}
                               
                if pargs.analysis_type == 'cc': option_dict['-p'] = pargs.sample_proportion
                if not(pargs.skip_genomic_overlap): option_dict['-m'] = metatable_from_inferno_genomic_overlap
                
                cmd = [conf.PYTHON_APP, path.join(script_dir, 'coloc.py')]
                cmd.extend(_option_dict2list(option_dict))
                
                if pargs.skip_coloc is False:
                    run_cmd(cmd)
                    step_number += 1
                    logger.info("Co-localization analysis done. Time elapsed: %.2f sec." % (timer()-timeit_coloc_start))
                
    ## Combine summary report files of each step to generate overall summary report
    output_dirs = list(output_dir_dict.values())
    output_dirs.sort()
    with open(path.join(pargs.output_dir, 'overall_summary_report.txt'), 'wb') as w_fp:
        for output_subdir in output_dirs:
            try:
                with open(path.join(output_subdir, 'summary_report.txt'), 'rb') as r_fp:
                    copyfileobj(r_fp, w_fp)
            except IOError:
                pass ## Allow user to skip certain steps
    
    ## Calculate numbers of SNPs in each preprocessing step
    if len(step_output_tsv_list)>0:
        from sparkinferno.preprocessing import preprocessing_summary_stat
        spark, sc = inferno.make_spark_session()
        
        logger.info('Generating preprocessing summary statistics')
        # timeit_preprocessing_summary_start = timer()
        preprocessing_summary_stat(template_table_for_locus_overlap_summary_stat, step_output_tsv_list, path.join(pargs.output_dir, 'preprocessing_summary_stat.tsv'), temp_dir=conf.TMP_PATH)
    
    ## Remove tmp dir if it's in the output dir
    if _is_path_exist(default_tmp_dir) and (default_tmp_dir == path.join(pargs.output_dir, 'tmp')) and not(pargs.keep_intermediate_files):
        from shutil import rmtree
        rmtree(default_tmp_dir)

    logger.info("All done. Total time elapsed: %.2f sec." % (timer()-timeit_script_start))
