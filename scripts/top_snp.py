#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import path, makedirs, getcwd
import sys
import argparse
import pyspark
import sparkinferno.inferno as inferno
import sparkinferno.config as conf
import sparkinferno.preprocessing as preprocessing
import datetime
import logging
import re


def _is_path_exist(dir, error_msg=False):
    if path.exists(dir):
        return True
    else:
        if error_msg: sys.stderr.write(dir+": No such file or directory\n")
        return False

if __name__ == "__main__":
    usage = "%(prog)s <-g gwas_summary_stats/-T top_snp_list -c col# -p col# -A col# -a col# -P col#> [options]"
    parser = argparse.ArgumentParser(usage=usage, description='Output a formatted TSV file of top SNPs as an input for other GWAS pre-processing scripts, e.g., pval_expansion.py, ld_pruning.py and ld_expansion.py.')
    group1 = parser.add_argument_group('GWAS summary statistics (or plus top SNP list)', 'Provide a GWAS summary statistics file (-g) with top SNP list (-t) for looking up the user defined top SNPs from the GWAS file; or without top SNP list for identifying genome-wide significant SNPs. Then, prepare a GWAS BED file with the calibration of effect directions for p-value expansion analysis.')
    group1.add_argument('-g', '--gwas_summary', action='store', default=None, help='The file path of GWAS summary statistics [Default: None]')
    group1.add_argument('-c', '--chr_col', type=int, help='The column number of chromosomes (1-based, -c 1 indicates the first column). This is required.')
    group1.add_argument('-p', '--pos_col', type=int, help='The column number of SNP positions (1-based). This is required.')
    group1.add_argument('-A', '--allele1_col', type=int, help='The column number of genomic refernece alleles (1-based). This is required.')
    group1.add_argument('-a', '--allele2_col', type=int, help='The column number of alternative alleles (1-based). This is required.')
    group1.add_argument('-P', '--pval_col', type=int, help='The column number of SNP p-values (1-based). If the -t/--top_snp_list option is not given, this is required to identify genome-wide significant SNPs.')
    group1.add_argument('-b', '--beta_col', type=int, help='The column number of SNP beta values (1-based)')
    group1.add_argument('-s', '--se_col', type=int, help='The column number of standard errors (1-based)')
    group1.add_argument('-f', '--af_col', type=int, help='The column number of SNP allele frequency values (1-based)')
    group1.add_argument('-r', '--rsid_col', type=int, help='The column number of SNP rsIDs (1-based)')
    parser.add_argument('-e', '--effect_dir_col', type=int, help='The column number of effect directions (1-based) for the GWAS meta-analysis in GWAS summary statistics')
    group1.add_argument('-d', '--delimiter', action='store', default="\t", help='Split columns of GWAS summary statistics using the delimiter [Default: "\\t"]')
    group1.add_argument('--top_snp_thresh', default=5e-8, type=float, help='The absolute p-value cutoff to use for genome-wide significance analysis  [Default: 5e-8]')
    group1.add_argument('-t', '--top_snp_list', action='store', default=None, help='Input a user-defined top SNPs list. The columns MUST contain "chr", "pos" and "region_name" columns. Providing optional columns such as "allele1" and "allele2" or "rsID" contributes to distinguish corresponding SNPs precisely. [Default: None]')
    
    group2 = parser.add_argument_group('Top SNP list only', 'Provide a top SNP list only (-T) for generation of a formatted table which can become an input of ld_pruning.py or ld_expansion.py')
    group2.add_argument('-T', '--top_snp_only', action='store', default=None, help='Input a user-defined top SNPs list. The columns MUST contain "chr", "pos" and "region_name" columns. Providing optional columns such as "allele1" and "allele2" or "rsID" contributes to distinguish corresponding SNPs precisely. [Default: None]')
    
    parser.add_argument('-C', '--create_template', action='store_true', default=False, help='Create an empty template of top SNP list at OUTPUT_DIR/user_top_snp_list.tsv [Default: False]')
    parser.add_argument('-R', '--rsid_only', action='store_true', default=False, help='The top SNP list contains rsID only [Default: False]')
    parser.add_argument('-G', '--genome_build', action='store', default='hg19', help='The version of human genome reference [Default: hg19]')
    parser.add_argument('-H', '--no_header', action='store_true', default=False, help='The top SNP list DO NOT contain a header [Default: False]')
    parser.add_argument('-o', '--output_dir', action='store', default=getcwd(), help="The path to the desired output directory [Default: {}]".format(getcwd()))
    parser.add_argument('-B', '--output_bed', action='store_true', default=False, help='Save results as a BED file for downstream analysis of INFERNO2 [Default: False]')
    parser.add_argument('--config', action='store', help='Provide a specific configuration file to use')
    parser.add_argument('--prefix', action='store', default='', help='Add a prefix to ouptput files [Default: None]')
    parser.add_argument('--skip_logging', action='store_true', default=False, help='Skip logging messages as a file [Default: False]')


    ## Show help without providing any options
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
        
    pargs = parser.parse_args()
    
    ## Generate output dir
    if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    ## Process configuration file
    conf_file_in_output_dir = path.join(pargs.output_dir, 'sparkinferno.cfg')
    if _is_path_exist(conf_file_in_output_dir):
        ## sparkinferno.cfg is found at the output dir, the analysis has beed conducted before
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
        else:
            conf.read_config_file(conf_file_in_output_dir)
    else:
        ## A new started analysis, no existing configuration file is found in the output directory
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
        else:
            parser.error('The --config option is required for a new started analysis')
        
    ## Initialize the logging module
    this_script_name = path.splitext(path.basename(__file__))[0]
    logger = logging.getLogger(this_script_name)
    if not pargs.skip_logging:
        ## Set log path and initialize file handler
        log_dir = path.join(pargs.output_dir, 'log')
        if not _is_path_exist(log_dir): makedirs(log_dir)
        
        log_file_path = path.join(log_dir, "%s_%s.log" % (this_script_name, datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")))
        file_handler = logging.FileHandler(log_file_path)
        ## Set formatter
        file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
        logger.addHandler(file_handler)
                
    # if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    # pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    if not (bool(pargs.gwas_summary) or bool(pargs.top_snp_only) or pargs.create_template):
        parser.error('--gwas_summary, --top_snp_only, or --create_template must be given one of them at least.')
    
    logger.info('Running command: %s' % ' '.join([re.sub(r'(^$| +)', r'"\1"', arg, count=1) for arg in sys.argv])) ## Show quotation marks between a space or null character. (Example: -d " " or --prefix "")
    
    summary_report_msg_list = ["[Summary Report of Top SNP]"]
    
    if pargs.prefix != '': pargs.prefix = pargs.prefix + '_'
    
    if pargs.create_template:
        print('Other options are ignored when -C/--create_template is given')
        preprocessing.create_top_snp_list(target_path=path.join(pargs.output_dir, 'user_top_snp_list.tsv'))
        sys.exit(0)
        
    elif bool(pargs.top_snp_only):
        if not _is_path_exist(pargs.top_snp_only, True): sys.exit(1)
        
        spark, sc = inferno.make_spark_session()
        top_snp_output_filename = path.join(pargs.output_dir, "%suser_defined_top_snps.tsv" % pargs.prefix)
        top_snp_df = preprocessing.load_top_snp_list(pargs.top_snp_only, conf.GADB_META_FILE, is_header=not(pargs.no_header), rsID_only=pargs.rsid_only, delimiter=pargs.delimiter, genome_build=pargs.genome_build)
        
        summary_report_msg_list.append("Found %d user defined top SNP(s) from 1000 Genomes" % top_snp_df.count())
        logger.info(summary_report_msg_list[-1])
        
        ## Assign these variables to the input of preprocessing.save_df_as_bed()
        af_col_name = None
        rsid_col_name = 'rsID'
        allele1_col_name = 'allele1'
        allele2_col_name = 'allele2'
        pval_col_name = None
    
    elif bool(pargs.gwas_summary): ## Given a GWAS
        if pargs.top_snp_list is None and not(pargs.chr_col and pargs.pos_col and pargs.allele1_col and pargs.allele2_col and pargs.pval_col):
            parser.error('Options --chr_col, --pos_col, --allele1_col, --allele2_col, and --pval_col are all required to identify genome-wide significant SNPs')
        elif not(pargs.chr_col and pargs.pos_col and pargs.allele1_col and pargs.allele2_col):
            parser.error('Options --chr_col, --pos_col, --allele1_col, and --allele2_col are all required for GWAS summary statistics')
        
        if not _is_path_exist(pargs.gwas_summary, True): sys.exit(1)
        
        ## Get summary stats header
        schema_list = open(pargs.gwas_summary, 'r').readline().rstrip().split(pargs.delimiter)
        
        ## Convert column numbers from options with the "_col" suffix to *_col_name variables using schema_list to match up
        for parg in vars(pargs).keys():
            if parg[-4:] == '_col':
                parg_val = eval("pargs.%s" % parg)
                if parg_val is not None:
                    ## e.g., chr_col_name = schema_list[pargs.chr_col-1]
                    globals()["%s_name" % parg] = schema_list[parg_val-1]
                else:
                    globals()["%s_name" % parg] = None
        
        kwargs = dict(
            chr_col_name = chr_col_name,
            pos_col_name = pos_col_name,
            af_col_name = af_col_name,
            rsid_col_name = rsid_col_name,
            allele1_col_name = allele1_col_name,
            allele2_col_name = allele2_col_name,
            pval_col_name = pval_col_name,
            beta_col_name = beta_col_name,
            se_col_name = se_col_name,
            effect_dir_col_name = effect_dir_col_name)
            
        spark, sc = inferno.make_spark_session()
            
        ## Load GWAS summary statistics
        summary_stats_df = preprocessing.load_file_to_df(pargs.gwas_summary, delimiter=pargs.delimiter, **kwargs).persist(pyspark.StorageLevel.DISK_ONLY)
        
        ## Get gwas_summary file name
        GWAS_summary_stats_file_name = path.splitext(pargs.gwas_summary)[0]
        
        ## Convert GWAS summary statistics to BED and index it if the file is not existing
        bed_index_path = GWAS_summary_stats_file_name+'.bed.gz'
        if not _is_path_exist(bed_index_path+'.tbi'):
            bed_path = preprocessing.summary_stats_df_to_bed(summary_stats_df, GWAS_summary_stats_file_name+'.bed', temp_dir=conf.TMP_PATH, tabix_app=conf.TABIX_APP, bgzip_app=conf.BGZIP_APP)
        
        ## Step for generating top SNPs
        if pargs.top_snp_list is None:
            ## Get the genome-wide significant SNPs from GWAS summary statistics
            top_snp_output_filename = path.join(pargs.output_dir, "%sgenome-wide_significant_snps.pval_%.0e.tsv" % (pargs.prefix, pargs.top_snp_thresh))
            
            top_snp_df = preprocessing.get_genome_wide_significant_top_snp(summary_stats_df, pval_col_name=pval_col_name, top_snp_thresh=pargs.top_snp_thresh).persist(pyspark.StorageLevel.DISK_ONLY)
            
            summary_report_msg_list.append("Found %d distinct genome-wide significant SNP(s) from %d GWAS SNP(s)" % (top_snp_df.select('snp_id').distinct().count(), summary_stats_df.count()))
            logger.info(summary_report_msg_list[-1])

        else:
            ## Get the top SNPs from user defined
            if not _is_path_exist(pargs.top_snp_list, True): sys.exit(1)
            top_snp_output_filename = path.join(pargs.output_dir, "%suser_defined_gwas_top_snps.tsv" % pargs.prefix)
            
            ## Load top snp list
            top_snp_list_df = preprocessing.load_top_snp_list(pargs.top_snp_list, conf.GADB_META_FILE, is_header=not(pargs.no_header), rsID_only=pargs.rsid_only, delimiter=pargs.delimiter, genome_build=pargs.genome_build)
            ## Lookup top SNPs in GWAS summary statistics
            top_snp_df = preprocessing.get_top_snp_from_gwas_summary(top_snp_list_df, summary_stats_df, bed_index_path, tabix_app=conf.TABIX_APP, chr_col_name=chr_col_name, pos_col_name=pos_col_name, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, rsid_col_name=rsid_col_name).persist(pyspark.StorageLevel.DISK_ONLY)
            
            summary_report_msg_list.append("Found %d user defined top SNP(s) from %d GWAS SNP(s)" % (top_snp_df.count(), summary_stats_df.count()))
            logger.info(summary_report_msg_list[-1])
        
        summary_stats_df.unpersist(blocking=True)
        
    ## Save df as a file
    preprocessing.save_df(top_snp_df, top_snp_output_filename, temp_dir=conf.TMP_PATH)
    
    ## Output BED format for downstream analysis of INFERNO2
    if pargs.output_bed:
        preprocessing.save_df_as_bed(top_snp_df, top_snp_output_filename.replace('.tsv', '.bed'), af_col_name=af_col_name, rsid_col_name=rsid_col_name, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, pval_col_name=pval_col_name, temp_dir=conf.TMP_PATH)

    top_snp_df.unpersist(blocking=True)
    
    ## Generate summary report
    open(path.join(pargs.output_dir, 'summary_report.txt'), 'w').write("\n".join(summary_report_msg_list)+"\n\n")
