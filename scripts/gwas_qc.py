#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pyspark
from pyspark.sql import functions as F
from os import path, makedirs, getcwd
import sys
import argparse
import sparkinferno.inferno as inferno
import sparkinferno.config as conf
import sparkinferno.preprocessing as preprocessing
import datetime
import logging
import re


def _is_path_exist(dir, error_msg=False):
    if path.exists(dir):
        return True
    else:
        if error_msg: logger.error(dir+': No such file or directory')
        return False

if __name__ == "__main__":
    usage = "%(prog)s <-g GWAS_summary_stats -c col# -p col# -A col# -a col#> [options]"
    parser = argparse.ArgumentParser(usage=usage, description='GWAS QC')
    parser.add_argument('-g', '--gwas_summary', required=True, action='store', default=None, help='The file path of GWAS summary statistics [Default: None]')
    parser.add_argument('-c', '--chr_col', type=int, required=True, help='The column number of chromosomes (1-based, -c 1 indicates the first column) in GWAS summary statistics. This is required.')
    parser.add_argument('-p', '--pos_col', type=int, required=True, help='The column number of SNP positions (1-based) in GWAS summary statistics. This is required.')
    parser.add_argument('-A', '--allele1_col', type=int, required=True, help='The column number of effect alleles (1-based) in GWAS summary statistics. This is required.')
    parser.add_argument('-a', '--allele2_col', type=int, required=True, help='The column number of non-effect alleles (1-based) in GWAS summary statistics. This is required.')
    parser.add_argument('-P', '--pval_col', type=int, help='The column number of SNP p-values (1-based) in GWAS summary statistics. If redundant SNPs are found in GWAS summary statistics, using this column to distinguish the smallest SNP. Otherwise, reserving the first one among the redundant SNPs.')
    parser.add_argument('-b', '--beta_col', type=int, help='The column number of SNP beta values (1-based) in GWAS summary statistics. Required to define expanded sets that all have consistent minor allele effect directions.')
    parser.add_argument('-s', '--se_col', type=int, help='The column number of standard errors (1-based) in GWAS summary statistics')
    parser.add_argument('-f', '--af_col', type=int, help='The column number of SNP allele frequency values (1-based) in GWAS summary statistics. Required to provide if the --skip_check_MAF or --skip_check_ambiguous option is not given.')
    parser.add_argument('-r', '--rsid_col', type=int, help='The column number of SNP rsIDs (1-based) in GWAS summary statistics')
    parser.add_argument('-e', '--effect_dir_col', type=int, help='The column number of effect directions (1-based) for the GWAS meta-analysis in GWAS summary statistics')
    parser.add_argument('-d', '--delimiter', action='store', default="\t", help='Split columns of the input using the delimiter [Default: "\\t"]')
    parser.add_argument('-N', '--skip_check_negative_strand', action='store_true', default=False, help='Skip checking negative-strand SNPs [Default: False]')
    parser.add_argument('-M', '--skip_check_ambiguous', action='store_true', default=False, help='Skip checking SNPs with a strand-independent allele-pair (i.e. A/T or C/G SNPs [Default: False]')
    parser.add_argument('-F', '--skip_check_MAF', action='store_true', default=False, help='Skip checking allele frequency between observed and reference SNPs [Default: False]')
    parser.add_argument('--skip_normalizing_allele', action='store_true', default=False, help='Skip normalizing alleles (i.e., resolving reference and alternative alleles). By default, the QC step performs the process of allele normalization via the information from SNPs of 1000 Genomes Project or genome reference (for those SNPs with tagged "SNP not found" or "Unmatchable allele") and then generate "normalized_ref" and "normalized_alt" columns in the output. [Default: False]')
    parser.add_argument('-T', '--diff_MAF_threshold', default=0.15, type=float, help='Allowed difference between observed and reference allele frequency [Default: 0.15]')
    parser.add_argument('-G', '--genome_build', action='store', default='hg19', help='The version of human genome reference [Default: hg19]')
    parser.add_argument('-D', '--data_source', action='store', choices=['1kg', 'dbsnp'], default='1kg', help='Database for matching up with 1000 Genomes Project (1kg) or dbSNP (Not support yet) [Default: 1kg]')
    parser.add_argument('-S', '--super_population', action='store', choices=['GLOBAL', 'AFR', 'AMR', 'EAS', 'EUR', 'SAS'], default='GLOBAL', help='Set "GLOBAL" for all populations or a specific super population of 1000 Genomes data. Five super populations are provided: AFR (African), AMR (Ad Mixed American), EAS (East Asian), EUR (European) and SAS (South Asian). [Default: GLOBAL]')
    parser.add_argument('-m', '--normalization_method', action='store', choices=['alt', 'af'], default='alt', help='Support using alternative alleles (alt) from 1000 Genomes Project as effect alleles, or applying allele frequency (af) to normalize the effect direction of beta values [Default: alt]')
    parser.add_argument('-o', '--output_dir', action='store', default=getcwd(), help="The path to the desired output directory. The output file is named as [GWAS_SUMMARY_STATS_FILE_NAME].qc.tsv. [Default: {}]".format(getcwd()))
    parser.add_argument('-O', '--qc_available', action='store_true', default=False, help='Output SNPs as a file without INDELs. The output file is named as [GWAS_SUMMARY_STATS_FILE_NAME].qc_available.tsv. [Default: False]')
    parser.add_argument('-x', '--exclude_unspecified_cols', action='store_true', default=False, help='Skip reading unspecified columns [Default: False]')
    parser.add_argument('-X', '--exclude_bed', action='store', default=None, help='Provide a BED format file containing excluded regions [Default: None]')
    parser.add_argument('--config', action='store', help='Provide a specific configuration file to use')
    parser.add_argument('--prefix', action='store', default='', help='Add a prefix to ouptput files [Default: Null]')
    parser.add_argument('--skip_logging', action='store_true', default=False, help='Skip logging messages as a file [Default: False]')


    ## Show help without providing any options
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
        
    pargs = parser.parse_args()
    
    ## Generate output dir
    if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    ## Process configuration file
    conf_file_in_output_dir = path.join(pargs.output_dir, 'sparkinferno.cfg')
    applied_conf_path = ''
    if _is_path_exist(conf_file_in_output_dir):
        ## sparkinferno.cfg is found at the output dir, the analysis has beed conducted before
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
            applied_conf_path = pargs.config
        else:
            conf.read_config_file(conf_file_in_output_dir)
            applied_conf_path = conf_file_in_output_dir
    else:
        ## A new started analysis, no existing configuration file is found in the output directory
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
            applied_conf_path = pargs.config
        else:
            parser.error('The --config option is required for a new started analysis')
    
    ## Initialize the logging module
    this_script_name = path.splitext(path.basename(__file__))[0]
    logger = logging.getLogger(this_script_name)
    if not pargs.skip_logging:
        ## Set log path and initialize file handler
        log_dir = path.join(pargs.output_dir, 'log')
        if not _is_path_exist(log_dir): makedirs(log_dir)
        
        log_file_path = path.join(log_dir, "%s_%s.log" % (this_script_name, datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")))
        file_handler = logging.FileHandler(log_file_path)
        ## Set formatter
        file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
        logger.addHandler(file_handler)
        
    if not _is_path_exist(pargs.gwas_summary, True): sys.exit(1)
    # if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    # pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    logger.info('Running command: %s' % ' '.join([re.sub(r'(^$| +)', r'"\1"', arg, count=1) for arg in sys.argv])) ## Show quotation marks between a space or null character. (Example: -d " " or --prefix "")
    
    summary_report_msg_list = ["[Summary Report of GWAS QC]"]
    
    if pargs.prefix != '': pargs.prefix = pargs.prefix + '_'
    
    ## Get gwas_summary file name
    GWAS_summary_stats_file_name = pargs.prefix + path.splitext(path.basename(pargs.gwas_summary))[0]
    
    ## Get summary stats header
    schema_list = open(pargs.gwas_summary, 'r').readline().rstrip().split(pargs.delimiter)
    
    ## Convert column numbers from options with the "_col" suffix to *_col_name variables using schema_list to match up
    for parg in vars(pargs).keys():
        if parg[-4:] == '_col':
            parg_val = eval("pargs.%s" % parg)
            if parg_val is not None:
                ## e.g., chr_col_name = schema_list[pargs.chr_col-1]
                globals()["%s_name" % parg] = schema_list[parg_val-1]
            else:
                globals()["%s_name" % parg] = None
    
    kwargs = dict(
        chr_col_name = chr_col_name,
        pos_col_name = pos_col_name,
        af_col_name = af_col_name,
        rsid_col_name = rsid_col_name,
        allele1_col_name = allele1_col_name,
        allele2_col_name = allele2_col_name,
        pval_col_name = pval_col_name,
        beta_col_name = beta_col_name,
        se_col_name = se_col_name,
        effect_dir_col_name = effect_dir_col_name)
        
    if (not(pargs.skip_check_MAF and pargs.skip_check_ambiguous) and (af_col_name is None)):
        logger.error('The --af_col option is required if --skip_check_MAF and --skip_check_ambiguous options are not given')
        sys.exit(1)
        
    spark, sc = inferno.make_spark_session()

    ## Load GWAS summary stats
    gwas_df = preprocessing.load_file_to_df(pargs.gwas_summary, delimiter=pargs.delimiter, exclude_unspecified_cols=pargs.exclude_unspecified_cols, **kwargs).drop('region_pos').persist(pyspark.StorageLevel.DISK_ONLY)
    # ld_pruning_df.show(truncate=False)
    
    ## Check redundant SNPs
    agg_cols = [c for c in gwas_df.columns if c not in ['snp_id', pval_col_name]]
    exprs = [F.first(x).alias(x) for x in agg_cols]
    if bool(pargs.pval_col):
        gwas_df = gwas_df.withColumn(pval_col_name, gwas_df[pval_col_name].cast('double'))
        exprs.append(F.min(pval_col_name).alias(pval_col_name))
    
    gwas_df_unique = gwas_df.groupBy('snp_id')\
                            .agg(F.count('snp_id').alias('snp_count'), *exprs)\
                            .persist(pyspark.StorageLevel.DISK_ONLY)
    snp_count_df = gwas_df_unique.filter(gwas_df_unique['snp_count'] > 1).cache()
    if snp_count_df.count() > 0:
        for redundant_snp_id in snp_count_df.select('snp_id').collect():
            if bool(pargs.pval_col):
                logger.warn("Found redundant SNP in {}, pick up one with the smallest p-value instead.".format(redundant_snp_id['snp_id']))
            else:
                logger.warn("Found redundant SNP in {}, pick up the first one instead due to the p-value column is not provided.".format(redundant_snp_id['snp_id']))
        
        gwas_df = gwas_df_unique.select(gwas_df.columns)
    snp_count_df.unpersist(blocking=True)
    gwas_df_unique.unpersist(blocking=True)
    
    ## Get a dataframe with QC_category column and a dictionary of the numbers of QC stats
    overlap_df, qc_summary_num_dict = preprocessing.gwas_qc(gwas_df, conf.GADB_META_FILE, chr_col_name=chr_col_name, pos_col_name=pos_col_name, af_col_name=af_col_name, rsid_col_name=rsid_col_name, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, skip_check_negative_strand=pargs.skip_check_negative_strand, skip_check_ambiguous=pargs.skip_check_ambiguous, skip_check_MAF=pargs.skip_check_MAF, skip_normalizing_allele=pargs.skip_normalizing_allele, diff_MAF_threshold=pargs.diff_MAF_threshold, exclude_bed=pargs.exclude_bed, genome_build=pargs.genome_build, data_source=pargs.data_source, super_population=pargs.super_population, temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP, samtools_app=conf.SAMTOOLS_APP, conf_path=applied_conf_path)
    
    gwas_df.unpersist(blocking=True)
    
    ## Add normalized columns
    if bool(beta_col_name) or bool(af_col_name) or bool(effect_dir_col_name):
        normalized_cols = []
        if bool(af_col_name): normalized_cols.append('minor allele frequency')
        if bool(beta_col_name): normalized_cols.append('beta values')
        if bool(effect_dir_col_name): normalized_cols.append('effect directions')
        
        logger.info("Normalizing {} by comparison allele1/allele2 with alt/ref alleles".format(', '.join(normalized_cols)))
        
        if pargs.skip_normalizing_allele:
            overlap_df = preprocessing.normalize_effect_direction(overlap_df, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, af_col_name=af_col_name, beta_col_name=beta_col_name, effect_dir_col_name=effect_dir_col_name, target_col_prefix=pargs.data_source)
        else:
            overlap_df = preprocessing.normalize_effect_direction(overlap_df, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, af_col_name=af_col_name, beta_col_name=beta_col_name, effect_dir_col_name=effect_dir_col_name, target_col_prefix='normalized')
    
    else:
        logger.warn('The required column numbers are not provided. Skip normalization for minor allele frequency, beta values, and effect directions.')
    
    overlap_df = overlap_df.drop('snp_id').persist(pyspark.StorageLevel.DISK_ONLY)
    
    summary_report_msg_list.append('%d SNP(s) in the GWAS summary statistics' % overlap_df.count())
    logger.info(summary_report_msg_list[-1])
    summary_report_msg_list.append("GWAS QC summary:\n{}".format("\n".join(["\t"+k+': '+str(qc_summary_num_dict[k]) for k in sorted(qc_summary_num_dict.keys())])))
    logger.info("\n"+summary_report_msg_list[-1])
        
    ## Save df as a file
    preprocessing.save_df(overlap_df, path.join(pargs.output_dir, GWAS_summary_stats_file_name+'.qc.tsv'), temp_dir=conf.TMP_PATH)
    
    ## Save available SNPs as a file
    if pargs.qc_available:
        qc_available_file_name = GWAS_summary_stats_file_name+'.qc_available.tsv'
        available_snp_df = overlap_df.filter((F.col('QC_category') != 'INDEL variant') & (F.col('QC_category') != 'Excluded by user')).persist(pyspark.StorageLevel.DISK_ONLY)
        preprocessing.save_df(available_snp_df, path.join(pargs.output_dir, qc_available_file_name), temp_dir=conf.TMP_PATH)
        
        summary_report_msg_list.append('%d SNP(s) without "INDEL variant" and "Excluded by user" categories are available in %s for downstream analyses' % (available_snp_df.count(), qc_available_file_name))
        logger.info(summary_report_msg_list[-1])
        
    ## Save initially specified column names
    specified_column_names = [chr_col_name, 'pos_start', pos_col_name, allele1_col_name, allele2_col_name, pval_col_name, af_col_name, beta_col_name, se_col_name, rsid_col_name, effect_dir_col_name]
    ## Remove None values
    specified_column_names = [i for i in specified_column_names if i]
    with open(path.join(pargs.output_dir, 'specified_column_names.tsv'), 'w') as fp:
        fp.write("\t".join(specified_column_names))
    
    ## Generate summary report
    open(path.join(pargs.output_dir, 'summary_report.txt'), 'w').write("\n".join(summary_report_msg_list)+"\n\n")
