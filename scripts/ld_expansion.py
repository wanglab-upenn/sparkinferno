#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import path, makedirs, getcwd
import sys
import argparse
import pyspark
import sparkinferno.inferno as inferno
import sparkinferno.config as conf
import sparkinferno.preprocessing as preprocessing
import datetime
import logging
import re


def _is_path_exist(dir, error_msg=False):
    if path.exists(dir):
        return True
    else:
        if error_msg: sys.stderr.write(dir+": No such file or directory\n")
        return False

if __name__ == "__main__":
    usage = "%(prog)s <-t top_snp_list/LD_pruning_snps -c col# -p col# -A col# -a col#> [options]"
    parser = argparse.ArgumentParser(usage=usage, description='LD expansion by PLINK')
    parser.add_argument('-t', '--top_snp_list', required=True, action='store', default=None, help='Provide a formatted top SNPs list created by top_snp.py or LD-pruning SNP list created by ld_pruning.py. This is required. [Default: None]')
    parser.add_argument('-c', '--chr_col', type=int, required=True, help='The column number of chromosomes (1-based, -c 1 indicates the first column). This is required.')
    parser.add_argument('-p', '--pos_col', type=int, required=True, help='The column number of SNP positions (1-based). This is required.')
    parser.add_argument('-A', '--allele1_col', type=int, required=True, help='The column number of genomic refernece alleles in the top SNP list (1-based). This is required.')
    parser.add_argument('-a', '--allele2_col', type=int, required=True, help='The column number of alternative alleles in the top SNP list (1-based). This is required.')
    parser.add_argument('--ld_threshold', default=0.7, type=float, help='The r2 threshold for LD expansion [Default: 0.7]')
    parser.add_argument('--window_size', default=500000, type=int, help='How far away from each SNP to expand [Default: 500000]')
    parser.add_argument('-P', '--pval_col', type=int, help='The column number of SNP p-values (1-based)')
    parser.add_argument('-b', '--beta_col', type=int, help='The column number of SNP beta values (1-based)')
    parser.add_argument('-s', '--se_col', type=int, help='The column number of standard errors (1-based)')
    parser.add_argument('-f', '--af_col', type=int, help='The column number of SNP allele frequency values (1-based)')
    parser.add_argument('-r', '--rsid_col', type=int, help='The column number of SNP rsIDs (1-based)')
    parser.add_argument('-e', '--effect_dir_col', type=int, help='The column number of effect directions (1-based) for the GWAS meta-analysis in GWAS summary statistics')
    parser.add_argument('-g', '--gwas_summary', action='store', default=None, help='The file containig GWAS summary statistics [Default: None]')
    parser.add_argument('-d', '--delimiter', action='store', default="\t", help='Split columns of GWAS summary stats using the delimiter [Default: "\\t"]')
    parser.add_argument('-G', '--genome_build', action='store', default='hg19', help='The version of human genome reference [Default: hg19]')
    parser.add_argument('-q', '--quiet', action='store_true', default=False, help='Silence the message from PLINK [Default: False]')
    parser.add_argument('-S', '--super_population', action='store', choices=['AFR', 'AMR', 'EAS', 'EUR', 'SAS'], default='EUR', help='Specify a super population of 1000 Genomes genotype VCF file for LD pruning and expansion. Five super populations are provided: AFR (African), AMR (Ad Mixed American), EAS (East Asian), EUR (European) and SAS (South Asian). [Default: EUR]')
    parser.add_argument('-o', '--output_dir', action='store', default=getcwd(), help="The path to the desired output directory [Default: {}]".format(getcwd()))
    parser.add_argument('-B', '--output_bed', action='store_true', default=False, help='Save results as a BED file for downstream analysis of INFERNO2 [Default: False]')
    parser.add_argument('--config', action='store', help='Provide a specific configuration file to use')
    parser.add_argument('--prefix', action='store', default='', help='Add a prefix to ouptput files [Default: Null]')
    parser.add_argument('--skip_logging', action='store_true', default=False, help='Skip logging messages as a file [Default: False]')
    parser.add_argument('--keep_intermediate_files', action='store_true', default=False, help='Keep intermediate files/folders. Note: This function requires additional disk space. [Default: False]')


    ## Show help without providing any options
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
        
    pargs = parser.parse_args()
    
    ## Generate output dir
    if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    ## Process configuration file
    conf_file_in_output_dir = path.join(pargs.output_dir, 'sparkinferno.cfg')
    applied_conf_path = ''
    if _is_path_exist(conf_file_in_output_dir):
        ## sparkinferno.cfg is found at the output dir, the analysis has beed conducted before
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
            applied_conf_path = pargs.config
        else:
            conf.read_config_file(conf_file_in_output_dir)
            applied_conf_path = conf_file_in_output_dir
    else:
        ## A new started analysis, no existing configuration file is found in the output directory
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
            applied_conf_path = pargs.config
        else:
            parser.error('The --config option is required for a new started analysis')
    
    ## Initialize the logging module
    this_script_name = path.splitext(path.basename(__file__))[0]
    logger = logging.getLogger(this_script_name)
    logger_for_workers = logging.getLogger("%s.worker" % this_script_name)
    if not pargs.skip_logging:
        ## Set log path and initialize file handler
        log_dir = path.join(pargs.output_dir, 'log')
        if not _is_path_exist(log_dir): makedirs(log_dir)
        
        log_file_path = path.join(log_dir, "%s_%s.log" % (this_script_name, datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")))
        file_handler = logging.FileHandler(log_file_path)
        ## Set formatter
        file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
        logger.addHandler(file_handler)
        
        file_handler_workers = logging.FileHandler(log_file_path)
        file_handler_workers.setFormatter(logging.Formatter(fmt='', datefmt=''))
        logger_for_workers.addHandler(file_handler_workers)
    
    if not _is_path_exist(pargs.top_snp_list, True): sys.exit(1)
    if bool(pargs.gwas_summary):
        if not _is_path_exist(pargs.gwas_summary, True): sys.exit(1)
    # if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    # pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    logger.info('Running command: %s' % ' '.join([re.sub(r'(^$| +)', r'"\1"', arg, count=1) for arg in sys.argv])) ## Show quotation marks between a space or null character. (Example: -d " " or --prefix "")
    
    summary_report_msg_list = ["[Summary Report of LD Expansion]"]
    
    if pargs.prefix != '': pargs.prefix = pargs.prefix + '_'
    
    ## Get top SNP list/LD pruning SNP list header
    schema_list = open(pargs.top_snp_list, 'r').readline().rstrip().split("\t")
    
    ## Check if a user uses a wrong file as an input of top SNP list
    if ('rsID (optional)' in schema_list):
        sys.stderr.write("Error: Found the unformatted top SNP list. Please perform top_snp.py first to obtain the formatted top SNP list.\n")
        sys.exit(1)
    
    ## Convert column numbers from options with the "_col" suffix to *_col_name variables using schema_list to match up
    for parg in vars(pargs).keys():
        if parg[-4:] == '_col':
            parg_val = eval("pargs.%s" % parg)
            if parg_val is not None:
                ## e.g., chr_col_name = schema_list[pargs.chr_col-1]
                globals()["%s_name" % parg] = schema_list[parg_val-1]
            else:
                globals()["%s_name" % parg] = None
    
    kwargs = dict(
        chr_col_name = chr_col_name,
        pos_col_name = pos_col_name,
        af_col_name = af_col_name,
        rsid_col_name = rsid_col_name,
        allele1_col_name = allele1_col_name,
        allele2_col_name = allele2_col_name,
        pval_col_name = pval_col_name,
        beta_col_name = beta_col_name,
        se_col_name = se_col_name,
        effect_dir_col_name = effect_dir_col_name)
        
    spark, sc = inferno.make_spark_session()

    if 'snp_id' in schema_list:
        ## Load top SNP list/LD expansion SNP list
        ld_pruning_df = preprocessing.load_file_to_df(pargs.top_snp_list, **kwargs).select(schema_list)
        # ld_pruning_df.show(truncate=False)
    else:
        sys.stderr.write("Error: Unknown format file\n")
        sys.exit(1)
    
    ## Match up GWAS pval expansion with 1k Genomes and convert to VCF for the input of LD expansion
    plink_ld_expansion_dir = path.join(pargs.output_dir, 'PLINK_LD_expansion')
    vcf_path_df = preprocessing.matchup_1kGenomes_with_window_size(ld_pruning_df, conf.GADB_META_FILE, plink_ld_expansion_dir, temp_dir=conf.TMP_PATH, tabix_app=conf.TABIX_APP, chr_col_name=chr_col_name, pos_col_name=pos_col_name, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, window_size=pargs.window_size, super_population=pargs.super_population, genome_build=pargs.genome_build, conf_path=applied_conf_path, quiet=pargs.quiet)
    # vcf_path_df.show(truncate=False)
    
    if pargs.keep_intermediate_files:
        ## Save df as a file
        preprocessing.save_df(vcf_path_df, path.join(plink_ld_expansion_dir, "matchup_1kGenomes.window_size_%d.tsv" % pargs.window_size), temp_dir=conf.TMP_PATH)

    if bool(pargs.gwas_summary):
        ## Load GWAS summary stats
        summary_stats_df = preprocessing.load_file_to_df(pargs.gwas_summary, delimiter = pargs.delimiter, **kwargs)
    
        ## Perform LD expansion
        ld_expansion_df = preprocessing.ld_expansion(ld_pruning_df, vcf_path_df, gwas_df=summary_stats_df, chr_col_name=chr_col_name, pos_col_name=pos_col_name, af_col_name=af_col_name, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, ld_threshold=pargs.ld_threshold, window_size=pargs.window_size, quiet=pargs.quiet, plink_app=conf.PLINK_APP, conf_path=applied_conf_path).persist(pyspark.StorageLevel.DISK_ONLY)
        # ld_expansion_df.show(truncate=False)
    else:
        ld_expansion_df = preprocessing.ld_expansion(ld_pruning_df, vcf_path_df, chr_col_name=chr_col_name, pos_col_name=pos_col_name, af_col_name=af_col_name, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, ld_threshold=pargs.ld_threshold, window_size=pargs.window_size, quiet=pargs.quiet, plink_app=conf.PLINK_APP, conf_path=applied_conf_path).persist(pyspark.StorageLevel.DISK_ONLY)
    
    summary_report_msg_list.append("Identified %d distinct LD expanded SNP(s) from %d input SNP(s)" % (\
        ld_expansion_df.select('snp_id').distinct().count(), \
        ld_pruning_df.select('snp_id').distinct().count()))
    logger.info(summary_report_msg_list[-1])
    
    ld_expansion_df_output_filename = path.join(pargs.output_dir, "%sLD_expansion_snps.window_size_%s.ld_threshold_%.1f.tsv" % (pargs.prefix, pargs.window_size, pargs.ld_threshold))
    
    ld_pruning_df.unpersist(blocking=True)
    
    ## Save df as a file
    preprocessing.save_df(ld_expansion_df, ld_expansion_df_output_filename, temp_dir=conf.TMP_PATH)
    
    ## Output BED format for downstream analysis of INFERNO2
    if pargs.output_bed:
        preprocessing.save_df_as_bed(ld_expansion_df, ld_expansion_df_output_filename.replace('.tsv', '.bed'), af_col_name=af_col_name, rsid_col_name=rsid_col_name, allele1_col_name=allele1_col_name, allele2_col_name=allele2_col_name, pval_col_name=pval_col_name, temp_dir=conf.TMP_PATH)
    
    ## Remove intermediate files
    if not pargs.keep_intermediate_files:
        from shutil import rmtree
        rmtree(plink_ld_expansion_dir)
        
    ld_expansion_df.unpersist(blocking=True)
    
    ## Generate summary report
    open(path.join(pargs.output_dir, 'summary_report.txt'), 'w').write("\n".join(summary_report_msg_list)+"\n\n")
