#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pyspark
from pyspark.sql import functions as F
from os import path, makedirs, getcwd
import sys
import argparse
import sparkinferno.inferno as inferno
import sparkinferno.config as conf
import datetime
import logging
import re
from decimal import Decimal, ROUND_HALF_UP


def _is_path_exist(dir, error_msg=False):
    if path.exists(dir):
        return True
    else:
        if error_msg: logger.error(dir+": No such file or directory\n")
        return False


def _add_summary_report():
    global summary_report_msg_list
    
    summary_report_msg_list.append("Identified {} overlaps ({}% of the unique input SNPs/intervals) from {} intervals of {} {} tracks".format(\
        annotateGADB_df_count, \
        Decimal(str(100.0*annotateGADB_df.select('chr', 'chrStart', 'chrEnd').distinct().count()/input_distinct_count)).quantize(Decimal('.01'), rounding=ROUND_HALF_UP), \
        tracks_df.select(F.sum(F.col('Number of intervals').cast('int'))).collect()[0][0], \
        tracks_df.count(), \
        current_data_source))


if __name__ == "__main__":
    usage = "%(prog)s <-b INFERNO_bed> [options]"
    parser = argparse.ArgumentParser(usage=usage, description='Parallelized array/collection of genomic analyses (including analysis of regulatory elements, eQTLs, TFBS, etc) across all integrated datasets to characterize candidate genetic variants, their tissue contexts, affected regulatory elements, and target genes.')
    parser.add_argument('-b', '--bed', required=True, action='store', default=None, help='Provide an INFERNO BED format file created by ld_expansion.py. This is required. [Default: None]')
    parser.add_argument('--fantom5_window_size', default=1000, type=int, help='How far away from each SNP to expand [Default: 1000]')
    parser.add_argument('--replicate_frequency', default=1000, type=int, help='How many times of simulation in the empirical test [Default: 1000]')
    parser.add_argument('-G', '--genome_build', action='store', default='hg19', help='The version of human genome reference (hg19/hg38) [Default: hg19]')
    parser.add_argument('--gtex_version', action='store', choices=['v7', 'v8'], default='v7', help='The GETx version using in INFERNO genomic overlap and co-localization analysis steps [Default: v7]')
    parser.add_argument('--enrichment_method', action='store', choices=['empirical_pval', 'fisher_exact'], default='empirical_pval', help='Support empirical p-value (empirical_pval) or Fisher\'s exact test (fisher_exact) method in enrichment analysis [Default: empirical_pval]')
    parser.add_argument('-o', '--output_dir', action='store', default=getcwd(), help="The path to the desired output directory [Default: {}]".format(getcwd()))
    parser.add_argument('-t', '--tsv', action='store', default=None, help='Provide a TSV file created by ld_expansion.py for the generation of a full metatable instead of the input BED file [Default: None]')
    parser.add_argument('-g', '--gwas_summary', action='store', default=None, help='Provide a .qc.tsv or .qc_available.tsv file created by gwas_qc.py for the generation of a full metatable instead of the input BED file [Default: None]')
    parser.add_argument('--config', action='store', help='Provide a specific configuration file to use')
    parser.add_argument('--prefix', action='store', default='', help='Add a prefix to ouptput files [Default: Null]')
    parser.add_argument('--skip_fantom5', action='store_true', default=False, help='Skip FANTOM5 overlap [Default: False]')
    parser.add_argument('--skip_gtex', action='store_true', default=False, help='Skip GTEx overlap [Default: False]')
    parser.add_argument('--skip_chromhmm', action='store_true', default=False, help='Skip ChromHMM enhancers overlap [Default: False]')
    parser.add_argument('--skip_genomic_partition', action='store_true', default=False, help='Skip INFERNO genomic partition overlap [Default: False]')
    parser.add_argument('--skip_targetscan', action='store_true', default=False, help='Skip TargetScan overlap [Default: False]')
    parser.add_argument('--skip_dashr2', action='store_true', default=False, help='Skip DASHR2 overlap [Default: False]')
    parser.add_argument('--skip_homer', action='store_true', default=False, help='Skip Homer transcription factor binding sites overlap [Default: False]')
    parser.add_argument('--skip_enrichment', action='store_true', default=False, help='Skip enrichment analysis in track and tissue summaries [Default: False]')
    parser.add_argument('--skip_genomic_element_plot', action='store_true', default=False, help='Skip to generate a bar plot for genomic elements [Default: False]')
    parser.add_argument('--skip_tissue_category_plot', action='store_true', default=False, help='Skip to generate a tile plot for tissue category [Default: False]')
    parser.add_argument('--skip_logging', action='store_true', default=False, help='Skip logging messages as a file [Default: False]')
    parser.add_argument('--keep_intermediate_files', action='store_true', default=False, help='Keep intermediate files/folders. Note: This function requires additional disk space. [Default: False]')
    # parser.add_argument('--remove_metatable_part_file_dir', action='store_true', default=False, help=argparse.SUPPRESS) ## This hidden option is used for removing part files in a metatable folder when this folder doesn't be used in the further step (co-localization analysis), for example, input lead SNPs only.


    ## Show help without providing any options
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
        
    pargs = parser.parse_args()
    
    ## Generate output dir
    if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    ## Process configuration file
    conf_file_in_output_dir = path.join(pargs.output_dir, 'sparkinferno.cfg')
    applied_conf_path = ''
    if _is_path_exist(conf_file_in_output_dir):
        ## sparkinferno.cfg is found at the output dir, the analysis has beed conducted before
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
            applied_conf_path = pargs.config
        else:
            conf.read_config_file(conf_file_in_output_dir)
            applied_conf_path = conf_file_in_output_dir
    else:
        ## A new started analysis, no existing configuration file is found in the output directory
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
            applied_conf_path = pargs.config
        else:
            parser.error('The --config option is required for a new started analysis')
    
    ## Expand event queue capacity for merging massive records of interval summaries into metaTable
    conf.SPARK_CONF.append(('spark.scheduler.listenerbus.eventqueue.capacity', '100000'))
    
    ## Initialize the logging module
    this_script_name = path.splitext(path.basename(__file__))[0]
    logger = logging.getLogger(this_script_name)
    logger_for_workers = logging.getLogger("%s.worker" % this_script_name)
    if not pargs.skip_logging:
        ## Set log path and initialize file handler
        log_dir = path.join(pargs.output_dir, 'log')
        if not _is_path_exist(log_dir): makedirs(log_dir)
        
        log_file_path = path.join(log_dir, "%s_%s.log" % (this_script_name, datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")))
        file_handler = logging.FileHandler(log_file_path)
        ## Set formatter
        file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
        logger.addHandler(file_handler)
        
        file_handler_workers = logging.FileHandler(log_file_path)
        file_handler_workers.setFormatter(logging.Formatter(fmt='', datefmt=''))
        logger_for_workers.addHandler(file_handler_workers)
    
    if not _is_path_exist(pargs.bed, True): sys.exit(1)
    if bool(pargs.tsv):
        if not _is_path_exist(pargs.tsv, True): sys.exit(1)
    # if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    # pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    if (not pargs.skip_gtex) and (pargs.genome_build == 'hg19') and (pargs.gtex_version == 'v8'):
        logger.warning("The GTEx v8 doesn't support hg19, use the GTEx version v7 by default.")
        pargs.gtex_version = 'v7'
    elif (not pargs.skip_gtex) and (pargs.genome_build == 'hg38') and (pargs.gtex_version != 'v8'):
        logger.warning("The GTEx %s doesn't support hg38, use the GTEx version v8 by default." % pargs.gtex_version)
        pargs.gtex_version = 'v8'
    
    logger.info('Running command: %s' % ' '.join([re.sub(r'(^$| +)', r'"\1"', arg, count=1) for arg in sys.argv])) ## Show quotation marks between a space or null character. (Example: -d " " or --prefix "")
    
    summary_report_msg_list = ["[Summary Report of INFERNO Genomic Overlap]"]
    
    if pargs.prefix != '': pargs.prefix = pargs.prefix + '_'
    
    ## == Overlapping tracks ==
    spark, sc = inferno.make_spark_session()
    gadb_df = inferno.loadgadb(conf.GADB_META_FILE).cache()
    ## GADB Filters
    if pargs.genome_build == 'hg38' or pargs.genome_build == 'hg38-lifted':
        FANTOM5 = "Data Source:FANTOM5_Enhancers-lifted,Output type:Enhancer peaks,Genome build:%s-lifted" % pargs.genome_build
        TargetScan = "Data Source:TargetScan_v7p2-lifted,Genome build:%s-lifted" % pargs.genome_build
    else:
        FANTOM5 = "Data Source:FANTOM5_Enhancers,Output type:Enhancer peaks,Genome build:%s" % pargs.genome_build
        TargetScan = "Data Source:TargetScan_v7p2,Genome build:%s" % pargs.genome_build

    GTEx = "Data Source:GTEx_%s,Output type:eQTL_significant_associations" % pargs.gtex_version
    ChromHMM_enhancers = "Data Source:ROADMAP_Enhancers,Genome build:%s" % pargs.genome_build
    genomic_partition = "Data Source:INFERNO_genomic_partition,Genome build:%s" % pargs.genome_build
    DASHR2 = "Data Source:DASHR2,Genome build:%s" % pargs.genome_build
    HOMER = "Data Source:HOMER,Genome build:%s" % pargs.genome_build
    
    interval_file_paths = [] ## This list saves paths of interval_summary directories as an input for metatable generation
    annotateGADB_file_paths = [] ## This list saves paths of GADB_summary directories as an input for generation of a tile plot
    
    input_bed_rdd = inferno.load_bedfile_as_rdd(pargs.bed, partition_num=4).persist(pyspark.StorageLevel.DISK_ONLY)
    ## Get number of unique position
    input_distinct_count = input_bed_rdd.map(lambda x: "{}:{}-{}".format(x[0],x[1],x[2])).distinct().count()
    
    summary_report_msg_list.append("Found {} unique SNP(s)/interval(s) in the input BED file".format(input_distinct_count))
    logger.info(summary_report_msg_list[-1])
    
    if not pargs.skip_fantom5:
        logger.info('Overlapping FANTOM5 tracks')
        logger.info('Applying filter: ' + FANTOM5)
        tracks_df = inferno.select(gadb_df, FANTOM5)
        current_data_source = tracks_df.select('Data Source').collect()[0]['Data Source']
        
        tissue_category_dfs = tracks_df.select('Data Source', 'Tissue category').distinct() ## Collect all tissue categories of FANTOM5, GTEx, and ChromHMM_enhancers (ROADMAP)
        
        overlap_df = inferno.loadgiggle(input_bed_rdd, tracks_df, method='cmd_partitions', window_size=pargs.fantom5_window_size, temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP)
        annotateGADB_df = inferno.annotateGADB(overlap_df, tracks_df).persist(pyspark.StorageLevel.DISK_ONLY)
        annotateGADB_df_count = annotateGADB_df.count()
        
        _add_summary_report()
        logger.info(summary_report_msg_list[-1])
        
        if annotateGADB_df_count > 0:
            inferno.saveAnnotateGADB(annotateGADB_df, pargs.prefix+'FANTOM5', pargs.output_dir, temp_dir=conf.TMP_PATH)
            annotateGADB_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'FANTOM5', 'GADB_summary'))
            interval_df1 = inferno.interval_summary(annotateGADB_df, data_source=current_data_source, prefix='FANTOM5')
            inferno.saveInterval(interval_df1, pargs.prefix+'FANTOM5', pargs.output_dir, temp_dir=conf.TMP_PATH)
            interval_df1.unpersist(blocking=True)
            interval_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'FANTOM5', 'interval_summary'))
            track_df = inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source, method=pargs.enrichment_method, replicate_frequency=pargs.replicate_frequency, window_size=pargs.fantom5_window_size, temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP) if not(pargs.skip_enrichment) else inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTrack(track_df, pargs.prefix+'FANTOM5', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            track_df.unpersist(blocking=True)
            tissue_df = inferno.tissue_summary(input_bed_rdd, annotateGADB_df, current_data_source, method=pargs.enrichment_method, replicate_frequency=pargs.replicate_frequency, window_size=pargs.fantom5_window_size, temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP) if not(pargs.skip_enrichment) else inferno.tissue_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTissue(tissue_df, pargs.prefix+'FANTOM5', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            tissue_df.unpersist(blocking=True)
        annotateGADB_df.unpersist(blocking=True)
    
    if not pargs.skip_gtex:
        logger.info('Overlapping GTEx tracks')
        logger.info('Applying filter: ' + GTEx)
        tracks_df = inferno.select(gadb_df, GTEx)
        current_data_source = tracks_df.select('Data Source').collect()[0]['Data Source']
        
        if 'tissue_category_dfs' in dir(): ## Prevent error if skipping some data sources
            tissue_category_dfs = tissue_category_dfs.union(tracks_df.select('Data Source', 'Tissue category').distinct())
        else:
            tissue_category_dfs = tracks_df.select('Data Source', 'Tissue category').distinct()
        
        overlap_df = inferno.loadgiggle(input_bed_rdd, tracks_df, method='cmd_partitions', temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP)
        annotateGADB_df = inferno.annotateGADB(overlap_df, tracks_df, current_data_source, ensg2symbol_file=conf.ENSEMBL_ID_TO_GENE_SYMBOL_FILE).persist(pyspark.StorageLevel.DISK_ONLY)
        annotateGADB_df_count = annotateGADB_df.count()
        
        _add_summary_report()
        logger.info(summary_report_msg_list[-1])
        
        if annotateGADB_df_count > 0:
            inferno.saveAnnotateGADB(annotateGADB_df, pargs.prefix+'GTEx', pargs.output_dir, temp_dir=conf.TMP_PATH)
            annotateGADB_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'GTEx', 'GADB_summary'))
            interval_df2 = inferno.interval_summary(annotateGADB_df, data_source=current_data_source, prefix='GTEx')
            inferno.saveInterval(interval_df2, pargs.prefix+'GTEx', pargs.output_dir, temp_dir=conf.TMP_PATH)
            interval_df2.unpersist(blocking=True)
            interval_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'GTEx', 'interval_summary'))
            track_df = inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTrack(track_df, pargs.prefix+'GTEx', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            track_df.unpersist(blocking=True)
            tissue_df = inferno.tissue_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTissue(tissue_df, pargs.prefix+'GTEx', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            tissue_df.unpersist(blocking=True)
        annotateGADB_df.unpersist(blocking=True)
    
    if not pargs.skip_chromhmm:
        logger.info('Overlapping ChromHMM enhancer (ROADMAP) tracks')
        logger.info('Applying filter: ' + ChromHMM_enhancers)
        tracks_df = inferno.select(gadb_df, ChromHMM_enhancers)
        current_data_source = tracks_df.select('Data Source').collect()[0]['Data Source']
        
        if 'tissue_category_dfs' in dir(): ## Prevent error if skipping some data sources
            tissue_category_dfs = tissue_category_dfs.union(tracks_df.select('Data Source', 'Tissue category').distinct())
        else:
            tissue_category_dfs = tracks_df.select('Data Source', 'Tissue category').distinct()
            
        overlap_df = inferno.loadgiggle(input_bed_rdd, tracks_df, method='cmd_partitions', temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP)
        annotateGADB_df = inferno.annotateGADB(overlap_df, tracks_df).persist(pyspark.StorageLevel.DISK_ONLY)
        annotateGADB_df_count = annotateGADB_df.count()
        
        _add_summary_report()
        logger.info(summary_report_msg_list[-1])
        
        if annotateGADB_df_count > 0:
            inferno.saveAnnotateGADB(annotateGADB_df, pargs.prefix+'ChromHMM_enhancers', pargs.output_dir, temp_dir=conf.TMP_PATH)
            annotateGADB_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'ChromHMM_enhancers', 'GADB_summary'))
            interval_df3 = inferno.interval_summary(annotateGADB_df, data_source=current_data_source, prefix='ChromHMM_enhancers')
            inferno.saveInterval(interval_df3, pargs.prefix+'ChromHMM_enhancers', pargs.output_dir, temp_dir=conf.TMP_PATH)
            interval_df3.unpersist(blocking=True)
            interval_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'ChromHMM_enhancers', 'interval_summary'))
            track_df = inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source, method=pargs.enrichment_method, replicate_frequency=pargs.replicate_frequency, temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP) if not(pargs.skip_enrichment) else inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTrack(track_df, pargs.prefix+'ChromHMM_enhancers', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            track_df.unpersist(blocking=True)
            tissue_df = inferno.tissue_summary(input_bed_rdd, annotateGADB_df, current_data_source, method=pargs.enrichment_method, replicate_frequency=pargs.replicate_frequency, temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP) if not(pargs.skip_enrichment) else inferno.tissue_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTissue(tissue_df, pargs.prefix+'ChromHMM_enhancers', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            tissue_df.unpersist(blocking=True)
        annotateGADB_df.unpersist(blocking=True)

    if not pargs.skip_genomic_partition:
        logger.info('Overlapping genomic partition tracks')
        logger.info('Applying filter: ' + genomic_partition)
        tracks_df = inferno.select(gadb_df, genomic_partition)
        current_data_source = tracks_df.select('Data Source').collect()[0]['Data Source']
        overlap_df = inferno.loadgiggle(input_bed_rdd, tracks_df, method='cmd_partitions', temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP)
        annotateGADB_df = inferno.annotateGADB(overlap_df, tracks_df, 'INFERNO_genomic_partition').persist(pyspark.StorageLevel.DISK_ONLY)
        annotateGADB_df_count = annotateGADB_df.count()
        
        _add_summary_report()
        logger.info(summary_report_msg_list[-1])
        
        if annotateGADB_df_count > 0:
            inferno.saveAnnotateGADB(annotateGADB_df, pargs.prefix+'Genomic_partition', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            interval_df4 = inferno.interval_summary(annotateGADB_df, data_source=current_data_source, prefix='Genomic_partition')
            inferno.saveInterval(interval_df4, pargs.prefix+'Genomic_partition', pargs.output_dir, temp_dir=conf.TMP_PATH)
            interval_df4.unpersist(blocking=True)
            interval_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'Genomic_partition', 'interval_summary'))
            track_df = inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source, method=pargs.enrichment_method, replicate_frequency=pargs.replicate_frequency, temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP) if not(pargs.skip_enrichment) else inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTrack(track_df, pargs.prefix+'Genomic_partition', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            track_df.unpersist(blocking=True)
            
            ## Generate a tile plot for tissue category
            if not pargs.skip_genomic_element_plot:
                logger.info('Generating genomic element plot')
            
                genomic_element_list_path = inferno.generate_genomic_element_list_for_bar_plot(input_bed_rdd, annotateGADB_df, path.join(pargs.output_dir, pargs.prefix+'genomic_element_list.tsv'), temp_dir=conf.TMP_PATH)
                
                if _is_path_exist(genomic_element_list_path):
                    inferno.genomic_element_bar_plot(genomic_element_list_path, path.join(pargs.output_dir, pargs.prefix+'genomic_element_plot.pdf'), rscript_app=conf.RSCRIPT_APP)
                else:
                    logger.warning('Generating the genomic element list failed. Skip genomic element plot.')
        
        annotateGADB_df.unpersist(blocking=True)

    if not pargs.skip_targetscan:
        logger.info('Overlapping TargetScan tracks')
        logger.info('Applying filter: ' + TargetScan)
        tracks_df = inferno.select(gadb_df, TargetScan)
        current_data_source = tracks_df.select('Data Source').collect()[0]['Data Source']
        overlap_df = inferno.loadgiggle(input_bed_rdd, tracks_df, method='cmd_partitions', temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP)
        annotateGADB_df = inferno.annotateGADB(overlap_df, tracks_df, 'TargetScan_v7p2').persist(pyspark.StorageLevel.DISK_ONLY)
        annotateGADB_df_count = annotateGADB_df.count()
        
        _add_summary_report()
        logger.info(summary_report_msg_list[-1])
        
        if annotateGADB_df_count > 0:
            inferno.saveAnnotateGADB(annotateGADB_df, pargs.prefix+'TargetScan', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            interval_df5 = inferno.interval_summary(annotateGADB_df, data_source=current_data_source, prefix='TargetScan')
            inferno.saveInterval(interval_df5, pargs.prefix+'TargetScan', pargs.output_dir, temp_dir=conf.TMP_PATH)
            interval_df5.unpersist(blocking=True)
            interval_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'TargetScan', 'interval_summary'))
            track_df = inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source, method=pargs.enrichment_method, replicate_frequency=pargs.replicate_frequency, temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP) if not(pargs.skip_enrichment) else inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTrack(track_df, pargs.prefix+'TargetScan', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            track_df.unpersist(blocking=True)
        annotateGADB_df.unpersist(blocking=True)
        
    if not pargs.skip_dashr2:
        logger.info('Overlapping DASHR2 non-coding RNA tracks')
        logger.info('Applying filter: ' + DASHR2)
        tracks_df = inferno.select(gadb_df, DASHR2)
        current_data_source = tracks_df.select('Data Source').collect()[0]['Data Source']
        overlap_df = inferno.loadgiggle(input_bed_rdd, tracks_df, method='cmd_partitions', temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP)
        annotateGADB_df = inferno.annotateGADB(overlap_df, tracks_df, 'DASHR2').persist(pyspark.StorageLevel.DISK_ONLY)
        annotateGADB_df_count = annotateGADB_df.count()
        
        _add_summary_report()
        logger.info(summary_report_msg_list[-1])
        
        if annotateGADB_df_count > 0:
            inferno.saveAnnotateGADB(annotateGADB_df, pargs.prefix+'DASHR2', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            interval_df6 = inferno.interval_summary(annotateGADB_df, data_source='DASHR2', prefix='DASHR2')
            inferno.saveInterval(interval_df6, pargs.prefix+'DASHR2', pargs.output_dir, temp_dir=conf.TMP_PATH)
            interval_df6.unpersist(blocking=True)
            interval_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'DASHR2', 'interval_summary'))
            track_df = inferno.track_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTrack(track_df, pargs.prefix+'DASHR2', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            track_df.unpersist(blocking=True)
            tissue_df = inferno.tissue_summary(input_bed_rdd, annotateGADB_df, current_data_source)
            inferno.saveTissue(tissue_df, pargs.prefix+'DASHR2', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            tissue_df.unpersist(blocking=True)
        annotateGADB_df.unpersist(blocking=True)
        
    if not pargs.skip_homer:
        logger.info('Overlapping HOMER motif tracks')
        logger.info('Applying filter: ' + HOMER)
        tracks_df = inferno.select(gadb_df, HOMER)
        current_data_source = tracks_df.select('Data Source').collect()[0]['Data Source']
        overlap_df = inferno.loadgiggle(input_bed_rdd, tracks_df, method='cmd_partitions', temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP).persist(pyspark.StorageLevel.DISK_ONLY)
        motif_df = inferno.get_motif_overlap(overlap_df, motif_pwm_file=conf.MOTIF_PWM_FILE, conf_path=applied_conf_path)
        inferno.saveMotif(motif_df, pargs.prefix+'HOMER', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
        annotateGADB_df = inferno.annotateGADB(overlap_df, tracks_df).persist(pyspark.StorageLevel.DISK_ONLY)
        annotateGADB_df_count = annotateGADB_df.count()
        
        _add_summary_report()
        logger.info(summary_report_msg_list[-1])
        
        if annotateGADB_df_count > 0:
            inferno.saveAnnotateGADB(annotateGADB_df, pargs.prefix+'HOMER', pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
            interval_df7 = inferno.interval_summary(motif_df, data_source=current_data_source, prefix='HOMER')
            inferno.saveInterval(interval_df7, pargs.prefix+'HOMER', pargs.output_dir, temp_dir=conf.TMP_PATH)
            interval_df7.unpersist(blocking=True)
            interval_file_paths.append(path.join(pargs.output_dir, pargs.prefix+'HOMER', 'interval_summary'))
        overlap_df.unpersist(blocking=True)
        motif_df.unpersist(blocking=True)
        annotateGADB_df.unpersist(blocking=True)
    
    input_bed_rdd.unpersist()    
    
    ## == Generate metatable by dataframes of overlapping tracks ==
    if len(interval_file_paths)>0:
        logger.info('Generating metatable')
        metatable_template = pargs.tsv if bool(pargs.tsv) else pargs.bed    
        metatable = inferno.metaTable_from_files(metatable_template, interval_file_paths, gwas_tsv_path=pargs.gwas_summary)
        
        ## Add concordant tissue information in metatable
        if ('tissue_category_dfs' in dir()) and (len(annotateGADB_file_paths)>0):
            snp_tissue_combination_df = inferno.get_snp_and_tissue_category_combinations(metatable_template, tissue_category_dfs, annotateGADB_file_paths, concordant_data_sources=['FANTOM5_Enhancers', 'ROADMAP_Enhancers', 'GTEx_'+pargs.gtex_version]).persist(pyspark.StorageLevel.DISK_ONLY)
            
            metatable = inferno.add_concordant_tissue_into_metatable(snp_tissue_combination_df, metatable, concordant_data_sources=['FANTOM5_Enhancers', 'ROADMAP_Enhancers', 'GTEx_'+pargs.gtex_version])
        
        metatable = metatable.persist(pyspark.StorageLevel.DISK_ONLY)
        inferno.saveMetatable(metatable, pargs.output_dir, pargs.prefix, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
        
        ## Generate GWAS locus summary stat from the metatable
        if 'region_name' in metatable.columns:
            logger.info('Generating locus overlap summary stat')
            locus_overlap_summary_stat_df = inferno.metaTable_locus_overlap_summary_stat(metatable)
            inferno.save_locus_overlap_summary_stat(locus_overlap_summary_stat_df, pargs.output_dir, pargs.prefix, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
        
        metatable.unpersist(blocking=True)
    
    else:
        logger.warning('No any overlap found. Generating metatable failed.')
        
    ## Generate a tile plot for tissue category
    if 'snp_tissue_combination_df' in dir():
        if not pargs.skip_tissue_category_plot:
            logger.info('Generating tissue category plot')
        
            tissue_category_list_path = inferno.generate_tissue_category_list_for_tile_plot(snp_tissue_combination_df, path.join(pargs.output_dir, pargs.prefix+'tissue_category_list.tsv'), data_sources=['FANTOM5_Enhancers', 'ROADMAP_Enhancers', 'FANTOM5_Enhancers+ROADMAP_Enhancers', 'GTEx_'+pargs.gtex_version], temp_dir=conf.TMP_PATH)
            
            if _is_path_exist(tissue_category_list_path):
                inferno.tissue_category_tile_plot(tissue_category_list_path, path.join(pargs.output_dir, pargs.prefix+'tissue_category_plot.pdf'), rscript_app=conf.RSCRIPT_APP)
            else:
                logger.warning('Generating the tissue category list failed. Skip tissue category plot.')
                
        # snp_tissue_combination_df.unpersist()
    
    ## Remove intermediate files
    if not pargs.keep_intermediate_files:
        from shutil import rmtree
        ## Remove interval_summary folders
        for interval_file_path in interval_file_paths:
            rmtree(interval_file_path)
        ## Remove GADB_summary folders
        for annotateGADB_file_path in annotateGADB_file_paths:
            rmtree(annotateGADB_file_path)
            
    ## Generate summary report
    open(path.join(pargs.output_dir, 'summary_report.txt'), 'w').write("\n".join(summary_report_msg_list)+"\n\n")
