#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import path, makedirs, getcwd, remove
import sys
import argparse
import pyspark
import sparkinferno.inferno as inferno
import sparkinferno.config as conf
import sparkinferno.gig as gig
import sparkinferno.colocalization as coloc
import datetime
import logging
import re


def _is_path_exist(dir, error_msg=False):
    if path.exists(dir):
        return True
    else:
        if error_msg: sys.stderr.write(dir+": No such file or directory\n")
        return False

if __name__ == "__main__":
    usage = "%(prog)s <-b INFERNO_bed -g GWAS_summary_stats_bed/GWAS_Giggle_index -s #sample_size> [options]"
    parser = argparse.ArgumentParser(usage=usage, description='Co-localization analysis')
    parser.add_argument('-b', '--bed', required=True, action='store', default=None, help='Provide an INFERNO BED format file created by top_snp.py, pval_expansion.py, ld_pruning.py, or ld_expansion.py with -B/--output_bed option. This is required. [Default: None]')
    parser.add_argument('-g', '--gwas_summary_bed', required=True, action='store', default=None, help='The file containig a BED format file (or bed.gz) of GWAS summary statistics created by top_snp.py. Or using -g to provide the Giggle index path instead. This is required. [Default: None]')
    parser.add_argument('-s', '--sample_size', type=int, required=True, help='Number of samples in GWAS. This is required.')
    parser.add_argument('-I', '--giggle_index', action='store_true', default=False, help='Indicate that the path of GWAS summary statistics behind -g is the directory of Giggle index [Default: False]')
    parser.add_argument('-t', '--analysis_type', action='store', default='quant', choices=['quant', 'cc'], help='The data type of the GWAS dataset. Choosing "quant" or "cc" type denotes GWAS data is quantitative or case-control type, respectively. When specified as the cc type, the -p/--sample_proportion option for a case/control proportion of samples MUST be provided, simultaneously. [Default: quant]')
    parser.add_argument('-p', '--sample_proportion', type=float, help='Number of sample proportion in GWAS')
    parser.add_argument('-e', '--expansion_type', action='store', choices=['LD_block', 'constant'], default='LD_block', help='Define how to obtain expanded SNPs from each tag SNP. This option can be set as "LD_block" or "constant", the former represents using the length of LD block to expand SNPs from GWAS; the latter represents using a constant value to expand. [Default: LD_block]')
    parser.add_argument('-w', '--ld_block_window_size', default=500000, type=int, help='How far away from each SNP to expand in an LD block [Default: 500000]')
    parser.add_argument('-W', '--expansion_window_size', default=500000, type=int, help='How far away of tag SNP to expand. This option is enabled when the --expansion_type option is set "constant." [Default: 500000]')
    parser.add_argument('-G', '--genome_build', action='store', default='hg19', help='The version of human genome reference (hg19/hg38) [Default: hg19]')
    parser.add_argument('--gtex_version', action='store', choices=['v7', 'v8'], default='v7', help='The GETx version of eQTL datasets [Default: v7]')
    parser.add_argument('--snp_id_col', default=4, type=int, help='The column number of snp_id in the GWAS summary statistics [Default: 4]')
    parser.add_argument('--ref_col', default=5, type=int, help='The column number of referece allele in the GWAS summary statistics [Default: 5]')
    parser.add_argument('--alt_col', default=6, type=int, help='The column number of alternate allele in the GWAS summary statistics [Default: 6]')
    parser.add_argument('--pval_col', default=8, type=int, help='The column number of p-value in the GWAS summary statistics [Default: 8]')
    parser.add_argument('--maf_col', default=9, type=int, help='The column number of minor allele frequency (MAF) in the GWAS summary statistics [Default: 9]')
    parser.add_argument('--beta_col', default=-1, type=int, help='The column number of beta value in the GWAS summary statistics')
    parser.add_argument('--se_col', default=-1, type=int, help='The column number of standard error in the GWAS summary statistics')
    parser.add_argument('--rsid_col', default=-1, type=int, help='The column number of rsID in the GWAS summary statistics')
    parser.add_argument('--h4_cutoff', default=0.5, type=float, help='The cutoff value of locus posterior probabilities of H4 (one common causal variant) in coloc summary stats [Default: 0.5]')
    parser.add_argument('--abf_cutoff', default=0.5, type=float, help='The cutoff for a cumulative value of the SNP posterior probabilities of H4 [Default: 0.5]')
    parser.add_argument('--nsnps_cutoff', default=5, type=int, help='The cutoff of SNP number in a SNP-gene association [Default: 5]')
    parser.add_argument('-m', '--metatable', action='store', default=None, help='Provide a path of the metatable (i.e., coalesced_metatable.tsv) created by inferno_genomic_overlap.py to integrate coloc summary stats behind the metatable [Default: None]')
    parser.add_argument('--gzip', action='store_true', default=False, help='Compress input and output files for colocalization analysis [Default: False]')
    parser.add_argument('-o', '--output_dir', action='store', default=getcwd(), help="The path to the desired output directory [Default: {}]".format(getcwd()))
    parser.add_argument('--config', action='store', help='Provide a specific configuration file to use')
    parser.add_argument('--prefix', action='store', default='', help='Add a prefix to ouptput files [Default: Null]')
    parser.add_argument('--skip_logging', action='store_true', default=False, help='Skip logging messages as a file [Default: False]')
    parser.add_argument('--keep_intermediate_files', action='store_true', default=False, help='Keep intermediate files/folders. Note: This function requires additional disk space. [Default: False]')

    
    ## Show help without providing any options
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
    
    pargs = parser.parse_args()
    
    ## Generate output dir
    if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    ## Process configuration file
    conf_file_in_output_dir = path.join(pargs.output_dir, 'sparkinferno.cfg')
    applied_conf_path = ''
    if _is_path_exist(conf_file_in_output_dir):
        ## sparkinferno.cfg is found at the output dir, the analysis has beed conducted before
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
            applied_conf_path = pargs.config
        else:
            conf.read_config_file(conf_file_in_output_dir)
            applied_conf_path = conf_file_in_output_dir
    else:
        ## A new started analysis, no existing configuration file is found in the output directory
        if bool(pargs.config):
            ## --config is provided
            if not _is_path_exist(pargs.config, True): sys.exit(1)
            conf.read_config_file(pargs.config)
            applied_conf_path = pargs.config
        else:
            parser.error('The --config option is required for a new started analysis')
    
    ## Initialize the logging module
    this_script_name = path.splitext(path.basename(__file__))[0]
    logger = logging.getLogger(this_script_name)
    if not pargs.skip_logging:
        ## Set log path and initialize file handler
        log_dir = path.join(pargs.output_dir, 'log')
        if not _is_path_exist(log_dir): makedirs(log_dir)
        
        log_file_path = path.join(log_dir, "%s_%s.log" % (this_script_name, datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")))
        file_handler = logging.FileHandler(log_file_path)
        ## Set formatter
        file_handler.setFormatter(logging.Formatter(fmt=conf.LOG_FORMAT[conf.LOG_LEVEL], datefmt=conf.LOG_DATEFMT))
        logger.addHandler(file_handler)
    
    if not _is_path_exist(pargs.bed, True): sys.exit(1)
    if not _is_path_exist(pargs.gwas_summary_bed, True): sys.exit(1)
    # if not _is_path_exist(pargs.output_dir): makedirs(pargs.output_dir)
    # pargs.output_dir = path.abspath(path.expanduser(pargs.output_dir))
    
    if pargs.analysis_type == 'cc' and pargs.sample_proportion is None:
        ## Confirm the analysis type of the GWAS dataset for coloc package. Choosing "quant" (default) or "cc" type denotes GWAS data is quantitative or case-control type, respectively. When specified as the cc type, the pargs.sample_proportion for case/control sample proportion MUST be provided, simultaneously.
        parser.error('The option -p/--sample_proportion for a case-control proportion of samples is required in the case-control (cc) type')
        sys.exit(1)
    
    if pargs.genome_build == 'hg19' and pargs.gtex_version == 'v8':
        logger.warning("The GTEx v8 doesn't support hg19, use the GTEx version v7 by default.")
        pargs.gtex_version = 'v7'
    elif pargs.genome_build == 'hg38' and pargs.gtex_version != 'v8':
        logger.warning("The GTEx %s doesn't support hg38, use the GTEx version v8 by default." % pargs.gtex_version)
        pargs.gtex_version = 'v8'
        
    logger.info('Running command: %s' % ' '.join([re.sub(r'(^$| +)', r'"\1"', arg, count=1) for arg in sys.argv])) ## Show quotation marks between a space or null character
    
    summary_report_msg_list = ["[Summary Report of Co-localization Analysis]"]
    
    if pargs.prefix != '': pargs.prefix = pargs.prefix + '_'
    
    spark, sc = inferno.make_spark_session()
    
    if pargs.giggle_index:
        gwas_giggle_index_dir = pargs.gwas_summary_bed
    else:
        gwas_giggle_index_dir = gig.make_giggle_index(pargs.gwas_summary_bed, path.join(pargs.output_dir, 'gwas_index'), temp_dir=conf.TMP_PATH, giggle_app=conf.GIGGLE_APP, bgzip_app=conf.BGZIP_APP)
    
    ## Generate a parameter dictionary for coloc functions
    coloc_paras = coloc.init_parameters(output_base_dir=pargs.output_dir, gwas_sample_size=pargs.sample_size, expansion_type=pargs.expansion_type, ld_block_window_size=pargs.ld_block_window_size, expansion_window_size=pargs.expansion_window_size, analysis_type=pargs.analysis_type, gwas_sample_proportion=pargs.sample_proportion, gwas_snp_id_col_pos=pargs.snp_id_col, gwas_ref_col_pos=pargs.ref_col, gwas_alt_col_pos=pargs.alt_col, gwas_pval_col_pos=pargs.pval_col, gwas_maf_col_pos=pargs.maf_col, gwas_beta_col_pos=pargs.beta_col, gwas_se_col_pos=pargs.se_col, gwas_rsID_col_pos=pargs.rsid_col, coloc_h4_cutoff=pargs.h4_cutoff, coloc_abf_cutoff=pargs.abf_cutoff, coloc_nsnps_cutoff=pargs.nsnps_cutoff, genome_build=pargs.genome_build, gtex_version=pargs.gtex_version, gzip=pargs.gzip, temp_dir=conf.TMP_PATH, gadb_meta_file=conf.GADB_META_FILE, ensg2symbol_file=conf.ENSEMBL_ID_TO_GENE_SYMBOL_FILE, gtex_sample_size_file=conf.GTEX_SAMPLE_SIZE_FILE, prefix=pargs.prefix, giggle_app=conf.GIGGLE_APP, tabix_app=conf.TABIX_APP, bgzip_app=conf.BGZIP_APP, plink_app=conf.PLINK_APP, rscript_app=conf.RSCRIPT_APP, config_file=applied_conf_path)
    
    inferno_bed_df = coloc.load_inferno_bed(pargs.bed).persist(pyspark.StorageLevel.DISK_ONLY)
    
    inferno_bed_df_count = inferno_bed_df.count()
    summary_report_msg_list.append("Found %d potentially causal SNPs in the input INFERNO BED file" % inferno_bed_df_count)
    logger.info(summary_report_msg_list[-1])
    
    ## Get top SNPs by region_pos from the input
    region_pos_coordinate_df = coloc.find_region_pos_coordinate(inferno_bed_df, coloc_paras).persist(pyspark.StorageLevel.DISK_ONLY)
    
    summary_report_msg_list.append("%d top SNPs in the input" % region_pos_coordinate_df.count())
    logger.info(summary_report_msg_list[-1])
    
    ## Find expanded SNPs from GWAS
    expanded_snps_from_gwas_dict_pickle_paths, count_expanded_snps = coloc.get_gwas_overlap(region_pos_coordinate_df, gwas_giggle_index_dir, coloc_paras)
    
    summary_report_msg_list.append("%d unique expanded SNPs in GWAS" % count_expanded_snps)
    logger.info(summary_report_msg_list[-1])
    
    ## Get corresponding tissues and genes from eQTL using each tag region
    top_tissue_gene_df = coloc.get_eqtl_gene_id(region_pos_coordinate_df, coloc_paras).persist(pyspark.StorageLevel.DISK_ONLY)
    
    summary_report_msg_list.append("%d overlapped tissues in eQTL" % top_tissue_gene_df.select('tissue').distinct().count())
    logger.info(summary_report_msg_list[-1])
    summary_report_msg_list.append("Identified %d SNP-gene association records from eQTL belong to tag regions" % top_tissue_gene_df.count())
    logger.info(summary_report_msg_list[-1])
    
    region_pos_coordinate_df.unpersist(blocking=True)
    
    ## Prepare an RDD format list of input file paths for R COLOC package
    coloc_input_paths_rdd = coloc.get_coloc_input_paths(top_tissue_gene_df, expanded_snps_from_gwas_dict_pickle_paths, coloc_paras).persist(pyspark.StorageLevel.DISK_ONLY)
    
    summary_report_msg_list.append("%d SNP-gene associations pass the threshold at least %d expanded SNPs in the association" % (coloc_input_paths_rdd.count(), pargs.nsnps_cutoff))
    logger.info(summary_report_msg_list[-1])
    
    top_tissue_gene_df.unpersist(blocking=True)
    
    ## Apply input files to R COLOC package
    coloc_summary_df = coloc.coloc(coloc_input_paths_rdd, coloc_paras).persist(pyspark.StorageLevel.DISK_ONLY)
    inferno.saveColocSummary(coloc_summary_df, pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
    
    ## Get tag SNP-tissue-target gene combinations with locus posterior probabilities of H4 > pargs.h4_cutoff
    H4_sig_genes_df = coloc.find_passed_h4_cutoff_combinations(coloc_summary_df, coloc_paras).persist(pyspark.StorageLevel.DISK_ONLY)
    
    summary_report_msg_list.append("%d unique tissue-target gene comparisons with locus posterior probabilities of H4 > %s" % (H4_sig_genes_df.count(), pargs.h4_cutoff))
    logger.info(summary_report_msg_list[-1])
    
    ## Summary statistics by SparkINFERNO
    coloc_summary_stats_df = coloc.summary_stats(H4_sig_genes_df, inferno_bed_df, coloc_paras).persist(pyspark.StorageLevel.DISK_ONLY)
    
    ## Save Summary statistics as files
    coloc.save_summary_stats(coloc_summary_stats_df, coloc_paras)
    
    ## Compute R2 by applying to PLINK
    coloc.add_ld_r2(coloc_summary_stats_df, coloc_paras)
        
    ## Summary of eqtl_info_highest_SNP_PP_H4.tsv
    highest_SNP_PP_H4_in_top_snp_count = \
        coloc_summary_stats_df.select('snp')\
                          .filter((coloc_summary_stats_df['is_in_top_snp'] == 1) & (coloc_summary_stats_df['is_max_H4'] == 1))\
                          .distinct()\
                          .count()
    
    total_highest_SNP_PP_H4_count = \
        coloc_summary_stats_df.select('snp')\
                          .filter(coloc_summary_stats_df['is_max_H4'] == 1)\
                          .distinct()\
                          .count()
    
    summary_report_msg_list.append("%d out of %d unique colocalized SNPs with highest SNP_PP_H4 values match up with the input potentially causal SNPs" % (highest_SNP_PP_H4_in_top_snp_count, total_highest_SNP_PP_H4_count))
    logger.info(summary_report_msg_list[-1])
    
    summary_report_msg_list.append("%d candidate SNPs out of the %d potentially causal SNPs have colocalization signals in at least one tissue and one gene" % (highest_SNP_PP_H4_in_top_snp_count, inferno_bed_df_count))
    logger.info(summary_report_msg_list[-1])
    
    ## Summary of eqtl_info_0.5_thresh_expanded.tsv
    summary_report_msg_list.append("%d out of %d unique colocalized SNPs in expanded sets of SNPs responsible for at least %s (abf_cutoff) of the probability match up with the input potentially causal SNPs" % (\
        coloc_summary_stats_df.select('snp')\
                      .filter(coloc_summary_stats_df['is_in_top_snp'] == 1)\
                      .distinct()\
                      .count(), \
        coloc_summary_stats_df.select('snp')\
                      .distinct()\
                      .count(), \
        str(pargs.abf_cutoff)))
    logger.info(summary_report_msg_list[-1])
    
    coloc_input_paths_rdd.unpersist()
    coloc_summary_df.unpersist(blocking=True)
    inferno_bed_df.unpersist()
    H4_sig_genes_df.unpersist(blocking=True)
    coloc_summary_stats_df.unpersist(blocking=True)
    
    if pargs.metatable is not None:
        ## Save the summary stats joining with the metatable
        ## Input INFERNO overlap metatable folder and eqtl_info.tsv file of coloc summary stats
        coloc_stats_eqtl_info_file = path.join(pargs.output_dir, pargs.prefix+'eqtl_info_highest_SNP_PP_H4.tsv')
        if not _is_path_exist(pargs.metatable, True): sys.exit(1)
        if not _is_path_exist(coloc_stats_eqtl_info_file, True): sys.exit(1)
        
        coloc_metatable_df = inferno.join_metatable_with_coloc_summary_stats(pargs.metatable, coloc_stats_eqtl_info_file, output_base_dir=pargs.output_dir, remove_part_file_dir=not(pargs.keep_intermediate_files), temp_dir=conf.TMP_PATH)
    
    ## Remove intermediate files
    if not pargs.keep_intermediate_files:
        from shutil import rmtree
        ## Remove the coloc_input folder
        coloc_input_dir = path.join(pargs.output_dir, 'coloc_input')
        if _is_path_exist(coloc_input_dir):
            rmtree(coloc_input_dir)
        ## Remove the gwas_index folder
        gwas_index_dir = path.join(pargs.output_dir, 'gwas_index')
        if _is_path_exist(gwas_index_dir) and not(pargs.giggle_index):
            ## Remove GIGGLE index folder of GWAS generating from the input BED file
            rmtree(gwas_index_dir)
        ## Remove LD block folder
        plink_ld_block_dir = path.join(pargs.output_dir, 'PLINK_LD_block')
        if _is_path_exist(plink_ld_block_dir):
            rmtree(plink_ld_block_dir)
    
    ## Generate summary report
    open(path.join(pargs.output_dir, 'summary_report.txt'), 'w').write("\n".join(summary_report_msg_list)+"\n\n")

    ## Remove temporary pickle files
    for f in expanded_snps_from_gwas_dict_pickle_paths.values():
        remove(f)