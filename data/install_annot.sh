#!/bin/bash
set -eu
set -o pipefail

if [ $# -ne 2 ]; then
	echo "USAGE: $0 <target_annot_dir> <template_metafile_URL>"
	exit 1
fi

TARGETDIR=${1:-annot_data}
ANNOT_URL=${2:-https://tf.lisanwanglab.org/GADB/metadata/metadata.latest.hg19.template}

set +e
GIGGLE=$(command -v giggle) 
if [ ! -x "${GIGGLE}" ]; then
  echo "ERROR: giggle not found, please make sure giggle is installed and is in the path"
  exit 1
fi
TABIX=$(command -v tabix)
if [ ! -x "${TABIX}" ]; then
  echo "ERROR: tabix not found, please make sure giggle is installed and is in the path"
  exit 1
fi
set -e

mkdir -p "${TARGETDIR}"
ANNOTDIR=$( cd "$TARGETDIR" && pwd ) # get absolute path for ANNOTDIR 

# download annotation metadata template
wget -N ${ANNOT_URL} -P $TARGETDIR/metadata/
meta_file_template="$TARGETDIR/metadata/${ANNOT_URL##*/}"

meta_file="${meta_file_template%.template}.tsv"

# replace TARGETDIR placeholder with actual directory name
export TARGETDIR="${ANNOTDIR}"
cat "${meta_file_template}" | envsubst > "${meta_file}"
#TARGETDIR="${TARGETDIR}" sed 's/"/\\\"/g;s/.*/echo "&"/e' "${meta_file_template}" > "${meta_file}"

# download annotation tracks
# FIXME: handle re-download, partial download, .1, .2, etc files
wgetCol=24
tail -n+2 "${meta_file_template}" | cut -f${wgetCol} | while read -r wgetCmd; do eval "${wgetCmd/wget /wget -N }"; done 
#tail -n+2 "${meta_file_template}" | cut -f${wgetCol} | awk '{$1=""; print; if (NR>5) exit;}' | xargs -L 1 wget

# index annotation tracks using Giggle
errorLog=${ANNOTDIR}/indexing.error.log

# find directories for indexing if no directory list is provided 
dirList=${ANNOTDIR}/annot.directories_to_index.txt
find "${ANNOTDIR}/" -iname '*.bed.gz' -printf "%h\n" | sort -u > "${dirList}"
ulimit -Sn 65536 # this is very important for large file collections 
numDir=$(wc -l "${dirList}" | awk '{print $1}')
echo "Using ${GIGGLE}"
echo "Found $numDir directories for indexing."

awk '{print ($1 "\t" NR)}' "${dirList}" | \
  while read -r d dirNum; do
    indexDir="${d}/giggle_index"
    echo "Indexing ${d} [$dirNum/$numDir]"
    if [ -d "${indexDir}" ]; then
      rm -r "${indexDir}"
    fi
    "${GIGGLE}" index -s -i "${d}/*.bed.gz" -o "${indexDir}" || echo -e "Failed to index:\t${d}" >> ${errorLog}
  done

# make tabix index for each individual file
find "${ANNOTDIR}" -iname '*.bed.gz' | xargs -L 1 "${TABIX}" -p bed

find "${ANNOTDIR}" -iname '*.vcf.gz' | xargs -L 1 "${TABIX}" -p vcf 

