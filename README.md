# SparkINFERNO

SparkINFERNO is a scalable high-throughput pipeline for inferring the molecular mechanisms of non-coding genetic variants

## License
SparkINFERNO is available for academic and nonprofit use for free ([MIT license](LICENSE.md))

## Hardware Requirements
A multi-core server with 64GB RAM is recommended when running the full pipeline. A typical run for the full pipeline will generate about 20 GB of result data.

## Prerequisites 
+ Linux-based operating system: CentOS, Ubuntu, etc.
+ Apache Spark 2.4+
+ Python 2.7 (or Python 3)
    * pyspark 2.4+
    * pandas 0.24+
    * scipy 0.19.1+
    * feather-format 0.4.0+
+ R 3.5.1+
    * coloc 3.2-1
    * devtools 2.2.1
    * snpStats 1.32.0
    * ggplot2 3.2.1
+ GIGGLE 0.6.3+
+ Tabix 1.7+
+ Samtools 1.10.2+
+ PLINK 1.90b4+
+ git

## Installation
In this instruction, we assign the base of install path to `/mnt/data` where is the location user can change to an appropriate path depending on the server environment. Briefly, at least two (or three) paths are needed:

1. The annotation data directory (e.g., /mnt/data/annotation_data_hg19 or /mnt/data/annotation_data_hg38)  
2. The working directory (e.g., /mnt/data/my_working_dir)  
3. The SparkINFERNO installation directory (e.g., /mnt/data/spark_inferno) **NOTE:** Required only for manual deployment   

### Using Docker image
For simplicity and faster configurations, we **highly recommend using Docker to perform SparkINFERNO**. The [Docker image](https://hub.docker.com/repository/docker/wanglab/spark-inferno) is avaiable in Docker hub. If you prefer to manually deploy SparkINFERNO on a cluster, please refer to the next section for manually deploy SparkINFERNO.

1. Get SparkINFERNO Docker image  
`docker pull wanglab/spark-inferno`  

    

2. Install functional genomics data repository  
`docker run --rm -v [target_annot_dir]:[target_annot_dir] wanglab/spark-inferno /opt/install_annot.sh [target_annot_dir] [annotation_metafile_URL]`    

    For example, to install annotation data repository for [GRCh37/hg19 genome build](https://tf.lisanwanglab.org/GADB/metadata/metadata.latest.hg19.template) into /mnt/data/annotation_data_hg19 directory:  
    `docker run --rm -v /mnt/data/annotation_data_hg19:/mnt/data/annotation_data_hg19 wanglab/spark-inferno /opt/install_annot.sh /mnt/data/annotation_data_hg19 https://tf.lisanwanglab.org/GADB/metadata/metadata.latest.hg19.template 0 1`  

    GRCh38/hg38 annotation data repository could be similarly installed using the [GRCh38/hg38 genome build](https://tf.lisanwanglab.org/GADB/metadata/metadata.latest.hg38.template) metadata and assigned [target_annot_dir] to annotation_data_hg38.  

3. Get Bash session inside docker  
`docker run -it -v /mnt/data/annotation_data_hg19:/mnt/data/annotation_data_hg19 -v /mnt/data/my_working_dir:/mnt/data/my_working_dir wanglab/spark-inferno /bin/bash`  

    **NOTE:** A minimum of two host directories need to be provided for docker container:  
    1. The annotation data directory (`/mnt/data/annotation_data_hg19` in the example command above)  
    2. The working directory (`/mnt/data/my_working_dir` in the example command above)  
    The working directory could contain the files necessary to run a SparkINFERNO analysis:  
    
      + input files (GWAS summary statistics file and/or lead SNP file)  
      + a configuration file (see the "Setup the SparkINFERNO configuration file" section below)  
      + an output directory for the SparkINFERNO analysis results  


### Deploy SparkINFERNO from source code  
Make sure [Apache SPARK](https://spark.apache.org/downloads.html), [GIGGLE](https://github.com/chienyuehlee/giggle/), [Tabix](https://sourceforge.net/projects/samtools/files/tabix/), [R](https://cran.r-project.org/), and [PLINK](https://www.cog-genomics.org/plink/) have been available and are suitable for the suggested versions in your cluster before installing SparkINFERNO.  

**NOTE:** We highly recommend using the GIGGLE link shown above, which is a patched version provided by our team to solve some potential issues of the original source code.  
  

#### Install SparkINFERNO and dependent modules
1. Install Python dependent modules  
`pip install libraries pandas pytest hypothesis psutil pyspark feather-format scipy`  
2. Clone SparkINFERNO  
`cd /mnt/data`  
`git clone https://bitbucket.org/wanglab-upenn/sparkinferno/spark_inferno`  
3. Install SparkINFERNO as a Python module  
`cd spark_inferno`  
`python setup.py install --user`  

    **NOTE:** Eliminate the `--user` option to install in the system directory (*root privilege is required*).  


#### Install required R packages
Make sure R version is 3.5.1 or above, otherwise please upgrade R before installing COLOC package and its dependencies.

1. `R -e "install.packages('BiocManager', repos='http://cran.us.r-project.org')"`  
2. `R -e "BiocManager::install('snpStats')"`  
3. `R -e "install.packages(c('coloc', 'feather', 'ggplot2'), repos='http://cran.us.r-project.org', dependencies=TRUE)"`  

#### Install functional genomics data repository
The functional genomics data repository requires at least **150 GB** of disk space for each genome build.

1. Change path to /mnt/data  
`cd /mnt/data`  
2. Install GRCh37/hg19 annotation data repository  
`bash /mnt/data/spark_inferno/data/install_annot.sh annotation_data_hg19 https://tf.lisanwanglab.org/GADB/metadata/metadata.latest.hg19.template 0 1`    
3. Install GRCh38/hg38 annotation data repository  
`bash /mnt/data/spark_inferno/data/install_annot.sh annotation_data_hg38 https://tf.lisanwanglab.org/GADB/metadata/metadata.latest.hg38.template 0 1`  
    
    **NOTE:** The path of the annotation data can be anywhere.  
    For example, the commands above show the repository stored in the path of /mnt/data/annotation_data_hg19 (or /mnt/data/annotation_data_hg38).  
    However, absolute paths are applied to GIGGLE indexes. Once the GIGGLE indexes are created, they can not be moved to another place, otherwise re-indexing will be required.  
    The last two parameters of the install_annot.sh denote forceOverwrite=0 and forceRestart=1, respectively.  
    

### AWS Spark Cluster Setup for SparkINFERNO  
#### Amazon Machine Image (AMI) Details  
*Installed software*  

+ Ubuntu Image  
+ Spark-2.4.0 installed  
+ Java 8  
+ Python 2.7 (or Python 3)  
  
*Networking*  
On AWS, all machines should be able to freely communicate with each other. You can place them in the same security group so that there are no firewalls blocking communications, and have all TCP ports open for those within the security group.  
  
*Spark configuration details*  

+ spark-defaults.conf  

    `spark.driver.port       7077`  
    `spark.driver.memory              8g`  
    `spark.executor.memory            4g`  
    `spark.eventLog.enabled           true`  
    `spark.eventLog.dir               /mnt/data/spark-log`  
    `spark.history.fs.logDirectory    /mnt/data/spark-log`  
    `spark.local.dir                  /mnt/data/tmp`  
    `spark.master.ui.port             8083`  
    
+ spark-env.conf  

    `export SPARK_WORKER_MEMORY="8g"`  
    `export SPARK_WORKER_CORES="3"`  
    `export SPARK_WORKER_PORT=9000`  
    `export SPARK_CONF_DIR=$SPARK_HOME/conf`  
    `export SPARK_TMP_DIR=/mnt/data/spark-log/tmp`  
    `export SPARK_PID_DIR=/mnt/data/spark-log/pids`  
    `export SPARK_LOG_DIR=/mnt/data/spark-log`  
    `export SPARK_WORKER_DIR=/mnt/data/spark-log/work`  
    `export SPARK_LOCAL_DIRS=/mnt/data/spark-log/tmp`  
    `export SPARK_MASTER_PORT=7077`  
    `export SPARK_MASTER_IP=<IP>`  
    
+ Configuration files should be the same on all machines, master and slave  
+ $SPARK_HOME/conf/slaves file has to have all slave IP addresses or hostnames  
+ On the master node, use $SPARK_HOME/sbin/start-all.sh to start cluster, $SPARK_HOME/sbin/stop-all.sh to stop cluster  

#### AWS Image Launching  

+ The image can be launched using instance type m5.4xlarge (16 CPUs)  
+ You can use private IPs for the slave machine and a public IP for the master/login node  

## Setup the SparkINFERNO configuration file
SparkINFERNO relies on a configuration file `spark_inferno/sparkinferno.cfg` to describe parameters for Apache SPARK, as well as where relevant executable files are. This is a text file following the VARIABLE = VALUE format, where VARIABLE is the relevant parameter and VALUE is the assignment for that parameter. Path parameters for executable files can be blank which means using the `$PATH` environment variable as default. The table below describes these variables in detail.  

**NOTE:** The [configuration file](https://bitbucket.org/wanglab-upenn/sparkinferno/src/master/sparkinferno.cfg) must be specified when running spark_inferno.py.

VARIABLE|VALUE|DESCRIPTION
:-------|:----|:----------
**Paths for executable files** ||
PYTHON_APP | /usr/bin/python | The path of Python executable file
GIGGLE_APP | /usr/bin/giggle | The path of Giggle executable file
TABIX_APP | /usr/bin/tabix | The path of Tabix executable file
PLINK_APP | /usr/bin/plink | The path of PLINK executable file
BGZIP_APP | /usr/bin/bgzip | The path of bgzip executable file
RSCRIPT_APP | /usr/bin/Rscript | The path of Rscript executable file
SAMTOOLS_APP | /usr/bin/samtools | The path of Samtools executable file
**Parameters for log** ||
LOG_LEVEL | INFO | The log level supports DEBUG, INFO, WARNING & ERROR
**Parameters for Apache SPARK**  ||
SPARK_CONF | [('spark.master', 'local[*]'), | Settings for Apache SPARK
|('spark.driver.memory', '56g'), | See [Spark Configuration](https://spark.apache.org/docs/latest/configuration.html) for details 
|('spark.executor.memory', '4g'), |
|('spark.driver.maxResultSize', '3g'), |
|('spark.sql.shuffle.partitions', '200'), |
|('spark.debug.maxToStringFields', '200')] |
**Other Parameters** ||
GADB_META_FILE |  | The path of functional annotation file. This value can be empty in the file and assigned by spark_inferno.py later.
TMP_PATH |  | The path of a temporary directory. This value can be empty in the file and assigned by spark_inferno.py later.


## Run SparkINFERNO - Options  

Once the SparkINFERNO configuration file setup is complete, you can run the full INFERNO pipeline using 'spark_inferno.py'.  
    
**NOTE:** Run `spark_inferno.py -h` to see the instructions and options for the main script.   


```bash

$ spark_inferno.py -h

usage: spark_inferno.py <-g GWAS_summary_stats/-t top_snp_list -c col# -p col# -A col# -a col#> [options]

Provide GWAS summary statistics and/or top (lead) SNP list to run the complete
SparkINFERNO pipeline (GWAS QC, finding top/genome-wide SNPs, p-value
expansion, LD pruning, LD expansion, INFERNO genomic overlap, and co-
localization analysis).

optional arguments:
  -h, --help            show this help message and exit
  -g GWAS_SUMMARY, --gwas_summary GWAS_SUMMARY
                        The file path of GWAS summary statistics [Default:
                        None]
  -c CHR_COL, --chr_col CHR_COL
                        The column number of chromosomes (1-based, -c 1
                        indicates the first column) in GWAS summary
                        statistics. This is required.
  -p POS_COL, --pos_col POS_COL
                        The column number of SNP positions (1-based) in GWAS
                        summary statistics. This is required.
  -A ALLELE1_COL, --allele1_col ALLELE1_COL
                        The column number of effect alleles (1-based) in GWAS
                        summary statistics. This is required.
  -a ALLELE2_COL, --allele2_col ALLELE2_COL
                        The column number of non-effect alleles (1-based) in
                        GWAS summary statistics. This is required.
  -P PVAL_COL, --pval_col PVAL_COL
                        The column number of SNP p-values (1-based) in GWAS
                        summary statistics. Required for identification of
                        genome-wide significant SNPs, p-value expansion step,
                        and co-localization analysis.
  -b BETA_COL, --beta_col BETA_COL
                        The column number of SNP beta values (1-based) in GWAS
                        summary statistics. Required to define expanded sets
                        that all have consistent minor allele effect
                        directions.
  -s SE_COL, --se_col SE_COL
                        The column number of standard errors (1-based) in GWAS
                        summary statistics
  -f AF_COL, --af_col AF_COL
                        The column number of SNP Allele frequency values
                        (1-based) in GWAS summary statistics. Required for
                        GWAS QC step if the --skip_check_MAF or
                        --skip_check_ambiguous option is not given.
  -r RSID_COL, --rsid_col RSID_COL
                        The column number of SNP rsIDs (1-based) in GWAS
                        summary statistics
  -e EFFECT_DIR_COL, --effect_dir_col EFFECT_DIR_COL
                        The column number of effect directions (1-based) for
                        the GWAS meta-analysis in GWAS summary statistics
  -d DELIMITER, --delimiter DELIMITER
                        Split columns of the GWAS summary statistics using the
                        delimiter [Default: "\t"]
  -t TOP_SNP_LIST, --top_snp_list TOP_SNP_LIST
                        Input a user-defined top SNPs list. The columns MUST
                        contain "chr", "pos" and "region_name" columns.
                        Providing optional columns such as "allele1" and
                        "allele2" or "rsID" contributes to distinguish
                        corresponding SNPs precisely. Using the
                        -C/--create_template option to create a list template.
                        [Default: None]
  -R, --rsid_only       The top SNP list contains rsID only [Default: False]
  -H, --no_header       The top SNP list DO NOT contain a header [Default:
                        False]
  -G GENOME_BUILD, --genome_build GENOME_BUILD
                        The version of human genome reference [Default: hg19]
  --gtex_version {v7,v8}
                        The GETx version using in INFERNO genomic overlap and
                        co-localization analysis steps [Default: v7]
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
                        The path to the desired output directory [Default: ./]
  --prefix PREFIX       Add a prefix to ouptput files [Default: None]
  --skip_logging        Skip logging messages as a file [Default: False]
  --config CONFIG       Provide a specific configuration file to use. This
                        option requires --annotation_metafile to be set
                        simultaneously in a new started analysis.
  --annotation_metafile ANNOTATION_METAFILE
                        Specify a path of annotation metafile and save into
                        the configuration file. This option requires --config
                        to be set simultaneously in a new started analysis.
  --tmp_dir TMP_DIR     Specify a path of the temp directory [Default:
                        OUTPUT_DIR/tmp]
  --keep_intermediate_files
                        Keep intermediate files/folders. Note: This function
                        requires additional disk space. [Default: False]
  --replace_dot         Replace all dots in the header with underlines in
                        place [Default: False]
  --version             Show SparkINFERNO version number

GWAS quality control:
  --skip_check_negative_strand
                        Skip checking negative-strand SNPs [Default: False]
  --skip_check_ambiguous
                        Skip checking SNPs with a strand-independent allele-
                        pair (i.e. A/T or C/G SNPs [Default: False]
  --skip_check_MAF      Skip checking allele frequency between observed and
                        reference SNPs [Default: False]
  --skip_normalizing_allele
                        Skip normalizing alleles (i.e., resolving reference
                        and alternative alleles). By default, the QC step
                        performs the process of allele normalization via the
                        information from SNPs of 1000 Genomes Project or
                        genome reference (for those SNPs with tagged "SNP not
                        found" or "Unmatchable allele") and then generate
                        "normalized_ref" and "normalized_alt" columns in the
                        output. [Default: False]
  --diff_MAF_threshold DIFF_MAF_THRESHOLD
                        Allowed difference between observed and reference
                        allele frequency [Default: 0.15]
  --data_source {1kg,dbsnp}
                        Database for matching up with 1000 Genomes Project
                        (1kg) or dbSNP (Not support yet) [Default: 1kg]
  --super_population_qc {GLOBAL,AFR,AMR,EAS,EUR,SAS}
                        Set "GLOBAL" for all populations or a specific super
                        population of 1000 Genomes data. Five super
                        populations are provided: AFR (African), AMR (Ad Mixed
                        American), EAS (East Asian), EUR (European) and SAS
                        (South Asian). [Default: GLOBAL]
  --use_1kg_maf         Using minor allele frequency from 1000 Genomes as GWAS
                        input. This function supports that the input GWAS
                        summary statistics without allele frequency
                        information can conduct colocalization analysis as
                        well by using allele frequency from 1000 Genomes
                        instead. Activate this option must meet the condition
                        that --af_col/-f and --skip_gwas_qc options are all
                        NOT given. [Default: False]
  --use_1kg_rsid        Using rsID from 1000 Genomes as GWAS input. Activate
                        this option must meet the condition that --rsid_col/-r
                        and --skip_gwas_qc options are all NOT given.
                        [Default: False]
  -x, --exclude_unspecified_cols
                        Skip reading unspecified columns [Default: False]
  -X EXCLUDE_BED, --exclude_bed EXCLUDE_BED
                        Provide a BED format file containing excluded regions
                        [Default: None]

Output a formatted TSV file of top SNPs as an input for downstream GWAS pre-processing:
  -T TOP_SNP_THRESH, --top_snp_thresh TOP_SNP_THRESH
                        The absolute p-value cutoff to use for genome-wide
                        significance analysis [Default: 5e-8]
  -C, --create_template
                        Create an empty template of top SNP list at
                        OUTPUT_DIR/user_top_snp_list.tsv [Default: False]

Expand tag SNPs by distance and significance:
  -L LOCUS_THRESH, --locus_thresh LOCUS_THRESH
                        The absolute p-value cutoff to use for locus-wide
                        significance analysis (only one of sig_multiplier and
                        locus_thresh may be specified) [Default: 5e-8]
  -S SIG_MULTIPLIER, --sig_multiplier SIG_MULTIPLIER
                        The multiplier range for significance to use (i.e. a
                        value of 10 means one order of magnitude)
  -D DIST_THRESHOLD, --dist_threshold DIST_THRESHOLD
                        How far away from each tag SNP to look, in base pair
                        [Default: 500000]
  -F, --full_table      Output the full table of p-value expansion [Default:
                        False]
  --pval_expansion_gw   Run p-value expansion step in genome-wide significant
                        analysis [Default: False]

Identify independent signals/loci using linkage disequilibrium (LD) pruning:
  --ld_threshold LD_THRESHOLD
                        The r2 threshold for LD pruning and expansion
                        [Default: 0.7]
  --window_size_kb WINDOW_SIZE_KB
                        How far away from each SNP to prune [Default: 500kb]
  --super_population_vcf {AFR,AMR,EAS,EUR,SAS}
                        Specify a super population of 1000 Genomes genotype
                        VCF file for LD pruning and expansion. Five super
                        populations are provided: AFR (African), AMR (Ad Mixed
                        American), EAS (East Asian), EUR (European) and SAS
                        (South Asian). [Default: EUR]
  -q, --quiet           Silence the message from PLINK [Default: False]

LD expansion by PLINK:
  --window_size WINDOW_SIZE
                        How far away from each SNP to expand [Default: 500000]

Perform the INFERNO pipeline for parallelized array/collection of genomic analyses:
  --fantom5_window_size FANTOM5_WINDOW_SIZE
                        How far away from each SNP to expand for FANTOM5
                        overlaps [Default: 1000]
  --replicate_frequency REPLICATE_FREQUENCY
                        How many times of simulation in the empirical test
                        [Default: 1000]
  --enrichment_method ENRICHMENT_METHOD
                        Support empirical p-value (empirical_pval) or Fisher's
                        exact test (fisher_exact) method in enrichment
                        analysis [Default: empirical_pval]
  --skip_fantom5        Skip FANTOM5 overlap [Default: False]
  --skip_gtex           Skip GTEx overlap [Default: False]
  --skip_chromhmm       Skip ChromHMM enhancers overlap [Default: False]
  --skip_genomic_partition
                        Skip INFERNO genomic partition overlap [Default:
                        False]
  --skip_targetscan     Skip TargetScan overlap [Default: False]
  --skip_dashr2         Skip DASHR2 overlap [Default: False]
  --skip_homer          Skip Homer transcription factor binding sites overlap
                        [Default: False]
  --skip_enrichment     Skip enrichment analysis in track and tissue summaries
                        [Default: False]
  --skip_genomic_element_plot
                        Skip to generate a bar plot for genomic elements
                        [Default: False]
  --skip_tissue_category_plot
                        Skip to generate a tile plot for tissue category
                        [Default: False]

Run co-localization analysis between GWAS and GTEx eQTL:
  --sample_size SAMPLE_SIZE
                        Number of samples in GWAS. This is required unless
                        --skip_coloc option is given.
  --analysis_type {quant,cc}
                        The data type of the GWAS dataset. Choosing "quant" or
                        "cc" type denotes GWAS data is quantitative or case-
                        control type, respectively. When specified as the cc
                        type, the --sample_proportion option for a
                        case/control proportion of samples MUST be provided,
                        simultaneously. [Default: quant]
  --sample_proportion SAMPLE_PROPORTION
                        Number of sample proportion in GWAS
  --expansion_type {LD_block,constant}
                        Define how to obtain expanded SNPs from each tag SNP.
                        This option can be set as "LD_block" or "constant",
                        the former represents using the length of LD block to
                        expand SNPs from GWAS; the latter represents using a
                        constant value to expand. [Default: LD_block]
  --expansion_window_size EXPANSION_WINDOW_SIZE
                        How far away from each tag SNP to expand. This option
                        is enabled when the --expansion_type option is set
                        "constant." [Default: 500000]
  --h4_cutoff H4_CUTOFF
                        The cutoff value of locus posterior probabilities of
                        H4 (one common causal variant) in coloc summary stats
                        [Default: 0.5]
  --abf_cutoff ABF_CUTOFF
                        The cutoff for a cumulative value of the SNP posterior
                        probabilities of H4 [Default: 0.5]
  --nsnps_cutoff NSNPS_CUTOFF
                        The cutoff of SNP number in a SNP-gene association
                        [Default: 5]
  --gzip                Compress input and output files for colocalization
                        analysis [Default: False]

Options for skipping specified steps:
  --skip_gwas_qc        Skip GWAS QC step. Strongly recommend that DO NOT skip
                        this step. [Default: False]
  --skip_top_snp        Skip identifying top SNP step [Default: False]
  --skip_pval_expansion
                        Skip p-value expansion step when giving top SNP list
                        and GWAS summary statistics files [Default: False]
  --skip_ld_pruning     Skip LD pruning step [Default: False]
  --skip_ld_expansion   Skip LD expansion step [Default: False]
  --skip_genomic_overlap
                        Skip INFERNO genomic overlap pipeline [Default: False]
  --skip_coloc          Skip co-localization analysis [Default: False]
```

---

## SparkINFERNO Examples
The SparkINFERNO pipeline supports three analysis methods with different phases depended on the input file(s), that can be generalized as:  

+ 1) GWAS summary statistics + defined lead SNPs  
+ 2) GWAS summary statistics only  
+ 3) lead SNPs only  

Here, we demonstrate three examples for the these methods, respectively.  

**NOTE:** Make sure to run following the commands below to prepare the demo files before getting started.  


1. Make the working directory (for manual deployment only) and enter it  
`mkdir /mnt/data/my_working_dir; cd $_`  
2. Copy SparkINFERNO configuration file into the working directory  
`cp /mnt/data/spark_inferno/sparkinferno.cfg /mnt/data/my_working_dir`  

    **NOTE:** Use `/opt/spark_inferno/sparkinferno.cfg` as the source of SparkINFERNO configuration file when running SparkINFERNO in the Docker container.  
    
    
3. Uncompress demo file  
`tar zxvf /mnt/data/spark_inferno/demo/SparkINFERNO_demo.tgz`  


Two additional files come up in `/mnt/data/my_working_dir` after uncompressing: `GWAS_summary_statistics.tsv` and `lead_snps.tsv`. The `demo_results` folder includes SparkINFERNO result files using three analysis methods (see below) for your reference.  


```bash
 
$ ls
demo_results  GWAS_summary_statistics.tsv  lead_snps.tsv  sparkinferno.cfg
```

### Example 1) GWAS summary statistics + defined lead SNPs  

#### *How to run:*  

```bash  
 
$ spark_inferno.py --chr_col 1 --pos_col 2 --allele1_col 3 --allele2_col 4 \
    --rsid_col 5 --pval_col 6 --af_col 7 \
	--beta_col 8 --se_col 9 --effect_dir_col 10 \
    --gwas_summary GWAS_summary_statistics.tsv \
    --top_snp_list lead_snps.tsv \
    --output_dir my_results/gwas+lead_snps \
    --sig_multiplier 10 \
    --sample_size 34652 \
    --analysis_type cc \
    --sample_proportion 0.37175343 \
    --gzip \
    --config sparkinferno.cfg \
    --annotation_metafile /mnt/data/annotation_data_hg19/metadata/metadata.latest.hg19.tsv
 
```

#### *Output organization:*

FOLDER|SUB-FOLDER/FILE|DESCRIPTION
:-----|:-------------|:-----------
1_GWAS_QC/ || The folder of GWAS QC results.
| log/*.log | Log messages from running the gwas_qc.py script.
| GWAS_summary_statistics.qc.tsv | The GWAS QC result shows in the QC_stats column.
| GWAS_summary_statistics.qc_available.tsv | GWAS summary statistics filtered out SNPs with "SNP not found", "Unmatchable allele", or "Ambiguous SNP" status. This file is available and used as the input of GWAS summary statistics for downstream analyses by default, unless the --skip_resolving_allele option is given.
| GWAS_summary_statistics.qc_available.bed.gz | BED fotmat file converted from GWAS_summary_statistics.qc_available.tsv.
| GWAS_summary_statistics.qc_available.bed.gz.tbi | Tabix index of the BED file.
| GWAS_summary_statistics.qc_available.bed.gz.header | The header of the BED file.
| summary_report.txt | Summary report of the step.
2_Top_SNP/ || The folder of user-defined lead SNPs.
| log/*.log | Log messages from running the top_snp.py script.
| summary_report.txt | Summary report of the step.
| user_defined_gwas_top_snps.bed | BED fotmat file converted from user_defined_gwas_top_snps.tsv.
| user_defined_gwas_top_snps.tsv | The user-defined lead SNPs and lookup correspondences from GWAS summary statistics.
3_P-value_expansion/ || The folder of p-value expansion SNPs.
| log/*.log | Log messages from running the pval_expansion.py script.
| pval_expanded_snps.sig_multiplier_10.0.bed | BED fotmat file converted from pval_expanded_snps.sig_multiplier_10.0.tsv.
| pval_expanded_snps.sig_multiplier_10.0.tsv | The p-value expansion result from GWAS summary statistics. The parameter "sig_multiplier_10.0" shown on the file name indicates using plus/minus one order of magnitude of p-value cutoff for significance. If using an absolute p-value cutoff for locus-wide significance instead of the multiplier range, the result file will be named as "pval_expanded_snps.locus_thresh_5e-08.tsv."
| summary_report.txt | Summary report of the step.
4_LD_pruning/ || The folder of LD pruning SNPs.
| log/*.log | Log messages from running the ld_pruning.py script which includes PLINK messages.
| LD_pruning_snps.window_size_500kb.ld_threshold_0.7.bed | BED fotmat file converted from LD_pruning_snps.window_size_500kb.ld_threshold_0.7.tsv.
| LD_pruning_snps.window_size_500kb.ld_threshold_0.7.tsv | A table of pruning significant SNPs. The parameter "window_size_500kb.ld_threshold_0.7" shown on the file name indicates using 500kb window size of each SNP and 0.7 r2 cutoff to prune.
| LD_pruning_excluded_snps.window_size_500kb.ld_threshold_0.7.tsv | A table of excluded SNPs after conducting LD pruning.
| summary_report.txt | Summary report of the step.
5_LD_expansion/ || The folder of LD expansion SNPs.
| log/*.log | Log messages from running the ld_expansion.py script which includes PLINK messages.
| LD_expansion_snps.window_size_500000.ld_threshold_0.7.bed | BED fotmat file converted from LD_expansion_snps.window_size_500000.ld_threshold_0.7.bed. SNPs in this file denote potentially causal SNPs that are applied to functional genomic overlap and co-localization analysis. 
| LD_expansion_snps.window_size_500000.ld_threshold_0.7.tsv | A table of significant expansion SNPs. The parameter "window_size_500000.ld_threshold_0.7" shown on the file name indicates using 500kb window size of each SNP and 0.7 r2 cutoff to conduct LD expansion.
| summary_report.txt | Summary report of the step.
6_INFERNO_genomic_overlap/ || The folder of genomic overlap results among functional genomics data collections.
| ChromHMM_enhancers/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and ChromHMM enhancers. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| DASHR2/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and small non-coding RNA genes from DASHR2. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| FANTOM5/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and FANTOM5 enhancers. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| GTEx/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and GTEx eQTL. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| Genomic_partition/coalesced_*_summary.tsv | Overlaps between potentially causal genomic partitions. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "track" for the result of grouping by each track of functional genomics data repository.
| Homer/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and transcription factor binding sites from HOMER. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "motif" for the details of overlapped motifs.
| log/*.log | Log messages from running the inferno_genomic_overlap.py script and overlap summaries.
| coalesced_locus_overlap_summary_stat.tsv | Summary statistics of overlapped SNPs by GWAS locus.
| coalesced_metatable.tsv | All interval summaries are collapsed as the meta-table file. This meta-table contains SNPs information from the input BED file, tissue types (and count), tissue categories (and count), concordant tissues, functionally genomic partitions, and motif sequences and positions from each functional annotation, etc.
| genomic_element_list.tsv | An input list uses for drawing genomic_element_plot.pdf.
| genomic_element_plot.pdf | A summary bar plot of genomic elements.
| summary_report.txt | Summary report of the step.
| tissue_category_list.tsv | An input list uses for drawing tissue_category_plot.pdf.
| tissue_category_plot.pdf | A summary plot of tissue categories and functional genomics overlaps across tag regions.
7_Co-localization_analysis/ || Co-localization analysis results of the GWAS and GTEx eQTL.
| coloc_output/ | The full co-localization results combined from the results and summary tables from R COLOC (library) outputs include input SNPs and estimation results across tag regions, eQTL target genes and tissues.
| log/*.log | Log messages from running the coloc.py script and result summaries.
| coalesced_coloc_stats_metatable.tsv | This file contains co-localized signals from each eQTL target gene and tissue and the information of the original meta-table obtaining from the INFERNO genomic overlap step (i.e., 6_INFERNO_genomic_overlap/coalesced_metatable.tsv). See detailed description of the meta-table columns in SparkINFERNO_metatable_description.tsv.
| coalesced_coloc_summary.tsv | A collapsed table from all summary tables generated by each R COLOC (library) analysis.
| eqtl_info_0.5_thresh_expanded.tsv | The table contains all co-localized signals whose SNP_PP_H4 values pass a threshold (default: 0.5).
| eqtl_info_highest_SNP_PP_H4.tsv | The co-localized signals with the highest SNP_PP_H4 value.
| summary_report.txt | Summary report of the step.
log/ | *.log | Log messages from running the spark_inferno.py script and running time for each step.
preprocessing_summary_stat.tsv || Summary statistics of SNPs from each pre-processing step.
overall_summary_report.txt || Combine summary_report.txt files from all steps.  


### Example 2: GWAS summary statistics only  

#### *How to run:*  

```bash  

$ spark_inferno.py --chr_col 1 --pos_col 2 \
    --allele1_col 3 --allele2_col 4 \
    --rsid_col 5 --pval_col 6 --af_col 7 \
	--beta_col 8 --se_col 9 --effect_dir_col 10 \
    --gwas_summary GWAS_summary_statistics.tsv \
    --output_dir my_results/gwas_only \
    --sample_size 34652 \
    --analysis_type cc \
    --sample_proportion 0.37175343 \
    --gzip \
    --config sparkinferno.cfg \
    --annotation_metafile /mnt/data/annotation_data_hg19/metadata/metadata.latest.hg19.tsv

```  

#### *Output organization:*  

FOLDER |SUB-FOLDER/FILE| DESCRIPTION
:------|:--------------|:-----------
1_GWAS_QC/ || The folder of GWAS QC results.
| log/*.log | Log messages from running the gwas_qc.py script.
| GWAS_summary_statistics.qc.tsv | The GWAS QC result shows in the QC_stats column.
| GWAS_summary_statistics.qc_available.tsv | GWAS summary statistics filtered out SNPs with "SNP not found", "Unmatchable allele", or "Ambiguous SNP" status. This file is available and used as the input of GWAS summary statistics for downstream analyses by default, unless the --skip_resolving_allele option is given.
| GWAS_summary_statistics.qc_available.bed.gz | BED fotmat file converted from GWAS_summary_statistics.qc_available.tsv.
| GWAS_summary_statistics.qc_available.bed.gz.tbi | Tabix index of the BED file.
| GWAS_summary_statistics.qc_available.bed.gz.header | The header of the BED file.
| summary_report.txt | Summary report of the step.
2_Top_SNP/ || The folder of genome-wide significant SNPs.
| log/*.log | Log messages from running the top_snp.py script.
| genome-wide_significant_snps.pval_5e-08.bed | BED fotmat file converted from genome-wide_significant_snps.pval_5e-08.tsv.
| genome-wide_significant_snps.pval_5e-08.tsv | The genome-wide significant SNPs that pass a threshold (default: 5e-8) will become candidate SNPs for analyses.
| summary_report.txt | Summary report of the step.
3_LD_pruning/ || The folder of LD pruning SNPs.
| log/*.log | Log messages from running the ld_pruning.py script which includes PLINK messages.
| LD_pruning_snps.window_size_500kb.ld_threshold_0.7.bed | BED fotmat file converted from LD_pruning_snps.window_size_500kb.ld_threshold_0.7.tsv.
| LD_pruning_snps.window_size_500kb.ld_threshold_0.7.tsv | A table of pruning significant SNPs. The parameter "window_size_500kb.ld_threshold_0.7" shown on the file name indicates using 500kb window size of each SNP and 0.7 r2 cutoff to prune.
| LD_pruning_excluded_snps.window_size_500kb.ld_threshold_0.7.tsv | A table of excluded SNPs after conducting LD pruning.
| summary_report.txt | Summary report of the step.
4_LD_expansion/ || The folder of LD expansion SNPs.
| log/*.log | Log messages from running the ld_expansion.py script which includes PLINK messages.
| LD_expansion_snps.window_size_500000.ld_threshold_0.7.bed | BED fotmat file converted from LD_expansion_snps.window_size_500000.ld_threshold_0.7.bed. SNPs in this file denote potentially causal SNPs that are applied to functional genomic overlap and co-localization analysis. 
| LD_expansion_snps.window_size_500000.ld_threshold_0.7.tsv | A table of significant expansion SNPs. The parameter "window_size_500000.ld_threshold_0.7" shown on the file name indicates using 500kb window size of each SNP and 0.7 r2 cutoff to conduct LD expansion.
| summary_report.txt | Summary report of the step.
5_INFERNO_genomic_overlap/ || The folder of genomic overlap results among functional genomics data collections.
| ChromHMM_enhancers/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and ChromHMM enhancers. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| DASHR2/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and small non-coding RNA genes from DASHR2. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| FANTOM5/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and FANTOM5 enhancers. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| Genomic_partition/coalesced_*_summary.tsv | Overlaps between potentially causal genomic partitions. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "track" for the result of grouping by each track of functional genomics data repository.
| GTEx/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and GTEx eQTL. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| Homer/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and transcription factor binding sites from HOMER. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "motif" for the details of overlapped motifs.
| TargetScan/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and predicted miRNA-target gene paires from TargetScan. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| log/*.log | Log messages from running the inferno_genomic_overlap.py script and overlap summaries.
| coalesced_locus_overlap_summary_stat.tsv | Summary statistics of overlapped SNPs by GWAS locus.
| coalesced_metatable.tsv | All interval summaries are collapsed as the meta-table file. This meta-table contains SNPs information from the input BED file, tissue types (and count), tissue categories (and count), concordant tissues, functionally genomic partitions, and motif sequences and positions from each functional annotation, etc.
| genomic_element_list.tsv | An input list uses for drawing genomic_element_plot.pdf.
| genomic_element_plot.pdf | A summary bar plot of genomic elements.
| summary_report.txt | Summary report of the step.
6_Co-localization_analysis/ || Co-localization analysis results of the GWAS and GTEx eQTL.
| coloc_output/ | The full co-localization results combined from the results and summary tables from R COLOC (library) outputs include input SNPs and estimation results across tag regions, eQTL target genes and tissues.
| log/*.log | Log messages from running the coloc.py script and result summaries.
| coalesced_coloc_stats_metatable.tsv | This file contains co-localized signals from each eQTL target gene and tissue and the information of the original meta-table obtaining from the INFERNO genomic overlap step (i.e., 5_INFERNO_genomic_overlap/coalesced_metatable.tsv). See detailed description of the meta-table columns in SparkINFERNO_metatable_description.tsv.
| coalesced_coloc_summary.tsv | A collapsed table from all summary tables generated by each R COLOC (library) analysis.
| eqtl_info_0.5_thresh_expanded.tsv | The table contains all co-localized signals whose SNP_PP_H4 values pass the threshold (default: 0.5).
| eqtl_info_highest_SNP_PP_H4.tsv | The co-localized signals with the highest SNP_PP_H4 value.
| summary_report.txt | Summary report of the step.
log/ | *.log | Log messages from running the spark_inferno.py script and running time for each step.
preprocessing_summary_stat.tsv || Summary statistics of SNPs from each pre-processing step.
overall_summary_report.txt || Combine summary_report.txt files from all steps.  


### Example 3) Lead SNPs only  

#### *How to run:*  

```bash  

$ spark_inferno.py --top_snp_list lead_snps.tsv \
    --output_dir my_results/lead_snps_only \
    --config sparkinferno.cfg \
    --annotation_metafile /mnt/data/annotation_data_hg19/metadata/metadata.latest.hg19.tsv

```

#### *Output organization:*

FOLDER |SUB-FOLDER/FILE| DESCRIPTION
:------|:--------------|:-----------
1_Top_SNP/ || The folder of user-defined lead SNPs.
| log/*.log | Log messages from running the top_snp.py script.
| summary_report.txt | Summary report of the step.
| user_defined_top_snps.bed | BED fotmat file converted from user_defined_top_snps.tsv.
| user_defined_top_snps.tsv | The user-defined lead SNPs and lookup correspondences from 1000 Genomes Project.
2_LD_pruning/ || The folder of LD pruning SNPs.
| log/*.log | Log messages from running the ld_pruning.py script which includes PLINK messages.
| LD_pruning_snps.window_size_500kb.ld_threshold_0.7.bed | BED fotmat file converted from LD_pruning_snps.window_size_500kb.ld_threshold_0.7.tsv.
| LD_pruning_snps.window_size_500kb.ld_threshold_0.7.tsv | A table of pruning significant SNPs. The parameter "window_size_500kb.ld_threshold_0.7" shown on the file name indicates using 500kb window size of each SNP and 0.7 r2 cutoff to prune.
| LD_pruning_excluded_snps.window_size_500kb.ld_threshold_0.7.tsv | A table of excluded SNPs after conducting LD pruning.
| summary_report.txt | Summary report of the step.
3_LD_expansion/ || The folder of LD expansion SNPs.
| log/*.log | Log messages from running the ld_expansion.py script which includes PLINK messages.
| LD_expansion_snps.window_size_500000.ld_threshold_0.7.bed | BED fotmat file converted from LD_expansion_snps.window_size_500000.ld_threshold_0.7.bed. SNPs in this file denote potentially causal SNPs that are applied to functional genomic overlap and co-localization analysis. 
| LD_expansion_snps.window_size_500000.ld_threshold_0.7.tsv | A table of significant expansion SNPs. The parameter "window_size_500000.ld_threshold_0.7" shown on the file name indicates using 500kb window size of each SNP and 0.7 r2 cutoff to conduct LD expansion.
| summary_report.txt | Summary report of the step.
4_INFERNO_genomic_overlap/ || The folder of genomic overlap results among functional genomics data collections.
| ChromHMM_enhancers/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and ChromHMM enhancers. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| DASHR2/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and small non-coding RNA genes from DASHR2. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| FANTOM5/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and FANTOM5 enhancers. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| Genomic_partition/coalesced_*_summary.tsv | Overlaps between potentially causal genomic partitions. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "track" for the result of grouping by each track of functional genomics data repository.
| GTEx/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and GTEx eQTL. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "tissue" for the result of grouping by each tissue; "track" for the result of grouping by each track of functional genomics data repository.
| Homer/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and transcription factor binding sites from HOMER. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "motif" for the details of overlapped motifs.
| TargetScan/coalesced_*_summary.tsv | Overlaps between potentially causal SNPs and predicted miRNA-target gene paires from TargetScan. Of the "coalesced_*_summary.tsv" TSV files, * can be "GADB" for all overlapped intervals from functional genomics data repository; "interval" for the result of grouping by each interval position; "track" for the result of grouping by each track of functional genomics data repository.
| log/*.log | Log messages from running the inferno_genomic_overlap.py script and overlap summaries.
| coalesced_locus_overlap_summary_stat.tsv | Summary statistics of overlapped SNPs by GWAS locus.
| coalesced_metatable.tsv | All interval summaries are collapsed as the meta-table file. This meta-table contains SNPs information from the input BED file, tissue types (and count), tissue categories (and count), concordant tissues, functionally genomic partitions, and motif sequences and positions from each functional annotation, etc.
| genomic_element_list.tsv | An input list uses for drawing genomic_element_plot.pdf.
| genomic_element_plot.pdf | A summary bar plot of genomic elements.
| summary_report.txt | Summary report of the step.
| tissue_category_list.tsv | An input list uses for drawing tissue_category_plot.pdf.
| tissue_category_plot.pdf | A summary plot of tissue categories and functional genomics overlaps across tag regions.
log/ | *.log | Log messages from running the spark_inferno.py script and running time for each step.
preprocessing_summary_stat.tsv || Summary statistics of SNPs from each pre-processing step.
overall_summary_report.txt || Combine summary_report.txt files from all steps.

**NOTE:** Output files from LD expansion result, with the same file name but different extensions (e.g., LD_expansion_snps.window_size_500000.ld_threshold_0.7.tsv vs LD_expansion_snps.window_size_500000.ld_threshold_0.7.bed), may not have the same number of rows. The TSV file contains all possible combinations among potentially causal SNPs, tag regions (region_name), independent signals/loci, tissue categories, etc., however, the BED file contains unique records for each potentially causal SNPs. For example, a potentially causal SNP is identified from 2 different tag regions, which means at least 2 records with the same SNP position can be found in the TSV file but only one record which has the highest R2 value can survive in the BED file. The same situation can happen in the p-value expansion result. The unique records for each potentially causal SNPs in the BED file are picked up the first one after sorting.  
